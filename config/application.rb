require_relative 'boot'

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Opencamp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.generators do |g|
      g.orm :active_record, primary_key_type: :uuid
      g.test_framework = nil
      g.system_tests = nil
      g.helper false
      g.jbuilder false
    end

    config.i18n.default_locale = :en
    config.i18n.fallbacks = [:en]
    config.i18n.available_locales = %i[en sk]
    config.time_zone = 'Bratislava'

    config.eager_load_paths << Rails.root.join('lib')

    # + App config
    app_config = Rails.application.config_for(:app)
    app_host = app_config.fetch(:host, 'localhost')
    app_port = app_config.fetch(:port, 80).to_i
    app_protocol = app_config.fetch(:protocol, 'http')

    config.action_mailer.default_url_options = {
        host: app_host,
        port: app_port,
        protocol: app_protocol,
    }
    Rails.application.routes.default_url_options[:host] = app_host
    Rails.application.routes.default_url_options[:port] = app_port
    Rails.application.routes.default_url_options[:protocol] = app_protocol

    config.active_job.queue_adapter = :sidekiq

    config.hosts = nil

    config.assets.css_compressor = nil

    config.active_storage.variant_processor = :vips

    config.active_record.yaml_column_permitted_classes = [Symbol, Date, Time, ActiveSupport::TimeWithZone, ActiveSupport::TimeZone, BigDecimal]
  end
end
