require 'sidekiq/web'

Rails.application.routes.draw do
  post "/graphql", to: "graphql#execute", defaults: { format: :json }
  get "/api/me.json", to: "api/credentials#me", defaults: {format: :json}
  get "/oauth/user", to: "api/credentials#oauth_user", defaults: {format: :json}

  resource :avatar, only: [:show]

  ActiveAdmin.routes(self)
  use_doorkeeper

  authenticate :user, lambda { |u| u.is_admin? } do
    namespace :admin do
      mount Sidekiq::Web => '/sidekiq'
    end

    namespace :api, defaults: {format: :json} do
      resources :discord_pairings, only: [:index, :show, :create]
    end
  end

  devise_for :users,
    controllers: { registrations: 'registrations' },
    path_names: {
      sign_in: 'login',
      sign_out: 'logout'
    }

  get 'dashboard/workgroups', to: 'workgroups#dashboard'
  get 'rooms/print', to: 'rooms#print'
  get 'rooms/ratings', to: 'rooms#ratings'

  resources :timetables, only: [:show]
  resources :documents, only: [:show]

  authenticate :user do
    resources :terms, only: [:index] do
      member do
        get :select
      end
    end

    get 'rooms/print', to: 'rooms#print'
    get 'rooms/ratings', to: 'rooms#ratings'

    get 'workgroups/print', to: 'workgroups#print'
    resources :uploads

    resources :discord_pairings, only: [:destroy] do
      collection do
        get 'pair/:token', to: 'discord_pairings#pair', as: 'pair'
      end
    end
  end

  get '*path', to: 'web#index', via: :all, constraints: lambda { |req|
    req.path.exclude? 'rails'
  }

  devise_scope :user do
    authenticated :user do
      root to: "web#index"
    end

    unauthenticated :user do
      root to: 'devise/sessions#new', as: 'unauthenticated_root'
    end
  end

  # mount ActionCable.server => '/cable'
end
