DOKKU=dokku

default:
	@echo "Specify command"

dump_db:
	@echo "Dumping database"
	$(DOKKU) postgres:export lstme-production-postgresql > db.dump

restore_db:
	@echo "Restoring database"
	pg_restore --no-owner --verbose --clean -d opencamp_production db.dump

reset_passwords:
	@echo "Reseting passwords of all local users to 'password'"
	echo "User.all.each do |u| u.password = 'password'; u.save; end;" | rails c

import_production_db: dump_db restore_db

pack_production_files:
	@echo "Packing remote files"
	ssh sunflower.lstme.sk tar -zcvf data.tar.gz /mnt/lstme-storage/data/apps/lstme-opencamp/storage

download_production_files:
	@echo "Downloading archive"
	scp sunflower.lstme.sk:data.tar.gz .

clear_production_pack:
	@echo "Clearing remote archive"
	ssh sunflower.lstme.sk rm data.tar.gz

unpack_production_files:
	@echo "Unpacking files"
	tar -xzf data.tar.gz
	rsync -a ./mnt/lstme-storage/data/apps/lstme-opencamp/storage/ ./storage
	rm -r ./mnt

import_production_files: pack_production_files download_production_files clear_production_pack unpack_production_files
