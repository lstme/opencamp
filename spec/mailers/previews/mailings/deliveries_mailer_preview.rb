module Mailings
  class DeliveriesMailerPreview < ActionMailer::Preview
    def send_delivery
      DeliveriesMailer.send_delivery(delivery)
    end

    private

    def delivery
      BuildCampaignDelivery.call(campaign, contact).result
    end

    def campaign
      Campaign.new(
        subject: + 'Dear {{ contact.first_name }}, ' + Faker::Lorem.sentence,
        body: Campaign.new.default_keys.map { |k| "#{k} = {{ #{k} }}" }.join("\n"),
        from_name: Faker::Name.name,
        from_email: 'from@example.com',
        reply_to_email: 'reply@example.com',
        cc_email: 'cc@example.com',
        bcc_email: 'bcc@example.com',
      )
    end

    def contact
      Contact.new(
        email: Faker::Internet.email,
        username: Faker::Name.name.parameterize,
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        born_at: Faker::Date.birthday,
        gender: Contact::GENDERS.sample,
        address: Faker::Address.full_address,
        phone: Faker::PhoneNumber.phone_number,
        parents_phone: Faker::PhoneNumber.phone_number,
        school: Faker::University.name,
        note: Faker::Lorem.sentences.join(' '),
      )
    end
  end
end
