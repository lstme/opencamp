module.exports = {
    client: {
        includes: ['./app/javascript/**/*.graphql'],
        service: {
            name: 'LocalService',
            localSchemaFile: './app/graphql/schema.graphql'
        }
    }
};
