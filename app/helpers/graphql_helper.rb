module GraphqlHelper
  def render_graphql_partial(partial, data = nil)
    result = JSON.parse(data.to_json, object_class: OpenStruct)
    render partial, data: result
  end

  def render_graphql_component(component, data = nil)
    json = data.to_json
    content_tag :div, nil, { data: { component => json } }
  end
end
