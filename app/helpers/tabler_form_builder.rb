class TablerFormBuilder < ActionView::Helpers::FormBuilder
  delegate :content_tag, to: :@template

  def text_field(attribute, options={})
    rich_field(attribute, options) do |has_error|
      super(attribute, options.merge(plain: true, class: cx(options.class, 'form-control', {'is-invalid': has_error})))
    end
  end

  def password_field(attribute, options={})
    rich_field(attribute, options) do |has_error|
      super(attribute, options.merge(plain: true, class: cx(options.class, 'form-control', {'is-invalid': has_error})))
    end
  end

  def date_field(attribute, options={})
    rich_field(attribute, options) do |has_error|
      super(attribute, options.merge(plain: true, class: cx(options.class, 'form-control', {'is-invalid': has_error})))
    end
  end

  def radio_field(attribute, options={})
    rich_field(attribute, options) do |has_error|
      options[:options].map do |value, label|
        content_tag :label, class: 'form-check form-check-inline' do
          radio_button(attribute, value, class: 'form-check-input', plain: true) +
          content_tag(:span, label, class: 'form-check-label')
        end
      end.join('').html_safe
    end
  end

  def check_field(attribute, options={})
    rich_field(attribute, options.merge(no_label: true)) do |has_error|
      content_tag :label, class: 'form-check' do
        check_box(attribute, class: 'form-check-input', plain: true) +
        content_tag(:span, options[:label], class: 'form-check-label')
      end
    end
  end

  private

  def rich_field(attribute, options={})
    has_error = object.errors.has_key? attribute
    content_tag :div, class: cx('form-group', options[:class]) do
      res = ''
      res += label(attribute, options[:label], class: 'form-label', plain: true) unless options[:no_label]
      res += yield(has_error)
      res += content_tag(:div, object.errors.full_messages_for(attribute).join(' '), class: 'invalid-feedback d-block') if has_error
      res.html_safe
    end
  end
end
