module ApplicationHelper
  def gender_icon(gender)
    case gender
    when 'MALE' then
      span User.gender_symbol(gender), class: 'status_tag gender gender-male'
    when 'FEMALE' then
      span User.gender_symbol(gender), class: 'status_tag gender gender-female'
    else
      span User.gender_symbol(gender), class: 'status_tag gender gender-unknown'
    end
  end

  def label_color_name(color, options)
    light_klass = label_lightness_class(color)
    div class: 'label_tag', style: "background-color: #{color}" do
      if options[:path]
        link_to(options[:name], options[:path], class: light_klass)
      else
        span(options[:name], class: light_klass)
      end
    end
  end

  def label_lightness_class(color)
    color.paint.light? ? 'label_tag_light' : 'label_tag_dark'
  end

  def alert_class(variant)
    case variant
    when 'notice' then 'alert-warning'
    when 'info' then 'alert-info'
    when 'alert' then 'alert-danger'
    when 'success' then 'alert-success'
    end
  end

  # Builds class attribute string from passed values
  # Inspired by https://github.com/JedWatson/classnames
  def cx(*args)
    classes = []
    args.each do |arg|
      next if !arg
      if arg.is_a? String
        classes << arg
      elsif arg.is_a? Array
        classes << cx(*arg)
      elsif arg.is_a? Hash
        arg.keys.each do |k|
          classes << k if arg[k]
        end
      end
    end
    classes.join ' '
  end

  ICON_MAP = {
    'image' => 'file-image',
    'audio' => 'file-audio',
    'video' => 'file-video',
    'application/pdf' => 'file-pdf',
    'application/msword' => 'file-word',
    'application/vnd.ms-word' => 'file-word',
    'application/vnd.oasis.opendocument.text' => 'file-word',
    'application/vnd.openxmlformatsfficedocument.wordprocessingml' => 'file-word',
    'application/vnd.ms-excel' => 'file-excel',
    'application/vnd.openxmlformatsfficedocument.spreadsheetml' => 'file-excel',
    'application/vnd.oasis.opendocument.spreadsheet' => 'file-excel',
    'application/vnd.ms-powerpoint' => 'file-powerpoint',
    'application/vnd.openxmlformatsfficedocument.presentationml' => 'file-powerpoint',
    'application/vnd.oasis.opendocument.presentation' => 'file-powerpoint',
    'text/plain' => 'file-text',
    'text/html' => 'file-code',
    'application/json' => 'file-code',
    'application/gzip' => 'file-archive',
    'application/zip' => 'file-archive',
  }

  def file_type_icon(attachment)
    return unless attachment.attached?

    if attachment.image?
      icon('fa', ICON_MAP['image'])
    elsif attachment.audio?
      icon('fa', ICON_MAP['audio'])
    elsif attachment.video?
      icon('fa', ICON_MAP['video'])
    else
      icon('fa', ICON_MAP.fetch(attachment.blob.content_type, 'file'))
    end
  end

  def navbar_data
    user_role = if current_user&.is_admin
      'Administrator'
    else
      attendance = current_term&.attendances&.find_by(user: current_user)
      if attendance.nil?
        'User'
      elsif attendance.is_a?(InstructorAttendance)
        'Instructor'
      elsif attendance.is_a?(ChildAttendance)
        'Child'
      end
    end
    data = {
      site_logo:,
      site_title:,
      menu: [],
      user_menu: if current_user
        {
          user_avatar: current_user.avatar.attached? ? rails_storage_proxy_path(current_user.avatar.variant(:icon)) : nil,
          user_name: current_user.full_name,
          user_role:,
          items: [
            { to: terms_path, icon: 'fa fa-mountain', title: 'Change active term' },
            { to: edit_user_registration_path, title: t('.profile'), icon: 'fa fa-user' },
            { variant: 'divider' },
            { to: destroy_user_session_path, title: t('.sign_out'), icon: 'fa fa-sign-out' },
          ]
        }
      else
        nil
      end,
    }

    unless policy(:instructor_navbar).show?
      data[:menu].push(
        { to: '/projects', icon: 'fa fa-chalkboard', title: 'Projekty' },
      )
    end

    if current_term.present? && policy(:instructor_navbar).show?
      data[:menu].push(
        { to: '/attendances', icon: 'fa fa-user', title: 'Onboard' },
        { to: '/people', icon: 'fa fa-address-book', title: 'People' },
        { to: '/rooms', icon: 'fa fa-bed', title: 'Rooms' },
        { to: '/workgroups', icon: 'fa fa-users', title: 'Workgroups' },
        { to: '/coins', icon: 'fa fa-coins', title: 'Coins' },
        { to: '/projects', icon: 'fa fa-chalkboard', title: 'Projects' },
        { to: '/data_lists', icon: 'fa fa-clipboard-list', title: 'Lists' },
        { to: uploads_path, icon: 'fa fa-file', title: 'Uploads' }
      )
    end

    if policy(:admin_navbar).show?
      data[:menu].push(
        { to: admin_root_path, icon: 'fa fa-cog', title: 'Admin' },
      )
    end

    data[:menu].each do |l|
      activate(l)
    end
    (data.dig(:user_menu, :items) || []).each do |l|
      activate(l)
    end

    data
  end

  def activate(link)
    link[:active] = is_active_link?(link[:to], :exact) if link[:to]
    link[:items].each do |l|
      activate(l)
    end if link[:items]
  end

  def app_data
    # this way works with current_user = nil
    policy = UserPolicy.new(current_user, nil)
    {
      instructor: policy.instructor?,
      admin: policy.admin?,
      term_id: current_user&.selected_term&.id,
      project_signup_enabled: current_user&.selected_term&.project_signup_enabled
    }
  end
end
