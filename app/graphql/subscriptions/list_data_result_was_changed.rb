module Subscriptions
  class ListDataResultWasChanged < BaseSubscription
    argument :id, ID, required: true

    payload_type [Types::ListDataResultType]

    def subscribe(id:)
      ListDataForList.call(list_id: id).result
    end

    def update(id:)
      list = List.find(id)
      attendance = Attendance.find(object[:attendance_id])

      [ListDataForList.prepare_list_data_result(list, attendance)]
    end
  end
end
