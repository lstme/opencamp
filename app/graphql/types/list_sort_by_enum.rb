module Types
  class ListSortByEnum < BaseEnum
    List::SORT_BY.map do |sort_by|
      value sort_by
    end
  end
end
