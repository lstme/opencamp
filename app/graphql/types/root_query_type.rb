module Types
  class RootQueryType < Types::BaseObject
    field_class AuthorizedField

    field :me, UserType, null: true

    def me
      context[:current_user]
    end

    field :selectable_terms, [TermType], null: false

    def selectable_terms
      get_selectable_terms
    end

    field :term, TermType, null: true do
      argument :id, ID, required: true
    end

    def term(id:)
      get_selectable_terms
        .includes(
          attendances: {
            user: {
              avatar_attachment: :blob,
            },
            room: {},
            workgroup: {},
            term: {}
          },
          rooms: {
            attendances: {
              user: {
                avatar_attachment: :blob,
              },
              room: {},
              workgroup: {},
              term: {}
            },
          },
          workgroups: {
            attendances: {
              user: {
                avatar_attachment: :blob,
              },
              room: {},
              workgroup: {},
              term: {}
            },
          },
        )
        .find(id)
    end

    field :attendance, AttendanceInterface, null: true, authorize: :instructor do
      argument :id, ID, required: true
    end

    def attendance(id:)
      FindAttendanceQuery.call(id: id).first
    end

    field :find_attendance, AttendanceInterface, null: true, authorize: :instructor do
      argument :user_id, ID, required: true
    end

    def find_attendance(user_id:)
      FindAttendanceQuery.call(user_id: user_id).where(term: get_term).first
    end

    field :room, RoomType, null: true, authorize: :instructor do
      argument :id, ID, required: true
    end

    def room(id:)
      FindRoomQuery.call(id: id).where(term: get_term).first
    end

    field :list, ListType, null: true, authorize: :instructor do
      argument :id, ID, required: true
    end

    def list(id:)
      FindListQuery.call(id: id).where(term: get_term).first
    end

    private

    def get_selectable_terms
      SelectableTermsQuery.call(user: context[:current_user])
    end

    def get_term_id
      context[:active_term_id] || context[:current_user].selected_term_id || Term.not_ended.first&.id
    end

    def get_term
      Term.find(get_term_id)
    end
  end
end
