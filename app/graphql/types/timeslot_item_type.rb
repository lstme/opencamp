module Types
  class TimeslotItemType < BaseModelObject
    field :title, String, null: false
    field :label, String, null: true
  end
end
