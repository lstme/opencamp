module Types
  class UserType < BaseModelObject
    field :username, String, null: false
    field :email, String, null: false

    field :first_name, String, null: false
    field :last_name, String, null: false
    field :gender, String, null: false
    field :born_at, String, null: false
    field :is_admin, Boolean, null: false

    field :display_name, String, null: false
    field :full_name, String, null: false
    field :gender_symbol, String, null: false
    field :age, Integer, null: false
    field :shirt_size, String, null: true

    field :avatar, AvatarType, null: true

    def avatar
      if object.association(:avatar_attachment).loaded?
        object.avatar if object.avatar.attached?
      else
        Loaders::AttachmentLoader.for(object.class, :avatar).load(object.id).then do |image|
          next if image.nil?

          image
        end
      end
    end

    field :search_name, String, null: false

    field :labels, [LabelType], null: false

    # fields available only to admin / instructor
    field :address, String, null: true, policy: UserPolicy, authorize: :instructor
    field :phone, String, null: true, policy: UserPolicy, authorize: :instructor
    field :parents_phone, String, null: true, policy: UserPolicy, authorize: :instructor
    field :school, String, null: true, policy: UserPolicy, authorize: :instructor
    field :note, String, null: true, policy: UserPolicy, authorize: :instructor
    field :parents_email, String, null: true, policy: UserPolicy, authorize: :instructor
  end
end
