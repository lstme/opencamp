module Types
  class TimeslotInputType < BaseInputObject
    argument :time, String, required: true
    argument :is_highlighted, Boolean, required: true
    argument :label, String, required: true
    argument :items, [Types::TimeslotItemInputType], required: true
  end
end
