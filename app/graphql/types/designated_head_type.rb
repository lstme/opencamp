module Types
  class DesignatedHeadType < BaseModelObject
    field :full_name, String, null: false
    def full_name
      object.user.full_name
    end

    field :avatar, AvatarType, null: true
    def avatar
      Loaders::AttachmentLoader.for(object.user.class, :avatar).load(object.user.id).then do |image|
        next if image.nil?

        image
      end
    end
  end
end
