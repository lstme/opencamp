module Types
  class AvatarType < BaseObject
    field :id, ID, null: false

    def id
      object.id
    end

    User.attachment_reflections['avatar'].variants.map do |key, options|
      field :"#{key.to_s.underscore}_url", String, null: false, description: options.to_s

      define_method(:"#{key.to_s.underscore}_url") do
        variant = object.record.avatar.variant(key)
        Rails.application.routes.url_helpers.rails_storage_proxy_path(variant)
      end
    end
  end
end
