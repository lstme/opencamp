module Types
  class LabelGroupType < BaseModelObject
    field :name, String, null: false
    field :color, String, null: false
    field :labels, [Types::LabelType], null: false
  end
end
