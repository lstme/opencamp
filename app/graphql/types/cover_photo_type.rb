module Types
  class CoverPhotoType < BaseObject
    field :id, ID, null: false

    def id
      object.id
    end

    Term.attachment_reflections['cover_photo'].variants.map do |key, options|
      field :"#{key.to_s.underscore}_url", String, null: false, description: options.to_s

      define_method(:"#{key.to_s.underscore}_url") do
        image = object.variant(options)
        Rails.application.routes.url_helpers.rails_storage_proxy_path(image)
      end
    end
  end
end
