module Types
  class OnboardingRequirementInputType < BaseModelObject
    field :key, String, null: false
    field :label, String, null: false
  end
end
