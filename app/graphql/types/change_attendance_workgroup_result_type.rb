module Types
  class ChangeAttendanceWorkgroupResultType < BaseObject
    field :attendance, AttendanceInterface, null: false
    field :from_workgroup, WorkgroupType, null: true
    field :to_workgroup, WorkgroupType, null: true
  end
end