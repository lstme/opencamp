module Types
  module BaseInterface
    include GraphQL::Schema::Interface

    field_class BaseField

    definition_methods do
      def resolve_type(object, _context)
        Types.const_get(('%sType' % object.type).to_sym)
      end
    end
  end
end
