module Types
  class UserInputType < BaseInputObject
    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :email, String, required: true
    argument :username, String, required: true
    argument :born_at, GraphQL::Types::ISO8601Date, required: true
    argument :gender, String, required: true
    argument :shirt_size, String, required: false
    argument :address, String, required: false
    argument :phone, String, required: false
    argument :school, String, required: false
    argument :parents_phone, String, required: false
    argument :parents_email, String, required: false
    argument :note, String, required: true
  end
end
