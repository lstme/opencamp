module Types
  class RoomScoreType < BaseModelObject
    field :id, ID, null: false
    field :score, Integer, null: false
    field :date, Iso8601Date, null: false
    field :room, RoomType, null: false
  end
end
