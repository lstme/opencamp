module Types
  class SubscriptionType < BaseObject
    field :list_data_result_was_changed, null: false, subscription: Subscriptions::ListDataResultWasChanged
  end
end
