module Types
  class AttendanceOnboardingRequirementType < BaseModelObject
    field :attendance, AttendanceInterface, null: false
    field :onboarding_requirement_id, ID, null: false
    field :data, GraphQL::Types::JSON, null: false
  end
end
