module Types
  class ListDataResultType < BaseObject
    field :id, ID, null: false
    field :list, ListType, null: false
    field :attendance, AttendanceInterface, null: false
    field :list_data, GraphQL::Types::JSON, null: false
  end
end
