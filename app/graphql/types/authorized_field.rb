module Types
  class AuthorizedField < BaseField
    argument_class BaseArgument

    current_user :current_user

    attr_reader :authorization_scope

    def initialize(*args, authorize: :exists, policy: UserPolicy, authorization_scope: -> {}, **kwargs, &block)
      @authorization_scope = authorization_scope
      super(*args, authorize: authorize, policy: policy, **kwargs, &block)
    end
  end
end
