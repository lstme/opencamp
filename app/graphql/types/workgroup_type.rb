module Types
  class WorkgroupType < BaseModelObject
    field :term_id, ID, null: false
    field :term, TermType, null: false

    field :name, String, null: false

    field :note, String, null: false

    field :attendances, [AttendanceInterface], null: false
    field :attendances_count, Integer, null: false
  end
end