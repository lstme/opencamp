module Types
  class ListType < BaseModelObject
    field :term, TermType, null: false
    field :name, String, null: false
    field :slug, String, null: false

    field :include_children, Boolean, null: false
    field :include_instructors, Boolean, null: false
    field :show_avatar, Boolean, null: false
    field :show_name, Boolean, null: false
    field :show_age, Boolean, null: false
    field :show_room, Boolean, null: false
    field :show_workgroup, Boolean, null: false
    field :sort_by, ListSortByEnum, null: false

    field :fields, [FieldInterface], null: false
    def fields
      object.list_fields.sort_by(&:position).map(&:field)
    end

    field :data, [ListDataResultType], null: false
    def data
      ListDataForList.call(list_id: object.id).result
    end
  end
end
