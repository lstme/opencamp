module Types
  class LabelType < BaseModelObject
    field :name, String, null: false
    field :label_group, Types::LabelGroupType, null: false
  end
end
