module Types
  class BaseField < GraphQL::Pundit::Field
    argument_class BaseArgument

    current_user :current_user

    def initialize(*args, camelize: false, **kwargs, &block)
      super(*args, camelize: camelize, **kwargs, &block)
    end
  end
end
