module Types
  class AttendanceTypeEnum < BaseEnum
    value 'ChildAttendance'
    value 'InstructorAttendance'
  end
end
