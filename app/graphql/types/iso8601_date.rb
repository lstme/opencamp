module Types
  class Iso8601Date < GraphQL::Schema::Scalar
    description "An ISO 8601-encoded date"

    # @param value [DateTime]
    # @return [String]
    def self.coerce_result(value, _ctx)
      value.iso8601
    end

    # @param str_value [String]
    # @return [DateTime]
    def self.coerce_input(str_value, _ctx)
      Date.iso8601(str_value)
    rescue ArgumentError
      # Invalid input
      nil
    end
  end
end
