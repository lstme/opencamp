module Types
  class FieldInputType < BaseInputObject
    argument :type, String, required: true
    argument :name, String, required: true
    argument :slug, String, required: true
    argument :settings, GraphQL::Types::JSON, required: true
  end
end
