module Types
  class TimetableType < BaseModelObject
    field :date, Iso8601Date, null: false
    field :designated_head, Types::DesignatedHeadType, null: false
    field :designated_room, Types::DesignatedRoomType, null: false
    field :notice, String, null: true
    field :timeslots, [Types::TimeslotType], null: false
  end
end
