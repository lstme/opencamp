module Types
  module FieldInterface
    include BaseModelInterface

    orphan_types DateFieldType, TimeFieldType, TimerFieldType, TimestampFieldType, CheckboxFieldType, NumberFieldType, TextFieldType

    field :term, TermType, null: false
    field :name, String, null: false
    field :slug, String, null: false

    field :settings, GraphQL::Types::JSON, null: false
  end
end
