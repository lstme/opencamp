module Types
  class TermType < BaseModelObject
    field :name, String, null: false

    field :starts_at, Iso8601Date, null: true
    field :ends_at, Iso8601Date, null: true

    field :address, String, null: true
    # field :latitude, Float, null: true
    # field :longitude, Float, null: true

    # field :rooms_count, Integer, null: false
    field :rooms, [RoomType], null: false

    # field :workgroups_count, Integer, null: false
    field :workgroups, [WorkgroupType], null: false

    # field :capacity, Integer, null: false
    field :phase, TermPhaseTypeEnum, null: false

    field :onboarding_requirements, [OnboardingRequirementType], null: false

    # field :attendances_count, Integer, null: false
    field :attendances, [AttendanceInterface], null: false

    # field :child_attendances_count, Integer, null: false
    # field :child_attendances, [ChildAttendanceType], null: false

    field :instructor_attendances, [InstructorAttendanceType], null: false
    # field :instructor_attendances_count, Integer, null: false

    field :lists, [ListType], null: true, policy: UserPolicy, authorize: :instructor

    def lists
      object.lists.order(created_at: :asc)
    end

    # field :fields, [FieldInterface], null: true, policy: UserPolicy, authorize: :instructor
    # def fields
    #   object.list_fields.sort_by(&:position).map(&:field)
    # end

    field :cover_photo, CoverPhotoType, null: true

    def cover_photo
      Loaders::AttachmentLoader.for(object.class, :cover_photo).load(object.id).then do |image|
        next if image.nil?

        image
      end
    end

    field :timetables, [TimetableType], null: false

    def timetables
      TimetablesQuery.new(term: object).call
    end

    field :current_attendance, AttendanceInterface, null: true

    def current_attendance
      CurrentAttendanceQuery.call(user: context[:current_user], term: object).first
    end

    field :timetable, TimetableType, null: true do
      argument :key, String, required: false
    end

    def timetable(key: nil)
      TimetableQuery.call(key: key, term: object).first
    end

    field :projects, [ProjectType], null: false

    def projects
      object.projects
        .published
        .includes(project_attendances: { attendance: :user })
        .order('category ASC, title ASC')
    end

    field :project_signup_enabled, Boolean, null: false
  end
end
