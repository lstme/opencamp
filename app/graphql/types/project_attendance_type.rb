module Types
  class ProjectAttendanceType < BaseModelObject
    field :project, ProjectType, null: false
    field :attendance, AttendanceInterface, null: false
  end
end
