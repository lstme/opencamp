module Types
  class TimeslotType < BaseModelObject
    field :time, String, null: false
    field :is_highlighted, Boolean, null: false
    field :label, String, null: false
    field :items, [Types::TimeslotItemType], null: false
  end
end
