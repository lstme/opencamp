module Types
  class TermPhaseTypeEnum < BaseEnum
    value 'created'
    value 'registrations_open'
    value 'onboarding'
    value 'in_progress'
    value 'off_boarding'
    value 'ended'
  end
end
