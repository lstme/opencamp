module Types
  module AttendanceInterface
    include BaseModelInterface

    orphan_types ChildAttendanceType, InstructorAttendanceType

    field :user_id, ID, null: false
    field :user, UserType, null: false

    field :term_id, ID, null: false
    field :term, TermType, null: false

    field :room_id, ID, null: true
    field :room, RoomType, null: true

    field :workgroup_id, ID, null: true
    field :workgroup, WorkgroupType, null: true

    field :paid_amount, Float, null: false
    field :should_pay_amount, Float, null: false
    field :note, String, null: false

    field :coins, Integer, null: false
    field :wallet_id, Integer, null: true

    field :transactions, [TransactionType], null: false
    field :transactions_count, Integer, null: false

    field :list_data, GraphQL::Types::JSON, null: false

    field :attendance_onboarding_requirements, [AttendanceOnboardingRequirementType], null: false
  end
end
