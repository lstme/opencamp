module Types
  class ChangeAttendanceRoomResultType < BaseObject
    field :attendance, AttendanceInterface, null: false
    field :from_room, RoomType, null: true
    field :to_room, RoomType, null: true
  end
end