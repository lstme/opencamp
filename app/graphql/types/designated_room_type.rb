module Types
  class DesignatedRoomType < BaseModelObject
    field :name, String, null: false
  end
end
