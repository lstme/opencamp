module Types
  class AttendanceInputType < BaseInputObject
    argument :note, String, required: true
    argument :paid_amount, Float, required: true
  end
end
