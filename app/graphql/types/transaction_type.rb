module Types
  class TransactionType < BaseModelObject
    field :attendance, AttendanceInterface, null: false

    field :amount, Integer, null: false
    field :note, String, null: false

    field :user, UserType, null: false
  end
end
