module Types
  class BaseArgument < GraphQL::Schema::Argument
    def initialize(*args, camelize: false, **kwargs, &block)
      super(*args, camelize: camelize, **kwargs, &block)
    end
  end
end
