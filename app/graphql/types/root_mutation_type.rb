module Types
  class RootMutationType < Types::BaseObject
    field_class AuthorizedAdminField

    field :update_attendance, AttendanceInterface, null: true, authorize: :instructor do
      argument :attendance_id, ID, required: true
      argument :attributes, AttendanceInputType, required: false
    end

    def update_attendance(attendance_id:, attributes:)
      attendance = Attendance.find(attendance_id)
      attendance.assign_attributes(attributes.to_h)
      attendance.save!
      attendance
    end

    field :update_attendance_onboarding_requirements, AttendanceInterface, null: false, authorize: :instructor do
      argument :attendance_id, ID, required: true
      argument :onboarding_requirements, [AttendanceOnboardingRequirementInputType], required: true
    end

    def update_attendance_onboarding_requirements(attendance_id:, onboarding_requirements:)
      Attendance.transaction do
        attendance = Attendance.find(attendance_id)

        attendance.onboarding_requirements.destroy_all

        onboarding_requirements.each do |onboarding_requirement_input|
          onboarding_requirement = attendance.attendance_onboarding_requirements
            .find_or_initialize_by(onboarding_requirement_id: onboarding_requirement_input[:onboarding_requirement_id])

          onboarding_requirement.data = onboarding_requirement_input[:data]
          onboarding_requirement.save!
        end

        attendance.reload
        attendance
      end
    end

    field :change_attendance_room, ChangeAttendanceRoomResultType, null: false, authorize!: :instructor do
      argument :attendance_id, ID, required: true
      argument :room_id, ID, required: false
    end

    def change_attendance_room(**attributes)
      ChangeAttendanceRoom.call(attributes).result
    end

    field :change_attendance_workgroup, ChangeAttendanceWorkgroupResultType, null: false, authorize!: :instructor do
      argument :attendance_id, ID, required: true
      argument :workgroup_id, ID, required: false
    end

    def change_attendance_workgroup(**attributes)
      ChangeAttendanceWorkgroup.call(attributes).result
    end

    # TODO change all attributes
    field :change_room, RoomType, null: true, authorize: :instructor do
      argument :room_id, ID, required: true
      argument :note, String, required: true
    end

    def change_room(**attributes)
      ChangeRoom.call(attributes).result
    end

    field :change_workgroup, WorkgroupType, null: true, authorize: :instructor do
      argument :workgroup_id, ID, required: true
      argument :note, String, required: true
    end

    def change_workgroup(**attributes)
      ChangeWorkgroup.call(attributes).result
    end

    field :update_attendances_list_data, [AttendanceInterface], null: false, authorize!: :instructor do
      argument :attendance_ids, [ID], required: true
      argument :list_id, ID, required: true
      argument :list_data, GraphQL::Types::JSON, required: true
    end

    def update_attendances_list_data(**attributes)
      UpdateAttendancesListData.call(attributes).result
    end

    field :create_transactions, [TransactionType], null: false, authorize!: :attendance_instructor, authorization_scope: -> (context) { context[:current_arguments][:attendance_ids] } do
      argument :attendance_ids, [ID], required: true
      argument :amount, Integer, required: true
      argument :note, String, required: true
    end

    def create_transactions(attendance_ids:, **attributes)
      CreateTransactions.call(context[:current_user].id, attendance_ids, attributes).result
    end

    # field :create_wallet_transaction, TransactionType, null: true, authorize: :instructor do
    #   argument :wallet_id, ID, required: true
    #   argument :amount, Integer, required: true
    #   argument :note, String, required: true
    # end
    # def create_wallet_transaction(wallet_id:, **attributes)
    #   CreateWalletTransaction.call(LatestTermQuery.call.first, wallet_id, attributes).result
    # end

    field :delete_transactions, [AttendanceInterface], null: false, authorize!: :instructor do
      argument :transaction_ids, [ID], required: true
    end

    def delete_transactions(transaction_ids:)
      DeleteTransactions.call(transaction_ids).result
    end

    field :create_list, ListType, null: true, authorize: :instructor do
      argument :list_input, ListInputType, required: true
    end

    def create_list(list_input:)
      # TODO: Implement create_list mutation
      puts "TODO: Implement create_list mutation"
      puts list_input.to_json
      nil
    end

    # field :current_term, Mutations::CurrentTermMutationType, null: false
    # def current_term
    #   Object.new
    # end

    field :update_user, UserType, null: true, authorize: :instructor do
      argument :user_id, ID, required: true
      argument :attributes, UserInputType, required: true
    end

    def update_user(user_id:, attributes:)
      user = User.find(user_id)
      user.assign_attributes(attributes.to_h)
      user.save!
      user
    end

    field :update_timetable, TimetableType, null: true, authorize: :instructor do
      argument :term_id, ID, required: true
      argument :timetable_id, ID, required: false
      argument :timetable_input, TimetableInputType, required: true
    end

    def update_timetable(term_id:, timetable_id: nil, timetable_input:)
      term = Term.find(term_id)

      timetable = if timetable_id.present?
        term.timetables.find(timetable_id)
      else
        term.timetables.build
      end

      timetable.assign_attributes(
        date: timetable_input[:date],
        designated_room_id: timetable_input[:designated_room_id],
        designated_head_id: timetable_input[:designated_head_id],
        notice: timetable_input[:notice],
        timeslots: timetable_input[:timeslots].map do |timeslot_data|
          timetable.timeslots.build(
            time: timeslot_data[:time],
            is_highlighted: timeslot_data[:is_highlighted],
            label: timeslot_data[:label],
          ).tap do |timeslot|
            timeslot.items = timeslot_data[:items].map do |item_data|
              timeslot.items.build(
                label: item_data[:label],
                title: item_data[:title],
              )
            end
          end
        end,
      )
      timetable.save!

      timetable
    end

    field :create_project_attendance, ProjectAttendanceType, null: true, authorize: :user do
      argument :project_id, ID, required: true
    end

    def create_project_attendance(project_id:)
      project = Project.published
        .find(project_id)

      term = project.term

      unless term.project_signup_enabled
        raise "Project signup is not enabled for this term"
      end

      attendance = term.attendances.find_by!(user: context[:current_user])

      project.project_attendances.create!(attendance:)
    end

    field :delete_project_attendance, ProjectAttendanceType, null: true, authorize: :user do
      argument :project_id, ID, required: true
    end

    def delete_project_attendance(project_id:)
      project = Project.published
        .find(project_id)

      term = project.term

      unless term.project_signup_enabled
        raise "Project signup is not enabled for this term"
      end

      attendance = term.attendances.find_by!(user: context[:current_user])

      project.project_attendances.find_by!(attendance:).destroy!
    end
  end
end
