module Types
  class AttendanceOnboardingRequirementInputType < BaseInputObject
    argument :onboarding_requirement_id, ID, required: true
    argument :data, GraphQL::Types::JSON, required: true
  end
end
