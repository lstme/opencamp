module Types
  class OnboardingRequirementType < BaseModelObject
    field :term, TermType, null: false

    field :title, String, null: false
    field :description, String, null: false

    field :position, Integer, null: false

    field :inputs, [OnboardingRequirementInputType], null: false

    def description
      if object.description.strip.empty?
        ''
      else
        ApplicationController.helpers.simple_format object.description
      end
    end

    def inputs
      object.onboarding_requirement_inputs
    end
  end
end
