module Types
  class RoomType < BaseModelObject
    field :term_id, ID, null: false
    field :term, TermType, null: false

    field :name, String, null: false

    field :capacity, Integer, null: false
    field :instructor_only, Boolean, null: false

    field :note, String, null: false
    field :score, Integer, null: false

    field :attendances_count, Integer, null: false
    field :attendances, [AttendanceInterface], null: false

    field :child_attendances_count, Integer, null: false
    field :child_attendances, [ChildAttendanceType], null: false

    field :instructor_attendances, [InstructorAttendanceType], null: false
    field :instructor_attendances_count, Integer, null: false

    field :room_scores, [RoomScoreType], null: false
  end
end
