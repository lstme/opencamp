module Types
  class TimetableInputType < BaseInputObject
    argument :date, String, required: true
    argument :designated_head_id, ID, required: true, description: "Should be id of instructor attendance"
    argument :designated_room_id, ID, required: true
    argument :notice, String, required: false
    argument :timeslots, [Types::TimeslotInputType], required: true
  end
end
