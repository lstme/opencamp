module Types
  class ListInputType < BaseInputObject
    argument :term_id, ID, required: true
    argument :name, String, required: true
    argument :slug, String, required: true

    argument :include_children, Boolean, required: true
    argument :include_instructors, Boolean, required: true
    argument :show_avatar, Boolean, required: true
    argument :show_name, Boolean, required: true
    argument :show_age, Boolean, required: true
    argument :show_room, Boolean, required: true
    argument :show_workgroup, Boolean, required: true
    argument :sort_by, ListSortByEnum, required: true

    argument :fields, [FieldInputType], required: true
  end
end
