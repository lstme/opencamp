module Types
  class TimeslotItemInputType < BaseInputObject
    argument :title, String, required: true
    argument :label, String, required: false
  end
end
