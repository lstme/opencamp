module Types
  class ProjectType < BaseModelObject
    field :term_id, ID, null: false
    field :term, TermType, null: false

    field :category, String, null: false
    field :project_alias, String, null: false

    def project_alias
      object.alias
    end

    field :title, String, null: false
    field :description, String, null: true
    field :prerequisites, String, null: true
    field :capacity, Integer, null: false
    field :slots, Integer, null: false
    field :teacher, UserType, null: false

    field :project_attendances, [ProjectAttendanceType], null: false
  end
end
