class Loaders::AttachmentLoader < GraphQL::Batch::Loader
  attr_reader :record_type, :attachment_name

  MAX_PG_QUERY_LIMIT = 1660

  def initialize(record_type, attachment_name)
    @record_type = record_type.to_s
    @attachment_name = attachment_name
  end

  def perform(record_ids)
    ids = record_ids.uniq

    # find records and fulfill promises
    ids.in_groups_of(MAX_PG_QUERY_LIMIT, false) do |group|
      ActiveStorage::Attachment.includes(:blob).where(
        record_type: record_type, record_id: group, name: attachment_name,
      )
        .each { |record| fulfill(record.record_id, record) }
    end

    # fulfill unfilfilled records
    ids.each { |id| fulfill(id, nil) unless fulfilled?(id) }
  end
end
