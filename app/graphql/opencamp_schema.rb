class OpencampSchema < GraphQL::Schema
  mutation Types::RootMutationType
  query Types::RootQueryType
  subscription Types::SubscriptionType

  use GraphQL::Subscriptions::ActionCableSubscriptions
  use GraphQL::Batch

  rescue_from GraphQL::ExecutionError do |exception, _object, _arguments, _context|
    graphql_error(exception.message)
  end

  rescue_from ActiveRecord::RecordInvalid do |exception, _object, arguments, _context|
    handle_subject_errors(exception&.record, exception&.record&.errors, arguments, exception)
  end

  rescue_from ActiveRecord::RecordNotFound do |exception, _object, arguments, _context|
    handle_subject_errors(nil, nil, arguments, exception)
  end

  rescue_from StandardError do |exception|
    Rails.logger.error("#{exception.class}: #{exception.message}")
    Rails.logger.error(exception.backtrace.join("\n"))
    graphql_error('Unknown errors occured', 500)
  end
end

def handle_subject_errors(subject, errors, _arguments, exception)
  options = {}
  # options.merge!(arguments: arguments.to_h) # TODO: potrebujeme? mrzne
  options[:model] = subject&.class.to_s

  options[:errors] = {
    messages: errors&.messages,
    details: errors&.details,
    full_messages: ErrorsSerializer.new(errors).full_messages,
  }

  full_message = errors&.full_messages&.join("\n")

  graphql_error_message = if exception.is_a?(ActiveRecord::RecordNotFound)
                            I18n.t('errors.not_found.message', model_name: exception.model)
                          elsif full_message.blank?
                            exception.message
                          else
                            full_message
                          end

  graphql_error(graphql_error_message, 422, **options)
end

def graphql_error(message, status = 200, **options)
  GraphQL::ExecutionError.new(message, options: options.merge(status: status))
end
