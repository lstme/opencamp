# == Schema Information
#
# Table name: uploads
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  term_id    :uuid             not null
#
# Indexes
#
#  index_uploads_on_term_id  (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Upload < ApplicationRecord
  belongs_to :term

  has_one_attached :file

  def file_name
    if file.attached?
      file.blob.filename
    end
  end

  def byte_size
    if file.attached?
      file.blob.byte_size
    end
  end

  def content_type
    if file.attached?
      file.blob.content_type
    end
  end
end
