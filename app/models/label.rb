# == Schema Information
#
# Table name: labels
#
#  id             :uuid             not null, primary key
#  name           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  label_group_id :uuid             not null
#
# Indexes
#
#  index_labels_on_label_group_id  (label_group_id)
#
# Foreign Keys
#
#  fk_rails_...  (label_group_id => label_groups.id) ON DELETE => restrict
#
class Label < ApplicationRecord
  belongs_to :label_group
  has_many :user_labels
  has_many :users, through: :user_labels

  validates :name, presence: true
end
