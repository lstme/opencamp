# == Schema Information
#
# Table name: user_labels
#
#  id       :uuid             not null, primary key
#  label_id :uuid             not null
#  user_id  :uuid             not null
#
# Indexes
#
#  index_user_labels_on_label_id              (label_id)
#  index_user_labels_on_user_id               (user_id)
#  index_user_labels_on_user_id_and_label_id  (user_id,label_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (label_id => labels.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id) ON DELETE => restrict
#
class UserLabel < ApplicationRecord
  belongs_to :user
  belongs_to :label

  validates :user, uniqueness: { scope: :label }
end
