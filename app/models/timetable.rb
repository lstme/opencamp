# == Schema Information
#
# Table name: timetables
#
#  id                 :uuid             not null, primary key
#  date               :date             not null
#  notice             :text             default(""), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  designated_head_id :uuid             not null
#  designated_room_id :uuid             not null
#  term_id            :uuid             not null
#
# Indexes
#
#  index_timetables_on_date                (date)
#  index_timetables_on_designated_head_id  (designated_head_id)
#  index_timetables_on_designated_room_id  (designated_room_id)
#  index_timetables_on_term_id             (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (designated_head_id => attendances.id) ON DELETE => restrict
#  fk_rails_...  (designated_room_id => rooms.id) ON DELETE => restrict
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Timetable < ApplicationRecord
  belongs_to :term

  belongs_to :designated_head, class_name: 'Attendance'
  belongs_to :designated_room, class_name: 'Room'

  has_many :timeslots, -> { order(time: :asc) }, dependent: :destroy

  validates :date, presence: true

  accepts_nested_attributes_for :timeslots, allow_destroy: true

  def display_name
    "#{term.name}: #{date}"
  end
end
