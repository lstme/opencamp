# == Schema Information
#
# Table name: rooms
#
#  id                           :uuid             not null, primary key
#  attendances_count            :integer          default(0), not null
#  capacity                     :integer          default(0), not null
#  child_attendances_count      :integer          default(0), not null
#  instructor_attendances_count :integer          default(0), not null
#  instructor_only              :boolean          default(FALSE), not null
#  name                         :string           not null
#  note                         :text             default(""), not null
#  score                        :integer          default(0), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  term_id                      :uuid             not null
#
# Indexes
#
#  index_rooms_on_term_id  (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Room < ApplicationRecord
  belongs_to :term, inverse_of: :rooms
  counter_culture :term, column_name: 'rooms_count'
  counter_culture :term, column_name: 'capacity', delta_column: 'capacity'

  has_many :attendances, inverse_of: :room
  has_many :child_attendances
  has_many :instructor_attendances
  has_many :room_scores

  scope :instructor_only, -> { where(instructor_only: true) }
  scope :full, -> { where('rooms.attendances_count >= rooms.capacity') }
  scope :free, -> { where('rooms.attendances_count < rooms.capacity') }
  scope :empty, -> { where('rooms.attendances_count = 0') }

  accepts_nested_attributes_for :room_scores, allow_destroy: true

  def display_name
    "#{term.name}: #{name} (#{attendances_count}/#{capacity} beds)"
  end

  def full?
    attendances_count >= capacity
  end
end
