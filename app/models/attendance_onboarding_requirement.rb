# == Schema Information
#
# Table name: attendance_onboarding_requirements
#
#  id                        :uuid             not null, primary key
#  data                      :jsonb            not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  attendance_id             :uuid             not null
#  onboarding_requirement_id :uuid             not null
#
# Indexes
#
#  index_att_onb_requirements_on_att_id_and_onb_requirement_id     (attendance_id,onboarding_requirement_id) UNIQUE
#  index_att_onboarding_requirements_on_onboarding_requirement_id  (onboarding_requirement_id)
#  index_attendance_onboarding_requirements_on_attendance_id       (attendance_id)
#
# Foreign Keys
#
#  fk_rails_...  (attendance_id => attendances.id)
#  fk_rails_...  (onboarding_requirement_id => onboarding_requirements.id) ON DELETE => restrict
#
class AttendanceOnboardingRequirement < ApplicationRecord
  belongs_to :attendance
  belongs_to :onboarding_requirement
end
