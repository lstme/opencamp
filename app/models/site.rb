# == Schema Information
#
# Table name: sites
#
#  id              :uuid             not null, primary key
#  singleton_guard :integer          default(0), not null
#  title           :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_sites_on_singleton_guard  (singleton_guard) UNIQUE
#
class Site < ApplicationRecord
  has_one_attached :logo do |attachable|
    attachable.variant :thumb, resize_to_limit: [256, 256]
  end

  def self.instance
    begin
      first!
    rescue ActiveRecord::RecordNotFound
      Site.create!(
        singleton_guard: 0,
        title: 'Opencamp',
      )
    end
  end
end
