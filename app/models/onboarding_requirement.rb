# == Schema Information
#
# Table name: onboarding_requirements
#
#  id          :uuid             not null, primary key
#  description :text             default(""), not null
#  position    :integer          default(0), not null
#  title       :string           default(""), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  term_id     :uuid             not null
#
# Indexes
#
#  index_onboarding_requirements_on_term_id  (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class OnboardingRequirement < ApplicationRecord
  belongs_to :term
  has_many :attendance_onboarding_requirements, dependent: :restrict_with_error
  has_many :onboarding_requirement_inputs, dependent: :destroy

  accepts_nested_attributes_for :onboarding_requirement_inputs, allow_destroy: true

  validates :title, presence: true
  validates :position, presence: true, numericality: true
end
