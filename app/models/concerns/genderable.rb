module Genderable
  extend ActiveSupport::Concern
  class_methods do
    GENDERS = %w[MALE FEMALE].freeze

    def with_gender(attribute_name, validate: true, scopes: true)
      if validate
        validates attribute_name, inclusion: { in: GENDERS }
      end

      if scopes
        scope :male, -> { where(attribute_name => 'MALE') }
        scope :female, -> { where(attribute_name => 'FEMALE') }
      end

      define_method(:"#{attribute_name}_symbol") do
        self.class.gender_symbol(send(attribute_name))
      end
    end

    def gender_symbol(gender)
      case gender
      when 'MALE' then
        '♂'
      when 'FEMALE' then
        '♀'
      else
        '⁉️'
      end.freeze
    end
  end
end
