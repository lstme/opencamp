# == Schema Information
#
# Table name: attendances
#
#  id                 :uuid             not null, primary key
#  coins              :integer          default(0), not null
#  list_data          :jsonb            not null
#  note               :string           default(""), not null
#  paid_amount        :decimal(8, 2)    default(0.0), not null
#  should_pay_amount  :decimal(8, 2)    default(0.0), not null
#  transactions_count :integer          default(0), not null
#  type               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  room_id            :uuid
#  term_id            :uuid             not null
#  user_id            :uuid             not null
#  wallet_id          :integer
#  workgroup_id       :uuid
#
# Indexes
#
#  index_attendances_on_room_id                (room_id)
#  index_attendances_on_term_id                (term_id)
#  index_attendances_on_user_id                (user_id)
#  index_attendances_on_user_id_and_room_id    (user_id,room_id) UNIQUE
#  index_attendances_on_user_id_and_term_id    (user_id,term_id) UNIQUE
#  index_attendances_on_wallet_id_and_term_id  (wallet_id,term_id) UNIQUE
#  index_attendances_on_workgroup_id           (workgroup_id)
#
# Foreign Keys
#
#  fk_rails_...  (room_id => rooms.id) ON DELETE => restrict
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id) ON DELETE => restrict
#  fk_rails_...  (workgroup_id => workgroups.id) ON DELETE => restrict
#
class InstructorAttendance < Attendance
  belongs_to :term
  counter_culture :term, column_name: 'instructor_attendances_count'
  belongs_to :room, optional: true
  counter_culture :room, column_name: 'instructor_attendances_count'
end
