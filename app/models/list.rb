# == Schema Information
#
# Table name: lists
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  settings   :jsonb            not null
#  slug       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  term_id    :uuid             not null
#
# Indexes
#
#  index_lists_on_term_id           (term_id)
#  index_lists_on_term_id_and_slug  (term_id,slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class List < ApplicationRecord
  SORT_BY = %w{
    name
    age
    room
    workgroup
  }.freeze

  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :term
  has_many :list_fields
  has_many :fields, through: :list_fields

  accepts_nested_attributes_for :fields, allow_destroy: true
  accepts_nested_attributes_for :list_fields, allow_destroy: true

  validates :name, presence: true
  validates :sort_by, presence: true, inclusion: { in: SORT_BY }

  jsonb_accessor :settings,
      include_children: [:boolean, default: true],
      include_instructors: [:boolean, default: false],
      show_avatar: [:boolean, default: true],
      show_name: [:boolean, default: true],
      show_age: [:boolean, default: false],
      show_room: [:boolean, default: false],
      show_workgroup: [:boolean, default: true],
      sort_by: [:string, default: SORT_BY.first]

  def should_generate_new_friendly_id?
    ((slug || '').empty? || (name_changed? && ((slug || '').empty?)))
  end

  def to_param
    id
  end
end
