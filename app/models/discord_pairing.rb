# == Schema Information
#
# Table name: discord_pairings
#
#  id            :uuid             not null, primary key
#  pairing_token :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  discord_id    :string           not null
#  user_id       :uuid
#
# Indexes
#
#  index_discord_pairings_on_discord_id     (discord_id)
#  index_discord_pairings_on_pairing_token  (pairing_token)
#  index_discord_pairings_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id) ON DELETE => restrict
#
class DiscordPairing < ApplicationRecord
  has_secure_token :pairing_token
  belongs_to :user, optional: true

  def to_json
    {
      id: id,
      user_id: user.id,
      discord_id: discord_id,
      first_name: user.first_name,
      last_name: user.last_name,
      is_instructor: user.attendances.any?{|a| a.is_a? InstructorAttendance},
      is_child: user.attendances.any?{|a| a.is_a? ChildAttendance},
      years: user.attendances.map{|a| a.term.starts_at.year}
    }
  end
end
