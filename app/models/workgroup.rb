# == Schema Information
#
# Table name: workgroups
#
#  id                :uuid             not null, primary key
#  attendances_count :integer          default(0), not null
#  name              :string           default(""), not null
#  note              :text             default(""), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  term_id           :uuid             not null
#
# Indexes
#
#  index_workgroups_on_term_id  (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Workgroup < ApplicationRecord
  belongs_to :term, inverse_of: :workgroups
  counter_culture :term, column_name: 'workgroups_count'

  has_many :attendances, inverse_of: :workgroup

  validates :name, presence: true

  def display_name
    "#{term.name}: #{name} (#{attendances_count} #{'attendant'.pluralize(attendances_count)})"
  end
end
