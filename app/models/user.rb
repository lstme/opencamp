# == Schema Information
#
# Table name: users
#
#  id                     :uuid             not null, primary key
#  address                :text
#  born_at                :date             not null
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string           not null
#  gender                 :string           not null
#  is_admin               :boolean          default(FALSE), not null
#  last_name              :string           not null
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  note                   :text             default(""), not null
#  parents_email          :string
#  parents_phone          :string
#  phone                  :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  school                 :text
#  shirt_size             :string
#  sign_in_count          :integer          default(0), not null
#  username               :string           default(""), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  selected_term_id       :uuid
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_selected_term_id      (selected_term_id)
#  index_users_on_username              (username) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (selected_term_id => terms.id) ON DELETE => restrict
#
class User < ApplicationRecord
  include Genderable
  attr_reader :age

  has_paper_trail

  devise :database_authenticatable, :recoverable, :rememberable, :validatable, :trackable, :registerable

  has_many :attendances
  has_many :user_labels
  has_many :labels, through: :user_labels
  has_many :discord_pairings

  has_many :access_grants,
           class_name: 'Doorkeeper::AccessGrant',
           foreign_key: :resource_owner_id,
           dependent: :destroy

  has_many :access_tokens,
           class_name: 'Doorkeeper::AccessToken',
           foreign_key: :resource_owner_id,
           dependent: :destroy

  belongs_to :selected_term, class_name: 'Term', optional: true

  has_one_attached :avatar do |attachable|
    attachable.variant :thumb, resize_to_limit: [256, 256], convert: :jpg
    attachable.variant :icon, resize_to_limit: [48, 48], convert: :jpg
    attachable.variant :medium, resize_to_limit: [1024, 1024], convert: :jpg
    attachable.variant :full, convert: :jpg
  end

  validates :email, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :born_at, presence: true
  validates :gender, inclusion: { in: GENDERS }

  scope :admin, -> { where(is_admin: true) }
  scope :adult, -> { where("EXTRACT(YEAR FROM AGE(NOW(), users.born_at)) >= 18") }
  scope :child, -> { where("EXTRACT(YEAR FROM AGE(NOW(), users.born_at)) < 18") }
  scope :label, -> { where(labels: { id: label_id }) }
  scope :doorkeeper, -> { where("username IN (?)", Doorkeeper::Application.pluck(:name)) }

  with_gender :gender

  default_scope do
    order(:last_name, :first_name)
  end

  def age
    ((Time.zone.now - born_at.to_time) / 1.year.seconds).floor
  end

  def full_name
    [last_name, first_name].join(' ').strip
  end

  def display_name
    [last_name, first_name].join(' ').strip + " #{gender_symbol}, #{age} r."
  end

  def search_name
    ActiveSupport::Inflector.transliterate(display_name).downcase
  end

  # disable email checking

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  def password_required?
    return false unless encrypted_password.present?
    super
  end

  def attended_terms
    Term.where(id: attendances.includes(:term).select(:term_id).distinct.pluck(:term_id))
  end

  def has_discord
    discord_pairings.any?
  end

  # https://github.com/heartcombo/devise/wiki/How-To:-Allow-users-to-sign_in-using-their-username-or-email-address
  # Attempt to find a user by it's email. If a record is found, send new
  # password instructions to it. If not user is found, returns a new user
  # with an email not found error.
  def self.send_reset_password_instructions attributes = {}
    recoverable = find_recoverable_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    recoverable.send_reset_password_instructions if recoverable.persisted?
    recoverable
  end

  def self.find_recoverable_or_initialize_with_errors required_attributes, attributes, error = :invalid
    (case_insensitive_keys || []).each { |k| attributes[k].try(:downcase!) }

    attributes = attributes.slice(*required_attributes)
    attributes.delete_if { |_key, value| value.blank? }

    if attributes.keys.size == required_attributes.size
      if attributes.key?(:username)
        login = attributes.delete(:username)
        record = find_record(login)
      else
        record = where(attributes).first
      end
    end

    unless record
      record = new

      required_attributes.each do |key|
        value = attributes[key]
        record.send("#{key}=", value)
        record.errors.add(key, value.present? ? error : :blank)
      end
    end
    record
  end

  def self.find_for_database_authentication(login)
    find_record(login[:email])
  end

  def self.find_record login
    where(["username = :value OR email = :value", { value: login }]).first
  end

  def self.find_for_doorkeeper_application(application)
    where(username: application.name).first
  end

  def self.create_doorkeeper_user!(application)
    app_user = User.create!(
      username: application.name,
      email: application.name + '@doorkeeper',
      first_name: application.name,
      last_name: 'doorkeeper',
      born_at: Time.zone.now,
      gender: 'MALE',
      note: 'Created by OAuth Application',
    )
  end
end
