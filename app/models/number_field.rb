# == Schema Information
#
# Table name: fields
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  settings   :jsonb            not null
#  slug       :string           not null
#  type       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  term_id    :uuid             not null
#
# Indexes
#
#  index_fields_on_term_id           (term_id)
#  index_fields_on_term_id_and_slug  (term_id,slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class NumberField < Field
end
