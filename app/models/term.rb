# == Schema Information
#
# Table name: terms
#
#  id                           :uuid             not null, primary key
#  address                      :string
#  attendances_count            :integer          default(0), not null
#  capacity                     :integer          default(0), not null
#  child_attendances_count      :integer          default(0), not null
#  ends_at                      :date
#  instructor_attendances_count :integer          default(0), not null
#  latitude                     :float
#  longitude                    :float
#  name                         :string           not null
#  phase                        :integer          default("created"), not null
#  project_signup_enabled       :boolean          default(FALSE), not null
#  rooms_count                  :integer          default(0), not null
#  starts_at                    :date
#  workgroups_count             :integer          default(0), not null
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#
# Indexes
#
#  index_terms_on_name  (name) UNIQUE
#
class Term < ApplicationRecord
  has_one_attached :cover_photo do |attachable|
    attachable.variant :thumb, resize_to_limit: [256, 256]
  end

  enum phase: {
      created: 0,
      registrations_open: 10,
      onboarding: 20,
      in_progress: 30,
      off_boarding: 40,
      ended: 50
  }

  scope :not_ended, -> { where.not(phase: :ended) }

  has_many :rooms, -> { order(name: :asc) }
  has_many :onboarding_requirements, -> { order(position: :asc) }
  has_many :workgroups, -> { order(name: :asc) }
  has_many :timetables, -> { order(created_at: :asc) }
  has_many :projects, -> { order(category: :asc) }

  has_many :attendances
  has_many :child_attendances
  has_many :instructor_attendances

  has_many :lists
  has_many :fields

  has_many :uploads

  validates :name, presence: true

  default_scope do
    order(:starts_at)
  end

  def is_onboarded?
    [:in_progress, :off_boarding, :ended].include? phase.to_sym
  end
end
