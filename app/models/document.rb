# == Schema Information
#
# Table name: documents
#
#  id              :uuid             not null, primary key
#  code            :text             default(""), not null
#  local_file_path :string           default(""), not null
#  mode            :string           default("inline_code"), not null
#  slug            :string           not null
#  title           :string           not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  term_id         :uuid             not null
#
# Indexes
#
#  index_documents_on_term_id           (term_id)
#  index_documents_on_term_id_and_slug  (term_id,slug) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Document < ApplicationRecord
  belongs_to :term

  enum mode: {
    inline_code: 'inline_code',
    local_file: 'local_file',
    raw: 'raw',
  }

  validates :slug, presence: true, uniqueness: { scope: :term_id }
  validates :title, presence: true
  validates :mode, inclusion: { in: Document.modes.values }

  extend FriendlyId
  friendly_id :title, use: :slugged

  def should_generate_new_friendly_id?
    ((slug || '').empty? || (title_changed? && ((slug || '').empty?)))
  end

  def slug_candidates
    [
      :title,
    ]
  end

  def to_param
    id
  end

  def render(context)
    source = case mode
             when 'inline_code' then
               code
             when 'local_file' then
               File.read(File.expand_path(local_file_path))
             when 'raw' then
               return code
             else
               raise NotImplementedError
             end

    ERB.new(source).result(context)
  end
end
