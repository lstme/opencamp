# == Schema Information
#
# Table name: transactions
#
#  id            :uuid             not null, primary key
#  amount        :integer          default(0), not null
#  note          :text             default(""), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  attendance_id :uuid             not null
#  user_id       :uuid             not null
#
# Indexes
#
#  index_transactions_on_attendance_id  (attendance_id)
#  index_transactions_on_user_id        (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (attendance_id => attendances.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id) ON DELETE => restrict
#
class Transaction < ApplicationRecord
  has_paper_trail

  belongs_to :attendance
  belongs_to :user
  counter_culture :attendance, column_name: 'coins', delta_column: 'amount'
  counter_culture :attendance, column_name: 'transactions_count'

  validates :amount, presence: true, numericality: { other_than: 0 }
end
