# == Schema Information
#
# Table name: room_scores
#
#  id         :uuid             not null, primary key
#  date       :date             not null
#  score      :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  room_id    :uuid             not null
#
# Indexes
#
#  index_room_scores_on_room_id  (room_id)
#
# Foreign Keys
#
#  fk_rails_...  (room_id => rooms.id) ON DELETE => restrict
#
class RoomScore < ApplicationRecord
  belongs_to :room
  counter_culture :room, column_name: 'score', delta_column: 'score'

end
