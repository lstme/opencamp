# == Schema Information
#
# Table name: onboarding_requirement_inputs
#
#  id                        :uuid             not null, primary key
#  key                       :string           not null
#  label                     :string           not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  onboarding_requirement_id :uuid             not null
#
# Indexes
#
#  index_onb_requirement_inputs_on_onboarding_requirement_id  (onboarding_requirement_id)
#
# Foreign Keys
#
#  fk_rails_...  (onboarding_requirement_id => onboarding_requirements.id)
#
class OnboardingRequirementInput < ApplicationRecord
  belongs_to :onboarding_requirement

  validates :key, presence: true
  validates :label, presence: true
end
