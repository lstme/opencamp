# == Schema Information
#
# Table name: label_groups
#
#  id         :uuid             not null, primary key
#  color      :string
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class LabelGroup < ApplicationRecord
  has_many :labels
  validates :name, presence: true
end
