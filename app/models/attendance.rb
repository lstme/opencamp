# == Schema Information
#
# Table name: attendances
#
#  id                 :uuid             not null, primary key
#  coins              :integer          default(0), not null
#  list_data          :jsonb            not null
#  note               :string           default(""), not null
#  paid_amount        :decimal(8, 2)    default(0.0), not null
#  should_pay_amount  :decimal(8, 2)    default(0.0), not null
#  transactions_count :integer          default(0), not null
#  type               :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  room_id            :uuid
#  term_id            :uuid             not null
#  user_id            :uuid             not null
#  wallet_id          :integer
#  workgroup_id       :uuid
#
# Indexes
#
#  index_attendances_on_room_id                (room_id)
#  index_attendances_on_term_id                (term_id)
#  index_attendances_on_user_id                (user_id)
#  index_attendances_on_user_id_and_room_id    (user_id,room_id) UNIQUE
#  index_attendances_on_user_id_and_term_id    (user_id,term_id) UNIQUE
#  index_attendances_on_wallet_id_and_term_id  (wallet_id,term_id) UNIQUE
#  index_attendances_on_workgroup_id           (workgroup_id)
#
# Foreign Keys
#
#  fk_rails_...  (room_id => rooms.id) ON DELETE => restrict
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#  fk_rails_...  (user_id => users.id) ON DELETE => restrict
#  fk_rails_...  (workgroup_id => workgroups.id) ON DELETE => restrict
#
class Attendance < ApplicationRecord
  TYPES = %w{ChildAttendance InstructorAttendance}.freeze

  has_paper_trail

  belongs_to :user
  belongs_to :term
  counter_culture :term, column_name: 'attendances_count'
  belongs_to :room, optional: true
  counter_culture :room, column_name: 'attendances_count'
  belongs_to :workgroup, optional: true
  counter_culture :workgroup, column_name: 'attendances_count'

  has_many :transactions
  has_many :project_attendances
  has_many :projects, through: :project_attendances

  has_many :attendance_onboarding_requirements, dependent: :destroy
  has_many :onboarding_requirements, through: :attendance_onboarding_requirements

  validates :type, inclusion: { in: TYPES }
  validates :user, uniqueness: { scope: :term, message: I18n.t('activerecord.errors.models.attendance.attributes.user.term_taken') }
  validates :user, uniqueness: { scope: :room, message: I18n.t('activerecord.errors.models.attendance.attributes.user.room_taken') }, if: :room_id?
  validates :paid_amount, numericality: { greater_than_or_equal_to: 0 }
  validate :validate_room
  validates :wallet_id, uniqueness: { scope: :term, message: I18n.t('activerecord.errors.models.attendance.attributes.wallet_id.taken') }, if: :wallet_id?

  scope :admins, -> { joins(:user).where(users: { is_admin: true }) }
  scope :instructors, -> { where(type: 'InstructorAttendance') }
  scope :children, -> { where(type: 'ChildAttendance') }
  scope :without_room, -> { where(room_id: nil) }

  def display_name
    "#{term.name}: #{user.display_name}"
  end

  private

  def validate_room
    errors.add(:room, :room_full) if room.present? && room_id_changed? && room.full?
    errors.add(:room, :different_term) if room.present? && (room.term.id != term.id)
    errors.add(:room, :instructor_only) if room.present? && type == 'ChildAttendance' && room.instructor_only
  end
end
