# == Schema Information
#
# Table name: project_attendances
#
#  id            :uuid             not null, primary key
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  attendance_id :uuid             not null
#  project_id    :uuid             not null
#
# Indexes
#
#  index_project_attendances_on_attendance_id                 (attendance_id)
#  index_project_attendances_on_project_id                    (project_id)
#  index_project_attendances_on_project_id_and_attendance_id  (project_id,attendance_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (attendance_id => attendances.id) ON DELETE => restrict
#  fk_rails_...  (project_id => projects.id)
#
class ProjectAttendance < ApplicationRecord
  belongs_to :project
  belongs_to :attendance

  validates :attendance_id, uniqueness: { scope: :project_id }
end
