# == Schema Information
#
# Table name: projects
#
#  id            :uuid             not null, primary key
#  alias         :string           not null
#  capacity      :integer          default(10), not null
#  category      :string           not null
#  description   :text
#  prerequisites :text
#  published     :boolean          default(FALSE), not null
#  slots         :integer          default(1), not null
#  title         :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  teacher_id    :uuid             not null
#  term_id       :uuid             not null
#
# Indexes
#
#  index_projects_on_teacher_id  (teacher_id)
#  index_projects_on_term_id     (term_id)
#
# Foreign Keys
#
#  fk_rails_...  (teacher_id => users.id)
#  fk_rails_...  (term_id => terms.id) ON DELETE => restrict
#
class Project < ApplicationRecord
  belongs_to :term, inverse_of: :projects
  belongs_to :teacher, class_name: 'User'

  has_many :project_attendances
  has_many :attendances, through: :project_attendances

  validates :alias, presence: true, uniqueness: { scope: :term_id }
  validates :capacity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :category, presence: true
  validates :slots, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :teacher, presence: true
  validates :term, presence: true
  validates :title, presence: true

  nilify_blanks only: [:description, :prerequisites]

  scope :published, -> { where(published: true) }
end
