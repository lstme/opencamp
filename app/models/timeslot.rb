# == Schema Information
#
# Table name: timeslots
#
#  id             :uuid             not null, primary key
#  is_highlighted :boolean          default(FALSE), not null
#  label          :string           not null
#  time           :string           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  timetable_id   :uuid             not null
#
# Indexes
#
#  index_timeslots_on_is_highlighted         (is_highlighted)
#  index_timeslots_on_timetable_id           (timetable_id)
#  index_timeslots_on_timetable_id_and_time  (timetable_id,time)
#
# Foreign Keys
#
#  fk_rails_...  (timetable_id => timetables.id) ON DELETE => restrict
#
class Timeslot < ApplicationRecord
  belongs_to :timetable

  has_many :items, class_name: 'TimeslotItem', dependent: :destroy

  validates :time, presence: true
  validates :is_highlighted, inclusion: { in: [true, false] }
  validates :label, presence: true

  accepts_nested_attributes_for :items, allow_destroy: true
end
