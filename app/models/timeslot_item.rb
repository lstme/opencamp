# == Schema Information
#
# Table name: timeslot_items
#
#  id          :uuid             not null, primary key
#  label       :string
#  title       :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  timeslot_id :uuid             not null
#
# Indexes
#
#  index_timeslot_items_on_timeslot_id  (timeslot_id)
#
# Foreign Keys
#
#  fk_rails_...  (timeslot_id => timeslots.id) ON DELETE => restrict
#
class TimeslotItem < ApplicationRecord
  belongs_to :timeslot
end
