# == Schema Information
#
# Table name: list_fields
#
#  id         :uuid             not null, primary key
#  position   :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  field_id   :uuid             not null
#  list_id    :uuid             not null
#
# Indexes
#
#  index_list_fields_on_field_id  (field_id)
#  index_list_fields_on_list_id   (list_id)
#
# Foreign Keys
#
#  fk_rails_...  (field_id => fields.id) ON DELETE => restrict
#  fk_rails_...  (list_id => lists.id) ON DELETE => restrict
#
class ListField < ApplicationRecord
  belongs_to :list
  belongs_to :field
end
