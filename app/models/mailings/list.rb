# == Schema Information
#
# Table name: mailings_lists
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
module Mailings
  class List < ApplicationRecord
    has_many :lists_contacts, dependent: :destroy
    has_many :contacts, through: :lists_contacts
    has_many :campaigns

    validates :name, presence: true

    def duplicate!
      self.class.create!(name: "#{name} (copy)", contacts: contacts)
    end
  end
end

