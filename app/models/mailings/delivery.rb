# == Schema Information
#
# Table name: mailings_deliveries
#
#  id             :uuid             not null, primary key
#  bcc_email      :string
#  body           :text
#  cc_email       :string
#  deliver_at     :datetime         not null
#  delivered_at   :datetime
#  error          :jsonb
#  failed_at      :datetime
#  from_email     :string
#  from_name      :string
#  reply_to_email :string
#  subject        :string
#  to_email       :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  campaign_id    :uuid             not null
#  contact_id     :uuid             not null
#
# Indexes
#
#  index_mailings_deliveries_on_campaign_id  (campaign_id)
#  index_mailings_deliveries_on_contact_id   (contact_id)
#
# Foreign Keys
#
#  fk_rails_...  (campaign_id => mailings_campaigns.id) ON DELETE => restrict
#  fk_rails_...  (contact_id => mailings_contacts.id) ON DELETE => restrict
#
module Mailings
  class Delivery < ApplicationRecord
    belongs_to :campaign
    belongs_to :contact

    has_event :deliver
    has_event :fail

    scope :pending, -> { not_delivered.not_failed.where('deliver_at < ?', Time.zone.now) }

    before_destroy :check_processed

    def to_liquid_hash
      attributes
        .slice('to_email', 'from_email', 'from_name', 'cc_email', 'bcc_email', 'reply_to_email')
        .merge('contact' => contact.to_liquid_hash)
    end

    def processed?
      delivered? || failed?
    end

    private

    def check_processed
      if processed?
        errors.add(:base, 'Delivery was already processed')
        throw(:abort)
      end
    end
  end
end
