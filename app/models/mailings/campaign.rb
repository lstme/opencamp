# == Schema Information
#
# Table name: mailings_campaigns
#
#  id             :uuid             not null, primary key
#  bcc_email      :string
#  body           :text
#  cc_email       :string
#  from_email     :string
#  from_name      :string
#  name           :string           not null
#  reply_to_email :string
#  send_rate      :integer          default(0), not null
#  started_at     :datetime
#  subject        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  list_id        :uuid             not null
#
# Indexes
#
#  index_mailings_campaigns_on_list_id  (list_id)
#
# Foreign Keys
#
#  fk_rails_...  (list_id => mailings_lists.id) ON DELETE => restrict
#
module Mailings
  class Campaign < ApplicationRecord
    belongs_to :list
    has_many :deliveries

    validates :name, presence: true

    has_event :start

    def locked?
      deliveries.any?
    end

    def default_body
      default_keys.map { |a| "{{ #{a} }}" }
    end

    def default_keys
      flatten_hash(Delivery.new(contact: Contact.new).to_liquid_hash).keys
    end

    private

    def flatten_hash(hash)
      hash.each_with_object({}) do |(k, v), h|
        if v.is_a? Hash
          flatten_hash(v).each do |h_k, h_v|
            h["#{k}.#{h_k}"] = h_v
          end
        else
          h[k] = v
        end
      end
    end
  end
end
