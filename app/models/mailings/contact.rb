# == Schema Information
#
# Table name: mailings_contacts
#
#  id            :uuid             not null, primary key
#  address       :string
#  born_at       :date
#  data          :jsonb
#  email         :string
#  first_name    :string
#  gender        :string
#  last_name     :string
#  note          :string
#  parents_phone :string
#  phone         :string
#  school        :string
#  username      :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_mailings_contacts_on_email  (email)
#
module Mailings
  class Contact < ApplicationRecord
    include Genderable
    has_many :lists_contacts
    has_many :lists, through: :lists_contacts

    validates :email, presence: true

    scope :no_lists, -> { includes(:lists_contacts).where(mailings_lists_contacts: { id: nil }) }

    with_gender :gender, validate: false

    def display_name
      email
    end

    def to_liquid_hash
      attributes.slice(
        'email', 'username', 'first_name', 'last_name', 'born_at', 'gender',
        'address', 'phone', 'parents_phone', 'school', 'note'
      )
    end

    def self.ransackable_scopes(auth_object = nil)
      %i(no_lists)
    end
  end
end
