# == Schema Information
#
# Table name: mailings_lists_contacts
#
#  id         :uuid             not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  contact_id :uuid             not null
#  list_id    :uuid             not null
#
# Indexes
#
#  index_mailings_lists_contacts_on_contact_id  (contact_id)
#  index_mailings_lists_contacts_on_list_id     (list_id)
#
# Foreign Keys
#
#  fk_rails_...  (contact_id => mailings_contacts.id) ON DELETE => restrict
#  fk_rails_...  (list_id => mailings_lists.id) ON DELETE => restrict
#
module Mailings
  class ListsContact < ApplicationRecord
    belongs_to :contact
    belongs_to :list
  end
end
