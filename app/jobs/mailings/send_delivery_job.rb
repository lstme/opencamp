module Mailings
  class SendDeliveryJob < ApplicationJob
    def perform(delivery_id)
      delivery = Mailings::Delivery.pending.find(delivery_id)
      send_email(delivery)
    end

    private

    def send_email(delivery)
      DeliveriesMailer.send_delivery(delivery).deliver_now
      delivery.deliver!
    rescue StandardError => e
      delivery.fail!
      delivery.update!(error: e)
      raise e
    end
  end
end
