module Mailings
  class SendScheduledDeliveriesJob < ApplicationJob

    def perform
      delivery_ids.map { |id| SendDeliveryJob.perform_async(id) }
    end

    private

    def delivery_ids
      Delivery.pending.ids
    end
  end
end
