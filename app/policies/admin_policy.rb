class AdminPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    admin?
  end

  def new?
    admin?
  end

  def create?
    admin?
  end

  def edit?
    admin?
  end

  def update?
    admin?
  end

  def destroy?
    admin?
  end

  def destroy_all?
    admin?
  end

  def preview?
    admin?
  end

  def send_emails?
    admin?
  end

  def import?
    admin?
  end

  def do_import?
    admin?
  end

  def duplicate?
    admin?
  end

  def user?
    true
  end

  def admin?
    user&.is_admin || false
  end

  def instructor?
    return false unless user.present? && user.selected_term.present?

    user.selected_term.instructor_attendances.exists?(user:)
  end

  def attendance_instructor?
    return false unless user

    Attendance
      .where(id: authorization_scope)
      .map(&:term)
      .uniq
      .all? do |term|
      term.instructor_attendances.where(user:).exists?
    end
  end
end
