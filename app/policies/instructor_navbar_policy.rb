class InstructorNavbarPolicy < UserPolicy
  def show?
    instructor? || admin?
  end
end
