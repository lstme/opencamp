class UserPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    admin?
  end

  def new?
    admin?
  end

  def create?
    admin?
  end

  def edit?
    admin?
  end

  def update?
    admin?
  end

  def destroy?
    admin?
  end

  def destroy_all?
    admin?
  end

  def exists?
    user&.present?
  end

  def instructor?
    return false unless user.present? && user.selected_term.present?

    user.selected_term.instructor_attendances.exists?(user:)
  end

  def admin?
    user&.is_admin || false
  end
end
