module Mailings
  class DeliveriesMailer < ApplicationMailer
    def send_delivery(delivery)
      mail(
          mail_options(delivery)
      )
    end

    private

    def mail_options(delivery)
      {
          from: from(delivery.from_name, delivery.from_email),
          to: delivery.to_email,
          cc: delivery.cc_email,
          bcc: delivery.bcc_email,
          reply_to: delivery.reply_to_email,
          subject: delivery.subject,
          body: delivery.body,
      }
    end

    def from(name, email)
      name.present? ? "#{name} <#{email}>" : email
    end
  end
end
