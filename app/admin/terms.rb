ActiveAdmin.register Term do
  permit_params :name, :starts_at, :ends_at, :address, :latitude, :longitude, :phase, :cover_photo, :project_signup_enabled

  config.sort_order = 'starts_at_desc'

  menu priority: 51, label: "<i class='fa fa-calendar-alt'></i> Terms".html_safe

  index do
    selectable_column
    column :name
    column :phase do |term|
      term.phase.humanize
    end
    column :starts_at
    column :ends_at
    column :address
    column :cover_photo do |term|
      image_tag rails_storage_proxy_path(term.cover_photo.variant(:thumb)) if term.cover_photo.attached?
    end
    column :project_signup_enabled

    column :rooms, label: 'Rooms' do |term|
      link_to term.rooms_count, admin_term_rooms_path(term_id: term.id)
    end
    column :capacity do |term|
      span term.capacity, class: (term.capacity >= term.attendances_count ? 'capacity-ok' : 'capacity-problem')
    end
    column "Children" do |term|
      link_to term.child_attendances_count, admin_attendances_path(q: { type_eq: 'ChildAttendance', term_id_eq: term.id })
    end
    column "Instructors" do |term|
      link_to term.instructor_attendances_count, admin_attendances_path(q: { type_eq: 'InstructorAttendance', term_id_eq: term.id })
    end
    column '' do |term|
      div class: 'table_actions' do
        link_to 'Select admin term', admin_term_admin_term_path(id: term), class: 'member_link', method: :post
      end
    end
    actions
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :starts_at
      f.input :ends_at
      f.input :project_signup_enabled, as: :boolean
      f.input :address, as: :text, input_html: { rows: 3 }
      f.input :latitude
      f.input :longitude
      f.input :phase
      f.input :cover_photo, as: :file
    end
    f.actions
  end

  member_action :admin_term, method: :post do
    term = Term.find(params[:id])
    cookies[:admin_term_id] = term.id
    redirect_back fallback_location: admin_terms_url, notice: 'Selected term %s for administration.' % term.name
  end
end
