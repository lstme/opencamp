include ApplicationHelper

ActiveAdmin.register User do
  permit_params :selected_term_id, :avatar, :first_name, :last_name, :born_at, :gender, :email, :parents_email,
  :address, :phone, :parents_phone, :school, :note, :shirt_size,
  :is_admin, :username, :password, :password_confirmation, label_ids: []

  menu priority: 30, label: "<i class='fa fa-user'></i> Users".html_safe

  config.sort_order = 'last_name_asc'
  config.per_page = 100

  scope :all
  scope :admin
  scope :male
  scope :female
  scope :adult
  scope :child
  scope :doorkeeper

  includes :labels

  show do
    attributes_table(*default_attribute_table_rows)
    attributes_table title: 'Extra' do
      row :avatar do |user|
        image_tag rails_storage_proxy_path(user.avatar.variant(:thumb)) if user.avatar.attached?
      end
      row :address
      row :phone
      row :parents_phone
      row :parents_email
      row :school
      row :note
      row :discord do |user|
        if user.discord_pairings.count > 0
          user.discord_pairings.map {|p| p.discord_id }
        else
          "Not paired"
        end
      end
    end
  end

  index do
    selectable_column
    column :first_name
    column :last_name
    column :email
    column :parents_email
    column :born_at
    column :gender do |user|
      gender_icon(user.gender)
    end
    column :age, sortable: 'born_at'
    column :shirt_size
    column :is_admin
    column :selected_term
    column :has_discord

    actions defaults: true do |user|
      span { link_to('Sudo', sudo_admin_user_path(user.id), class: 'member_link', data: { method: :post, confirm: 'Naozaj?' }) }
    end
  end

  filter :username_cont, label: 'Username'
  filter :first_name_or_last_name_cont, label: 'Name'
  filter :born_at
  filter :labels, as: :select, collection: proc { Label.order(:name).all }

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :selected_term
      f.input :avatar, as: :file
      f.input :username
      f.input :email
      f.input :first_name
      f.input :last_name
      f.input :born_at
      f.input :gender, as: :radio, collection: User::GENDERS
      f.input :is_admin
      f.input :password, input_html: { autocomplete: 'new-password' }
      f.input :password_confirmation, input_html: { autocomplete: 'new-password' }

    end
    f.inputs 'Details' do
      f.input :address, input_html: { rows: 3 }
      f.input :phone
      f.input :parents_phone
      f.input :parents_email
      f.input :school, input_html: { rows: 3 }
      f.input :shirt_size
      f.input :note, input_html: { rows: 5 }
    end
    f.inputs 'Labels' do
      f.input :labels, as: :check_boxes_labels, collection: Label.order(:name).all
    end
    f.actions
  end

  member_action :sudo, method: :post do
    reset_session
    sign_in(:user, User.find(params[:id]))

    redirect_to root_url
  end

  controller do
    def scoped_collection
      end_of_association_chain.includes(:discord_pairings, :selected_term).with_attached_avatar
    end

    def update
      model = :user

      if params[model][:password].blank?
        %w(password password_confirmation).map do |p|
          params[model].delete(p)
        end
      end

      super
    end
  end
end
