ActiveAdmin.register Workgroup do
  belongs_to :term, optional: true

  permit_params :term_id, :name, :description

  config.sort_order = 'name_asc'

  includes :term

  menu priority: 38, label: "<i class='fa fa-users'></i> Workgroups".html_safe

  index do
    selectable_column
    column :term
    column :name
    column :note

    column :attendances, label: 'Attendances' do |workgroup|
      link_to pluralize(workgroup.attendances_count, 'attendant'), admin_attendances_path(q: { workgroup_id_eq: workgroup.id })
    end

    actions
  end

  filter :name_cnt, label: 'Name'
  filter :note_cnt, label: 'Note'

  form do |f|
    f.inputs do
      f.input :term
      f.input :name
      f.input :note
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(:term)
          .where(term: SelectedAdminTerm.call.result)
          .order('terms.name DESC, workgroups.name ASC')
    end
  end
end
