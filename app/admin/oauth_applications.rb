ActiveAdmin.register Doorkeeper::Application do
  permit_params :name, :redirect_uri, :scopes, :confidential, :owner_id

  config.sort_order = 'created_at_desc'

  menu priority: 200, label: "<i class='fa fa-key'></i> OAuth Applications".html_safe

  index do
    selectable_column
    column :name
    column :confidential
    column :redirect_uri
    column :user do |app|
      user = User.find_for_doorkeeper_application(app)
      link_to user.email, admin_user_path(user) if user
    end
    column :scopes
    actions
  end

  filter :name_cont, label: 'Name'
  filter :owner

  form do |f|
    f.inputs do
      f.input :name
      f.input :redirect_uri, required: true, hint: 'Use one line per URI', input_html: {rows: 4}
      f.input :confidential
      f.input :scopes
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :uid
      row :secret
      row :redirect_uri
      row :scopes
      row :confidential
      row :created_at
      row :updated_at
    end

    panel 'Application OpenID Configuration' do
      div class: 'attributes_table' do
        table do
          [
            ['Client ID', resource.uid],
            ['Client Secret', resource.secret],
            ['Authorization URL', URI.join(request.base_url, '/oauth/authorize').to_s],
            ['Token URL', URI.join(request.base_url, '/oauth/token').to_s],
            ['User API URL', URI.join(request.base_url, '/oauth/user').to_s]
        ].map do |(key, value)|
            tr do
              td key
              td value
            end
          end
        end
      end
    end

    panel 'Application User' do
      user = User.find_for_doorkeeper_application(resource)

      if user
        attributes_table_for user do
          row :id
          row :username
          row :email do |u|
            link_to u.email, admin_user_path(u)
          end
          row :is_admin
        end
      else
        div do
          para 'No application user found'
          span do
            link_to 'Create User', create_user_admin_doorkeeper_application_path, method: :post, class: 'button'
          end
        end
      end
    end
  end

  member_action :create_user, method: :post do
    application = Doorkeeper::Application.find(params[:id])
    app_user = User.find_for_doorkeeper_application(application)

    if app_user.nil?
      User.create_doorkeeper_user!(application)
      redirect_to admin_doorkeeper_application_path(application), notice: 'User created'
    end
  end
end
