ActiveAdmin.register Attendance do
  permit_params :user_id, :term_id, :room_id, :workgroup_id, :wallet_id, :type, :note, :paid_amount, :should_pay_amount

  config.sort_order = ''
  config.per_page = 100

  menu priority: 20, label: "<i class='fa fa-briefcase'></i> Attendances".html_safe

  includes :user, :term, :room

  scope :all
  scope :admins
  scope :instructors
  scope :children
  scope :without_room

  index do
    selectable_column
    column :term
    column :room do |a|
      if a.room
        link_to a.room.name, admin_room_path(a.room)
      else
        '-'
      end
    end
    column :workgroup do |a|
      if a.workgroup
        link_to a.workgroup.name, admin_workgroup_path(a.workgroup)
      else
        '-'
      end
    end
    column :type do |a|
      a.type === 'ChildAttendance' ? 'Child' : 'Instructor'
    end
    column :user
    column :coins
    column :should_pay_amount
    column :paid_amount
    column :note
    column :wallet_id
    actions
  end

  filter :type, as: :select, collection: -> { Attendance::TYPES }
  filter :term
  filter :wallet_id
  filter :should_pay_amount
  filter :paid_amount
  filter :user_first_name_or_user_last_name_cont, label: 'User'

  form do |f|
    f.inputs do
      f.input :user
      f.input :term
      f.input :room, as: :select, collection: f.object.new_record? ? Room.all.includes(:term) : f.object.term.rooms.includes(:term)
      f.input :workgroup, as: :select, collection: f.object.new_record? ? Workgroup.all.includes(:term) : f.object.term.workgroups.includes(:term)
      f.input :type, as: :select, collection: Attendance::TYPES
      f.input :wallet_id, as: :number
      f.input :should_pay_amount, as: :number
      f.input :paid_amount, as: :number
      f.input :note, as: :text, input_html: { rows: 3 }
    end
    f.actions
  end

  csv do
    column :id
    column :term do |attendance|
      attendance.term.name
    end
    column :user do |attendance|
      [
          attendance.user.first_name,
          attendance.user.last_name,
      ].join(' ')
    end
    column :room do |attendance|
      attendance.room&.name
    end
    column :workgroup do |attendance|
      attendance.workgroup&.name
    end
    column :coins
    column :note
    column :wallet_id
  end

  batch_action :create_transaction, form: -> {
    { amount: :text, note: :textarea }
  } do |ids, inputs|
    service = CreateTransactions.call(current_user.id, ids, **inputs.symbolize_keys)
    redirect_back fallback_location: admin_attendances_url, notice: 'Created %d transactions.' % service.result.size
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(:user, :term)
          .where(term: SelectedAdminTerm.call.result)
          .order('terms.name DESC, attendances.type ASC, users.last_name ASC, users.first_name ASC')
    end
  end

  action_item :onboarding_overview, only: :index do
    link_to 'Onboarding Overview', onboarding_overview_admin_attendances_path
  end

  collection_action :onboarding_overview, method: :get do
    @attendances = scoped_collection.group_by(&:type)
    @child_attendances = @attendances['ChildAttendance'] || []
    @instructor_attendances = @attendances['InstructorAttendance'] || []
    @requirements = SelectedAdminTerm.call.result.onboarding_requirements
  end

  action_item :import, only: :index do
    link_to 'Import', new_import_admin_attendances_path
  end

  collection_action :new_import, method: :get do
  end

  collection_action :import_preview, method: :post do
    begin
      term = SelectedAdminTerm.call.result
      file = params[:import][:file].tempfile
      @attendances = AttendanceImportService.parse_csv_file(file, term)
    rescue StandardError => e
      redirect_to new_import_admin_attendances_path, alert: "Error importing data: #{e.message.to_s[0..1000]}"
    end
  end

  collection_action :import_submit, method: :post do
    begin
      term = SelectedAdminTerm.call.result
      AttendanceImportService.import_data(JSON.parse(params[:import][:data]), term)
      redirect_to admin_attendances_path, success: "Data imported successfully"
    rescue StandardError => e
      flash[:alert] = "Error importing data: #{e.message.to_s[0..1000]}"
      @attendances = JSON.parse(params[:import][:data])
      render :import_preview
    end
  end
end
