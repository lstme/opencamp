ActiveAdmin.register LabelGroup do
  actions :index, :new, :create, :edit, :update, :destroy
  permit_params :name, :color

  config.sort_order = 'name_asc'
  menu label: "<i class='fas fa-tags'></i> Label Groups".html_safe

  index do
    selectable_column
    column :id
    column :name do |label_group|
      label_color_name(label_group.color, name: label_group.name)
    end
    column :created_at
    column :updated_at
    actions
  end
end
