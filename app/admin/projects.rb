ActiveAdmin.register Project do
  belongs_to :teacher, class_name: 'User', optional: true

  permit_params :term_id, :published, :title, :alias, :category, :description, :prerequisites, :capacity, :slots, :teacher_id

  config.sort_order = 'category_asc'
  config.per_page = 100

  includes :term, :teacher, :project_attendances

  menu priority: 41, label: "<i class='fa fa-chalkboard'></i> Projects".html_safe

  scope :all
  scope :published

  batch_action :publish do |ids|
    batch_action_collection.find(ids).map do |project|
      project.update!(published: true)
    end
    redirect_to admin_projects_path, notice: "Selected projects have been published."
  end

  batch_action :unpublish do |ids|
    batch_action_collection.find(ids).map do |project|
      project.update!(published: false)
    end
    redirect_to admin_projects_path, notice: "Selected projects have been unpublished."
  end

  index do
    selectable_column

    column :term
    column :published
    column :category
    column :alias
    column :title
    column :teacher
    column :capacity
    column :slots
    column :created_at
    column :project_attendances do |project|
      span { link_to("#{project.project_attendances.size} #{Attendance.model_name.human.pluralize(project.project_attendances.size).downcase}", admin_project_project_attendances_path(project)) }
    end

    actions
  end

  filter :term
  filter :published
  filter :category
  filter :alias
  filter :title_cont
  filter :teacher
  filter :capacity
  filter :slots

  form do |f|
    f.inputs do
      f.input :term
      f.input :published
      f.input :teacher
      f.input :category
      f.input :alias
      f.input :title
      f.input :description, as: :text, input_html: { rows: 5 }
      f.input :prerequisites, as: :text, input_html: { rows: 5 }
      f.input :capacity, as: :number, input_html: { min: 1 }
      f.input :slots, as: :number, input_html: { min: 1 }
    end
    f.actions
  end

  controller do
    def create
      create! do |format|
        format.html { redirect_to admin_projects_path } if resource.valid?
      end
    end

    def scoped_collection
      end_of_association_chain
        .joins(:term)
        .where(term: SelectedAdminTerm.call.result)
        .order('terms.name DESC, projects.category ASC, projects.title ASC')
    end
  end
end
