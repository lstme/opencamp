ActiveAdmin.register ProjectAttendance do
  belongs_to :project

  permit_params :project_id, :attendance_id

  config.sort_order = 'created_at_desc'
  config.per_page = 100

  includes project: [:term], attendance: [:user]

  index do
    selectable_column

    column :project
    column :attendance
    column :created_at

    actions
  end

  filter :project
  filter :attendance
  filter :created_at

  form do |f|
    f.inputs('x-data': "{project_id: #{f.object.project.present? ? "'#{f.object.project.id}'" : "null"}}") do
      if f.object.new_record?
        f.input :project, input_html: { class: "default-select", 'x-model': 'project_id', 'x-on:change': "window.location.href = '#{new_admin_project_project_attendance_path('REPLACEME')}'.replace('REPLACEME', $el.value);" }
      else
        f.input :project, input_html: { class: "default-select" }
      end
      f.input :attendance, collection: f.object.project.present? ? f.object.project.term.child_attendances.map { |a| [a.user.display_name, a.id] } : []
    end
    f.actions
  end

  controller do
    def create
      create! do |format|
        format.html { redirect_to admin_project_project_attendances_path(resource.project) } if resource.valid?
      end
    end

    def scoped_collection
      end_of_association_chain
        .joins(project: :term)
        .where(project: { term: SelectedAdminTerm.call.result })
        .order(created_at: :desc)
    end
  end
end
