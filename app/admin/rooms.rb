ActiveAdmin.register Room do
  belongs_to :term, optional: true

  permit_params :term_id, :name, :capacity, :instructor_only, :note,
                room_scores_attributes: [:id, :date, :score, :_destroy]

  config.sort_order = ''
  config.per_page = 100

  includes :term

  menu priority: 35, label: "<i class='fa fa-bed'></i> Rooms".html_safe

  scope :all
  scope :instructor_only
  scope :full
  scope :free
  scope :empty

  index do
    selectable_column
    column :term, sortable: 'terms.name'
    column :name
    column :capacity
    column :score
    column :child_attendances do |room|
      link_to pluralize(room.child_attendances_count, 'child'), admin_attendances_path(q: { type_eq: 'ChildAttendance', room_id_eq: room.id })
    end
    column :instructor_attendances do |room|
      link_to pluralize(room.instructor_attendances_count, 'instructor'), admin_attendances_path(q: { type_eq: 'InstructorAttendance', room_id_eq: room.id })
    end
    column :attendances, label: 'All attendances' do |room|
      link_to pluralize(room.attendances_count, 'attendant'), admin_attendances_path(q: { room_id_eq: room.id })
    end
    column :instructor_only
    column :note
    actions
  end

  filter :term
  filter :instructor_only
  filter :name_cont, label: 'Name'

  form do |f|
    f.inputs do
      f.input :term
      f.input :name
      f.input :capacity
      f.input :instructor_only
      f.input :note
    end
    f.inputs do
      f.has_many :room_scores, allow_destroy: true do |b|
        b.input :date
        b.input :score
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :term
      row :name
      row :capacity
      row :instructor_only
      row :note
      row :score
    end
    panel "Room scores" do
      table_for room.room_scores do
        column :date
        column :score
      end
    end
    active_admin_comments
  end

  controller do
    def create
      create! do |format|
        format.html { redirect_to admin_rooms_path } if resource.valid?
      end
    end

    def update
      update! do |format|
        format.html { redirect_to admin_rooms_path } if resource.valid?
      end
    end

    def scoped_collection
      end_of_association_chain
        .joins(:term)
        .where(term: SelectedAdminTerm.call.result)
        .order('terms.name DESC, rooms.name ASC')
    end
  end
end
