ActiveAdmin.register Timetable do
  belongs_to :term, optional: true

  permit_params :term_id, :date, :designated_head_id, :designated_room_id, :notice,
                timeslots_attributes: [:id, :_destroy, :timetable_id, :time, :is_highlighted, :label, { items_attributes: [:id, :_destroy, :timeslot_id, :label, :title] }]

  config.sort_order = 'date_desc'

  includes :term

  menu priority: 39, label: "<i class='fa fa-clock'></i> Timetables".html_safe

  member_action :duplicate, method: :post do
    new_timetable = resource.dup
    new_timetable.date = Time.zone.now.to_date + 1.day
    new_timetable.timeslots = resource.timeslots.map(&:dup)
    new_timetable.save!

    redirect_to edit_admin_timetable_path(new_timetable)
  end

  index do
    selectable_column
    column :term
    column :date
    column :designated_head
    column :designated_room
    column :link do |record|
      [
        link_to('Public link (date)', timetable_path(record.date)),
        link_to('Public link (id)', timetable_path(record))
      ].join(' ').html_safe
    end
    column :created_at
    actions do |record|
      link_to 'Duplicate', duplicate_admin_timetable_path(record), method: :post, class: 'member_link'
    end
  end

  filter :term
  filter :date
  filter :designated_head
  filter :designated_room

  form do |f|
    f.inputs do
      f.input :term
      f.input :date
      f.input :designated_head, collection: InstructorAttendance.where(term: SelectedAdminTerm.call.result)
      f.input :designated_room, collection: Room.where(term: SelectedAdminTerm.call.result)
      f.input :notice, input_html: { rows: 3 }
      f.has_many :timeslots do |f2|
        f2.inputs 'Timeslots' do
          unless f2.object.nil?
            f2.input :_destroy, as: :boolean, label: 'Remove?'
          end
          f2.input :label
          f2.input :time
          f2.input :is_highlighted
          f2.has_many :items, style: 'margin-left: 100px;' do |f3|
            f3.inputs 'Items' do
              unless f3.object.nil?
                f3.input :_destroy, as: :boolean, label: 'Remove?'
              end
              f3.input :label
              f3.input :title
            end
          end unless f2.object.new_record?
        end
      end unless f.object.new_record?
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
        .joins(:term)
        .where(term: SelectedAdminTerm.call.result)
        .includes(:designated_head, :designated_room, :timeslots)
    end
  end
end
