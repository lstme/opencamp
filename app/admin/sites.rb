ActiveAdmin.register Site do
  permit_params :title, :logo

  menu priority: 100, label: "<i class='fa fa-mountain'></i> Site".html_safe

  index do
    selectable_column
    column :logo do |site|
      image_tag rails_storage_proxy_path(site.logo.variant(:thumb)) if site.logo.attached?
    end
    column :title
    actions
  end

  form do |f|
    f.inputs do
      f.input :logo, as: :file
      f.input :title
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain.with_attached_logo
    end
  end
end
