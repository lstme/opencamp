ActiveAdmin.register Label do
  actions :index, :new, :create, :edit, :update, :destroy
  permit_params :name, :label_group_id

  config.sort_order = 'name_asc'
  menu label: "<i class='fas fa-tag'></i> Labels".html_safe

  index do
    selectable_column
    column :id
    column :name do |label|
      label_group = label.label_group
      label_color_name(
          label_group.color,
          name: label.name
      )
    end
    column :label_group
    column :created_at
    column :updated_at
    actions
  end
end
