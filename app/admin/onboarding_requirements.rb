ActiveAdmin.register OnboardingRequirement do
  permit_params :term_id, :title, :description, :position,
    onboarding_requirement_inputs_attributes: [:id, :key, :label, :_destroy]

  config.sort_order = 'position_asc'

  menu priority: 50, label: "<i class='fa fa-check-square'></i> Requirements".html_safe

  index do
    selectable_column
    column :term
    column :position
    column :title
    column :description
    actions
  end

  filter :term
  filter :title_cont, label: 'Title'
  filter :description_cont, label: 'Description'

  show do
    attributes_table do
      row :term
      row :position
      row :title
      row :description
    end

    panel 'Inputs' do
      table_for onboarding_requirement.onboarding_requirement_inputs do
        column :key
        column :label
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :term
      f.input :position
      f.input :title
      f.input :description
      f.has_many :onboarding_requirement_inputs do |f2|
        unless f2.object.nil?
          f2.input :_destroy, as: :boolean, label: 'Remove?'
        end
        f2.input :key
        f2.input :label
      end
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(:term)
          .where(term: SelectedAdminTerm.call.result)
    end
  end

  action_item :copy_from_other_term, only: :index do
    link_to 'Copy from Term', copy_from_other_term_admin_onboarding_requirements_path, method: :post, data: {
      modal: true,
      action: :copy_from_other_term,
      title: 'Copy requirements from term',
      inputs: {
        term: Term.all.map { |term| [term.name, term.id] }
      }
    }
  end

  collection_action :copy_from_other_term, method: :post do
    current_term_id = SelectedAdminTerm.call.result.id
    Term.find(params[:term]).onboarding_requirements.each do |requirement|
      OnboardingRequirement.create!(
        term_id: current_term_id,
        title: requirement.title,
        description: requirement.description,
        position: requirement.position
      )
    end
    redirect_to admin_onboarding_requirements_path, notice: 'Requirements copied successfully'
  end
end
