module Mailings
  ActiveAdmin.register List do
    menu parent: 'Mailings'
    permit_params :name, contact_ids: []

    config.sort_order = "created_at_desc"

    duplicatable via: :duplicate!

    index do
      selectable_column
      column :name
      column :contacts do |list|
        list.contacts.count
      end
      column :campaigns do |list|
        list.campaigns.count
      end
      column :created_at
      column :updated_at

      actions
    end

    show do |list|
      attributes_table do
        row :name
        table_for list.campaigns.order(:name) do
          column 'Campaigns' do |campaign|
            link_to campaign.name, admin_mailings_campaign_path(campaign)
          end
          column :started_at
        end
        table_for list.contacts.order(:email) do
          column 'Contacts' do |contact|
            link_to contact.email, admin_mailings_contact_path(contact)
          end
          column :username
          column :first_name
          column :last_name
        end
        row :created_at
        row :updated_at
      end
    end

    form do |f|
      f.inputs do
        f.input :name
      end
      f.actions
    end
  end
end
