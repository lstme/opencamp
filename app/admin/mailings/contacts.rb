module Mailings
  ActiveAdmin.register Contact do
    menu parent: 'Mailings'

    permit_params :email, :username, :first_name, :last_name, :born_at, :gender, :address, :phone, :parents_phone, :school, :note

    config.per_page = [30, 50, 100]
    config.sort_order = "created_at_desc"

    active_admin_import template: 'import',
                        template_object: ActiveAdminImport::Model.new(
                            hint: "you can configure CSV options",
                            csv_options: { col_sep: ";", row_sep: nil, quote_char: nil }
                        )

    batch_action :add_to_list, form: -> { { list: List.pluck(:name, :id) } } do |ids, inputs|
      Contact.transaction do
        Contact.where(id: ids).find_each do |contact|
          ListsContact.find_or_create_by!(contact: contact, list_id: inputs[:list])
        end
      end
      redirect_to admin_mailings_list_path(inputs[:list]), notice: "Added #{ids.count} contacts."
    end

    batch_action :remove_from_list, form: -> { { list: List.pluck(:name, :id) } } do |ids, inputs|
      ListsContact.where(list_id: inputs[:list], contact_id: ids).destroy_all
      redirect_to admin_mailings_list_path(inputs[:list]), notice: "Removed #{ids.count} contacts."
    end

    filter :lists
    filter :no_lists, label: 'No lists', as: :boolean
    filter :email_cont, label: 'E-mail'
    filter :first_name_cont, label: 'First name'
    filter :last_name_cont, label: 'Last name'
    filter :born_at
    filter :note_cont, label: 'Note'

    index do
      selectable_column
      column :lists
      column :email
      column :first_name
      column :last_name
      column :born_at
      column :note
      column :created_at
      column :updated_at
      actions
    end

    form do |f|
      f.inputs 'Email' do
        f.input :email
      end
      f.inputs 'Meta data' do
        f.input :username
        f.input :first_name
        f.input :last_name
        f.input :born_at
        f.input :gender, as: :radio, collection: Contact::GENDERS
        f.input :address
        f.input :phone
        f.input :parents_phone
        f.input :school
        f.input :note
      end
      f.actions
    end
  end
end
