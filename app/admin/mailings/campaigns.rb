module Mailings
  ActiveAdmin.register Campaign do
    menu parent: 'Mailings'
    actions :index, :show, :new, :create, :edit, :update, :preview

    permit_params :name, :list_id, :subject, :body, :cc_email, :bcc_email, :reply_to_email, :send_rate,
                  :from_email, :from_name

    duplicatable via: :form

    config.sort_order = "created_at_desc"

    member_action :preview do
      if params[:delivery_id]
        @delivery = Delivery.find(params[:delivery_id])
      else
        contact = Contact.find(params[:contact_id])
        @delivery = BuildCampaignDelivery.call(resource, contact).result
      end
    end

    action_item :send_emails, only: :show do
      link_to 'SEND ALL EMAILS',
              send_emails_admin_mailings_campaign_path(resource),
              method: :put,
              data: { confirm: 'Are you sure?' }
    end
    member_action :send_emails, method: :put do
      CreateCampaignDeliveries.call(resource).result

      redirect_to admin_mailings_deliveries_path(q: { campaign_id_eq: resource.id }), notice: 'Started sending campaign emails'
    end

    index do
      selectable_column
      column :name
      column :list
      column :started_at
      column :created_at
      column :updated_at

      actions
    end

    show do |campaign|
      attributes_table do
        if campaign.deliveries.any?
          row :deliveries do
            link_to 'View & Manage deliveries', admin_mailings_deliveries_path(q: { campaign_id_eq: campaign.id })
          end
        end
        row :list
        row :name
        row :send_rate do |campaign|
          distance_of_time_in_words(campaign.send_rate)
        end
        row :subject
        row :body
        row :cc_email
        row :bcc_email
        row :reply_to_email
        row :from_email
        row :from_name
        row :created_at
        row :updated_at
      end
      panel "Preview emails to list \"#{campaign.list.name}\"" do
        table_for campaign.list.contacts do
          column :email
          column :username
          column :first_name
          column :last_name
          column :preview do |contact|
            link_to 'Preview email', preview_admin_mailings_campaign_path(campaign, contact_id: contact.id)
          end
        end
      end
    end

    form do |f|
      f.inputs do
        f.input :list, as: :select, collection: Mailings::List.order(:name)
        f.input :name
        f.input :send_rate, label: 'Send Rate (in seconds)'
      end
      f.inputs 'Email template' do
        f.input :subject
        f.input :body,
                label: 'Body Template',
                input_html: { value: f.object.body || f.object.default_body.join("\n") },
                hint: "Available Template Tags: #{f.object.default_body.join(', ')}"
        f.input :cc_email
        f.input :bcc_email
        f.input :reply_to_email
        f.input :from_email
        f.input :from_name
      end
      f.actions
    end
  end
end
