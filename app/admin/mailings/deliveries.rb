module Mailings
  ActiveAdmin.register Delivery do
    menu parent: 'Mailings'
    actions :index, :show, :destroy, :preview

    config.sort_order = 'deliver_at_desc'
    config.per_page = [30, 50, 100]

    member_action :preview do
      @delivery = resource
      render 'admin/mailings_campaigns/preview'
    end

    index do
      selectable_column

      column :campaign
      column :contact
      column :from_email
      column 'Scheduled at', :deliver_at
      column :status do |delivery|
        if delivery.delivered?
          span "Sent at #{l(delivery.delivered_at, format: :short)}", title: delivery.delivered_at, class: 'status_tag yes'
        elsif delivery.failed?
          span "Failed at #{l(delivery.failed_at, format: :short)}", title: delivery.failed_at, class: 'status_tag no'
        else
          span "Will be delivered at #{l(delivery.deliver_at, format: :short)}", title: delivery.deliver_at, class: 'status_tag maybe'
        end
      end
      column :preview do |delivery|
        link_to 'Preview email', preview_admin_mailings_delivery_path(delivery)
      end

      actions
    end
  end
end
