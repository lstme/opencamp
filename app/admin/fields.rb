ActiveAdmin.register Field do
  permit_params :term_id, :name, :slug, :type, :settings

  config.sort_order = 'created_at_desc'

  menu priority: 6, label: "<i class='fa fa-i-cursor'></i> Fields".html_safe

  index do
    selectable_column
    column :term
    column :name
    column :slug
    column :type
    column :settings

    actions
  end

  filter :term
  filter :name_cont, label: 'Name'
  filter :slug_cont, label: 'Slug'
  filter :type, as: :select, collection: -> { Field::TYPES }

  form do |f|
    f.inputs do
      f.input :term
      f.input :name
      f.input :slug
      f.input :type, as: :select, collection: Field::TYPES
      f.input :settings, as: :text, input_html: { class: 'json-editor' }
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(:term)
          .where('terms.id' => SelectedAdminTerm.call.result.id)
    end
  end
end
