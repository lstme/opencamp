ActiveAdmin.register Transaction do
  permit_params :attendance_id, :amount, :note

  config.sort_order = 'created_at_desc'

  menu priority: 10, label: "<i class='fa fa-coins'></i> Transactions".html_safe

  includes attendance: [:user, :term]

  index do
    selectable_column
    column :attendance
    column :amount
    column :note
    column :created_at
    actions
  end

  filter :attendance_user_first_name_or_attendance_user_last_name_cont, label: 'User'
  filter :attendance_term_id_eq, label: 'Term', collection: -> { Term.pluck(:name, :id) }, as: :select
  filter :amount
  filter :note_cont, label: 'Note'

  form do |f|
    f.inputs do
      f.input :attendance, as: :select, collection: Attendance.includes(:user, :term)
      f.input :amount
      f.input :note
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(attendance: :term)
          .where('terms.id' => SelectedAdminTerm.call.result.id)
    end
  end
end
