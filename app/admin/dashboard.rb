ActiveAdmin.register_page "Dashboard" do
  current_term_menu = {
      label: -> do
        SelectedAdminTerm.call.result&.name || '<No term>'
      end,
      priority: 1,
      html_options: { class: 'current_term' }
  }

  menu current_term_menu

  content title: proc { I18n.t("active_admin.dashboard") } do
    panel 'Links' do
      link_to root_path do
        icon('fa', 'home') + ' Home'
      end
    end

    panel "Recently updated content", style: 'margin-top: 30px' do
      table_for PaperTrail::Version.order(created_at: :desc).limit(50), {class: 'index_table index'} do # Use PaperTrail::Version if this throws an error
        column(span 'At / Action') do |v|
          (v.created_at.to_s(:long) + '<br />' + v.event).html_safe
        end
        column(span "Who") do |v|
          if v.whodunnit
            link_to User.find(v.whodunnit).email, [:admin, User.find(v.whodunnit)]
          else
            'Unknown'
          end
        end
        column(span "What") do |v|
          case v.item_type
          when 'User'
            if v.item.nil?
              'Deleted user'
            else
              link_to v.item.full_name, admin_user_path(v.item)
            end
          when 'Attendance'
            if v.item.nil?
              'Deleted attendance'
            else
              link_to "#{v.item.user.full_name} - #{v.item.term.name}", admin_attendance_path(v.item)
            end
          else
            v.item_type
          end
        end
        column(span 'Changes') do |v|
          div('data-diff': v.changeset.to_json, 'data-wide-keys': ['address'].to_json)
        end
      end
    end
  end # content
end
