ActiveAdmin.register List do
  permit_params :term_id, :name, :slug,
      *List.jsonb_store_key_mapping_for_settings.keys.map(&:to_sym),
      field_ids: [],
      list_fields_attributes: [:id, :field_id, :position, :_destroy]

  config.sort_order = 'created_at_desc'

  menu priority: 5, label: "<i class='fa fa-clipboard-list'></i> Lists".html_safe

  index do
    selectable_column
    column :term
    column :name
    column :slug
    column :fields do |list|
      pluralize(list.fields.count, 'field')
    end

    actions
  end

  filter :term
  filter :name_cont, label: 'Name'
  filter :slug_cont, label: 'Slug'

  form do |f|
    f.inputs do
      f.input :term
      f.input :name
      f.input :slug
      f.inputs 'Settings' do
        f.input :include_children, as: :boolean
        f.input :include_instructors, as: :boolean
        f.input :show_avatar, as: :boolean
        f.input :show_name, as: :boolean
        f.input :show_age, as: :boolean
        f.input :show_room, as: :boolean
        f.input :show_workgroup, as: :boolean
        f.input :sort_by, as: :select, collection: List::SORT_BY
        f.input :settings, as: :text, input_html: { class: 'json-editor', rows: 5, readonly: true }
      end
      f.has_many :list_fields do |f2|
        f2.inputs 'List fields' do
          unless f2.object.nil?
            f2.input :_destroy, as: :boolean, label: 'Remove?'
          end
          f2.input :field
          f2.input :position
        end
      end
      f.inputs 'Fields' do
        f.input :fields, as: :check_boxes, collection: f.object.term ? f.object.term.fields.order(slug: :asc).map { |field| ['%s (%s)' % [field.display_name, field.slug], field.id] } : []
      end
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
          .joins(:term)
          .where('terms.id' => SelectedAdminTerm.call.result.id)
    end

    def update
      update! do |format|
        format.html { redirect_to edit_admin_list_path(resource) } if resource.valid?
      end
    end
  end
end
