ActiveAdmin.register Document do
  belongs_to :term, optional: true

  permit_params :term_id, :slug, :title, :mode, :code, :local_file_path

  config.sort_order = 'updated_at_desc'

  includes :term

  menu priority: 40, label: "<i class='fa fa-file-alt'></i> Documents".html_safe

  member_action :duplicate, method: :post do
    new_document = resource.dup
    new_document.slug = "copy-#{resource.slug}"
    new_document.title = "Copy - #{resource.title}"
    new_document.save!

    redirect_to edit_admin_document_path(new_document)
  end

  index do
    selectable_column
    column :term
    column :slug
    column :title
    column :mode
    column :link do |record|
      link_to 'Public link', document_path(record.slug)
    end
    column :created_at
    column :updated_at
    actions do |record|
      link_to 'Duplicate', duplicate_admin_document_path(record), method: :post, class: 'member_link'
    end
  end

  filter :term
  filter :mode
  filter :title
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs do
      f.input :term
      f.input :slug
      f.input :title
      f.input :mode, as: :radio, collection: Document.modes.map { |key, value| [key.humanize, value] }, include_blank: false
      f.input :code
      f.input :local_file_path
    end
    f.actions
  end

  controller do
    def scoped_collection
      end_of_association_chain
        .joins(:term)
        .where(term: SelectedAdminTerm.call.result)
    end

    def create
      create! do |format|
        format.html { redirect_to edit_admin_document_path(resource) } if resource.valid?
      end
    end

    def update
      update! do |format|
        format.html { redirect_to edit_admin_document_path(resource) } if resource.valid?
      end
    end
  end
end
