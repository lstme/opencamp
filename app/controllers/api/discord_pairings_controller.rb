# frozen_string_literal: true

module Api
  # This should be authenticated via routes
  class DiscordPairingsController < ApiController
    skip_before_action :set_admin_term

    def index
      # This is joins() on purpose. Only load pairings that have user set
      pairings = DiscordPairing.joins(:user).includes(user: [attendances: [:term]]).all
      render json: pairings.map{|p| p.to_json}
    end

    def show
      pairing = DiscordPairing.find_by!(discord_id: params[:id])
      render json: pairing.to_json
    end

    def create
      pairing = DiscordPairing.find_or_create_by!(discord_id: create_params[:discord_id])
      pairing.regenerate_pairing_token

      render json: {
        url: pair_discord_pairings_url(token: pairing.pairing_token)
      }
    end

    private

    def create_params
      params.require(:pairing).permit(:discord_id).to_h
    end
  end
end
