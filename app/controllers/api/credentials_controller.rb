# frozen_string_literal: true

module Api
  class CredentialsController < ApiController
    skip_before_action :set_admin_term

    def me
      render json: current_user
    end

    def oauth_user
      if current_user.nil?
        render json: { error: 'unauthorized' }, status: :unauthorized
        return
      end

      # https://openid.net/specs/openid-connect-basic-1_0-31.html#StandardClaims
      render json: {
        sub: current_user.id,
        name: [current_user.first_name, current_user.last_name].join(' ').strip,
        given_name: current_user.first_name,
        family_name: current_user.last_name,
        # middle_name: string,
        # nickname: string,
        preferred_username: current_user.username,
        # profile: string,
        picture: current_user.avatar.attached? ? rails_storage_proxy_url(current_user.avatar) : nil,
        # website: string,
        email: current_user.email,
        email_verified: true,
        # gender: string,
        # birthdate: string,
        # zoneinfo: string,
        # locale: string,
        # phone_number: string,
        # phone_number_verified: boolean,
        # address: JSON,
        updated_at: current_user.updated_at.to_i,
      }
    end
  end
end
