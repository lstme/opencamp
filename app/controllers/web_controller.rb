class WebController < ApplicationController
  layout 'web'

  before_action :authenticate_user!
  before_action :require_current_term

  def index
  end

  private

  def authorize_instructor!
    authorize :instructor_navbar, :show?
  end

  def require_current_term
    if current_user && !current_user.selected_term
      redirect_to terms_path
    end
  end
end
