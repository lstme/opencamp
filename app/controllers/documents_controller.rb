class DocumentsController < WebController
  layout 'application'

  before_action :allow_iframe

  def show
    @document = Document.friendly
                        .includes(:term)
                        .find(params[:id])

    @output = @document.render(binding).html_safe

    if @document.mode == 'raw'
      render plain: @output, layout: false
    end
  rescue => ex
    message = ex.to_s
    backtrace = ex.backtrace.join("\n")
    render plain: "#{message}\n\n#{backtrace}", status: :internal_server_error, layout: false
  end
end
