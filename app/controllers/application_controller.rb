class ApplicationController < ActionController::Base
  include Pundit::Authorization
  include SetAdminTerm

  helper_method :site_title
  helper_method :site_logo
  helper_method :current_term
  helper_method :current_term_name
  helper_method :body_variant
  helper_method :body_variant_classes

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_paper_trail_whodunnit

  def admin_access_denied(error)
    flash[:alert] = error.message
    redirect_to root_url
  end

  private

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore
    error_message = t("#{policy_name}.#{exception.query}", scope: 'pundit', default: :default)

    redirect_to(request.referrer || root_path, alert: error_message)
  end

  def site_title
    if current_term_name.present?
      current_term_name
    else
      current_site.title
    end.to_s
  end

  def site_logo
    logo = current_site.logo
    logo.attached? ? rails_storage_proxy_path(logo.variant(:thumb)) : nil
  end

  def current_site
    @current_site ||= Site.instance
  end

  def current_term
    @current_term ||= current_user&.selected_term
  end

  def current_term_name
    @current_term_name ||= current_term&.name
  end

  def body_variant
    params[:variant]
  end

  def body_variant_classes
    case body_variant
    when 'dark' then
      'text-white bg-black'
    when 'light' then
      'text-black bg-white'
    else
      nil
    end
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :gender, :born_at])
  end

end
