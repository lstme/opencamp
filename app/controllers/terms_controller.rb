class TermsController < WebController

  skip_before_action :require_current_term

  def index
  end

  def select
    available_terms = SelectableTermsQuery.call(user: current_user)
    selected_term = available_terms.find(params[:id])

    current_user.update!(selected_term:)

    redirect_to :root
  end
end
