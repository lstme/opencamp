class ApiController < ActionController::API
  include ActionController::Cookies
  include DoorkeeperAuthorizable
  include Pundit::Authorization
  include SetAdminTerm

  helper_method :current_term
  helper_method :current_term_name

  private

  def current_term
    @current_term ||= LatestTermQuery.call.first
  end

  def current_term_name
    @current_term_name ||= current_term&.name
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end
end
