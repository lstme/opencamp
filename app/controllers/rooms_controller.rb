class RoomsController < WebController
  # before_action :authorize_instructor!
  skip_before_action :authenticate_user!, only: [:print, :ratings]
  after_action :allow_iframe, only: [:print, :ratings]

  def index
  end

  def print
    current_term = SelectedAdminTerm.call.result
    rooms = current_term
                &.rooms
                &.includes(attendances: [:room, :term, :user]) || []

    render locals: { rooms: rooms }, layout: 'print'
  end

  def ratings
    current_term = SelectedAdminTerm.call.result
    rooms = current_term
      &.rooms
      &.includes(:room_scores, attendances: [:room, :term, :user]) || []

    render locals: { rooms: rooms }, layout: 'print'
  end
end
