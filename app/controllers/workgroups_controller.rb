class WorkgroupsController < WebController
  before_action :authorize_instructor!, except: [:dashboard]
  after_action :allow_iframe, only: [:dashboard]

  skip_before_action :authenticate_user!, only: [:dashboard]

  def index
  end

  def dashboard
    workgroups = print_data
    render locals: { workgroups: workgroups }, layout: 'print'
  end

  def print
    workgroups = print_data
    render locals: { workgroups: workgroups }, layout: 'print'
  end

  private

  def print_data
    current_term = SelectedAdminTerm.call.result
    current_term
      &.workgroups
      &.includes(attendances: [:workgroup, :term, :user]) || []
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end
end
