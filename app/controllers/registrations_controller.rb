class RegistrationsController < Devise::RegistrationsController
  layout 'web', only: [:edit, :update]

  protected

  def after_update_path_for(resource)
    edit_registration_path(resource)
  end

  def update_resource(resource, params)
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end

    result = resource.update(params)

    # Cleanup passwords
    resource.password = resource.password_confirmation = nil
    result
  end

  def account_update_params
    params.require(:user).permit(:email, :first_name, :last_name, :gender, :born_at, :password, :password_confirmation).to_h
  end
end
