class UploadsController < WebController
  before_action :authorize_instructor!

  def index
    @upload = current_term.uploads.build
    @uploads = current_term.uploads
  end

  def create
    @upload = current_term.uploads.build(upload_params)

    if @upload.save
      redirect_to uploads_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @upload = current_term.uploads.find(params[:id])
    @upload.destroy

    redirect_to uploads_path, status: :see_other
  end

  private

  def upload_params
    params.require(:upload).permit(:file)
  end

end
