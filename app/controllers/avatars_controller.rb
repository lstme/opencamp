class AvatarsController < ApiController
  include ActionController::Streaming

  after_action :allow_iframe, only: [:show]

  def show
    user = if params[:user_id].present?
      User.find(params[:user_id])
    elsif params[:query].present?
      User.where(%{concat(first_name, ' ', last_name, ' ', username) ILIKE ?}, "%#{params[:query]}%").first
    end

    variant = if params[:variant].present?
      params[:variant].to_sym
    else
      :thumb
    end

    if user.present?
      redirect_to rails_blob_url(user.avatar.variant(variant))
    else
      render plain: 'Not found', status: :not_found
    end
  end
end
