class DiscordPairingsController < WebController
  def destroy
    pairing = current_user.discord_pairings.find(params[:id])

    if pairing.destroy!
      redirect_to :edit_user_registration, notice: 'Pairing removed'
    else
      redirect_to :edit_user_registration, notice: 'Could not remove pairing'
    end
  end

  def pair
    pairing = DiscordPairing.find_by(pairing_token: params[:token])
    if pairing
      pairing.user = current_user
      pairing.save!
      redirect_to :edit_user_registration, notice: 'Discord paired!'
    else
      redirect_to :edit_user_registration, notice: 'Could not find pairing'
    end
  end
end
