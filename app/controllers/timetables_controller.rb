class TimetablesController < WebController
  layout 'application'

  before_action :allow_iframe

  skip_before_action :authenticate_user!

  def show
    timetables_relation = Timetable
      .includes(:term, :designated_head, :designated_room)

    term = LatestTermQuery.call.first

    if params[:id] == 'latest'
      @timetable = timetables_relation
        .where(term:)
        .where('date <= ?', Time.zone.today)
        .order(date: :desc)
        .first
    else
      @timetable = timetables_relation
        .find_by(id: params[:id])

      unless @timetable
        @timetable = timetables_relation
          .joins(:term)
          .where(term:)
          .find_by(date: params[:id])
      end
    end

    unless @timetable
      raise ActiveRecord::RecordNotFound
    end

    @timetable_data = timetable_data(@timetable)
  end

  private

  def timetable_data(timetable)
    query = "query Timetable($key: String, $termId: ID!) {
      term(id: $termId) {
        id
        timetable(key: $key) {
          ...Timetable
          __typename
        }
        __typename
      }
    }

    fragment Timetable on Timetable {
      date
      designated_head {
        id
        full_name
        avatar {
          ...Avatar
          __typename
        }
        __typename
      }
      designated_room {
        id
        name
        __typename
      }
      id
      notice
      timeslots {
        id
        is_highlighted
        items {
          id
          title
          label
          __typename
        }
        label
        time
        __typename
      }
      __typename
    }

    fragment Avatar on Avatar {
      id
      icon_url
      medium_url
      thumb_url
      __typename
    }"
    date = timetable.date.to_s
    variables = {
      "key": date,
      "termId": timetable.term.id
    }
    context = {
      current_user: timetable.term.instructor_attendances.first.user
    }
    operation_name = nil
    res = OpencampSchema.execute(query, variables: variables, context: context, operation_name: operation_name)

    {
      date: date,
      timetable: res['data']['term']['timetable']
    }
  end
end
