module SupportsGraphql
  extend ActiveSupport::Concern

  def graphql_execute(query, variables: {}, operation_name: nil)
    PaperTrail.request.whodunnit = current_user&.id
    OpencampSchema.execute(query, variables: variables, context: context, operation_name: operation_name)
  end

  private

  def context
    {
        current_user: current_user,
        active_term_id: active_term_id
    }
  end

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  def handle_error_in_development(e)
    logger.error e.message
    logger.error e.backtrace.join("\n")

    render json: { error: { message: e.message, backtrace: e.backtrace }, data: {} }, status: 500
  end

  def active_term_id
    nil
  end
end
