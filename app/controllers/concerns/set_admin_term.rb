module SetAdminTerm
  extend ActiveSupport::Concern

  included do
    before_action :set_admin_term
  end

  private

  def set_admin_term
    cookie_term = Term.find_by(id: cookies[:admin_term_id])

    current_term = cookie_term || latest_term

    if current_term.present?
      RequestStore.store[:admin_term] = current_term
      cookies[:admin_term_id] = current_term.id
    end
  end

  def latest_term
    LatestTermQuery.call.first
  end
end
