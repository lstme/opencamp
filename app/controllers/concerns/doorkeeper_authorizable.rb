module DoorkeeperAuthorizable
  extend ActiveSupport::Concern

  private

  def devise_current_user
    @devise_current_user ||= warden.authenticate(scope: :user)
  end

  def doorkeeper_current_user
    if doorkeeper_token
      if doorkeeper_token.resource_owner_id.present?
        User.find(doorkeeper_token.resource_owner_id)
      else
        User.find_by!(username: doorkeeper_token.application.name)
      end
    end
  end

  def current_user
    devise_current_user || doorkeeper_current_user
  end
end
