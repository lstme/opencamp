class GraphqlController < ApiController
  include SupportsGraphql

  def execute
    result = graphql_execute(query, variables: variables, operation_name: operation_name)

    # if result.subscription?
    #   response.headers["X-Subscription-ID"] = result.context[:subscription_id]
    # end

    render json: result
  rescue StandardError => e
    raise e unless Rails.env.development?
    handle_error_in_development e
  end

  private

  def variables
    ensure_hash(params[:variables]&.permit!)
  end

  def query
    params[:query]
  end

  def operation_name
    params[:operationName]
  end

  def active_term_id
    request.headers['HTTP_X_ACTIVE_TERM_ID']
  end
end
