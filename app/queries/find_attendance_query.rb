class FindAttendanceQuery < ApplicationQuery
  queries Attendance

  def query
    res = relation.includes(:term)

    res = res.where(id: id) if id.present?
    res = res.where(user_id: user_id) if user_id.present?

    res.limit(1)
  end

  def id
    options[:id]
  end

  def user_id
    options[:user_id]
  end
end
