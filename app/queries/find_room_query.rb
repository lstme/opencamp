class FindRoomQuery < ApplicationQuery
  queries Room

  def query
    relation
        .includes(:term)
        .where(id: id)
        .limit(1)
  end

  def id
    options.fetch(:id)
  end
end
