class SelectableTermsQuery < ApplicationQuery
  queries Term

  def query
    user.is_admin ? relation : user.attended_terms
  end

  def user
    options.fetch(:user)
  end
end
