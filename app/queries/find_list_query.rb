class FindListQuery < ApplicationQuery
  queries List

  def query
    relation
        .where(id: id)
        .includes(:list_fields)
        .limit(1)
  end

  def id
    options.fetch(:id)
  end
end
