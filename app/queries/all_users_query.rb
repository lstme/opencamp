class AllUsersQuery < ApplicationQuery
  queries User

  def query
    relation
  end
end