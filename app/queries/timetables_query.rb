class TimetablesQuery < ApplicationQuery
  queries Timetable

  def query
    result = relation
    result = result.includes(:term, :designated_head, :designated_room)
    result.where(term:)
  end

  def term
    options[:term]
  end
end
