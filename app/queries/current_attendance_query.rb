class CurrentAttendanceQuery < ApplicationQuery
  queries Attendance

  def query
    relation
        .includes(
            :term,
            :user,
            room: {
                attendances: { user: { avatar_attachment: :blob } },
                child_attendances: { user: { avatar_attachment: :blob } },
                instructor_attendances: { user: { avatar_attachment: :blob } },
            }
        )
        .where(user: user, term: term)
  end

  def user
    options.fetch(:user)
  end

  def term
    # LatestTermQuery should be removed and
    options[:term] || LatestTermQuery.call.first
  end
end
