class TimetableQuery < ApplicationQuery
  queries Timetable

  def query
    result = relation
    result = result.includes(:term, :designated_head, :designated_room)

    result = result.where(term:)

    result = result.where(id: key).or(result.where(date: key)) if key
    result = result.order(date: :desc) unless key

    result.limit(1)
  end

  def key
    options[:key]
  end

  def preload
    options.fetch(:preload, true)
  end

  def term
    options[:term]
  end
end
