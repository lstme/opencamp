class AllLabelGroupsQuery < ApplicationQuery
  queries LabelGroup

  def query
    relation
  end
end