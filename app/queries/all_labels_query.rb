class AllLabelsQuery < ApplicationQuery
  queries Label

  def query
    relation
  end
end