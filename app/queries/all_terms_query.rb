class AllTermsQuery < ApplicationQuery
  queries Term

  def query
    relation
  end
end