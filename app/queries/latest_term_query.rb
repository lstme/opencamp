class LatestTermQuery < ApplicationQuery
  queries Term

  def query
    result = relation

    result = result.includes(
        attendances: { user: { avatar_attachment: :blob } },
        child_attendances: { user: { avatar_attachment: :blob } },
        instructor_attendances: { user: { avatar_attachment: :blob } },
    ) if preload

    result
        .reorder(ends_at: :desc)
        .limit(1)
  end

  def preload
    options.fetch(:preload, true)
  end
end