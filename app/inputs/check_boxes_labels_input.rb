class CheckBoxesLabelsInput < Formtastic::Inputs::CheckBoxesInput
  def choice_html(choice)
    label = get_label(choice[1])
    template.content_tag(
        :label,
        checkbox_input(choice) + label_color_name(label.label_group.color, name: label.name),
        label_html_options.merge(for: choice_input_dom_id(choice), class: '', style: 'display: block; margin: 15px;'))
  end

  def label_color_name(color, options)
    light_klass = label_lightness_class(color)
    template.content_tag :span, class: 'label_tag', style: "background-color: #{color}" do
      template.content_tag :span, options[:name], class: light_klass
    end
  end

  def get_label(id)
    Label.find(id)
  end
end
