import CoinsTimelineCard from 'molecule/CoinsTimelineCard';
import Message from 'atom/Message';
import RoomCard from 'molecule/RoomCard';
import UserCard, { UserCardSkeleton } from 'molecule/UserCard';
import { useSelectedTermId } from 'organism/AppContext';
import TimetableCard from 'organism/TimetableCard';
import PageContainer from 'template/PageContainer';
import { useHomeQuery } from '~graphql.generated';

const Home = () => {
    const termId = useSelectedTermId();
    const { data, loading } = useHomeQuery({ variables: { termId } });

    if (loading)
        return (
            <PageContainer>
                <UserCardSkeleton />
            </PageContainer>
        );
    if (!data) return null;

    if (data.term?.current_attendance == null) {
        return (
            <PageContainer>
                <Message message="You were not on this LSTME" />
            </PageContainer>
        );
    }

    return (
        <PageContainer>
            <UserCard user={data.me} attendance={data.term?.current_attendance} />
            <RoomCard className="mt-4" room={data.term?.current_attendance?.room} highlightUserId={data.me?.id} />
            <TimetableCard className="mt-4" />
            <CoinsTimelineCard className="mt-4" attendance={data.term?.current_attendance} />
        </PageContainer>
    );
};

export default Home;
