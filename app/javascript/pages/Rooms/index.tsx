import ErrorDisplay from 'atom/ErrorDisplay';
import { useIsInstructor, useSelectedTermId } from 'organism/AppContext';
import { useMemo, useState } from 'react';
import BoxList, { BoxListSkeleton } from 'template/BoxList';
import PageContainer from 'template/PageContainer';
import { AttendanceFragment, useChangeAttendanceRoomMutation, useTermRoomsQuery } from '~graphql.generated';
import Bed from './Bed';
import Room from './Room';

const Rooms = () => {
    const isInstructor = useIsInstructor();
    const termId = useSelectedTermId();
    const { data, loading, error } = useTermRoomsQuery({ variables: { termId } });

    const [filter, setFilter] = useState('');
    const filterFn = useMemo(() => {
        const normalizedFilter = filter.trim().toLowerCase();
        if (normalizedFilter.length === 0) return undefined;

        return (a: AttendanceFragment | string) => {
            const text = typeof a === 'string' ? a : a.user.full_name;
            return text
                .toLowerCase()
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '')
                .includes(normalizedFilter);
        };
    }, [filter]);

    const [editing, setEditing] = useState(false);

    const [changeAttendance] = useChangeAttendanceRoomMutation();
    const changeAttendanceRoomId = (attendanceId: string, roomId: string | null) => {
        if (!data || !data.term) return Promise.reject(new Error('Data not loaded yet'));

        return changeAttendance({
            variables: {
                id: attendanceId,
                roomId: roomId
            }
        });
    };

    const handleAttendanceSelect = (attendanceId: string, roomId: string) =>
        changeAttendanceRoomId(attendanceId, roomId);
    const handleAttendanceKick = (attendanceId: string) => changeAttendanceRoomId(attendanceId, null);

    if (error) return <ErrorDisplay error={error} />;
    if (loading)
        return (
            <PageContainer>
                <BoxListSkeleton />
            </PageContainer>
        );

    if (!data?.term) return null;

    const unassigned = data.term.attendances.filter(a => a.room == null);
    const rooms = data.term.rooms;

    let visibleUnassigned = unassigned;
    let visibleRooms = rooms;

    if (filterFn) {
        visibleUnassigned = unassigned.filter(filterFn);
        visibleRooms = rooms.filter(r => filterFn(r.name) || r.attendances.filter(filterFn).length > 0);
    }

    return (
        <PageContainer>
            <div className="page-header mt-0 mb-4">
                <div className="row align-items-center">
                    <div className="col-auto ms-auto d-flex">
                        <div className="me-3 d-none d-md-block">
                            <input
                                type="text"
                                className="form-control"
                                aria-label="Filter"
                                placeholder="Filter"
                                autoFocus
                                value={filter}
                                onChange={e => setFilter(e.target.value)}
                            />
                        </div>
                        {isInstructor && (
                            <button className="btn" onClick={() => setEditing(v => !v)}>
                                {editing ? 'View' : `Edit${unassigned.length > 0 ? ` (${unassigned.length} unassigned)` : ''}`}
                            </button>
                        )}
                    </div>
                </div>
            </div>

            <BoxList
                leading={
                    editing && visibleUnassigned.length > 0 && (
                        <div className="card">
                            <div className="card-header">
                                <div className="row align-items-center">
                                    <div className="col-auto">
                                        <div className="card-title">
                                            No room{' '}
                                            <span className="badge bg-info-lt">
                                                {unassigned.length} / {data.term.attendances.length}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                {visibleUnassigned.map(a => (
                                    <Bed key={a.id} attendance={a} highlight={filterFn?.(a)} />
                                ))}
                            </div>
                        </div>
                    )
                }
            >
                {visibleRooms.map(room => (
                    <BoxList.Box key={room.id}>
                        <Room
                            room={room}
                            readonly={!editing}
                            onAttendanceSelect={handleAttendanceSelect}
                            onAttendanceKick={handleAttendanceKick}
                            highlight={filterFn?.(room.name)}
                            highlightAttendance={filterFn}
                        />
                    </BoxList.Box>
                ))}
            </BoxList>
        </PageContainer>
    );
};

export default Rooms;
