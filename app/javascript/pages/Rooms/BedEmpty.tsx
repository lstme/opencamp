import cx from 'classnames';
import { FaPlus } from 'react-icons/fa';
import AttendanceSelect from '../../organisms/AttendanceSelect';

interface IProps {
    instructorOnly: boolean;
    onAttendanceSelect: (attendanceId: string) => Promise<any>;
    readonly?: boolean;
}

const BedEmpty = ({ instructorOnly, onAttendanceSelect, readonly }: IProps) => {
    if (readonly) {
        return (
            <div className="px-3 py-1 tw-h-[58px] tw-flex tw-items-center tw-justify-center text-muted small">
                Empty
            </div>
        );
    }

    return (
        <AttendanceSelect
            onChange={onAttendanceSelect}
            baseFilter={instructorOnly ? a => a.__typename === 'InstructorAttendance' : undefined}
            topTitle="Without room"
            topFilter={a => a.room == null}
        >
            {({ setOpen }) => (
                <div className="px-3 py-1 tw-h-[58px] tw-flex tw-items-center tw-justify-center">
                    <button
                        className={cx('btn w-100 btn-ghost-success', { 'text-instructor': instructorOnly })}
                        onClick={() => setOpen(true)}
                    >
                        <FaPlus className="icon" />
                        Add {instructorOnly ? 'instructor' : 'person'}
                    </button>
                </div>
            )}
        </AttendanceSelect>
    );
};

export default BedEmpty;
