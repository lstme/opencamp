import cx from 'classnames';
import Attendance, { IAttendanceProps } from 'molecule/Attendance';

interface IProps {
    attendance: IAttendanceProps['attendance'];
    onAttendanceKick?: (attendanceId: string) => Promise<any>;
    highlight?: boolean;
}

const Bed = ({ attendance, onAttendanceKick, highlight }: IProps) => (
    <Attendance
        className={cx('px-3 py-2 tw-min-h-[58px]', { 'bg-primary-lt': highlight })}
        details={['gender', 'age', 'workgroup']}
        attendance={attendance}
        onRemove={onAttendanceKick}
    />
);

export default Bed;
