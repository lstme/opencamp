import cx from 'classnames';
import { genNumArray } from 'helpers';
import RoomScoreBadge from 'molecule/RoomScoreBadge';
import RoomNote from 'organism/RoomNote';
import { AttendanceFragment, RoomFragment } from '~graphql.generated';
import Bed from './Bed';
import BedEmpty from './BedEmpty';

interface IProps {
    readonly?: boolean;
    room: RoomFragment;
    onAttendanceSelect: (attendanceId: string, roomId: string) => Promise<any>;
    onAttendanceKick?: (attendanceId: string) => Promise<any>;
    highlight?: boolean;
    highlightAttendance?: (attendance: AttendanceFragment) => boolean;
}

const Room = ({ readonly, room, onAttendanceSelect, onAttendanceKick, highlight, highlightAttendance }: IProps) => {
    const { id, name, capacity, attendances_count, attendances, instructor_only, note, score } = room;
    const freeBedsCount = capacity - attendances_count;

    const handleUserSelect = (attendanceId: string) => onAttendanceSelect(attendanceId, id);

    return (
        <div className="card tw-h-full">
            <div className={cx('card-header', { 'bg-primary-lt': highlight })}>
                <div className="card-title">
                    {name}{' '}
                    <span className="badge bg-info-lt">
                        {attendances_count} / {capacity}
                    </span>
                </div>
                <div className="position-absolute col-auto end-0 me-3">
                    <RoomScoreBadge score={score} id={id} />
                </div>
            </div>
            <RoomNote roomId={id} note={note} readonly={readonly} />
            <div className="border-top">
                {attendances.map(attendance => (
                    <Bed
                        key={attendance.id}
                        attendance={attendance}
                        onAttendanceKick={readonly ? undefined : onAttendanceKick}
                        highlight={highlightAttendance != null ? highlightAttendance(attendance) : undefined}
                    />
                ))}
                {genNumArray(freeBedsCount).map(index => (
                    <BedEmpty
                        key={index}
                        readonly={readonly}
                        instructorOnly={instructor_only}
                        onAttendanceSelect={handleUserSelect}
                    />
                ))}
            </div>
        </div>
    );
};

export default Room;
