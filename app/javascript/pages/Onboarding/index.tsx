import ErrorDisplay from 'atom/ErrorDisplay';
import Attendance from 'molecule/Attendance';
import CardSkeleton from 'molecule/CardSkeleton';
import { useSelectedTermId } from 'organism/AppContext';
import AttendanceForm from 'organism/AttendanceForm';
import AttendanceSelect from 'organism/AttendanceSelect';
import RequirementsForm from 'organism/RequirementsForm';
import UserForm from 'organism/UserForm';
import React, { useState } from 'react';
import { FaSearch } from 'react-icons/fa';
import PageContainer from 'template/PageContainer';
import { useFindAttendanceQuery, useOnboardingQuery } from '~graphql.generated';

const Onboarding: React.FC = () => {
    const termId = useSelectedTermId();
    const onboardingResult = useOnboardingQuery({ variables: { termId } });

    const [userId, setUserId] = useState<string>();

    const findAttendanceResult = useFindAttendanceQuery({
        variables: { userId: userId! },
        skip: !userId
    });

    if (onboardingResult.error) return <ErrorDisplay error={onboardingResult.error} />;

    const term = onboardingResult.data?.term;
    const attendance = findAttendanceResult.data?.find_attendance;

    return (
        <PageContainer>
            {onboardingResult.loading && (
                <div className="page-header placheolder-glow">
                    <div className="page-pretitle">Onboarding</div>
                    <div className="page-title">
                        <div className="placeholder placeholder-sm col-4"></div>
                    </div>
                </div>
            )}

            {term && (
                <div className="page-header mb-4">
                    <div className="row align-items-end">
                        <div className="col">
                            <div className="page-pretitle">Onboarding</div>
                            <div className="page-title">{term.name}</div>
                        </div>

                        {attendance && (
                            <div className="col-auto">
                                <div className="tw-flex tw-items-end">
                                    <Attendance attendance={attendance} />
                                    <button className="btn" onClick={() => findAttendanceResult.refetch()}>
                                        Refresh
                                    </button>
                                </div>
                            </div>
                        )}

                        <div className="col-auto">
                            <AttendanceSelect
                                onChange={(_, { user: { id } }) => {
                                    setUserId(id);
                                    return Promise.resolve();
                                }}
                            >
                                {({ open }) => (
                                    <button className="btn" onClick={() => open()}>
                                        <FaSearch className="icon" />
                                        {attendance ? 'Change' : 'Select'} user
                                    </button>
                                )}
                            </AttendanceSelect>
                        </div>
                    </div>
                </div>
            )}

            {findAttendanceResult.loading && (
                <div className="row">
                    <div className="col-12 col-md">
                        <CardSkeleton className="mb-3" />
                        <CardSkeleton className="mb-3" />
                    </div>
                    <div className="col-12 col-md-4">
                        <CardSkeleton />
                    </div>
                </div>
            )}

            {term && attendance && (
                <div className="row">
                    <div className="col-12 col-md">
                        <UserForm user={attendance.user} className="mb-3" />
                        <AttendanceForm attendance={attendance} className="mb-3" />
                    </div>
                    <div className="col-12 col-md-4">
                        <RequirementsForm
                            attendance={attendance}
                            requirements={term.onboarding_requirements}
                            onComplete={() => setUserId(undefined)}
                        />
                    </div>
                </div>
            )}
        </PageContainer>
    );
};

export default Onboarding;
