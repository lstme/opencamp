import { PageLoadingIndicator } from 'atom/LoadingIndicator';
import { useSelectedTermId } from 'organism/AppContext';
import * as React from 'react';
import PageContainer from 'template/PageContainer';
import { useDataListsQuery } from '~graphql.generated';

interface IProps {}

const DataLists = ({}: IProps) => {
    const termId = useSelectedTermId();
    const [filter, setFilter] = React.useState('');
    const { data, loading } = useDataListsQuery({ variables: { termId } });

    if (loading) return <PageLoadingIndicator />;

    const lists = data?.term?.lists ?? [];

    return (
        <PageContainer>
            <div>
                <div className="px-6 py-4 border-b flex">
                    <input
                        className="input w-full"
                        type="text"
                        placeholder="Filter"
                        value={filter}
                        onChange={e => setFilter(e.target.value)}
                    />
                </div>
                {lists.map(list => (
                    <a href={`/data_lists/${list.id}`} key={list.id} className="link px-6 py-4 block">
                        {list.name}
                    </a>
                ))}
            </div>
        </PageContainer>
    );
};

export default DataLists;
