import { isBefore, parseJSON } from 'date-fns';
import TermCard from 'molecule/TermCard';
import { useMemo } from 'react';
import PageContainer from 'template/PageContainer';
import { useTermsQuery } from '~graphql.generated';

const Terms = () => {
    const { data } = useTermsQuery();

    const terms = useMemo(() => {
        if (!data?.selectable_terms) return [];
        return [...data.selectable_terms].sort((a, b) =>
            isBefore(parseJSON(a.ends_at), parseJSON(b.ends_at)) ? 1 : -1
        );
    }, [data]);

    return (
        <PageContainer>
            <div className="row row-cards">
                {terms.map(term => (
                    <div className="col-md-6 col-xl-3" key={term.id}>
                        <TermCard term={term} />
                    </div>
                ))}
            </div>
        </PageContainer>
    );
};

export default Terms;
