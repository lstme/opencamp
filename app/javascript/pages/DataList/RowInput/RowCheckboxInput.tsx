import cx from 'classnames';
import * as React from 'react';

import { IRowInputProps } from '.';
import RowInputButton from './RowInputButton';

const RowCheckboxInput = ({ listItem, field, onChange }: IRowInputProps) => {
    const value = listItem.attendance.list_data[field.slug];
    const hasValue = value !== undefined && value !== null;
    const [loading, setLoading] = React.useState(false);

    const handleChange = (checked: boolean | null) => {
        setLoading(true);
        onChange(listItem.attendance.id, { [field.slug]: checked })
            .then(() => setLoading(false))
            .catch(err => {
                console.log(err);
                setLoading(false);
            });
    };

    return (
        <RowInputButton
            hasValue={hasValue}
            className={cx({
                'text-green-700': value === true,
                'text-red-700': value === false
            })}
            loading={loading}
            onClick={() => handleChange(!value)}
            onClear={() => handleChange(null)}
        >
            {hasValue ? (
                <i
                    className={cx('fa', {
                        'fa-check': value === true,
                        'fa-times': value === false
                    })}
                />
            ) : (
                '-'
            )}
        </RowInputButton>
    );
};

export default RowCheckboxInput;
