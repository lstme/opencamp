import { DataListQuery } from '~graphql.generated';
import RowCheckboxInput from './RowCheckboxInput';
import RowDateInput from './RowDateInput';
import RowNumberInput from './RowNumberInput';
import RowTextInput from './RowTextInput';
import RowTimeInput from './RowTimeInput';
import RowTimerInput from './RowTimerInput';
import RowTimestampInput from './RowTimestampInput';

export interface IRowInputProps {
    listItem: NonNullable<DataListQuery['list']>['data'][0];
    field: NonNullable<DataListQuery['list']>['fields'][0];
    onChange: (id: string, data: { [key: string]: any }) => Promise<any>;
}

const RowInput = (props: IRowInputProps) => {
    switch (props.field.__typename) {
        case 'CheckboxField':
            return <RowCheckboxInput {...props} />;
        case 'DateField':
            return <RowDateInput {...props} />;
        case 'NumberField':
            return <RowNumberInput {...props} />;
        case 'TextField':
            return <RowTextInput {...props} />;
        case 'TimeField':
            return <RowTimeInput {...props} />;
        case 'TimerField':
            return <RowTimerInput {...props} />;
        case 'TimestampField':
            return <RowTimestampInput {...props} />;
        default:
            return <pre>{JSON.stringify(props.field.__typename, null, 2)}</pre>;
    }
};

export default RowInput;
