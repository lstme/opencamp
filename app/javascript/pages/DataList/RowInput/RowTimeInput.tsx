import { endOfDay, parseISO, parseJSON, startOfDay } from 'date-fns';
import format from 'lib/format';
import * as React from 'react';
import DatePicker from 'react-datepicker';
import { SHORT_TIME_FORMAT } from '~constants';
import { IRowInputProps } from '.';
import RowInputButton from './RowInputButton';

type TTimeSettings = {
    interval?: number;
    caption?: string;
    min?: Date;
    max?: Date;
};

const RowTimeInput = ({ listItem, field, onChange }: IRowInputProps) => {
    const fieldValue = listItem.attendance.list_data[field.slug] || null;
    const settings = field.settings as TTimeSettings;
    const [loading, setLoading] = React.useState(false);

    const handleChange = (date: Date | null) => {
        setLoading(true);
        const v = date === null ? null : date.toISOString();
        onChange(listItem.attendance.id, { [field.slug]: v })
            .then(() => setLoading(false))
            .catch(e => {
                console.log(e);
                setLoading(false);
            });
    };

    const minTime = React.useMemo(() => startOfDay(new Date()), []);
    const maxTime = React.useMemo(() => endOfDay(new Date()), []);

    return (
        // @ts-ignore
        <DatePicker
            selected={fieldValue ? parseISO(fieldValue) : new Date()}
            onChange={handleChange}
            showTimeSelect
            showTimeSelectOnly
            timeIntervals={settings.interval || 15}
            timeCaption={settings.caption || 'Time'}
            timeFormat="HH:mm"
            popperPlacement="bottom"
            minTime={settings.min || minTime}
            maxTime={settings.max || maxTime}
            customInput={
                <RowInputButton hasValue={!!fieldValue} loading={loading} onClear={() => handleChange(null)}>
                    {fieldValue ? format(parseJSON(fieldValue), SHORT_TIME_FORMAT) : '--:--'}
                </RowInputButton>
            }
        />
    );
};

export default RowTimeInput;
