import * as React from 'react';

import { IRowInputProps } from '.';
import RowInputField from './RowInputField';

const RowNumberInput = ({ listItem, field, onChange }: IRowInputProps) => {
    const fieldValue = listItem.attendance.list_data[field.slug] || null;
    const [loading, setLoading] = React.useState(false);

    const handleChange = (newValue: string | null) => {
        const v = newValue === '' ? null : Number(newValue);
        if (v !== null && Number.isNaN(v)) return Promise.resolve(false);
        if (v === fieldValue) return Promise.resolve(true);

        setLoading(true);
        return onChange(listItem.attendance.id, { [field.slug]: v })
            .then(() => setLoading(false))
            .then(() => true)
            .catch(e => {
                console.log(e);
                setLoading(false);
                return false;
            });
    };

    return <RowInputField loading={loading} onValue={handleChange} value={fieldValue || ''} />;
};

export default RowNumberInput;
