import * as React from 'react';
import parseISO from 'date-fns/parseISO';

import { IRowInputProps } from '.';
import RowInputButton from './RowInputButton';
import Timer from '../../../molecules/Timer';

type TTimerValue = {
    start: string | null;
    end: string | null;
} | null;

type TTimerSettings = {
    marks?: number[];
};

const RowTimerInput = ({ field, listItem, onChange }: IRowInputProps) => {
    const value: TTimerValue = listItem.attendance.list_data[field.slug] || null;
    const settings = field.settings as TTimerSettings;
    const [loading, setLoading] = React.useState(false);

    const setValue = (value: TTimerValue | null) => {
        setLoading(true);
        onChange(listItem.attendance.id, { [field.slug]: value })
            .then(() => setLoading(false))
            .catch(e => {
                console.log(e);
                setLoading(false);
            });
    };

    const handleClick = () => {
        if (!value || value.start === null) {
            setValue({ start: new Date().toISOString(), end: null });
        } else if (value.end === null) {
            setValue({ ...value, end: new Date().toISOString() });
        }
        // MAYBE TODO: resume timer on next click, question is if
        // timer should resume from stopped time, or from former start
    };
    const handleClear = () => setValue(null);

    let content: React.ReactNode = '-';
    if (value && value.start) {
        const start = parseISO(value.start);
        if (!value.end) {
            content = (
                <span className="text-green-700">
                    <Timer marks={settings.marks} start={start} />
                </span>
            );
        } else {
            const end = parseISO(value.end);
            content = (
                <span className="text-red-700">
                    <Timer marks={settings.marks} start={start} end={end} />
                </span>
            );
        }
    }

    return (
        <RowInputButton hasValue={!!value} onClick={handleClick} onClear={handleClear} loading={loading}>
            {/* <span className="text-xs">
                {(value && value.start) || '?'}-{(value && value.end) || '?'}
            </span>
            <br /> */}
            {content}
        </RowInputButton>
    );
};

export default RowTimerInput;
