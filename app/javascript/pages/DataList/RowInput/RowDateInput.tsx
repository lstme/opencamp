import { parseISO, parseJSON } from 'date-fns';
import format from 'lib/format';
import * as React from 'react';
import DatePicker from 'react-datepicker';
import { SHORT_DATE_FORMAT } from '~constants';
import { IRowInputProps } from '.';
import RowInputButton from './RowInputButton';

const RowDateInput = ({ listItem, field, onChange }: IRowInputProps) => {
    const fieldValue = listItem.attendance.list_data[field.slug] || null;
    const [loading, setLoading] = React.useState(false);

    const handleChange = (date: Date | null) => {
        setLoading(true);
        const v = date === null ? null : date.toISOString();
        onChange(listItem.attendance.id, { [field.slug]: v })
            .then(() => setLoading(false))
            .catch(e => {
                console.log(e);
                setLoading(false);
            });
    };

    return (
        <DatePicker
            selected={fieldValue ? parseISO(fieldValue) : new Date()}
            onChange={handleChange}
            popperPlacement="bottom"
            customInput={
                <RowInputButton hasValue={!!fieldValue} loading={loading} onClear={() => handleChange(null)}>
                    {fieldValue ? format(parseJSON(fieldValue), SHORT_DATE_FORMAT) : '-'}
                </RowInputButton>
            }
        />
    );
};

export default RowDateInput;
