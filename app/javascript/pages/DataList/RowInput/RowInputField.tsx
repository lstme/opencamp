import cx from 'classnames';
import * as React from 'react';
import Input from '../../../atoms/Input';
import { ENTER_KEY_CODE } from '../../../constants';

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
    hasValue?: boolean;
    loading?: boolean;
    onClear?: () => void;
    // return success or fail of update / validation
    onValue?: (value: string | null) => Promise<boolean>;
    value?: string;
}

const RowInputField = React.forwardRef<HTMLInputElement, IProps>(
    ({ className, hasValue, onClear, onValue, value: inValue, ...props }: IProps, ref) => {
        const [value, setValue] = React.useState<string>(inValue || '');
        const [hasError, setHasError] = React.useState(false);

        const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
            setHasError(false);
            setValue(e.target.value);
        };

        const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
            setHasError(false);
            if (onValue) {
                onValue(value).then(res => {
                    setHasError(!res);
                });
            }
        };

        const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
            if (e.keyCode === ENTER_KEY_CODE) (e.target as HTMLInputElement).blur();
        };

        React.useEffect(() => {
            setValue(inValue || '');
            setHasError(false);
        }, [inValue]);

        return (
            <Input
                className={cx(
                    'text-center text-gray-800 placeholder-gray-400 rounded-none',
                    'w-full flex-1 p-0 h-8 leading-none focus:outline-none',
                    className,
                    {
                        'hover:text-primary focus:text-gray-800': !hasError,
                        'bg-red-100 border-red-700 hover:border-red-700 text-red-700 hover:text-red-700': hasError
                    }
                )}
                {...props}
                value={value}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                onBlur={handleBlur}
                ref={ref}
            />
        );
    }
);

export default RowInputField;
