import * as React from 'react';
import cx from 'classnames';

interface IProps extends React.HTMLAttributes<HTMLButtonElement> {
    hasValue?: boolean;
    onClear?: () => void;
    onClick?: () => void;
    loading?: boolean;
}

const RowInputButton = React.forwardRef<HTMLButtonElement, IProps>(
    ({ className, children, hasValue, loading, onClear, onClick }: IProps, ref) => {
        const timeout = React.useRef<number>();

        React.useEffect(() => {
            const scrollHandler = () => {
                window.clearTimeout(timeout.current);
                timeout.current = undefined;
            };
            window.addEventListener('scroll', scrollHandler);
            return () => {
                window.removeEventListener('scroll', scrollHandler);
            };
        });

        const handleClick = (e: React.MouseEvent) => {
            console.log('handleClick');
            onClick && onClick();
        };
        const handleContextMenu = (e: React.MouseEvent) => {
            onClear && onClear();
            e.preventDefault();
            e.stopPropagation();
        };

        // Mobile handlers
        const handleTouchStart = () => {
            if (onClear) {
                timeout.current = window.setTimeout(() => {
                    window.clearTimeout(timeout.current);
                    timeout.current = undefined;
                    return onClear();
                }, 1000);
            }
        };
        const handleTouchEnd = (e: React.TouchEvent<HTMLButtonElement>) => {
            if (onClick && timeout.current) onClick();
            timeout.current && window.clearTimeout(timeout.current);
            e.preventDefault();
        };

        return (
            <button
                className={cx(
                    'w-full flex-1 p-0 h-8 leading-none hover:text-primary focus:outline-none hover:bg-gray-100',
                    {
                        'text-gray-400': !hasValue,
                        'text-gray-800': hasValue,
                        'btn-loading': loading
                    },
                    className
                    // 'button button-sm bg-gray-200 text-gray-700 w-full flex-1 rounded-none text-xs p-0 h-8 leading-none',
                    // { 'button-green bg-green-200 text-gray-700': hasValue }
                )}
                onClick={handleClick}
                onContextMenu={handleContextMenu}
                onTouchStart={handleTouchStart}
                onTouchEnd={handleTouchEnd}
                // {...props}
                ref={ref}
                tabIndex={-1}
            >
                {children}
            </button>
        );
    }
);

export default RowInputButton;
