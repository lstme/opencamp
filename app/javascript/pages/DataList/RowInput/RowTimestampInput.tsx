import { parseJSON } from 'date-fns';
import format from 'lib/format';
import * as React from 'react';
import { LONG_TIME_FORMAT } from '~constants';

import { IRowInputProps } from '.';
import RowInputButton from './RowInputButton';

const RowTimestampInput = ({ field, listItem, onChange }: IRowInputProps) => {
    const value = listItem.attendance.list_data[field.slug] || false;
    const [loading, setLoading] = React.useState(false);

    const setValue = (value: Date | null) => {
        const v = value === null ? null : value.toISOString();
        setLoading(true);
        onChange(listItem.attendance.id, { [field.slug]: v })
            .then(() => setLoading(false))
            .catch(e => {
                console.log(e);
                setLoading(false);
            });
    };

    // Desktop handlers
    const handleClick = () => {
        if (!value) setValue(new Date());
    };
    const handleClear = () => setValue(null);

    return (
        <RowInputButton hasValue={!!value} onClick={handleClick} onClear={handleClear} loading={loading}>
            {value ? format(parseJSON(value), LONG_TIME_FORMAT) : '--:--'}
        </RowInputButton>
    );
};

export default RowTimestampInput;
