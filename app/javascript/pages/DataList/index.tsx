import Avatar from 'atom/Avatar';
import ErrorDisplay from 'atom/ErrorDisplay';
import { PageLoadingIndicator } from 'atom/LoadingIndicator';
import cx from 'classnames';
import { get } from 'helpers/get';
import NotFound from 'molecule/NotFound';
import * as React from 'react';
import { useParams } from 'react-router';
import PageContainer from 'template/PageContainer';
import {
    DataListQueryResult,
    useDataListQuery,
    useListChangedSubscription,
    useUpdateAttendancesListDataMutation
} from '~graphql.generated';
import RowInput from './RowInput';

type TItem = NonNullable<NonNullable<NonNullable<DataListQueryResult['data']>['list']>['data']>[0];
type TUserColumn = 'avatar' | 'name' | 'age' | 'room' | 'workgroup';

const DataList = () => {
    const id = useParams<{ id: string }>().id ?? '';
    const [updateListData] = useUpdateAttendancesListDataMutation();
    const { data, loading, error } = useDataListQuery({ variables: { id } });
    useListChangedSubscription({ variables: { id } });
    const list = data && data.list;

    if (loading) return <PageLoadingIndicator />;
    if (error) return <ErrorDisplay error={error} />;
    if (!list) return <NotFound>List not found</NotFound>;

    const listDataResults = get(list, 'data') || [];

    const handleChange = (attendanceId: string, listData: { [key: string]: any }) =>
        updateListData({
            variables: {
                ids: [attendanceId],
                listId: list.id,
                listData
            }
        });

    const userColumns: TUserColumn[] = [];
    if (list.show_avatar) userColumns.push('avatar');
    if (list.show_name) userColumns.push('name');
    if (list.show_age) userColumns.push('age');
    if (list.show_room) userColumns.push('room');
    if (list.show_workgroup) userColumns.push('workgroup');

    const whoContent = (item: TItem, col: TUserColumn) => {
        const user = item.attendance.user;
        const type = item.attendance.__typename;

        switch (col) {
            case 'age':
                return user.age;
            case 'avatar':
                return (
                    <Avatar className="w-8 h-8 rounded-full mr-2 object-cover" avatar={user.avatar} variant="icon" />
                );
            case 'name':
                return (
                    <span
                        className={cx({
                            'text-instructor': type === 'InstructorAttendance',
                            'text-child': type === 'ChildAttendance'
                        })}
                    >{`${user.last_name} ${user.first_name}`}</span>
                );
            case 'room':
                return get(item.attendance, 'room', 'name');
            case 'workgroup':
                return get(item.attendance, 'workgroup', 'name');
        }
    };

    return (
        <PageContainer fluid>
            <div>
                <table className="w-full select-none">
                    <thead>
                        <tr className="border-b">
                            <th className="px-6 py-4 font-normal text-left" colSpan={userColumns.length}>
                                {list.name}
                            </th>
                            <th className="py-4 text-sm text-gray-600">
                                <div className="flex justify-end items-center">
                                    {list.fields.map(field => (
                                        <div key={field.id} className="flex-1">
                                            {field.name}
                                        </div>
                                    ))}
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {listDataResults.map(listItem => (
                            <React.Fragment key={listItem.id}>
                                <tr className="hover:bg-gray-100">
                                    {userColumns.map(col => (
                                        <td key={col} className="px-6 pt-2 border-gray-300 border-b">
                                            {whoContent(listItem, col)}
                                        </td>
                                    ))}
                                    <td className="p-0 border-gray-300 border-b">
                                        <div className="flex items-center">
                                            {list.fields.map((f, i) => (
                                                <div
                                                    className="flex-1 flex items-stretch justify-center border border-transparent hover:border-primary"
                                                    key={i}
                                                >
                                                    <RowInput listItem={listItem} onChange={handleChange} field={f} />
                                                </div>
                                            ))}
                                        </div>
                                    </td>
                                </tr>
                            </React.Fragment>
                        ))}
                    </tbody>
                </table>
            </div>
        </PageContainer>
    );
};

export default DataList;
