import ErrorDisplay from 'atom/ErrorDisplay';
import cx from 'classnames';
import { pluralize } from 'helpers';
import Attendance from 'molecule/Attendance';
import { useSelectedTermId } from 'organism/AppContext';
import AttendanceDetailModal, { AttendanceDetailModalHandle } from 'organism/AttendanceDetailModal';
import { PeopleListSkeleton } from 'page/People/PeopleList';
import * as React from 'react';
import Modal from 'react-bootstrap/Modal';
import { FaCoins, FaPlus, FaSearch } from 'react-icons/fa';
import PageContainer from 'template/PageContainer';
import {
    AttendanceDetailDocument,
    AttendanceDetailQuery,
    useCoinsAttendancesQuery,
    useCreateTransactionsMutation
} from '~graphql.generated';
import CoinsTransactionForm, { ICoinsTransactionFormValue } from './CoinsTransactionForm';

type TFilter = 'all' | 'selected';

interface IProps {}

const Coins = ({}: IProps) => {
    const termId = useSelectedTermId();
    const [createTransactions] = useCreateTransactionsMutation();
    const { data, loading, error } = useCoinsAttendancesQuery({ variables: { termId } });
    const [selectedMap, setSelected] = React.useState<{
        [key: string]: boolean;
    }>({});
    const [activeFilter, setActiveFilter] = React.useState<TFilter>('all');
    const modalRef = React.useRef<AttendanceDetailModalHandle>(null);
    const selectedIds = Object.keys(selectedMap).filter(k => selectedMap[k]);
    const [showModal, setShowModal] = React.useState(false);
    const [filter, setFilter] = React.useState('');

    React.useEffect(() => {
        if (!showModal) setFilter(''); // Reset filter when closing modal
    }, [showModal]);

    const allAttendances = data?.term?.attendances ?? [];
    const selectedAttendances = allAttendances.filter(att => selectedMap[att.id] === true);

    let visibleAttendances = [...allAttendances];
    if (filter) {
        visibleAttendances = visibleAttendances.filter(a =>
            a.user.search_name.toLowerCase().match(filter.toLowerCase())
        );
    }
    visibleAttendances = visibleAttendances.sort((a, b) => a.user.search_name.localeCompare(b.user.search_name));

    if (activeFilter === 'selected') {
        visibleAttendances = visibleAttendances.filter(a => selectedMap[a.id]);
    }

    const handleSubmit = (value: ICoinsTransactionFormValue) =>
        createTransactions({
            variables: {
                amount: value.amount,
                note: value.note,
                attendanceIds: selectedIds
            },
            update: (proxy, res) => {
                if (!res.data) return;

                // Update any loaded attendance detail transactions list
                res.data.create_transactions.forEach(newTransaction => {
                    const attId = newTransaction.attendance.id;
                    const qSelector = {
                        query: AttendanceDetailDocument,
                        variables: { id: attId }
                    };

                    let attendanceData: AttendanceDetailQuery | null = null;
                    try {
                        attendanceData = proxy.readQuery(qSelector);
                    } catch {
                        return;
                    }

                    if (!attendanceData?.attendance) return;

                    proxy.writeQuery({
                        ...qSelector,
                        data: {
                            ...attendanceData,
                            attendance: {
                                ...attendanceData.attendance,
                                transactions: [...attendanceData.attendance.transactions, newTransaction]
                            }
                        }
                    });
                });
            }
        }).then(() => setShowModal(false));

    const handleActiveFilter = (filter: TFilter) => {
        setFilter('');
        setActiveFilter(filter);
    };

    const pills = [
        {
            key: 'all' as TFilter,
            title: `All`,
            count: allAttendances.length
        },
        {
            key: 'selected' as TFilter,
            title: `Selected`,
            count: selectedIds.length
        }
    ];

    const showAttendance = (id: string) => {
        modalRef.current?.open(id);
    };

    return (
        <>
            <AttendanceDetailModal ref={modalRef} />

            <PageContainer>
                {showModal && (
                    <Modal show onHide={() => setShowModal(false)}>
                        <CoinsTransactionForm attendances={selectedAttendances} onSubmit={handleSubmit} />
                    </Modal>
                )}
                <div className="card">
                    <div className="card-header">
                        <input
                            className="form-control"
                            type="text"
                            placeholder="Filter"
                            value={filter}
                            onChange={e => setFilter(e.target.value)}
                            autoFocus
                        />
                        <button
                            className="btn btn-primary ms-4"
                            disabled={selectedIds.length === 0}
                            onClick={() => setShowModal(true)}
                        >
                            <FaPlus className="icon" />{' '}
                            {selectedIds.length === 0
                                ? 'Select attendances'
                                : `Create ${selectedIds.length} ${pluralize('transaction', selectedIds.length)}`}
                        </button>
                    </div>

                    {error && (
                        <div className="card-body">
                            <ErrorDisplay error={error} />
                        </div>
                    )}

                    {/* Filter pills */}
                    <div className="navbar-nav">
                        <ul className="nav nav-bordered border-bottom-0 ps-3">
                            {pills.map(pill => (
                                <li key={pill.key} className="nav-item">
                                    <a
                                        className={cx('nav-link cursor-pointer', {
                                            active: activeFilter === pill.key
                                        })}
                                        onClick={() => handleActiveFilter(pill.key)}
                                    >
                                        {pill.title}
                                        <span className="badge bg-primary-lt badge-pill ms-2">{pill.count}</span>
                                    </a>
                                </li>
                            ))}
                        </ul>
                    </div>

                    {loading && <PeopleListSkeleton />}

                    {/* Attendances table */}
                    <div className="list-group list-group-flush">
                        {visibleAttendances.map(att => (
                            <div
                                className={cx('list-group-item cursor-pointer', {
                                    active: selectedMap[att.id]
                                })}
                                key={att.id}
                                onClick={e => {
                                    if ((e.target as HTMLElement).nodeName === 'I') return;
                                    setSelected({
                                        ...selectedMap,
                                        [att.id]: !selectedMap[att.id]
                                    });
                                }}
                            >
                                <div className="row align-items-center">
                                    <div className="col">
                                        <Attendance attendance={att} />
                                    </div>
                                    <div className="text-end col-auto">
                                        <FaCoins className="icon text-coin" />
                                        <span className="ms-2">{att.coins}</span>
                                        {att.wallet_id && <span className="ms-2">[{att.wallet_id}]</span>}
                                    </div>
                                    <div className="text-end col-2 col-sm-1">
                                        <button
                                            className="btn btn-ghost-secondary btn-icon"
                                            onClick={e => {
                                                showAttendance(att.id);
                                                e.stopPropagation();
                                            }}
                                        >
                                            <FaSearch className="icon" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </PageContainer>
        </>
    );
};

export default Coins;
