import { ApolloError } from 'apollo-client';
import Attendance from 'molecule/Attendance';
import ScrollWithShadow from 'molecule/ScrollWithShadow';
import * as React from 'react';
import { AttendanceFragment } from '~graphql.generated';
import ErrorDisplay from 'atom/ErrorDisplay';

export interface ICoinsTransactionFormValue {
    amount: number;
    note: string;
}

interface IProps {
    attendances: AttendanceFragment[];
    onSubmit: (value: ICoinsTransactionFormValue) => Promise<any>;
}

const CoinsTransactionForm = ({ attendances, onSubmit }: IProps) => {
    const [error, setError] = React.useState<ApolloError | undefined>(undefined);
    const [saving, setSaving] = React.useState(false);
    const [state, setState] = React.useState<ICoinsTransactionFormValue>({
        amount: 0,
        note: ''
    });

    const handleSubmit = () => {
        setError(undefined);
        setSaving(true);
        onSubmit(state).catch(e => {
            setSaving(false);
            setError(e);
        });
    };

    return (
        <>
            <div className="modal-header">
                <div className="modal-title">{`Transaction for ${attendances.length} attendances`}</div>
            </div>

            {error && (
                <div className="modal-body">
                    <ErrorDisplay className="mb-0" error={error} />
                </div>
            )}

            <div className="modal-body">
                <label htmlFor="amount" className="form-label">
                    Amount
                </label>
                <input
                    className="form-control"
                    type="number"
                    pattern="-?\d*"
                    value={state.amount === 0 ? '' : state.amount.toString()}
                    onChange={e =>
                        setState({
                            ...state,
                            amount: Number(e.target.value)
                        })
                    }
                    autoFocus
                />
                <label htmlFor="note" className="form-label mt-2">
                    Note
                </label>
                <input
                    className="form-control"
                    value={state.note}
                    onChange={e => setState({ ...state, note: e.target.value })}
                />

                <label htmlFor="note" className="form-label mt-2">
                    Attendances <div className="badge bg-primary-lt">{attendances.length}</div>
                </label>

                {/* 460 is about the size of other stuff on screen with single-line error */}
                <ScrollWithShadow style={{ maxHeight: 'calc(100vh - 460px)' }}>
                    <div className="list-group">
                        {attendances.map(att => (
                            <Attendance className="list-group-item" attendance={att} />
                        ))}
                    </div>
                </ScrollWithShadow>
            </div>

            <div className="modal-footer">
                <button className="btn btn-primary" disabled={saving} onClick={handleSubmit}>
                    Create {attendances.length} transactions
                </button>
            </div>
        </>
    );
};

export default CoinsTransactionForm;
