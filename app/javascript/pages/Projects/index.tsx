import ErrorDisplay from 'atom/ErrorDisplay';
import { useIsInstructor, useProjectSignupEnabled, useSelectedTermId } from 'organism/AppContext';
import { useState } from 'react';
import BoxList, { BoxListSkeleton } from 'template/BoxList';
import PageContainer from 'template/PageContainer';
import {
    ProjectsDocument,
    ProjectsQuery,
    ProjectsQueryVariables,
    useCreateProjectAttendanceMutation,
    useDeleteProjectAttendanceMutation,
    useMeQuery,
    useProjectsQuery
} from '~graphql.generated';
import Project from './Project';
import ProjectsSchedule from './ProjectsSchedule';
import { useSignupOrders } from './useSignupOrders';

const Projects = () => {
    const { data: meData } = useMeQuery();
    const me = meData?.me;
    const projectSignupEnabled = useProjectSignupEnabled();
    const termId = useSelectedTermId();
    const { data, loading, error } = useProjectsQuery({ variables: { termId }, pollInterval: 5000 });
    const projects = data?.term?.projects ?? [];

    const [showSchedule, setShowSchedule] = useState(false);

    const orders = useSignupOrders(projects);

    const [createAttendance] = useCreateProjectAttendanceMutation({
        update: (cache, res, { variables }) => {
            const newAtt = res.data?.create_project_attendance;
            if (newAtt) {
                const cacheData = cache.readQuery<ProjectsQuery, ProjectsQueryVariables>({
                    query: ProjectsDocument,
                    variables: { termId }
                });
                const term = cacheData?.term;
                if (!term) return;

                const newCacheData: ProjectsQuery = {
                    ...cacheData,
                    term: {
                        ...term,
                        projects:
                            cacheData.term?.projects.map(p => {
                                if (p.id === variables?.projectId) {
                                    return { ...p, project_attendances: [...p.project_attendances, newAtt] };
                                } else {
                                    return p;
                                }
                            }) ?? []
                    }
                };

                cache.writeQuery({
                    query: ProjectsDocument,
                    variables: { termId },
                    data: newCacheData
                });
            }
        }
    });
    const [deleteProjectAttendance] = useDeleteProjectAttendanceMutation({
        update: (cache, res, { variables }) => {
            const removedAtt = res.data?.delete_project_attendance;
            if (removedAtt) {
                const cacheData = cache.readQuery<ProjectsQuery, ProjectsQueryVariables>({
                    query: ProjectsDocument,
                    variables: { termId }
                });
                const term = cacheData?.term;
                if (!term) return;

                const newCacheData: ProjectsQuery = {
                    ...cacheData,
                    term: {
                        ...term,
                        projects:
                            cacheData.term?.projects.map(p => {
                                if (p.id === variables?.projectId) {
                                    return {
                                        ...p,
                                        project_attendances: p.project_attendances.filter(a => a.id !== removedAtt.id)
                                    };
                                } else {
                                    return p;
                                }
                            }) ?? []
                    }
                };

                cache.writeQuery({
                    query: ProjectsDocument,
                    variables: { termId },
                    data: newCacheData
                });
            }
        }
    });

    const handleCreate = (projectId: string) => {
        return createAttendance({ variables: { projectId } });
    };

    const handleDelete = (projectId: string) => {
        return deleteProjectAttendance({ variables: { projectId } });
    };

    if (error) return <ErrorDisplay error={error} />;
    if (loading)
        return (
            <PageContainer>
                <BoxListSkeleton />
            </PageContainer>
        );

    if (!data?.term || !me) return null;

    return (
        <PageContainer fluid={showSchedule}>
            <div className="page-header mt-0 mb-4">
                <div className="row align-items-center">
                    <div className="col-auto ms-auto d-flex">
                        <div className="me-3 d-none d-md-block"></div>
                        <button className="btn" onClick={() => setShowSchedule(v => !v)}>
                            {showSchedule ? 'Zoznam' : `Rozvrh`}
                        </button>
                    </div>
                </div>
            </div>

            {showSchedule ? (
                <ProjectsSchedule projects={projects} orders={orders} />
            ) : (
                <BoxList>
                    {projects.map(project => (
                        <BoxList.Box key={project.id}>
                            <Project
                                project={project}
                                currentUserId={me?.id}
                                onAdd={projectSignupEnabled ? () => handleCreate(project.id) : undefined}
                                onRemove={projectSignupEnabled ? () => handleDelete(project.id) : undefined}
                                orders={orders}
                            />
                        </BoxList.Box>
                    ))}
                </BoxList>
            )}
        </PageContainer>
    );
};

export default Projects;
