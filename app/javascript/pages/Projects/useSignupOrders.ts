import { ProjectsQuery } from '~graphql.generated';

type Project = NonNullable<ProjectsQuery['term']>['projects'];
type Attendance = Project[0]['project_attendances'][0];

interface Res {
    [attendanceId: string]: number;
}

export const useSignupOrders = (project: NonNullable<ProjectsQuery['term']>['projects']): Res => {
    const res: Res = {};

    const userSignups: { [userId: string]: Attendance[] } = {};
    project.forEach(p => {
        p.project_attendances.forEach(a => {
            userSignups[a.attendance.user.id] ||= [];
            userSignups[a.attendance.user.id].push(a);
        });
    });

    Object.entries(userSignups).forEach(([userId, attendances]) => {
        attendances.sort((a, b) => new Date(a.created_at).getTime() - new Date(b.created_at).getTime());
        userSignups[userId] = [];
        attendances.forEach((a, i) => {
            res[a.id] = i;
        });
    });

    return res;
};
