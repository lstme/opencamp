import Avatar from 'atom/Avatar';
import cx from 'classnames';
import SimpleAttendance from 'molecule/SimpleAttendance';
import { useIsInstructor } from 'organism/AppContext';
import * as React from 'react';
import { FaPlus } from 'react-icons/fa';
import { ProjectFragment } from '~graphql.generated';

interface IProps {
    project: ProjectFragment;
    currentUserId?: string;
    onRemove?: () => Promise<any>;
    onAdd?: () => Promise<any>;
    orders: Record<string, number>;
}

const Project: React.FC<IProps> = ({ project, currentUserId, onAdd, onRemove, orders }) => {
    const isInstructor = useIsInstructor();
    const attendances_count = project.project_attendances.length;
    const meAttendance = project.project_attendances.find(a => a.attendance.user.id === currentUserId);

    const sortedAttendances = React.useMemo(() => {
        return [...project.project_attendances].sort((a, b) => orders[a.id] - orders[b.id]);
    }, [project.project_attendances]);

    return (
        <div className="card tw-h-full">
            <div className={cx('card-header')}>
                <div className="card-title">
                    {project.project_alias}{' '}
                    <span className="badge bg-info-lt">
                        {attendances_count} / {project.capacity}
                    </span>
                </div>
                <div className="position-absolute col-auto end-0 me-3">{project.category}</div>
            </div>
            <div className="list-group-header">{project.title}</div>
            <div className={'px-3 py-2 text-instructor'}>
                <div className="row gx-0 align-items-center">
                    <div className="col-auto">
                        <Avatar avatar={project.teacher.avatar} variant="icon" size="sm" />
                    </div>
                    <div className={cx('col-auto px-2 text-instructor')}>{project.teacher.full_name}</div>
                </div>
            </div>
            <div className="border-top list-group-header">
                Popis
                <span className="tw-float-right tw-font-bold">SLOTY: {project.slots}</span>
            </div>
            <div className="px-3">
                <div className="row align-items-center">
                    <div className="col py-3">{project.description}</div>
                </div>
            </div>
            {project.prerequisites && (
                <>
                    <div className="border-top list-group-header">Požiadavky</div>
                    <div className="px-3">
                        <div className="row align-items-center">
                            <div className="col py-3">{project.prerequisites}</div>
                        </div>
                    </div>
                </>
            )}
            <div className="border-top">
                {sortedAttendances.map(attendance => (
                    <SimpleAttendance
                        key={attendance.id}
                        className={cx('px-3 py-2', { 'bg-primary-lt': attendance === meAttendance })}
                        attendance={attendance.attendance}
                        onRemove={attendance === meAttendance ? onRemove : undefined}
                        accessory={
                            (isInstructor || attendance.attendance.user.id == currentUserId) &&
                            orders?.[attendance.id] != null ? (
                                <span className="badge bg-info-lt tw-mr-3">{orders[attendance.id] + 1}</span>
                            ) : undefined
                        }
                    />
                ))}
                {onAdd != null && meAttendance == null && (
                    <div className="px-3 py-1 tw-h-[58px] tw-flex tw-items-center tw-justify-center">
                        <button className={cx('btn w-100 btn-ghost-success')} onClick={onAdd}>
                            <FaPlus className="icon" />
                            Prihlásiť sa
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Project;
