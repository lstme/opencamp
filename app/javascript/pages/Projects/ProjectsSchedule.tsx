import cx from 'classnames';
import { useSelectedTermId } from 'organism/AppContext';
import { useEffect, useMemo, useState } from 'react';
import { ProjectFragment, useAttendanceSelectUsersQuery } from '~graphql.generated';

interface IProps {
    projects: ProjectFragment[];
    orders: Record<string, number>;
}

const COLORS = [
    '#f87171',
    '#fb923c',
    '#fbbf24',
    '#facc15',
    '#a3e635',
    '#4ade80',
    '#34d399',
    '#2dd4bf',
    '#22d3ee',
    '#38bdf8',
    '#60a5fa',
    '#818cf8',
    '#a78bfa',
    '#c084fc',
    '#e879f9',
    '#f472b6',
    '#fb7185',
    '#fecaca',
    '#fed7aa',
    '#fde68a',
    '#fef08a',
    '#d9f99d',
    '#bbf7d0',
    '#a7f3d0',
    '#99f6e4',
    '#a5f3fc',
    '#bae6fd',
    '#bfdbfe',
    '#c7d2fe',
    '#ddd6fe',
    '#e9d5ff',
    '#f5d0fe',
    '#fbcfe8',
    '#fecdd3'
];

interface IPlacement {
    room: string;
    slot: string;
    project: string;
}

const uniq = (arr: any[]) => Array.from(new Set(arr));

const ProjectsSchedule: React.FC<IProps> = ({ projects, orders }) => {
    const termId = useSelectedTermId();
    const { data } = useAttendanceSelectUsersQuery({ variables: { termId } });

    const [showOut, setShowOut] = useState(false);
    const pendingAttendances = data?.term?.attendances.filter(a => a.__typename == 'ChildAttendance') ?? [];

    const projectLookup = useMemo(
        () =>
            projects.reduce<Record<string, ProjectFragment>>((acc, p) => {
                acc[p.project_alias] = p;
                return acc;
            }, {}),
        [projects]
    );

    useEffect(() => {
        fetch('/documents/projects_schedule')
            .then(res => res.json())
            .then(data => {
                setRooms(data.rooms);
                setSlots(data.slots);
                setPlacements(data.placements);
            });
    }, []);

    const [selectedProject, setSelectedProject] = useState<string | null>(null);

    // key is room_slot, value is project id
    const [placements, setPlacements] = useState<IPlacement[]>([]);
    const [rooms, setRooms] = useState<string[]>([]);
    const [slots, setSlots] = useState<string[]>([]);
    const placedProjects = uniq(placements.map(p => p.project));

    const handlePlaceProject = (project: string, slot: string, room: string) => {
        setPlacements([...placements, { project, slot, room }]);
    };

    const handleDeleteProject = (project: string, slot: string, room: string) => {
        setPlacements(placements.filter(p => p.project !== project || p.slot !== slot || p.room !== room));
    };

    const pendingProjects = projects.flatMap(p => new Array(p.slots).fill(p.project_alias));

    const projectColors = placedProjects.reduce<Record<string, string>>((acc, key, i) => {
        acc[key] = COLORS[i % COLORS.length];
        return acc;
    }, {});

    placements.forEach(p => {
        pendingProjects.splice(pendingProjects.indexOf(p.project), 1);
    });

    const projectsBySlot = projects.reduce<Record<string, ProjectFragment[]>>((acc, project) => {
        placements
            .filter(p => p.project === project.project_alias)
            .forEach(placement => {
                const slot = placement.slot;
                if (!slot) return acc;
                if (!acc[slot]) acc[slot] = [];
                acc[slot].push(project);
            });
        return acc;
    }, {});

    const attendancesBySlot = slots.map(slot => {
        const projects = projectsBySlot[slot] ?? [];
        const attendances: Record<string, { prio: number; user: any }> = {};
        let i = 0;
        let changed = 1;
        while (changed > 0) {
            changed = 0;
            projects.forEach(project => {
                const sorted = [...project.project_attendances].sort((a, b) => orders[a.id] - orders[b.id]);
                sorted.forEach((attendance, i) => {
                    const userId = attendance.attendance.user.id;
                    const prevAttendance = attendances[userId];
                    const remainingCapacity =
                        project.capacity -
                        sorted.slice(0, i).filter(a => attendances[a.attendance.user.id].prio === orders[a.id]).length;
                    const order = remainingCapacity > 0 ? orders[attendance.id] : 1000;
                    if (prevAttendance == null || order < prevAttendance.prio) {
                        attendances[userId] = {
                            prio: order,
                            user: attendance.attendance.user.full_name
                        };
                        changed++;
                    }
                });
            });

            i++;
            if (i > 10000) {
                console.error('Infinite loop');
                break;
            }
        }
        return attendances;
    });

    const pendingAttendancesBySlot = slots.map((slot, i) => {
        const atts = attendancesBySlot[i];
        return pendingAttendances
            .filter(a => atts[a.user.id] == null || atts[a.user.id].prio > 100)
            .sort((a, b) => a.user.full_name.localeCompare(b.user.full_name));
    });

    return (
        <div className="card">
            <div className="card-header">
                <h3 className="card-title tw-flex-1">Projects Schedule</h3>
                <button className="btn btn-sm btn-primary" onClick={() => setShowOut(v => !v)}>
                    {showOut ? 'Hide' : 'Show'} purple
                </button>
                <button
                    className="btn btn-sm btn-primary"
                    onClick={() => navigator.clipboard.writeText(JSON.stringify({ rooms, slots, placements }))}
                >
                    Copy data
                </button>
            </div>
            <div className="tw-flex tw-flex-wrap tw-gap-2 tw-p-2">
                {pendingProjects.sort().map((p, i) => (
                    <div key={i} className="">
                        <ProjectTag
                            project={projectLookup[p]}
                            key={p.id}
                            onClick={() => setSelectedProject(p)}
                            active={p === selectedProject}
                        />
                    </div>
                ))}
            </div>
            <div className="table-responsive"></div>
            <table className="table table-striped table-vcenter card-table">
                <thead>
                    <tr>
                        <th></th>
                        {slots.map(slot => (
                            <th key={slot} className="tw-text-center">
                                {slot}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {rooms.map(room => (
                        <tr key={room}>
                            <td>{room}</td>
                            {slots.map((slot, i) => {
                                const projectKey = placements.find(p => p.slot === slot && p.room === room)?.project;
                                const project = projectLookup[projectKey!];
                                const slotAttendances = attendancesBySlot[i] ?? {};
                                const attendances = [...(project?.project_attendances ?? [])].sort(
                                    (a, b) => orders[a.id] - orders[b.id]
                                );
                                const visibleAttendances = attendances.filter(a => {
                                    if (showOut) return true;
                                    return orders[a.id] === slotAttendances[a.attendance.user.id]?.prio;
                                });
                                return (
                                    <td key={slot} style={{ verticalAlign: 'top' }}>
                                        {project != null ? (
                                            <div>
                                                <div className="tags-list">
                                                    <ProjectTag
                                                        className={cx('w-full')}
                                                        project={project}
                                                        color={projectColors[project.project_alias]}
                                                        active={projectKey === selectedProject}
                                                        onClick={() => setSelectedProject(project.project_alias)}
                                                        onDelete={() =>
                                                            handleDeleteProject(project.project_alias, slot, room)
                                                        }
                                                        count={visibleAttendances.length}
                                                    />
                                                </div>
                                                {visibleAttendances.map(attendance => {
                                                    const out =
                                                        orders[attendance.id] !==
                                                        slotAttendances[attendance.attendance.user.id]?.prio;
                                                    const prio = orders[attendance.id];
                                                    if (out && !showOut) return null;
                                                    return (
                                                        <div
                                                            key={attendance.id}
                                                            className={cx('tw-px-2 tw-flex tw-justify-between', {
                                                                'tw-bg-purple-500': out,
                                                                'tw-bg-green-500': !out && prio < 3,
                                                                'tw-bg-orange-500': !out && prio < 6 && prio >= 3,
                                                                'tw-bg-red-500': !out && prio >= 6
                                                            })}
                                                        >
                                                            {attendance.attendance.user.full_name}
                                                            <span className="tag tw-ml-4">{orders[attendance.id]}</span>
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                        ) : selectedProject != null ? (
                                            <button onClick={() => handlePlaceProject(selectedProject, slot, room)}>
                                                HERE
                                            </button>
                                        ) : null}
                                    </td>
                                );
                            })}
                        </tr>
                    ))}
                    <tr>
                        <td>NO PROJECT</td>
                        {slots.map((slot, i) => (
                            <td key={slot} style={{ verticalAlign: 'top' }}>
                                {pendingAttendancesBySlot[i].map(a => (
                                    <div key={a.user.id}>{a.user.full_name}</div>
                                ))}
                            </td>
                        ))}
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

const ProjectTag: React.FC<{
    className?: string;
    project: ProjectFragment;
    onDelete?: () => void;
    onClick?: () => void;
    active?: boolean;
    color?: string;
    count?: number;
}> = ({ project, className, onDelete, onClick, active, color, count }) => {
    return (
        <div
            className={cx(
                'tw-flex tw-justify-between tw-items-center tw-w-full tw-m-0 tw-rounded-t-md tw-py-1 tw-px-2 tw-border-1 tw-border-solid',
                className,
                {
                    'tw-border-blue-500 ': active,
                    'tw-opacity-30': project.project_attendances.length === 0
                }
            )}
            onClick={onClick}
            style={color ? { backgroundColor: color } : undefined}
        >
            <div>
                <span className="tw-font-bold">
                    {project.project_alias} -{' '}
                    <span className="badge">
                        {count ?? project.project_attendances.length}/{project.capacity}
                    </span>
                </span>
                <br />
                <span>{project.teacher.full_name}</span>
            </div>
            {onDelete && (
                <a
                    href="#"
                    className="btn-close"
                    onClick={e => {
                        e.preventDefault();
                        onDelete();
                    }}
                ></a>
            )}
        </div>
    );
};

export default ProjectsSchedule;
