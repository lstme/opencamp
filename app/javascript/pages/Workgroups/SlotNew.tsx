import cx from 'classnames';
import { FaPlus } from 'react-icons/fa';
import AttendanceSelect from '../../organisms/AttendanceSelect';

interface IProps {
    onAttendanceSelect: (attendanceId: string) => Promise<any>;
}

const SlotNew = ({ onAttendanceSelect }: IProps) => (
    <AttendanceSelect
        onChange={onAttendanceSelect}
        baseFilter={a => a.__typename === 'ChildAttendance'}
        topTitle="Without workgroup"
        topFilter={a => a.workgroup == null}
    >
        {({ setOpen }) => (
            <div className="px-3 py-1">
                <button className={cx('btn w-100 btn-sm btn-ghost-success')} onClick={() => setOpen(true)}>
                    <FaPlus className="icon" />
                    Add person
                </button>
            </div>
        )}
    </AttendanceSelect>
);

export default SlotNew;
