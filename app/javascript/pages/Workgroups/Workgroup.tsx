import WorkgroupNote from 'organism/WorkgroupNote';
import { WorkgroupFragment } from '~graphql.generated';
import Slot from './Slot';
import SlotNew from './SlotNew';

interface IProps {
    readonly?: boolean;
    workgroup: WorkgroupFragment;
    onAttendanceClick?: (attendanceId: string, workgroupId: string) => void;
    onAttendanceSelect: (attendanceId: string, workgroupId: string) => Promise<any>;
    onAttendanceKick: (attendanceId: string) => Promise<any>;
}

const Workgroup = ({ readonly, workgroup, onAttendanceClick, onAttendanceSelect, onAttendanceKick }: IProps) => {
    const { id, name, attendances_count, attendances, note } = workgroup;
    const handleUserSelect = (attendanceId: string) => onAttendanceSelect(attendanceId, id);

    return (
        <div className="card">
            <div className="card-header">
                <div className="card-title">
                    {name} <span className="badge bg-info-lt">{attendances_count}</span>
                </div>
            </div>
            <WorkgroupNote workgroupId={id} note={note} readonly={readonly} />
            <div className="border-top">
                {attendances.map(attendance => (
                    <Slot
                        key={attendance.id}
                        attendance={attendance}
                        onClick={onAttendanceClick ? () => onAttendanceClick(attendance.id, id) : undefined}
                        onAttendanceKick={readonly ? undefined : onAttendanceKick}
                    />
                ))}
                {!readonly && <SlotNew onAttendanceSelect={handleUserSelect} />}
            </div>
        </div>
    );
};

export default Workgroup;
