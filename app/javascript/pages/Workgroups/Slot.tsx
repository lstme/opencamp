import classNames from 'classnames';
import Attendance from 'molecule/Attendance';
import { AttendanceFragment } from '~graphql.generated';

interface IProps {
    attendance: AttendanceFragment;
    onAttendanceKick?: (attendanceId: string) => Promise<any>;
    onClick?: () => void;
}

const Slot = ({ attendance, onAttendanceKick, onClick }: IProps) => {
    const handleClick = onAttendanceKick == null ? onClick : undefined;
    return (
        <Attendance
            className={classNames('px-3 py-2', { 'list-group-item cursor-pointer': handleClick != null })}
            attendance={attendance}
            onClick={handleClick}
            onRemove={onAttendanceKick}
            details={['gender', 'age', 'room']}
        />
    );
};

export default Slot;
