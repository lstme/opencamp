import ErrorDisplay from 'atom/ErrorDisplay';
import { useIsInstructor, useSelectedTermId } from 'organism/AppContext';
import AttendanceDetailModal, { AttendanceDetailModalHandle } from 'organism/AttendanceDetailModal';
import { useMemo, useRef, useState } from 'react';
import BoxList, { BoxListSkeleton } from 'template/BoxList';
import PageContainer from 'template/PageContainer';
import { useChangeAttendanceWorkgroupMutation, useTermWorkgroupsQuery } from '~graphql.generated';
import Slot from './Slot';
import Workgroup from './Workgroup';

const Workgroups = () => {
    const isInstructor = useIsInstructor();
    const termId = useSelectedTermId();
    const { data, loading, error } = useTermWorkgroupsQuery({ variables: { termId } });
    const modalRef = useRef<AttendanceDetailModalHandle>(null);
    const showAttendance = (id: string) => {
        modalRef.current?.open(id);
    };

    const [editing, setEditing] = useState(false);

    const attendances = useMemo(() => {
        if (data?.term == null) return [];
        return data.term.attendances.filter(a => a.__typename === 'ChildAttendance');
    }, [data]);

    const [changeAttendance] = useChangeAttendanceWorkgroupMutation();
    const changeAttendanceWorkgroupId = (attendanceId: string, workgroupId: string | null) => {
        if (!data || !data.term) return Promise.reject(new Error('Data not loaded yet'));

        return changeAttendance({
            variables: {
                id: attendanceId,
                workgroupId: workgroupId
            }
        });
    };

    const handleAttendanceSelect = (attendanceId: string, workgroupId: string) =>
        changeAttendanceWorkgroupId(attendanceId, workgroupId);
    const handleAttendanceKick = (attendanceId: string) => changeAttendanceWorkgroupId(attendanceId, null);

    if (error) return <ErrorDisplay error={error} />;
    if (loading)
        return (
            <PageContainer>
                <BoxListSkeleton />
            </PageContainer>
        );

    if (!data?.term) return null;

    const unassigned = attendances.filter(a => a.workgroup == null);
    const workgroups = data.term.workgroups;

    return (
        <PageContainer>
            <AttendanceDetailModal ref={modalRef} />

            {isInstructor && (
                <div className="page-header mt-0 mb-4">
                    <div className="row align-items-center">
                        <div className="col-auto ms-auto">
                            <button className="btn" onClick={() => setEditing(v => !v)}>
                                {editing ? 'View' : `Edit${unassigned.length > 0 ? ` (${unassigned.length} unassigned)` : ''}`}
                            </button>
                        </div>
                    </div>
                </div>
            )}

            <BoxList
                leading={
                    editing && unassigned.length > 0 && (
                        <div className="card">
                            <div className="card-header">
                                <div className="row align-items-center">
                                    <div className="col-auto">
                                        <div className="card-title">
                                            No workgroup{' '}
                                            <span className="badge bg-info-lt">
                                                {unassigned.length} / {attendances.length}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                {unassigned.map(a => (
                                    <Slot key={a.id} attendance={a} />
                                ))}
                            </div>
                        </div>
                    )
                }
            >
                {workgroups.map(workgroup => (
                    <BoxList.Box key={workgroup.id}>
                        <Workgroup
                            readonly={!editing}
                            workgroup={workgroup}
                            onAttendanceClick={id => showAttendance(id)}
                            onAttendanceSelect={handleAttendanceSelect}
                            onAttendanceKick={handleAttendanceKick}
                        />
                    </BoxList.Box>
                ))}
            </BoxList>
        </PageContainer>
    );
};

export default Workgroups;
