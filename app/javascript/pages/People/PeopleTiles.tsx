import Avatar from 'atom/Avatar';
import { groupBy } from 'lodash';
import * as React from 'react';
import { AttendanceFragment } from '~graphql.generated';
import { PEOPLE_LIST_TITLE } from './PeopleList';

interface IProps {
    people: AttendanceFragment[];
    onSelect: (id: string) => void;
}

const PeopleTiles = ({ people, onSelect }: IProps) => {
    const groups = groupBy(people, att => att.__typename);

    return (
        <div className="list-group list-group-flush">
            {Object.keys(groups).map(typename => (
                <React.Fragment key={typename}>
                    <div className="list-group-header sticky-top">{PEOPLE_LIST_TITLE[typename]}</div>
                    <div className="row row-cards py-2">
                        {groups[typename].map(att => (
                            <div key={att.id} className="col-6 col-sm-4 col-lg-3">
                                <div className="card card-sm">
                                    <Avatar
                                        onClick={() => onSelect(att.id)}
                                        omitBaseClassname
                                        size="xl"
                                        avatar={att.user.avatar}
                                        className="card-img-top"
                                        style={{ paddingTop: '100%' }}
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </React.Fragment>
            ))}
        </div>
    );
};

export default PeopleTiles;
