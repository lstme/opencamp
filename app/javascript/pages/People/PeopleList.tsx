import Attendance from 'molecule/Attendance';
import * as React from 'react';
import { AttendanceFragment } from '~graphql.generated';

interface IProps {
    people: AttendanceFragment[];
    onSelect: (id: string) => void;
}

export const PEOPLE_LIST_TITLE: { [key: string]: string } = {
    ChildAttendance: 'Students',
    InstructorAttendance: 'Instructors'
};

const PeopleList = ({ people, onSelect }: IProps) => (
    <div className="list-group list-group-flush">
        {people.map((att, i) => (
            <React.Fragment key={att.id}>
                {people[i - 1]?.__typename !== att.__typename && (
                    <div className="list-group-header sticky-top">{PEOPLE_LIST_TITLE[att.__typename ?? '']}</div>
                )}
                <div className="list-group-item cursor-pointer" onClick={() => onSelect(att.id)}>
                    <Attendance attendance={att} avatarSize="default" />
                </div>
            </React.Fragment>
        ))}
    </div>
);

export const PeopleListSkeleton = () => (
    <ul className="list-group list-group-flush placeholder-glow">
        {new Array(10).fill(1).map((_, i) => (
            <li key={i} className="list-group-item">
                <div className="row align-items-center">
                    <div className="col-auto">
                        <div className="avatar placeholder"></div>
                    </div>
                    <div className="col-9">
                        <div className="placeholder placeholder-xs col-9"></div>
                        <div className="placeholder placeholder-xs col-7"></div>
                    </div>
                </div>
            </li>
        ))}
    </ul>
);

export default PeopleList;
