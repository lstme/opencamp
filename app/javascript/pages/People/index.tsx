import ErrorDisplay from 'atom/ErrorDisplay';
import cx from 'classnames';
import { useSelectedTermId } from 'organism/AppContext';
import AttendanceDetailModal, { AttendanceDetailModalHandle } from 'organism/AttendanceDetailModal';
import * as React from 'react';
import { FaList, FaThLarge } from 'react-icons/fa';
import PageContainer from 'template/PageContainer';
import { usePeopleQuery } from '~graphql.generated';
import PeopleList, { PeopleListSkeleton } from './PeopleList';
import PeopleTiles from './PeopleTiles';

type TPeopleDisplayForm = 'list' | 'tiles';

const People = () => {
    const termId = useSelectedTermId();
    const [displayForm, setDisplayForm] = React.useState<TPeopleDisplayForm>('list');
    const modalRef = React.useRef<AttendanceDetailModalHandle>(null);
    const { data, loading, error } = usePeopleQuery({ variables: { termId } });

    const [filter, setFilter] = React.useState('');

    let visible = (data && data.term && data.term.attendances) || [];
    if (filter) {
        const needle = filter.toLowerCase();
        visible = visible.filter(a => a.user.search_name.toLowerCase().match(needle));
    }
    visible = [...visible].sort((a, b) =>
        a.__typename === b.__typename
            ? a.user.search_name.localeCompare(b.user.search_name)
            : a.__typename === 'ChildAttendance'
            ? -1
            : 1
    );

    const showAttendance = (id: string) => {
        modalRef.current?.open(id);
    };

    return (
        <>
            <AttendanceDetailModal ref={modalRef} />
            <PageContainer>
                <div className="card">
                    <div className="card-body py-3">
                        <div className="d-flex">
                            <div className="w-100 me-2">
                                <input
                                    type="text"
                                    className="form-control"
                                    aria-label="Filter"
                                    placeholder="Filter"
                                    value={filter}
                                    onChange={e => setFilter(e.target.value)}
                                />
                            </div>
                            <div className="ms-auto">
                                <div className="btn-group">
                                    <button
                                        className={cx('btn btn-icon', { 'btn-primary': displayForm === 'list' })}
                                        onClick={() => setDisplayForm('list')}
                                    >
                                        <FaList className="icon" />
                                    </button>
                                    <button
                                        className={cx('btn btn-icon', { 'btn-primary': displayForm === 'tiles' })}
                                        onClick={() => setDisplayForm('tiles')}
                                    >
                                        <FaThLarge className="icon" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ErrorDisplay error={error} />
                    {loading ? (
                        <PeopleListSkeleton />
                    ) : displayForm === 'list' ? (
                        <PeopleList people={visible} onSelect={showAttendance} />
                    ) : (
                        <PeopleTiles people={visible} onSelect={showAttendance} />
                    )}
                </div>
            </PageContainer>
        </>
    );
};

export default People;
