import cx from 'classnames';

export interface NavbarLinkData {
    to?: string;
    icon?: string;
    title?: string | null;
    active?: boolean;
    variant?: 'divider' | 'link';
    items?: NavbarLinkData[];
}

interface NavbarData {
    site_logo?: string;
    site_title: string;
    menu: NavbarLinkData[];
    user_menu?: UserMenuProps;
}

interface IProps {
    data: NavbarData;
}

const Navbar = ({ data }: IProps) => (
    <div className="mb-3">
        <header className="navbar navbar-expand-md navbar-light d-print-none">
            <div className="container-xl">
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbar-menu"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <h1 className="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
                    <a href=".">
                        {data.site_logo && (
                            <img
                                src={data.site_logo}
                                width="32"
                                height="32"
                                alt="Tabler"
                                className="navbar-brand-image"
                            />
                        )}
                        <span className={cx({ 'ps-2': !!data.site_logo })}>{data.site_title}</span>
                    </a>
                </h1>
                {data.user_menu && <UserMenu {...data.user_menu} />}
                <div className="collapse navbar-collapse" id="navbar-menu">
                    <div className="d-flex flex-column flex-md-row flex-fill align-items-stretch align-items-md-center">
                        <ul className="navbar-nav">
                            {data.menu.map((menuItem, i) => (
                                <NavbarLink key={i} linkData={menuItem} />
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
);

interface NavbarLinkProps {
    linkData: NavbarLinkData;
    level?: number;
    className?: string;
}

const NavbarLink = ({ linkData: { icon, to, active, title }, className }: NavbarLinkProps) => (
    <li className={cx('nav-item', { active }, className)}>
        <a className="nav-link" href={to}>
            {icon && (
                <span className="nav-link-icon d-md-none d-lg-inline-block">
                    <i className={cx(icon, 'icon')} />
                </span>
            )}
            <span className="nav-link-title">{title}</span>
            {/* <span className="badge badge-sm bg-red">2</span> */}
        </a>
    </li>
);

interface UserMenuProps {
    user_avatar?: string;
    user_name: string;
    user_role: string;
    items: NavbarLinkData[];
}

const UserMenu = ({ user_avatar, user_name, user_role, items }: UserMenuProps) => (
    <div className="navbar-nav flex-row order-md-last">
        <div className="nav-item dropdown">
            <a
                className="nav-link d-flex lh-1 text-reset p-0 cursor-pointer"
                data-bs-toggle="dropdown"
                aria-label="Open user menu"
            >
                <span
                    className="avatar avatar-sm"
                    style={{ backgroundImage: user_avatar ? `url('${user_avatar}')` : undefined }}
                />
                <div className="d-none d-xl-block ps-2">
                    <div>{user_name}</div>
                    <div className="mt-1 small text-muted">{user_role}</div>
                </div>
            </a>
            <div className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                {items.map((item, i) =>
                    item.variant === 'divider' ? (
                        <div key={i} className="dropdown-divider"></div>
                    ) : (
                        <a key={i} href={item.to} className={cx('dropdown-item', { active: item.active })}>
                            {/* {item.icon && <i className={cx('icon dropdown-item-icon', item.icon)} />} */}
                            {item.title}
                        </a>
                    )
                )}
            </div>
        </div>
    </div>
);

export default Navbar;
