import { genNumArray } from 'helpers';
import format from 'lib/format';
import React from 'react';
import { ROOM_SCORE_DATE_FORMAT } from '~constants';
import { useRoomScoresQuery } from '~graphql.generated';
import ErrorDisplay from '../../atoms/ErrorDisplay';

interface IProps {
    id: string;
}

const RoomScores = ({ id }: IProps) => {
    const { data, loading, error } = useRoomScoresQuery({ variables: { id } });
    const room = data && data.room;

    return (
        <React.Fragment>
            <div className="modal-header">{room && room.name}</div>
            {loading && <TableSkeleton />}
            {error && (
                <div className="modal-body">
                    <ErrorDisplay error={error} />
                </div>
            )}
            {room && (
                <table className="table table-vcenter card-table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        {room.room_scores.map(score => (
                            <tr key={score.id} className="px-6 py-2 flex justify-between">
                                <td>{format(new Date(score.date), ROOM_SCORE_DATE_FORMAT)}</td>
                                <td>{score.score}</td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total</th>
                            <th>{room.score}</th>
                        </tr>
                    </tfoot>
                </table>
            )}
        </React.Fragment>
    );
};

const TableSkeleton = () => (
    <table className="table card-table placeholder-glow">
        <thead>
            <tr>
                <th>Date</th>
                <th>Score</th>
            </tr>
        </thead>
        <tbody>
            {genNumArray(4).map((_, i) => (
                <tr key={i}>
                    <td>
                        <div className="placeholder placeholder-xs col-6"></div>
                    </td>
                    <td>
                        <div className="placeholder placeholder-xs col-2"></div>
                    </td>
                </tr>
            ))}
        </tbody>
        <tfoot>
            <tr>
                <th>Total</th>
                <th>
                    <div className="placeholder placeholder-xs col-3"></div>
                </th>
            </tr>
        </tfoot>
    </table>
);

export default RoomScores;
