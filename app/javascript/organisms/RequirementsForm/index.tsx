import cx from 'classnames';
import { useEffect, useState } from 'react';
import {
    FindAttendanceQuery,
    OnboardingRequirementFragment,
    useUpdateAttendanceOnboardingRequirementsMutation
} from '~graphql.generated';
import TextField from 'atom/TextField';

interface IProps {
    requirements: OnboardingRequirementFragment[];
    attendance: FindAttendanceQuery['find_attendance'];
    onComplete?: () => void;
}

const RequirementsForm = ({ requirements, attendance, onComplete }: IProps) => {
    const [update] = useUpdateAttendanceOnboardingRequirementsMutation();

    const aors = attendance?.attendance_onboarding_requirements ?? [];

    const handleToggle = (requirement_id: string) => {
        let aorsInput = aors.map(a => ({ onboarding_requirement_id: a.onboarding_requirement_id, data: a.data }));

        const aor = aors.find(a => a.onboarding_requirement_id === requirement_id);
        if (aor == null) {
            aorsInput.push({ onboarding_requirement_id: requirement_id, data: {} });
        } else {
            aorsInput = aorsInput.filter(a => a.onboarding_requirement_id !== requirement_id);
        }

        update({
            variables: {
                attendanceId: attendance?.id!,
                onboarding_requirements: aorsInput
            },
            optimisticResponse: attendance
                ? {
                      __typename: 'RootMutation',
                      update_attendance_onboarding_requirements: {
                          __typename: 'ChildAttendance',
                          ...attendance,
                          attendance_onboarding_requirements: aorsInput.map(a => ({
                              __typename: 'AttendanceOnboardingRequirement',
                              onboarding_requirement_id: a.onboarding_requirement_id,
                              data: a.data
                          }))
                      }
                  }
                : undefined
        });
    };

    const handleDataChange = (key: string, newData: Record<string, string>) => {
        const aorsInput = aors.map(a => {
            if (a.onboarding_requirement_id === key) {
                return { onboarding_requirement_id: key, data: newData };
            }
            return { onboarding_requirement_id: a.onboarding_requirement_id, data: a.data };
        });

        // Optimistic response solves race condition when user blurs by clicking on another requirement
        // which would trigger the other update with old data and override this request
        update({
            variables: {
                attendanceId: attendance?.id!,
                onboarding_requirements: aorsInput
            },
            optimisticResponse: attendance
                ? {
                      __typename: 'RootMutation',
                      update_attendance_onboarding_requirements: {
                          __typename: 'ChildAttendance',
                          ...attendance,
                          attendance_onboarding_requirements: aorsInput.map(a => ({
                              __typename: 'AttendanceOnboardingRequirement',
                              onboarding_requirement_id: a.onboarding_requirement_id,
                              data: a.data
                          }))
                      }
                  }
                : undefined
        });
    };

    const handleComplete = () => {
        const allChecked = requirements.length === aors.length;
        if (!allChecked) {
            const res = confirm('Are you sure you want to complete this onboarding? Not all requirements are checked.');
            if (!res) return;
        }
        onComplete?.();
    };

    return (
        <div className="card">
            <div className="card-header">
                <div className="card-title">Requirements</div>
            </div>
            <div className="list-group list-group-flush">
                {requirements.map(requirement => {
                    const aor = aors.find(r => r.onboarding_requirement_id === requirement.id);
                    const checked = aor != null;
                    return (
                        <div
                            key={requirement.id}
                            className={cx('list-group-item cursor-pointer', { active: checked })}
                            onClick={() => handleToggle(requirement.id)}
                        >
                            <div className="row align-items-center">
                                <div className="col-auto">
                                    <input
                                        type="checkbox"
                                        className="form-check-input"
                                        checked={checked}
                                        onChange={e => {
                                            handleToggle(requirement.id);
                                            e.stopPropagation();
                                        }}
                                    />
                                </div>
                                <div className="col">
                                    <div className="text-reset d-block">{requirement.title}</div>
                                    <div
                                        className="d-block text-muted mt-n1"
                                        dangerouslySetInnerHTML={{ __html: requirement.description }}
                                    />
                                    {requirement.inputs.length > 0 && (
                                        <div className="text-muted mt-2" onClick={e => e.stopPropagation()}>
                                            {requirement.inputs.map(input => (
                                                <div key={input.key} className="mt-1">
                                                    <RequirementInput
                                                        input={input}
                                                        onChange={newValue =>
                                                            handleDataChange(requirement.id, {
                                                                ...aor?.data,
                                                                [input.key]: newValue
                                                            })
                                                        }
                                                        disabled={!checked}
                                                        value={aor?.data[input.key] ?? ''}
                                                    />
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
            {onComplete && (
                <div className="card-footer text-end">
                    <button className="btn btn-primary" onClick={handleComplete}>
                        Complete onboarding
                    </button>
                </div>
            )}
        </div>
    );
};

const RequirementInput = ({
    input,
    value,
    disabled,
    onChange
}: {
    input: OnboardingRequirementFragment['inputs'][number];
    value: string;
    disabled?: boolean;
    onChange: (newValue: string) => void;
}) => {
    const [val, setVal] = useState(value);
    useEffect(() => {
        setVal(value);
    }, [value]);
    return (
        <TextField
            disabled={disabled}
            label={input.label}
            value={val}
            onChange={e => setVal(e.target.value)}
            onBlur={e => {
                if (val === value) return;
                onChange(val ?? '');
            }}
        />
    );
};

export default RequirementsForm;
