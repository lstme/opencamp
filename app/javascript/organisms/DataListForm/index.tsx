import cx from 'classnames';
import { CreateDataListMutationVariables, ListSortByEnum, useCreateDataListMutation } from '~graphql.generated';
import Checkbox from '../../atoms/Checkbox';
import ErrorDisplay from '../../atoms/ErrorDisplay';
import Field from '../../atoms/Field';
import Input from '../../atoms/Input';
// import ModalBox from '../../atoms/ModalBox';
import PlusButton from '../../atoms/PlusButton';
import Radio from '../../atoms/Radio';
import { slugify } from '../../helpers';
import useFormReducer from '../../lib/useFormReducer';

export enum EColumnType {
    'Timestamp' = 'timestamp', // button when pressed will store current timestamp
    'Date' = 'date', // date picker
    'Time' = 'time', // time picker
    'Timer' = 'timer', // shows timer that starts and ends when tapped with something for resetting
    'Number' = 'number', // input with +- buttons
    'Checkbox' = 'checkbox', // checkbox
    'Text' = 'string' // text input
}

interface IProps {
    id?: string | null;
}

type IField = CreateDataListMutationVariables['input']['fields'][0];
type IFormState = CreateDataListMutationVariables['input'];

const INITIAL_FIELD: IField = {
    name: '',
    settings: {},
    slug: '',
    type: ''
};

const INITIAL_STATE: IFormState = {
    term_id: '',
    name: '',
    slug: '',
    include_children: true,
    include_instructors: false,
    show_age: false,
    show_avatar: false,
    show_name: true,
    show_room: false,
    show_workgroup: false,
    sort_by: ListSortByEnum.Name,
    fields: [INITIAL_FIELD]
};

const DataListForm = ({}: IProps) => {
    const [createDataList, mutation] = useCreateDataListMutation();
    const { state, onChange } = useFormReducer<IFormState>(INITIAL_STATE);

    const onFieldChange = (idx: number, changes: Partial<IField>) => {
        const field = state.fields[idx];
        const newFields = state.fields.slice(0);
        newFields.splice(idx, 1, { ...field, ...changes });
        return onChange('fields', newFields);
    };

    const onFieldRemove = (idx: number) => {
        const newFields = state.fields.slice(0);
        newFields.splice(idx, 1);
        return onChange('fields', newFields);
    };

    const handleSave = () => createDataList({ variables: { input: state } });

    const checkbox = (key: keyof IFormState, label: string, className?: string) => (
        <Checkbox
            key={key}
            checked={!!state[key] || false}
            onChange={value => onChange(key, value)}
            label={label}
            className={className}
        />
    );

    return (
        <div>
            {mutation.error && <ErrorDisplay error={mutation.error} />}
            <fieldset className="px-6 pt-4">
                <Input
                    className="w-full"
                    hint={`Slug: ${state.slug}`}
                    id="name"
                    label="List name"
                    value={state.name}
                    onChange={e => {
                        onChange('name', e.target.value);
                        onChange('slug', slugify(e.target.value));
                    }}
                />
            </fieldset>
            <fieldset className="px-6 pt-4">
                <label className="label block">Include</label>
                <div className="flex">
                    {checkbox('include_children', 'Children', 'flex-1')}
                    {checkbox('include_instructors', 'Instructors', 'flex-1')}
                </div>
            </fieldset>
            <div className="flex px-6 pt-4">
                <fieldset className="flex-1">
                    <label className="label block">Display</label>
                    {checkbox('show_avatar', 'Avatar')}
                    {checkbox('show_name', 'Name')}
                    {checkbox('show_age', 'Age')}
                    {checkbox('show_room', 'Room')}
                    {checkbox('show_workgroup', 'Workgroup')}
                </fieldset>
                <fieldset className="flex-1">
                    <label className="label block">Default sort</label>
                    {Object.keys(ListSortByEnum).map(k => (
                        <Radio
                            key={ListSortByEnum[k as keyof typeof ListSortByEnum]}
                            checked={state.sort_by === ListSortByEnum[k as keyof typeof ListSortByEnum]}
                            onChange={() => onChange('sort_by', ListSortByEnum[k as keyof typeof ListSortByEnum])}
                            label={k}
                        />
                    ))}
                </fieldset>
            </div>
            <div className="px-6 py-4 mt-4 border-t">
                <label className="label block">Fields</label>
                {state.fields.map((field, i) => (
                    <Field className="mb-4" hint={`Slug: ${field.slug}`} key={i}>
                        <select
                            className="input self-stretch flex-shrink-0 rounded-r-none text-sm"
                            onChange={e =>
                                onFieldChange(i, {
                                    type: e.target.value as EColumnType
                                })
                            }
                        >
                            {Object.keys(EColumnType).map(k => (
                                <option
                                    key={EColumnType[k as keyof typeof EColumnType]}
                                    value={EColumnType[k as keyof typeof EColumnType]}
                                >
                                    {k}
                                </option>
                            ))}
                        </select>
                        <input
                            className="input flex-grow rounded-l-none"
                            value={field.name}
                            onChange={e =>
                                onFieldChange(i, {
                                    name: e.target.value,
                                    slug: slugify(e.target.value)
                                })
                            }
                        />
                        <button
                            className="button button-red-outline button-rounded text-xs ml-2"
                            onClick={() => onFieldRemove(i)}
                        >
                            <i className="fa fa-times icon" />
                        </button>
                    </Field>
                ))}
                <PlusButton onClick={() => onChange('fields', [...state.fields, INITIAL_FIELD])} variant="green" />
            </div>
            <div className="mt-4 px-6 py-4 border-t">
                <button
                    className={cx('btn btn-primary w-100', { 'btn-loading': mutation.loading })}
                    onClick={handleSave}
                >
                    Save
                </button>
            </div>
            <pre className="bg-gray-200 text-xs" style={{ position: 'fixed', top: 0, left: 0 }}>
                {JSON.stringify(state, null, 2)}
                <hr />
                {JSON.stringify(mutation, null, 2)}
            </pre>
        </div>
    );
};

export default DataListForm;
