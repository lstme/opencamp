import ErrorDisplay from 'atom/ErrorDisplay';
import Message from 'atom/Message';
import React, { forwardRef, useEffect, useImperativeHandle, useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import {
    AttendanceDetailDocument,
    AttendanceDetailQuery,
    useAttendanceDetailQuery,
    useCreateTransactionsMutation,
    useDeleteTransactionsMutation
} from '~graphql.generated';
import AttendanceDetail, { AttendanceDetailSkeleton } from './AttendanceDetail';

interface IProps {
    onClose?: () => void;
}

export interface AttendanceDetailModalHandle {
    open: (attendanceId: string) => void;
    close: () => void;
}

// Modal component that will open when instructor click on user
const AttendanceDetailModal = forwardRef<AttendanceDetailModalHandle, IProps>(({ onClose }, ref) => {
    const [attendanceId, setAttendanceId] = useState<string>();
    const [show, setShow] = useState(attendanceId != null);

    useImperativeHandle(ref, () => ({
        open: (attendanceId: string) => {
            setAttendanceId(attendanceId);
            setShow(true);
        },
        close: () => {
            setShow(false);
        }
    }));

    const { data, error, loading } = useAttendanceDetailQuery({
        variables: { id: attendanceId! },
        skip: !attendanceId
    });
    const [createTransaction] = useCreateTransactionsMutation();
    const [deleteTransaction] = useDeleteTransactionsMutation();

    const handleSubmit = (amount: number, note: string) => {
        if (attendanceId == null) return Promise.reject(new Error('No attendance selected'));
        return createTransaction({
            variables: {
                amount,
                attendanceIds: [attendanceId],
                note
            },
            update: (proxy, res) => {
                const qSelector = {
                    query: AttendanceDetailDocument,
                    variables: { id: attendanceId }
                };
                const data = proxy.readQuery(qSelector) as AttendanceDetailQuery;
                if (!data.attendance) return;

                const newTransactions = [...data.attendance.transactions!, ...res.data!.create_transactions];

                proxy.writeQuery({
                    ...qSelector,
                    data: {
                        ...data,
                        attendance: {
                            ...data.attendance,
                            transactions: newTransactions
                        }
                    }
                });
            }
        });
    };

    const handleDelete = (transactionId: string) =>
        deleteTransaction({
            variables: { ids: [transactionId] },
            update: proxy => {
                const qSelector = {
                    query: AttendanceDetailDocument,
                    variables: { id: attendanceId }
                };
                const data = proxy.readQuery(qSelector) as AttendanceDetailQuery;
                if (!data.attendance) return;

                const newTransactions = data.attendance.transactions!.filter(t => t.id !== transactionId);

                proxy.writeQuery({
                    ...qSelector,
                    data: {
                        ...data,
                        attendance: {
                            ...data.attendance,
                            transactions: newTransactions
                        }
                    }
                });
            }
        });

    const modalContent = () => {
        if (!attendanceId) return null;

        if (loading) return <AttendanceDetailSkeleton />;
        if (error) return <ErrorDisplay error={error} />;

        if (!data || !data.attendance) {
            return <Message message="Attendance not found" variant="alert" />;
        }

        return (
            <>
                <AttendanceDetail
                    attendance={data.attendance}
                    onClose={onClose}
                    onTransactionCreate={handleSubmit}
                    onTransactionDelete={handleDelete}
                />
            </>
        );
    };

    return (
        <Modal
            show={show}
            onHide={() => setShow(false)}
            onExited={() => {
                setAttendanceId(undefined);
                onClose?.();
            }}
            contentClassName="div"
        >
            {modalContent()}
        </Modal>
    );
});

export default AttendanceDetailModal;
