import Avatar from 'atom/Avatar';
import Message from 'atom/Message';
import cx from 'classnames';
import { useScrollStatus } from 'lib/useScrollStatus';
import ScrollWithShadow from 'molecule/ScrollWithShadow';
import TransactionsTimeline from 'molecule/TransactionsTimeline';
import TransactionForm, { ITransactionFormValue } from 'molecule/TransactionsTimeline/TransactionForm';
import * as React from 'react';
import { FaCoins } from 'react-icons/fa';
import { AttendanceDetailQuery } from '~graphql.generated';

interface IProps {
    attendance: AttendanceDetailQuery['attendance'];
    onClose?: () => void;
    onTransactionCreate?: (amount: number, note: string) => Promise<any>;
    onTransactionDelete?: (transactionId: string) => Promise<any>;
}

const AttendanceDetail = ({ attendance, onClose, onTransactionCreate, onTransactionDelete }: IProps) => {
    const [newTransaction, setNewTransaction] = React.useState<ITransactionFormValue>({ amount: 0, note: '' });
    const { ref, scrollable, atTop, atBottom } = useScrollStatus();

    const handleSubmit = onTransactionCreate
        ? () =>
              onTransactionCreate(newTransaction.amount, newTransaction.note).then(() =>
                  setNewTransaction({ amount: 0, note: '' })
              )
        : undefined;

    if (attendance == null) return null;

    const { user, room, transactions } = attendance;

    return (
        <React.Fragment>
            <div className="modal-header">
                <div className="modal-title">
                    {user.first_name} {user.last_name}
                    {onClose && <button className="btn-close" onClick={onClose} />}
                </div>
            </div>

            <div className="modal-body">
                <div className="row align-items-center">
                    <div className="col-auto">
                        <Avatar avatar={user.avatar} size="xl" />
                    </div>
                    <div className="col space-y-1">
                        <div className="fw-bold">
                            {user.first_name} {user.last_name}
                        </div>
                        <div>
                            {user.gender_symbol} {user.age}r.
                        </div>
                        <div>
                            <FaCoins className="icon text-coin" />
                            <span className="ms-2">{attendance.coins}</span>
                            {attendance.wallet_id && <span className="ms-2">[{attendance.wallet_id}]</span>}
                        </div>
                        <div>
                            <i className="fa fa-bed icon" />
                            <span className="ms-2">{room ? room.name : 'No room'}</span>
                        </div>
                    </div>
                </div>
                {handleSubmit && (
                    <div className="mt-4">
                        <TransactionForm onChange={setNewTransaction} onSubmit={handleSubmit} value={newTransaction} />
                    </div>
                )}
            </div>

            <ScrollWithShadow style={{ maxHeight: '20rem' }}>
                {transactions!.length > 0 ? (
                    <TransactionsTimeline
                        className="list-group-flush"
                        onDelete={onTransactionDelete}
                        transactions={transactions!}
                    />
                ) : (
                    <div className="modal-body pt-0">
                        <Message className="mb-0" message="No transactions" variant="info" />
                    </div>
                )}
            </ScrollWithShadow>
        </React.Fragment>
    );
};

export const AttendanceDetailSkeleton = () => (
    <React.Fragment>
        <div className="modal-header placeholder-glow">
            <div className="placeholder placeholder-xs col-9"></div>
        </div>

        <div className="modal-body placeholder-glow">
            <div className="row align-items-center">
                <div className="col-auto">
                    <div className="avatar avatar-xl placeholder" />
                </div>
                <div className="col space-y-1">
                    <div className="fw-bold">
                        <div className="placeholder placeholder-xs col-4"></div>
                    </div>
                    <div>
                        <div className="placeholder placeholder-xs col-6"></div>
                    </div>
                    <div>
                        <div className="placeholder placeholder-xs col-5"></div>
                    </div>
                    <div>
                        <div className="placeholder placeholder-xs col-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
);

export default AttendanceDetail;
