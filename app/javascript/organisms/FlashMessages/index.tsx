import Message from 'atom/Message';
import * as React from 'react';

const FlashMessages = () => {
    const [messages, setMessages] = React.useState<any[]>(() => {
        try {
            const flashMessagesElement = document.querySelector('#flash');
            if (!flashMessagesElement) return [];
            const messagesData = flashMessagesElement.getAttribute('data-messages');
            if (!messagesData) return [];
            const messages = JSON.parse(messagesData);
            return messages;
        } catch (error) {
            return [];
        }
    });

    return (
        <React.Fragment>
            {messages.map((message: any, i: number) => (
                <Message
                    key={i}
                    variant={message.key}
                    message={message.value}
                    className="mb-4"
                    onClose={() => setMessages(m => m.filter((_, idx) => idx !== i))}
                />
            ))}
        </React.Fragment>
    );
};

export default FlashMessages;
