import { objectKeys } from 'organism/AttendanceImportPreview/helpers';
import * as React from 'react';

interface IProps {
    data: Record<string, [any, any]>;
    wideKeys?: string[];
}

const DiffDisplay: React.FC<IProps> = ({ data, wideKeys = [] }) => {
    const isNew = objectKeys(data).every(key => data[key][0] == null || data[key][0] == '');
    const isDelete = objectKeys(data).every(key => data[key][1] == null || data[key][1] == '');

    return (
        <React.Fragment>
            {objectKeys(data).map(key => (
                <Row
                    key={key}
                    label={key}
                    prevValue={data[key][0]}
                    value={data[key][1]}
                    isNew={isNew}
                    isDelete={isDelete}
                    wide={wideKeys.includes(key)}
                />
            ))}
        </React.Fragment>
    );
};

const Row: React.FC<{
    isDelete?: boolean;
    isNew?: boolean;
    label: string;
    prevValue: any;
    value: any;
    wide?: boolean;
}> = ({ isDelete, isNew, label, prevValue, value, wide }) => (
    <div style={{ display: 'flex', alignItems: 'flex-start', borderBottom: '1px dashed #e4eaec' }}>
        <span style={{ display: 'inline-block', minWidth: 130 }}>{label}:</span>
        {wide ? (
            <div style={{ display: 'inline-block', whiteSpace: 'pre-wrap' }}>
                {!isNew && (
                    <>
                        <span
                            style={{
                                display: 'block',
                                width: 200,
                                color: 'orange'
                            }}
                        >
                            {prevValue}
                        </span>
                        {!isDelete && <span style={{ display: 'block', width: 200 }}>&rArr;</span>}
                    </>
                )}
                <span style={{ display: 'block', width: 200 }}>{value}</span>
            </div>
        ) : (
            <>
                {!isNew && (
                    <span
                        style={{
                            color: 'orange',
                            display: 'inline-block',
                            minWidth: 200
                        }}
                    >
                        {prevValue}
                    </span>
                )}
                {!isDelete && (
                    <span style={{ display: 'inline-block', minWidth: 150 }}>
                        {!isNew && <span>&rArr; </span>}
                        {value}
                    </span>
                )}
            </>
        )}
    </div>
);

export default DiffDisplay;
