import { createRoot } from 'react-dom/client';
import DiffDisplay from './DiffDisplay';

const containers = document.querySelectorAll('[data-diff]');
containers.forEach(container => {
    const root = createRoot(container);
    const data = container.getAttribute('data-diff') ?? '{}';
    const wideKeys = container.getAttribute('data-wide-keys');
    root.render(<DiffDisplay data={JSON.parse(data)} wideKeys={wideKeys ? JSON.parse(wideKeys) : undefined} />);
});
