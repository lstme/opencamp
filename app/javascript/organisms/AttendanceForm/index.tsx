import ErrorDisplay from 'atom/ErrorDisplay';
import TextareaField from 'atom/TextareaField';
import TextField from 'atom/TextField';
import cx from 'classnames';
import { formatEuro } from 'helpers/index';
import { isEqual } from 'lodash';
import * as React from 'react';
import { AttendanceDataFragment, useUpdateAttendanceMutation } from '~graphql.generated';

interface IProps {
    attendance: AttendanceDataFragment;
    className?: string;
}

const initialValue = (a: AttendanceDataFragment) => ({
    note: a.note,
    paid_amount: a.paid_amount.toString()
});

const AttendanceForm = ({ attendance, className }: IProps) => {
    const [formData, setFormData] = React.useState<{ paid_amount: string; note: string }>(initialValue(attendance));
    const dirty = !isEqual(initialValue(attendance), formData);

    const [updateAttendance, { loading, error }] = useUpdateAttendanceMutation();

    const handleSave = () => {
        if (!formData || !dirty) return;

        return updateAttendance({
            variables: {
                attendanceId: attendance.id,
                attributes: {
                    note: formData.note,
                    paid_amount: Number(formData.paid_amount)
                }
            }
        });
    };

    const fieldProps = (id: keyof typeof formData, label: string) => ({
        id,
        className: 'mb-2',
        label,
        value: formData[id] ?? '',
        onChange: (e: { target: { value: string } }) => setFormData({ ...formData, [id]: e.target.value })
    });

    if (!formData) return null;

    const remaining = attendance.should_pay_amount - Number(formData.paid_amount);

    return (
        <form
            className={cx('card', className)}
            onSubmit={e => {
                e.preventDefault();
                handleSave();
            }}
        >
            <div className="card-header">
                <div className="card-title">Attendance details</div>
            </div>
            <div className="card-body">
                <TextField
                    {...fieldProps('paid_amount', 'Paid amount')}
                    hint={
                        <>
                            Should pay: ${formatEuro(attendance.should_pay_amount)}. Remaining:{' '}
                            <span className={cx({ 'text-danger': remaining > 0, 'text-success': remaining <= 0 })}>
                                ${formatEuro(remaining)}
                            </span>
                        </>
                    }
                />
                <TextareaField {...fieldProps('note', 'Note')} rows={8} />
                {error && <ErrorDisplay error={error} />}
            </div>
            <div className="card-footer text-end">
                <button
                    className={cx('btn btn-primary', { 'btn-loading': loading })}
                    onClick={handleSave}
                    disabled={!dirty}
                >
                    Update attendance data
                </button>
            </div>
        </form>
    );
};

export default AttendanceForm;
