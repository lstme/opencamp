import { FaBan, FaCheck, FaEdit } from 'react-icons/fa';
import * as React from 'react';
import { useChangeWorkgroupMutation } from '~graphql.generated';

interface IProps {
    readonly?: boolean;
    workgroupId: string;
    note: string;
}

const WorkgroupNote = ({ readonly, workgroupId, note }: IProps) => {
    const [edit, setEdit] = React.useState(false);
    const [value, setValue] = React.useState(note);

    const [changeWorkgroup] = useChangeWorkgroupMutation();

    const handleSubmit = () => {
        changeWorkgroup({
            variables: { workgroup_id: workgroupId, note: value },
            optimisticResponse: {
                change_workgroup: {
                    id: workgroupId,
                    note: value
                }
            }
        });
        setEdit(false);
    };
    const handleCancel = () => {
        setEdit(false);
    };

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        switch (e.key) {
            case 'Enter':
                handleSubmit();
                break;
            case 'Escape':
                handleCancel();
                break;
        }
    };

    return edit ? (
        <div className="p-2">
            <div className="input-group">
                <input
                    className="form-control"
                    type="text"
                    placeholder="Note"
                    value={value}
                    onChange={e => setValue(e.target.value)}
                    onKeyDown={handleKeyDown}
                    autoFocus
                />
                <button className="btn btn-outline-success btn-icon" onClick={handleSubmit}>
                    <FaCheck className="icon" />
                </button>
                <button className="btn btn-outline-danger btn-icon" onClick={handleCancel}>
                    <FaBan className="icon" />
                </button>
            </div>
        </div>
    ) : (
        <div className="px-3">
            <div className="row align-items-center">
                <div className="col py-3">{note || <span>-</span>}</div>
                {!readonly && (
                    <div className="col-auto">
                        <button className="btn btn-ghost-secondary btn-sm btn-icon" onClick={() => setEdit(true)}>
                            <FaEdit className="icon" />
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default WorkgroupNote;
