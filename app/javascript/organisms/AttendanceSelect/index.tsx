import { ApolloError } from 'apollo-client';
import ErrorDisplay from 'atom/ErrorDisplay';
import Attendance from 'molecule/Attendance';
import ScrollWithShadow from 'molecule/ScrollWithShadow';
import { useSelectedTermId } from 'organism/AppContext';
import * as React from 'react';
import Modal from 'react-bootstrap/Modal';
import { AttendanceFragment, useAttendanceSelectUsersQuery } from '~graphql.generated';
import { parseSearch } from '../../helpers';

interface IRenderProps {
    close: () => void;
    isOpen: boolean;
    open: () => void;
    setOpen: (open: boolean) => void;
}

interface IProps {
    children?: (props: IRenderProps) => React.ReactNode;
    onChange?: (value: string, attendance: AttendanceFragment) => Promise<any>;
    baseFilter?: (att: AttendanceFragment) => boolean;
    topFilter?: (att: AttendanceFragment) => boolean;
    topTitle?: string;
    otherTitle?: string;
}

const AttendanceSelect = ({
    children,
    onChange,
    baseFilter,
    topFilter,
    topTitle = 'Top',
    otherTitle = 'Other'
}: IProps) => {
    const termId = useSelectedTermId();
    const { data } = useAttendanceSelectUsersQuery({ variables: { termId } });

    const [isOpen, setOpen] = React.useState(false);
    const [filter, setFilter] = React.useState('');
    const [error, setError] = React.useState<ApolloError | undefined>();

    const attendances = React.useMemo(() => {
        if (!data?.term?.attendances) return [];

        let res = [...data.term.attendances].sort((a, b) => {
            if (a.__typename === b.__typename) {
                return a.user.search_name.localeCompare(b.user.search_name);
            }
            // Children first
            return a.__typename === 'ChildAttendance' ? -1 : 1;
        });

        if (baseFilter !== undefined) {
            res = res.filter(baseFilter);
        }

        return res;
    }, [data]);

    let visibleAttendances = attendances.filter(attendance =>
        parseSearch(attendance.user.search_name).match(parseSearch(filter))
    );
    let topAttendances: AttendanceFragment[] = [];

    if (topFilter != null) {
        topAttendances = visibleAttendances.filter(topFilter);
        visibleAttendances = visibleAttendances.filter(a => !topFilter(a));
    }

    const handleSelect = (attendance: AttendanceFragment) => {
        setError(undefined);
        if (onChange) {
            onChange(attendance.id!, attendance)
                .then(() => setOpen(false))
                .catch(e => setError(e));
        } else {
            setOpen(false);
        }
    };

    const renderProps = React.useMemo(
        () => ({
            close: () => setOpen(false),
            isOpen,
            open: () => setOpen(true),
            setOpen
        }),
        [isOpen]
    );

    React.useEffect(() => {
        if (!isOpen) {
            // Reset filter and error when closing modal
            setFilter('');
            setError(undefined);
        }
    }, [isOpen]);

    const handleSubmit: React.FormEventHandler<HTMLFormElement> = e => {
        e.preventDefault();
        e.stopPropagation();

        if (visibleAttendances.length === 1) {
            handleSelect(visibleAttendances[0]);
        }
    };

    return (
        <>
            {children && children(renderProps)}
            {isOpen && (
                <Modal show onHide={renderProps.close}>
                    <React.Fragment>
                        <div className="modal-header">
                            <div className="modal-title">
                                <form onSubmit={handleSubmit}>
                                    <input
                                        type="text"
                                        className="form-control"
                                        aria-label="Filter"
                                        placeholder="Filter"
                                        autoFocus
                                        value={filter}
                                        onChange={e => setFilter(e.target.value)}
                                    />
                                </form>
                            </div>
                        </div>

                        {error && (
                            <div className="card-body p-3">
                                <ErrorDisplay error={error} />
                            </div>
                        )}

                        <ScrollWithShadow className="list-group list-group-flush" style={{ maxHeight: '40rem' }}>
                            {topAttendances.length > 0 && (
                                <>
                                    <div className="list-group-header sticky-top">{topTitle}</div>
                                    {topAttendances.map(attendance => (
                                        <Attendance
                                            key={attendance.id}
                                            attendance={attendance}
                                            className="list-group-item cursor-pointer"
                                            onClick={() => handleSelect(attendance)}
                                        />
                                    ))}
                                    <div className="list-group-header sticky-top">{otherTitle}</div>
                                </>
                            )}
                            {visibleAttendances.map(attendance => (
                                <Attendance
                                    key={attendance.id}
                                    attendance={attendance}
                                    className="list-group-item cursor-pointer"
                                    onClick={() => handleSelect(attendance)}
                                />
                            ))}
                        </ScrollWithShadow>
                    </React.Fragment>
                </Modal>
            )}
        </>
    );
};

export default AttendanceSelect;
