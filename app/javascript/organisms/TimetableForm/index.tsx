import ErrorDisplay from 'atom/ErrorDisplay';
import SelectField from 'atom/SelectField';
import cx from 'classnames';
import { DocumentNode } from 'graphql';
import format from 'lib/format';
import Timetable from 'molecule/Timetable';
import TimetableInput from 'molecule/Timetable/TimetableInput';
import { useSelectedTermId } from 'organism/AppContext';
import { useCallback, useState } from 'react';
import { FaBan, FaClone, FaEye, FaPencilAlt } from 'react-icons/fa';
import { SERVER_DATE_FORMAT, TIMETABLE_DATE_FORMAT } from '~constants';
import {
    FormOptionsQuery,
    TimetableDocument,
    TimetableFragment,
    TimetableQueryVariables,
    UpdateTimetableMutationVariables,
    useFormOptionsQuery,
    useTimetableLazyQuery,
    useTimetableQuery,
    useUpdateTimetableMutation
} from '~graphql.generated';

interface IProps {
    id?: string;
    date: string;
    onClose: () => void;
}

const isComplete = (t: Partial<TimetableFragment> | TimetableFragment): t is TimetableFragment =>
    t.date != null && t.designated_head != null && t.designated_room != null && t.timeslots != null;

const TimetableForm = ({ id, date: dateString, onClose }: IProps) => {
    const date = new Date(dateString);
    const termId = useSelectedTermId();
    const [loadTimetable, loadTimetableResult] = useTimetableLazyQuery();
    const [mode, setMode] = useState<'edit' | 'preview'>('edit');

    // List for copy select
    const [otherTimetable, setOtherTimetable] = useState<NonNullable<FormOptionsQuery['term']>['timetables'][0]>();
    const formOptionsResult = useFormOptionsQuery({ variables: { termId } });
    const timetables = formOptionsResult.data?.term?.timetables ?? [];
    const designatedHeads = formOptionsResult.data?.term?.instructor_attendances ?? [];
    const designatedRooms = formOptionsResult.data?.term?.rooms ?? [];

    const [updateTimetable, updateTimetableResult] = useUpdateTimetableMutation({
        refetchQueries: [
            { query: TimetableDocument, variables: { key: dateString, termId } } as {
                query: DocumentNode;
                variables: TimetableQueryVariables;
            }
        ]
    });

    // Form data
    const [timetable, setTimetable] = useState<Partial<TimetableFragment>>({ date, timeslots: [] });

    // Load data for id passed as prop (when editing)
    useTimetableQuery({
        variables: { key: id!, termId },
        skip: !id,
        onCompleted: res => {
            if (res.term?.timetable) {
                setTimetable({ ...res.term.timetable, date });
            } else {
                console.warn(`Could not load specified timetable (key=${id})`);
            }
        }
    });

    const handleLoadTimetable = useCallback(() => {
        if (!otherTimetable) return;
        loadTimetable({ variables: { key: otherTimetable.id, termId } }).then(res => {
            if (res.data?.term?.timetable) {
                setTimetable({ ...res.data.term.timetable, date });
            } else {
                console.warn(`Could not load specified timetable (key=${otherTimetable.id})`);
            }
        });
    }, [otherTimetable]);

    const handleSubmit = () => {
        if (!timetable) return;
        if (!isComplete(timetable)) return;

        const input: UpdateTimetableMutationVariables['timetableInput'] = {
            date: format(timetable.date, SERVER_DATE_FORMAT),
            designated_head_id: timetable.designated_head.id,
            designated_room_id: timetable.designated_room.id,
            notice: timetable.notice,
            timeslots: timetable.timeslots.map(slot => ({
                is_highlighted: slot.is_highlighted,
                label: slot.label.trim(),
                time: slot.time.trim(),
                items: slot.items.map(slotItem => ({
                    label: slotItem.label,
                    title: slotItem.title
                }))
            }))
        };

        updateTimetable({
            variables: {
                termId,
                timetableId: id ?? null,
                timetableInput: input
            }
        });
    };

    const loading = loadTimetableResult.loading || updateTimetableResult.loading || formOptionsResult.loading;
    const error = loadTimetableResult.error || updateTimetableResult.error || formOptionsResult.error;

    return (
        <>
            <div className="card-header">
                <div className="card-title">{format(date, TIMETABLE_DATE_FORMAT)}</div>
                <div className="card-actions btn-actions user-select-none">
                    {mode === 'edit' && (
                        <a className="btn-action cursor-pointer" title="Preview" onClick={() => setMode('preview')}>
                            <FaEye className="icon" />
                        </a>
                    )}
                    {mode === 'preview' && (
                        <a className="btn-action cursor-pointer" title="Preview" onClick={() => setMode('edit')}>
                            <FaPencilAlt className="icon" />
                        </a>
                    )}
                    <a className="btn-action cursor-pointer" title="Cancel" onClick={onClose}>
                        <FaBan className="icon" />
                    </a>
                </div>
            </div>
            {error && (
                <div className="card-body">
                    <ErrorDisplay error={error} />
                </div>
            )}
            {mode === 'edit' && (
                <div className="card-body">
                    <div className="row align-items-end">
                        <div className="col">
                            <SelectField
                                label="Copy values from"
                                id="copyFrom"
                                items={timetables}
                                placeholder={otherTimetable == null ? 'Select date' : undefined}
                                value={otherTimetable}
                                onChange={setOtherTimetable}
                                labelGetter={t =>
                                    `${t.date} - ${t.designated_room.name} - ${t.designated_head.full_name}`
                                }
                            />
                        </div>
                        <div className="col-auto">
                            <button
                                className="btn btn-secondary"
                                disabled={otherTimetable == null}
                                onClick={handleLoadTimetable}
                            >
                                <FaClone className="icon" /> Copy
                            </button>
                        </div>
                    </div>
                    {timetable && (
                        <TimetableInput
                            value={timetable}
                            onChange={setTimetable}
                            designatedHeads={designatedHeads}
                            designatedRooms={designatedRooms}
                        />
                    )}
                </div>
            )}
            {mode === 'preview' && timetable && <Timetable timetable={timetable} />}
            <div className="card-footer">
                <div className="d-flex">
                    <a className="btn btn-link" onClick={onClose}>
                        Cancel
                    </a>
                    <a
                        className={cx('btn btn-primary ms-auto', { disabled: loading || !isComplete(timetable) })}
                        onClick={handleSubmit}
                    >
                        {loading ? 'Saving...' : 'Save'}
                    </a>
                </div>
            </div>
        </>
    );
};

export default TimetableForm;
