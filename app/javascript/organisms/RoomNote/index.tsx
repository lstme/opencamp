import * as React from 'react';
import { FaBan, FaCheck, FaEdit } from 'react-icons/fa';
import { useChangeRoomMutation } from '~graphql.generated';

interface IProps {
    roomId: string;
    note: string;
    readonly?: boolean;
}

const RoomNote = ({ roomId, note, readonly }: IProps) => {
    const [edit, setEdit] = React.useState(false);
    const [value, setValue] = React.useState(note);

    const [changeRoom] = useChangeRoomMutation();

    const handleSubmit = () => {
        changeRoom({
            variables: { room_id: roomId, note: value },
            optimisticResponse: {
                change_room: {
                    id: roomId,
                    note: value
                }
            }
        });
        setEdit(false);
    };
    const handleCancel = () => {
        setEdit(false);
    };

    const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        switch (e.key) {
            case 'Enter':
                handleSubmit();
                break;
            case 'Escape':
                handleCancel();
                break;
        }
    };

    return edit ? (
        <div className="p-2 tw-min-h-[72px] tw-flex tw-items-center">
            <div className="input-group">
                <input
                    className="form-control"
                    type="text"
                    placeholder="Note"
                    value={value}
                    onChange={e => setValue(e.target.value)}
                    onKeyDown={handleKeyDown}
                    autoFocus
                />
                <button className="btn btn-outline-success btn-icon" onClick={handleSubmit}>
                    <FaCheck className="icon" />
                </button>
                <button className="btn btn-outline-danger btn-icon" onClick={handleCancel}>
                    <FaBan className="icon" />
                </button>
            </div>
        </div>
    ) : (
        <div className="px-3">
            <div className="row align-items-center tw-min-h-[72px]">
                <div className="col py-3">{note || <span>-</span>}</div>
                {readonly !== true && (
                    <div className="col-auto">
                        <button className="btn btn-ghost-secondary btn-sm btn-icon" onClick={() => setEdit(true)}>
                            <FaEdit className="icon" />
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default RoomNote;
