import { IAttendancePreviewData } from './AttendanceImportPreview';

export const objectKeys = <T extends object>(obj: T) => Object.keys(obj) as (keyof T)[];

export const attendancePreviewIsNewUser = (attendance: IAttendancePreviewData): boolean =>
    attendance.user_data.id == '';

export const attendancePreviewUserChangedKeys = (
    attendance: IAttendancePreviewData
): (keyof IAttendancePreviewData['user_data'])[] => {
    return objectKeys(attendance.user_data).filter(k => attendance.user[k] != attendance.user_data[k]);
};

export const attendancePreviewIsNewAttendance = (attendance: IAttendancePreviewData): boolean =>
    attendance.attendance == null;

export const attendancePreviewAttendanceChangedKeys = (
    attendance: IAttendancePreviewData
): (keyof IAttendancePreviewData['attendance_data'])[] => {
    return objectKeys(attendance.attendance_data).filter(
        k => attendance.attendance?.[k] != attendance.attendance_data[k]
    );
};

export const attendancePreviewHasChanges = (attendance: IAttendancePreviewData): number => {
    let changes = 0;
    if (!attendancePreviewIsNewUser(attendance)) changes += attendancePreviewUserChangedKeys(attendance).length;
    if (!attendancePreviewIsNewAttendance(attendance))
        changes += attendancePreviewAttendanceChangedKeys(attendance).length;
    return changes;
};

export const normalizePhoneNumber = (value: string): string => {
    return value.replace(/[^\d\+]/g, '').replace(/^0/, '+421');
};

export const attendancePreviewStatus = (attendance: IAttendancePreviewData): 'not_changed' | 'changed' | 'new' => {
    if (attendancePreviewIsNewUser(attendance)) {
        return 'new';
    }
    if (attendancePreviewHasChanges(attendance) > 0) {
        return 'changed';
    }
    return 'not_changed';
};
