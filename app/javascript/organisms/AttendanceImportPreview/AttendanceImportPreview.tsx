import cx from 'classnames';
import * as React from 'react';
import AttendanceImportItem from './AttendanceImportItem';
import AttendanceImportOverview from './AttendanceImportOverview';
import { attendancePreviewStatus } from './helpers';

interface IUserData {
    id: string;
    username: string;
    email: string;
    first_name: string;
    last_name: string;
    born_at: string;
    phone: string;
    parents_phone: string;
    address: string;
    shirt_size: string;
    parents_email: string;
    gender: string;
}

interface IAttendanceData {
    attendance_note: string;
    should_pay: string;
    paid: string;
}

export interface IAttendancePreviewData {
    index: number;
    user_data: IUserData;
    attendance_data: IAttendanceData;
    attendance: IAttendanceData | null;
    new: boolean;
    user: Partial<IUserData>; // Can be "new" users
    other_users: IUserData[];
}

interface IProps {
    data: IAttendancePreviewData[];
}

type TScope = 'not_changed' | 'changed' | 'new';
const SCOPES: TScope[] = ['not_changed', 'changed', 'new'];
const SCOPE_LABELS: { [key in TScope]: string } = {
    not_changed: 'Not changed',
    changed: 'Changed',
    new: 'New'
};

const AttendanceImportPreview: React.FC<IProps> = ({ data }) => {
    const [showOverview, setShowOverview] = React.useState(true);
    const [attendances, setAttendances] = React.useState(data);
    const [selectedScopes, setSelectedScopes] = React.useState<TScope[]>(SCOPES);

    React.useEffect(() => {
        // For css
        document.body.classList.add('index', 'edit');

        window.onbeforeunload = () => true;
        return () => {
            window.onbeforeunload = null;
        };
    }, []);

    const [rowIndex, setRowIndex] = React.useState(Math.min(...data.map(v => v.index)));

    const handleChange = (newData: IAttendancePreviewData) => {
        setAttendances(v => v.map(a => (a.index === newData.index ? newData : a)));

        const oldStatus = attendancePreviewStatus(attendances.find(a => a.index === newData.index)!);
        const newStatus = attendancePreviewStatus(newData);
        if (oldStatus !== newStatus && !selectedScopes.includes(newStatus)) {
            setSelectedScopes([...selectedScopes, newStatus]);
        }
    };

    const handleSubmit = () => {
        if (!confirm('Are you sure you want to import these values?')) return;

        const form = document.querySelector('form.import') as HTMLFormElement | null;
        if (form == null) return;
        const input = form.querySelector('input[name="import[data]"]') as HTMLInputElement | null;
        if (input == null) return;
        input.value = JSON.stringify(attendances);
        window.onbeforeunload = null;
        form.submit();
    };

    if (attendances.length === 0) {
        return <h2>No data to import</h2>;
    }

    const attendanceGroups = attendances.reduce(
        (acc, a) => {
            acc[attendancePreviewStatus(a)].push(a);
            return acc;
        },
        {
            new: [] as IAttendancePreviewData[],
            changed: [] as IAttendancePreviewData[],
            not_changed: [] as IAttendancePreviewData[]
        }
    );

    const visibleAttendances = selectedScopes
        .reduce((acc, scope) => acc.concat(attendanceGroups[scope]), [] as IAttendancePreviewData[])
        .sort((a, b) => a.index - b.index);

    const handleDelete = (data: IAttendancePreviewData) => {
        if (!confirm('Are you sure you want to delete this row?')) return;

        setAttendances(v => v.filter(a => a !== data));
        changeRowIndex(-1); // TODO: check if this is correct
    };

    const attendance = attendances.find(a => a.index === rowIndex);

    const changeRowIndex = (d: number) => {
        // Select next or previous visible row
        const newAttendance = visibleAttendances[Math.max(visibleAttendances.indexOf(attendance!) + d, 0)];
        if (newAttendance) {
            setRowIndex(newAttendance.index);
        }
    };

    return (
        <React.Fragment>
            <div className="table_tools" style={{ display: 'flex', alignItems: 'flex-start' }}>
                <div className="scopes" style={{ flex: 1, display: 'flex', justifyContent: 'start' }}>
                    {!showOverview && (
                        <ul
                            className="table_tools_segmented_control scope-default-group"
                            style={{ marginRight: '10px' }}
                        >
                            {SCOPES.map(scope => (
                                <li
                                    key={scope}
                                    className={cx('scope', { selected: selectedScopes.includes(scope) })}
                                    style={{ display: 'inline-block' }}
                                >
                                    <a
                                        href="#"
                                        className="table_tools_button tw-cursor-pointer"
                                        onClick={e => {
                                            e.preventDefault();
                                            setSelectedScopes(v =>
                                                v.includes(scope) ? v.filter(s => s !== scope) : [...v, scope]
                                            );
                                        }}
                                    >
                                        {SCOPE_LABELS[scope]} ({attendanceGroups[scope].length})
                                    </a>
                                </li>
                            ))}
                        </ul>
                    )}
                    {!showOverview && (
                        <ul className="table_tools_segmented_control scope-default-group">
                            <li className="scope" style={{ display: 'inline-block' }}>
                                <a
                                    href="#"
                                    className="table_tools_button tw-cursor-pointer"
                                    onClick={e => {
                                        e.preventDefault();
                                        changeRowIndex(-1);
                                    }}
                                >
                                    Prev
                                </a>
                            </li>
                            <li className="scope" style={{ display: 'inline-block' }}>
                                <a>
                                    {attendance ? attendances.indexOf(attendance) + 1 : '??'} / {attendances.length}
                                </a>
                            </li>
                            <li className="scope" style={{ display: 'inline-block' }}>
                                <a
                                    href="#"
                                    className="table_tools_button tw-cursor-pointer"
                                    onClick={e => {
                                        e.preventDefault();
                                        changeRowIndex(+1);
                                    }}
                                >
                                    Next
                                </a>
                            </li>
                        </ul>
                    )}
                </div>

                <div style={{ display: 'flex' }}>
                    {showOverview ? (
                        <button className="button" onClick={() => setShowOverview(false)}>
                            Edit values
                        </button>
                    ) : (
                        <button className="button" onClick={() => setShowOverview(true)}>
                            Show import overview
                        </button>
                    )}
                    {showOverview && (
                        <button className="button" onClick={handleSubmit} style={{ marginLeft: '10px' }}>
                            Execute import with values
                        </button>
                    )}
                </div>
            </div>
            {showOverview ? (
                <AttendanceImportOverview
                    attendances={attendances}
                    onEdit={a => {
                        setRowIndex(a.index);
                        setShowOverview(false);
                    }}
                    onChange={setAttendances}
                />
            ) : attendance != null ? (
                <AttendanceImportItem
                    attendance={attendance}
                    onChange={handleChange}
                    onDelete={() => handleDelete(attendance)}
                />
            ) : (
                <p>No row selected</p>
            )}
        </React.Fragment>
    );
};

export default AttendanceImportPreview;
