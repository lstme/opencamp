import * as React from 'react';
import { IAttendancePreviewData } from './AttendanceImportPreview';
import { attendancePreviewHasChanges, attendancePreviewIsNewUser, normalizePhoneNumber, objectKeys } from './helpers';

interface IProps {
    attendance: IAttendancePreviewData;
    onDelete: () => void;
    onChange: (newData: IAttendancePreviewData) => void;
}

const AttendanceImportItem: React.FC<IProps> = ({ attendance, onDelete, onChange }) => {
    const isNew = attendancePreviewIsNewUser(attendance);
    const hasChanges = attendancePreviewHasChanges(attendance) > 0;

    const handleChange = <T extends 'user_data' | 'attendance_data'>(
        key: T,
        attribute: keyof IAttendancePreviewData[T],
        value: string
    ) => {
        onChange({ ...attendance, [key]: { ...attendance[key], [attribute]: value } });
    };

    const handleOtherUserChange = (newUser: { id: string; label: string }) => {
        const newId = newUser.id === '-1' ? null : newUser.id;
        const otherUser = attendance.other_users.find(u => u.id == newId);
        if (otherUser) {
            onChange({
                ...attendance,
                user: { ...otherUser },
                user_data: { ...attendance.user_data, id: otherUser.id ?? '' }
            });
        }
    };

    return (
        <React.Fragment>
            <h2 style={{ color: isNew ? 'red' : hasChanges ? 'orange' : 'green' }}>
                <button style={{ float: 'right' }} className="button" onClick={onDelete}>
                    Remove this row
                </button>
                Row {attendance.index}: {attendance.user_data.first_name} {attendance.user_data.last_name}
            </h2>

            <form action="">
                {attendance.other_users.length > 1 ? (
                    <fieldset className="inputs">
                        <legend>
                            <span>Possible users matched by username</span>
                        </legend>
                        <ol>
                            <SelectField
                                label="Select user"
                                name="other_users"
                                onChange={handleOtherUserChange}
                                options={[
                                    ...attendance.other_users.map(user => ({
                                        id: user.id ?? '-1',
                                        label:
                                            user.id == null
                                                ? 'Create new user'
                                                : `${user.first_name} ${user.last_name} (${user.username})`
                                    }))
                                ]}
                                value={attendance.user_data.id}
                            />
                        </ol>
                    </fieldset>
                ) : null}

                <fieldset className="inputs">
                    <legend>
                        <span>
                            User data{' '}
                            <small style={{ color: '#5E6469' }}>
                                (
                                {isNew
                                    ? 'New user - User will be created'
                                    : hasChanges
                                    ? 'User will be updated with new values'
                                    : 'User will not change'}
                                )
                            </small>
                        </span>
                    </legend>
                    <ol>
                        {objectKeys(attendance.user_data).map(key => {
                            return (
                                <InputField
                                    key={key}
                                    name={key}
                                    label={key}
                                    value={attendance.user_data[key]}
                                    hint={
                                        <InputHint
                                            value={attendance.user_data[key]}
                                            originalValue={attendance.user[key]}
                                            isAddress={key === 'address'}
                                            isPhoneNumber={key === 'phone' || key === 'parents_phone'}
                                            onChange={v => handleChange('user_data', key, v)}
                                        />
                                    }
                                    onChange={v => handleChange('user_data', key, v)}
                                    textarea={key === 'address'}
                                />
                            );
                        })}
                    </ol>
                </fieldset>
                <fieldset className="inputs">
                    <legend>
                        <span>Attendance data</span>
                    </legend>
                    <ol>
                        {objectKeys(attendance.attendance_data).map(key => (
                            <InputField
                                key={key}
                                name={key}
                                label={key}
                                hint={
                                    <InputHint
                                        value={attendance.attendance_data[key]}
                                        originalValue={attendance.attendance?.[key]}
                                        numeric={key === 'should_pay' || key === 'paid'}
                                        onChange={v => handleChange('attendance_data', key, v)}
                                    />
                                }
                                value={attendance.attendance_data[key]}
                                onChange={v => handleChange('attendance_data', key, v)}
                                textarea={key === 'attendance_note'}
                            />
                        ))}
                    </ol>
                </fieldset>
            </form>
        </React.Fragment>
    );
};

const InputHint = ({
    value,
    originalValue,
    numeric = false,
    isPhoneNumber = false,
    isAddress = false,
    onChange
}: {
    value: string;
    originalValue: string | undefined;
    numeric?: boolean;
    isPhoneNumber?: boolean;
    isAddress?: boolean;
    onChange: (newValue: string) => void;
}) => {
    const normalizedNumber = isPhoneNumber ? normalizePhoneNumber(value) : undefined;

    const isChanged =
        originalValue != null && (numeric ? Number(originalValue) !== Number(value) : originalValue !== value);
    const isDenormalized = value != null && normalizedNumber != null && value !== normalizedNumber;

    if (!isChanged && !isDenormalized) return null;

    return (
        <>
            {isDenormalized && (
                <button
                    className="button small"
                    style={{ marginRight: 10 }}
                    onClick={e => {
                        e.preventDefault();
                        onChange(normalizedNumber);
                    }}
                >
                    {normalizedNumber}
                </button>
            )}
            {isChanged && (
                <>
                    {isAddress ? (
                        <p style={{ display: 'inline-block', margin: 0 }}>
                            Original: <br />
                            <span style={{ whiteSpace: 'pre-wrap' }}>{originalValue}</span>
                        </p>
                    ) : (
                        <>Original: {originalValue}</>
                    )}

                    <button
                        className="button small"
                        style={{ marginLeft: 10 }}
                        onClick={e => {
                            e.preventDefault();
                            onChange(originalValue);
                        }}
                    >
                        Keep
                    </button>
                </>
            )}
        </>
    );
};

const InputField = (props: {
    name: string;
    label: string;
    value: string;
    onChange: (newValue: string) => void;
    hint?: React.ReactNode;
    textarea?: boolean;
}) => (
    <li className="string input required stringish">
        <label htmlFor={props.name} className="label">
            {props.label}
        </label>
        {props.textarea ? (
            <textarea
                id={props.name}
                value={props.value}
                name={props.name}
                onChange={e => props.onChange(e.target.value)}
                rows={props.name.endsWith('note') ? 5 : 3}
            />
        ) : (
            <input
                id={props.name}
                type="text"
                value={props.value}
                name={props.name}
                onChange={e => props.onChange(e.target.value)}
            />
        )}
        {props.hint != null && (
            <p className="inline-hints" style={{ color: 'orange' }}>
                {props.hint}
            </p>
        )}
    </li>
);

interface ISelectFieldProps<T> {
    name: string;
    label: string;
    value: string;
    onChange: (newValue: T) => void;
    options: T[];
}

const SelectField = <T extends { id: string; label: string }>({
    label,
    name,
    onChange,
    options,
    value
}: ISelectFieldProps<T>) => {
    return (
        <li className="string input required stringish">
            <label htmlFor={name} className="label">
                {label}
            </label>
            <select
                id={name}
                value={value}
                name={name}
                onChange={e => {
                    const option = options.find(opt => opt.id == e.target.value)!;
                    onChange(option);
                }}
                ref={node => {
                    // Disable select2
                    setTimeout(() => {
                        if (node) {
                            if ($(node).hasClass('select2-hidden-accessible')) {
                                $(node).select2('destroy');
                            }
                        }
                    }, 100);
                }}
            >
                {options.map(option => (
                    <option key={option.id} value={option.id}>
                        {option.label}
                    </option>
                ))}
            </select>
        </li>
    );
};

export default AttendanceImportItem;
