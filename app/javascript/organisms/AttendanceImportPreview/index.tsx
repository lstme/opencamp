import { createRoot } from 'react-dom/client';
import AttendanceImportPreview from './AttendanceImportPreview';

const container = document.getElementById('attendance-import-preview');
if (container != null) {
    const root = createRoot(container);
    const data = container.getAttribute('data-data') ?? '{}';
    root.render(<AttendanceImportPreview data={JSON.parse(data)} />);
}
