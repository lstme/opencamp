import * as React from 'react';
import { IAttendancePreviewData } from './AttendanceImportPreview';
import {
    attendancePreviewAttendanceChangedKeys,
    attendancePreviewStatus,
    attendancePreviewUserChangedKeys,
    normalizePhoneNumber
} from './helpers';

interface IProps {
    attendances: IAttendancePreviewData[];
    onEdit: (attendance: IAttendancePreviewData) => void;
    onChange: (attendances: IAttendancePreviewData[]) => void;
}

const AttendanceImportOverview: React.FC<IProps> = ({ attendances, onEdit, onChange }) => {
    const handleChange = (a: IAttendancePreviewData) => {
        onChange(attendances.map(attendance => (attendance.index === a.index ? a : attendance)));
    };
    return (
        <div className="panel">
            <div className="panel_contents">
                <table className="table index">
                    <thead>
                        <tr>
                            <th>Row</th>
                            <th>Email / Status</th>
                            <th>User changes</th>
                            <th>Attendance changes</th>
                        </tr>
                    </thead>
                    <tbody>
                        {attendances.map(attendance => (
                            <AttendanceRow
                                key={attendance.index}
                                attendance={attendance}
                                onChange={handleChange}
                                onEdit={onEdit}
                            />
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

const AttendanceRow = ({
    attendance,
    onChange,
    onEdit
}: {
    attendance: IAttendancePreviewData;
    onChange: (attendance: IAttendancePreviewData) => void;
    onEdit: (attendnace: IAttendancePreviewData) => void;
}) => {
    const status = attendancePreviewStatus(attendance);
    const handleUserChange = (otherUser: IAttendancePreviewData['other_users'][number]) => {
        onChange({
            ...attendance,
            user: { ...otherUser },
            user_data: { ...attendance.user_data, id: otherUser.id ?? '' }
        });
    };
    return (
        <tr key={attendance.index}>
            <td style={{ verticalAlign: 'top' }}>
                {attendance.index}
                <button
                    style={{ marginLeft: 10 }}
                    type="button"
                    className="button small"
                    onClick={() => onEdit(attendance)}
                >
                    Edit
                </button>
            </td>
            <td style={{ verticalAlign: 'top' }}>
                {attendance.user_data.email}
                <br />
                <span style={{ color: status === 'new' ? 'red' : status === 'changed' ? 'orange' : 'green' }}>
                    {status}
                </span>
                {status === 'new' && attendance.other_users.length > 1 && (
                    <div>
                        <strong>Possible users:</strong>
                        <ul>
                            {attendance.other_users
                                .filter(u => u.id != null)
                                .map(user => (
                                    <li key={user.id}>
                                        <a
                                            href="#"
                                            style={{ color: 'orange' }}
                                            onClick={e => {
                                                e.preventDefault();
                                                handleUserChange(user);
                                            }}
                                        >
                                            {user.email}
                                        </a>
                                    </li>
                                ))}
                        </ul>
                    </div>
                )}
                <br />
            </td>
            <td style={{ verticalAlign: 'top' }}>
                <div style={{ maxWidth: 600 }}>
                    {attendancePreviewUserChangedKeys(attendance).map(key => (
                        <DiffRow
                            key={key}
                            label={key}
                            prevValue={attendance.user?.[key]}
                            value={attendance.user_data[key]}
                            wide={key === 'address'}
                            isPhoneNumber={key === 'phone' || key === 'parents_phone'}
                            isNew={status === 'new'}
                            onChange={newValue =>
                                onChange({
                                    ...attendance,
                                    user_data: {
                                        ...attendance.user_data,
                                        [key]: newValue
                                    }
                                })
                            }
                        />
                    ))}
                </div>
            </td>
            <td style={{ verticalAlign: 'top' }}>
                <div style={{ maxWidth: 300 }}>
                    {attendancePreviewAttendanceChangedKeys(attendance).map(key => (
                        <DiffRow
                            key={key}
                            label={key}
                            prevValue={attendance.attendance?.[key]}
                            value={attendance.attendance_data[key]}
                            wide={key === 'attendance_note'}
                            isNew={attendance.attendance == null}
                            onChange={newValue =>
                                onChange({
                                    ...attendance,
                                    attendance_data: {
                                        ...attendance.attendance_data,
                                        [key]: newValue
                                    }
                                })
                            }
                        />
                    ))}
                </div>
            </td>
        </tr>
    );
};

const DiffRow = ({
    label,
    prevValue,
    value,
    wide = false,
    isPhoneNumber = false,
    isNew = false,
    onChange
}: {
    label: string;
    prevValue: string | undefined;
    value: string;
    wide?: boolean;
    isPhoneNumber?: boolean;
    isNew?: boolean;
    onChange: (newValue: string) => void;
}) => (
    <div style={{ display: 'flex', alignItems: 'flex-start', borderBottom: '1px dashed #e4eaec' }}>
        <span style={{ display: 'inline-block', minWidth: 120 }}>{label}:</span>
        {wide ? (
            <div style={{ display: 'inline-block', whiteSpace: 'pre-wrap' }}>
                {!isNew && (
                    <>
                        <a
                            href="#"
                            style={{ color: 'orange', display: 'block', width: 200 }}
                            onClick={e => {
                                e.preventDefault();
                                onChange(prevValue ?? '');
                            }}
                        >
                            {prevValue}
                        </a>
                        <span style={{ display: 'block', width: 200 }}>&rArr;</span>
                    </>
                )}
                <span style={{ display: 'block', width: 200 }}>{value}</span>
            </div>
        ) : (
            <>
                {!isNew && (
                    <a
                        href="#"
                        style={{ color: 'orange', display: 'inline-block', minWidth: 150 }}
                        onClick={e => {
                            e.preventDefault();
                            onChange(prevValue ?? '');
                        }}
                    >
                        {prevValue}
                    </a>
                )}
                <span style={{ display: 'inline-block', minWidth: 150 }}>
                    {!isNew && <span>&rArr; </span>}
                    {value}
                    {isPhoneNumber && <PhoneFixButton value={value} onFix={onChange} />}
                </span>
            </>
        )}
    </div>
);

const PhoneFixButton = ({ value, onFix }: { value: string; onFix: (newValue: string) => void }) => {
    const normalizedNumber = normalizePhoneNumber(value);
    if (normalizedNumber === value) {
        return null;
    }

    return (
        <button
            className="button small"
            style={{ marginLeft: 10 }}
            onClick={() => {
                onFix(normalizedNumber);
            }}
        >
            {normalizedNumber}
        </button>
    );
};

export default AttendanceImportOverview;
