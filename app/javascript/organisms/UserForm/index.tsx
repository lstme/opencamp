import Avatar from 'atom/Avatar';
import ErrorDisplay from 'atom/ErrorDisplay';
import TextareaField from 'atom/TextareaField';
import TextField from 'atom/TextField';
import cx from 'classnames';
import { isEqual } from 'lodash';
import * as React from 'react';
import { UserDataFragment, useUpdateUserMutation } from '~graphql.generated';

interface IProps {
    user: UserDataFragment;
    className?: string;
}

const UserForm = ({ user, className }: IProps) => {
    const [formData, setFormData] = React.useState<Omit<UserDataFragment, 'avatar' | '__typename'>>(user);
    const dirty = !isEqual(user, formData);

    const [updateUser, { loading, error }] = useUpdateUserMutation();

    const handleSave = () => {
        if (!formData || !dirty) return;

        return updateUser({
            variables: {
                userId: user.id,
                attributes: {
                    born_at: formData.born_at,
                    email: formData.email,
                    first_name: formData.first_name,
                    gender: formData.gender,
                    last_name: formData.last_name,
                    note: formData.note ?? '',
                    username: formData.username,
                    address: formData.address,
                    parents_email: formData.parents_email,
                    parents_phone: formData.parents_phone,
                    phone: formData.phone,
                    school: formData.school,
                    shirt_size: formData.shirt_size
                }
            }
        });
    };

    if (!formData) return null;

    const fieldProps = (id: keyof typeof formData, label: string) => ({
        id,
        className: 'mb-2',
        label,
        value: formData[id] ?? '',
        onChange: (e: { target: { value: string } }) => setFormData({ ...formData, [id]: e.target.value })
    });

    return (
        <form
            className={cx('card', className)}
            onSubmit={e => {
                e.preventDefault();
                handleSave();
            }}
        >
            <div className="card-header">
                <div className="card-title">User details</div>
            </div>
            <div className="card-body">
                <div className="row">
                    <div className="col-12 col-md-4">
                        <Avatar
                            className="d-block w-100 mb-2"
                            style={{ paddingTop: '100%' }}
                            avatar={user.avatar}
                            omitBaseClassname
                        />
                        <div className="d-flex gap-2">
                            <TextField {...fieldProps('first_name', 'First name')} />
                            <TextField {...fieldProps('last_name', 'Last name')} />
                        </div>
                        <TextField {...fieldProps('email', 'Email')} />
                        <TextField {...fieldProps('username', 'Username')} />
                    </div>
                    <div className="col-12 col-md-4">
                        <TextField {...fieldProps('born_at', 'Born at')} type="date" />
                        {/* TODO: select */}
                        <TextField {...fieldProps('gender', 'Gender')} />
                        <TextField {...fieldProps('shirt_size', 'Shirt size')} />
                        <TextareaField {...fieldProps('address', 'Address')} rows={3} />
                        <TextField {...fieldProps('phone', 'Phone')} />
                        <TextField {...fieldProps('school', 'School')} />
                    </div>
                    <div className="col-12 col-md-4">
                        <TextField {...fieldProps('parents_phone', "Parent's phone")} />
                        <TextField {...fieldProps('parents_email', "Parent's email")} />
                        <TextareaField {...fieldProps('note', 'Note')} rows={3} />
                    </div>
                </div>
                {error && <ErrorDisplay className="mt-2" error={error} />}
            </div>
            <div className="card-footer text-end">
                <button
                    className={cx('btn btn-primary', { 'btn-loading': loading })}
                    onClick={handleSave}
                    disabled={!dirty}
                >
                    Update user data
                </button>
            </div>
        </form>
    );
};

export default UserForm;
