import { ApolloError } from 'apollo-client';
import ErrorDisplay from 'atom/ErrorDisplay';
import cx from 'classnames';
import { addDays } from 'date-fns';
import format from 'lib/format';
import Timetable, { TimetableEmpty, TimetableSkeleton } from 'molecule/Timetable';
import DashboardTimetable from 'molecule/Timetable/DashboardTimetable';
import { SERVER_DATE_FORMAT, TIMETABLE_DATE_FORMAT } from '~constants';
import { TimetableFragment } from '~graphql.generated';

interface IProps {
    date: string;
    setDate?: (newDate: string) => void;
    timetable?: TimetableFragment | null;
    loading?: boolean;
    error?: ApolloError;
    onEdit?: () => void;
    variant?: 'dashboard' | 'default';
}

/**
 * This component is used as standalone entrypoint where data is passed from JSON so make sure
 * all the relevant props are JSON serializable (such as date)
 */
const TimetableDisplay = ({
    date: dateString,
    setDate,
    loading,
    error,
    timetable,
    onEdit,
    variant = 'default'
}: IProps) => {
    const date = new Date(dateString);
    const changeDate = (offset: number) => {
        setDate?.(format(addDays(date, offset), SERVER_DATE_FORMAT));
    };

    const timetableExists = loading || error ? undefined : timetable != null;
    const handleEdit = timetableExists === true ? onEdit : undefined;
    const handleCreate = timetableExists === false ? onEdit : undefined;

    return (
        <>
            <div className="card-header">
                <div className={cx('card-title')}>{format(date, TIMETABLE_DATE_FORMAT)}</div>
                <div className="card-actions btn-actions user-select-none">
                    {!!setDate && (
                        <>
                            <a className="btn-action cursor-pointer" onClick={() => changeDate(-1)}>
                                <i className="fal fa-chevron-left icon" />
                            </a>
                            <a className="btn-action cursor-pointer" onClick={() => changeDate(1)}>
                                <i className="fal fa-chevron-right icon" />
                            </a>
                        </>
                    )}
                    {!!onEdit && (
                        <div className="dropdown">
                            <a
                                className="btn-action dropdown-toggle cursor-pointer"
                                data-bs-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                            >
                                <i className="fal fa-ellipsis-h icon"></i>
                            </a>
                            <div className="dropdown-menu dropdown-menu-end">
                                <a
                                    className={cx('dropdown-item cursor-pointer', { disabled: !handleEdit })}
                                    onClick={handleEdit}
                                >
                                    Edit
                                </a>
                                <a
                                    className={cx('dropdown-item cursor-pointer', { disabled: !handleCreate })}
                                    onClick={handleCreate}
                                >
                                    Create
                                </a>
                            </div>
                        </div>
                    )}
                </div>
            </div>

            {timetable ? (
                variant === 'dashboard' ? (
                    <DashboardTimetable timetable={timetable} />
                ) : (
                    <Timetable timetable={timetable} />
                )
            ) : loading ? (
                <TimetableSkeleton />
            ) : error ? (
                <div className="card-body">
                    <ErrorDisplay error={error} />
                </div>
            ) : (
                <TimetableEmpty />
            )}
        </>
    );
};

export default TimetableDisplay;
