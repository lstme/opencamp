import cx from 'classnames';
import format from 'lib/format';
import { useIsAdmin, useSelectedTermId } from 'organism/AppContext';
import TimetableForm from 'organism/TimetableForm';
import * as React from 'react';
import { SERVER_DATE_FORMAT } from '~constants';
import { useTimetableQuery } from '~graphql.generated';
import TimetableDisplay from './TimetableDisplay';

interface IProps {
    className?: string;
}

const TimetableCard = ({ className }: IProps) => {
    const termId = useSelectedTermId();
    const canEdit = useIsAdmin();

    const [editing, setEditing] = React.useState<{ id?: string; date: string }>();
    const [date, setDate] = React.useState<string>(format(new Date(), SERVER_DATE_FORMAT));

    const { data, loading, error } = useTimetableQuery({
        variables: { key: date, termId }
    });

    return (
        <div className={cx('card', className)}>
            {editing ? (
                <TimetableForm {...editing} onClose={() => setEditing(undefined)} />
            ) : (
                <TimetableDisplay
                    date={date}
                    setDate={setDate}
                    error={error}
                    loading={loading}
                    onEdit={canEdit ? () => setEditing({ id: data?.term?.timetable?.id, date }) : undefined}
                    timetable={data?.term?.timetable}
                />
            )}
        </div>
    );
};

export default TimetableCard;
