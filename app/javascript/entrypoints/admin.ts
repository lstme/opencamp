import './admin.scss';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

import '../organisms/AttendanceImportPreview';
import '../organisms/DiffDisplay';

// Active admin modal for any links with data-modal attribute

declare global {
    const ActiveAdmin: ActiveAdmin;
    interface ActiveAdmin {
        ModalDialog: (
            title: string,
            inputs: Record<string, string>,
            callback: (inputs: Record<string, string>) => void
        ) => void;
    }
}

$(() => {
    $('a[data-modal]').on('click', function (e) {
        e.stopPropagation(); // prevent Rails UJS click event
        e.preventDefault();

        const $link = $(this);
        const title = $link.data('title') ?? 'Modal';
        const inputs = $link.data('inputs');

        ActiveAdmin.ModalDialog(title, inputs, data => {
            const method = $link.data('method') ?? 'get';
            const action = $link.attr('href') ?? '';

            var $form = $('<form>', { action, method });
            const authenticity_token = $('meta[name="csrf-token"]').attr('content')!;
            $('<input>')
                .attr({ type: 'hidden', name: 'authenticity_token', value: authenticity_token })
                .appendTo($form);
            $.each(data, function (key, val) {
                $('<input>')
                    .attr({
                        type: 'hidden',
                        name: key,
                        value: val
                    })
                    .appendTo($form);
            });
            $form.appendTo('body').trigger('submit');
        });

        return false;
    });
});
