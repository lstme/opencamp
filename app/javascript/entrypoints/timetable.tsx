import '@tabler/core/src/js/tabler';

import { createRoot } from 'react-dom/client';
import TimetableDisplay from 'organism/TimetableCard/TimetableDisplay';

const container = document.getElementById('timetable');
if (container != null) {
    const root = createRoot(container);
    const dataString = container.getAttribute('data-timetable') ?? '{}';
    const data = JSON.parse(dataString);
    root.render(<TimetableDisplay {...data} variant="dashboard" />);
}
