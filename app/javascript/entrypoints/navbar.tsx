import { createRoot } from 'react-dom/client';
import Navbar from 'organism/Navbar';

const container = document.getElementById('navbar');
if (container) {
    const root = createRoot(container);
    const data = container.getAttribute('data-navbar') ?? '{}';
    root.render(<Navbar data={JSON.parse(data)} />);
}
