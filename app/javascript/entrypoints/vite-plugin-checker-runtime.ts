// @ts-ignore
// /@vite-plugin-checker-runtime is externalized in production build, so this file
// will explode if included in production. Make sure to only include it in development
import { inject } from '/@vite-plugin-checker-runtime';
inject({ overlayConfig: true });
