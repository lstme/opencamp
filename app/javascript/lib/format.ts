import { format as ogFormat } from 'date-fns';
import { sk } from 'date-fns/locale';

export const format = (date: Date, formatString: string) => ogFormat(date, formatString, { locale: sk });

export default format;
