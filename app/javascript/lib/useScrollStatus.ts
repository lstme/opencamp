import * as React from 'react';

interface IState {
    atBottom: boolean;
    atTop: boolean;
    scrollable: boolean;
}

interface IOptions {
    bottomOffset?: number;
    topOffset?: number;
}

interface IUseScollStatusResult extends IState {
    ref: (node: HTMLDivElement) => void;
    scrollingParentRef: React.MutableRefObject<HTMLElement | Window | null>;
}

export const useScrollStatus = (opts?: IOptions | undefined): IUseScollStatusResult => {
    const bottomOffset = opts?.bottomOffset ?? 0;
    const topOffset = opts?.topOffset ?? 0;

    const [state, setState] = React.useState<IState>({
        atBottom: true,
        atTop: true,
        scrollable: false
    });

    const stateRef = React.useRef(state);
    const mounted = React.useRef(true);
    const targetElementRef = React.useRef<HTMLElement | null>(null);
    const scrollingParentRef = React.useRef<HTMLElement | Window | null>(null);

    React.useEffect(() => {
        mounted.current = true;
        return () => {
            mounted.current = false;
        };
    }, []);

    stateRef.current = state;

    const updateState = React.useCallback((newState: IState): void => {
        if (
            newState.atBottom === stateRef.current.atBottom &&
            newState.atTop === stateRef.current.atTop &&
            newState.scrollable === stateRef.current.scrollable
        ) {
            return;
        }
        if (!mounted.current) return;
        setState(newState);
    }, []);

    const initialize = React.useCallback(
        (node: HTMLElement, resized: boolean = false) => {
            if (!resized && targetElementRef.current === node) return;

            targetElementRef.current = node;

            if (resized || scrollingParentRef.current == null) {
                if (targetElementRef.current == null) {
                    scrollingParentRef.current = window;
                } else {
                    scrollingParentRef.current = getElementsScrollingParent(targetElementRef.current);
                }
            }

            const container = scrollingParentRef.current;

            if (container == null) {
                updateState({ atBottom: true, atTop: true, scrollable: false });
                return;
            }

            const handleScroll = (): void => {
                let scrollOffset = 0;
                let windowHeight = 0;
                let contentHeight = 0;

                if (container === window) {
                    scrollOffset = window.pageYOffset;
                    windowHeight = window.innerHeight;
                    contentHeight = window.document.body.clientHeight;
                } else {
                    const elem = container as HTMLElement;
                    scrollOffset = elem.scrollTop;
                    windowHeight = elem.clientHeight;
                    contentHeight = elem.scrollHeight;
                }

                updateState({
                    atTop: scrollOffset <= topOffset,
                    atBottom: scrollOffset >= contentHeight - windowHeight - bottomOffset,
                    scrollable: true
                });
            };

            handleScroll();
            // in case there is animation or something going on
            // (like when collapsing ReportSectionBox), check
            // scroll status again after some delay
            setTimeout(handleScroll, 100);
            container.addEventListener('scroll', handleScroll);
        },
        [bottomOffset, topOffset, updateState]
    );

    const ref = React.useCallback(
        (node: HTMLDivElement) => {
            if (node == null) return;
            initialize(node);

            // In case the node resizes by itself, remeasure, to make sure
            // it is still scrollable
            new ResizeObserver(() => {
                initialize(node, true);
            }).observe(node);
        },
        [initialize]
    );

    return {
        ref,
        ...state,
        scrollingParentRef
    };
};

const getElementsScrollingParent = (element: HTMLElement): HTMLElement | Window | null => {
    let scrollingParent: HTMLElement | Window | null = null;
    let parent: HTMLElement | null = element;

    while (parent != null) {
        if (parent.clientHeight < parent.scrollHeight || parent.tagName === 'HTML') {
            scrollingParent = parent;
            break;
        }

        // If any parent has set overflow and does not have height that needs to be scrolled
        // return null as there is nothing to scroll that would have effect on "stickines"
        const styles = window.getComputedStyle(parent);
        const anyOverflows =
            styles.overflow !== 'visible' || styles.overflowX !== 'visible' || styles.overflowY !== 'visible';
        if (anyOverflows) break;

        parent = parent.parentElement;
    }

    if (scrollingParent?.tagName === 'HTML') {
        scrollingParent = window;
    }

    return scrollingParent;
};
