import * as React from 'react';

interface IOnChangeAction<T> {
    type: 'ON_CHANGE';
    payload: {
        field: keyof T;
        value: any;
    };
}

type TReducer<T> = (state: T, action: IOnChangeAction<T>) => T;

const formReducer = <T>(state: T, action: IOnChangeAction<T>): T => {
    switch (action.type) {
        case 'ON_CHANGE': {
            return { ...state, [action.payload.field]: action.payload.value };
        }
        default:
            return state;
    }
};

const useFormReducer = <T>(
    initialState: T
): {
    state: T;
    onChange: <J extends keyof T>(field: J, value: T[J]) => void;
} => {
    const [state, dispatch] = React.useReducer<TReducer<T>>(formReducer, initialState);

    const onChange = React.useCallback(
        (field: keyof T, value: any) =>
            dispatch({
                type: 'ON_CHANGE',
                payload: {
                    field,
                    value
                }
            }),
        [dispatch]
    );

    return { state, onChange };
};

export default useFormReducer;
