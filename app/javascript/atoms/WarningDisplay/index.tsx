import cx from 'classnames';
import * as React from 'react';

interface IProps {
    className?: string;
    children?: React.ReactNode;
}

const WarningDisplay = ({ className, children }: IProps) => {
    if (!children) return null;

    return (
        <div className={cx('message message-notice', className)}>
            <p className="text whitespace-pre">{children}</p>
        </div>
    );
};

export default WarningDisplay;
