import cx from 'classnames';
import { useAvatarModal } from 'molecule/AvatarModal';
import * as React from 'react';
import { AvatarFragment } from '~graphql.generated';
import avatarPlaceholderImage from '../../assets/images/avatar-placeholder.png';

type TAvatarVariant = 'icon' | 'medium' | 'thumb';
export type AvatarSize = 'xl' | 'lg' | 'md' | 'default' | 'sm' | 'xs';

interface IProps {
    avatar: AvatarFragment | null;
    name?: string;
    className?: string;
    interactive?: boolean;
    variant?: TAvatarVariant;
    size?: AvatarSize;
    omitBaseClassname?: boolean;
}

const Avatar = ({
    avatar,
    name,
    className,
    interactive = true,
    variant = 'thumb',
    size = 'default',
    omitBaseClassname = false,
    onClick,
    style,
    ...rest
}: IProps & React.HTMLAttributes<HTMLImageElement>) => {
    const [loading, setLoading] = React.useState(true);
    const [imgSrc, setImgSrc] = React.useState<string | undefined>(name ? undefined : avatarPlaceholderImage);
    const avatarModal = useAvatarModal();
    // const [showLarge, setShowLarge] = React.useState(false);
    const modalUrl = avatar && avatar.medium_url;

    React.useEffect(() => {
        const url = {
            icon: avatar?.icon_url,
            medium: avatar?.medium_url,
            thumb: avatar?.thumb_url
        }[variant];

        if (!url) {
            setLoading(false);
            setImgSrc(avatarPlaceholderImage);
            return;
        }

        const img = new Image();
        img.onload = () => {
            setLoading(false);
            setImgSrc(url);
        };
        img.src = url;
        return () => {
            img.remove();
        };
    }, [avatar]);

    const handleClick = interactive
        ? onClick ??
          (modalUrl
              ? e => {
                    e.preventDefault();
                    e.stopPropagation();
                    avatarModal.open(modalUrl);
                }
              : undefined)
        : undefined;

    const cname = cx(
        className,
        {
            avatar: !omitBaseClassname
        },
        {
            'avatar-xl': size === 'xl',
            'avatar-lg': size === 'lg',
            'avatar-md': size === 'md',
            'avatar-sm': size === 'sm',
            'avatar-xs': size === 'xs'
        },
        { 'opacity-25': loading },
        { 'cursor-pointer': handleClick }
    );
    const backgroundImage = imgSrc ? `url('${imgSrc}')` : undefined;
    const content = !imgSrc
        ? name
              ?.split(' ')
              .map(s => s[0])
              .join('') ?? '??'
        : null;

    return (
        <span
            className={cname}
            onClick={handleClick}
            style={{
                backgroundImage,
                backgroundSize: 'cover',
                backgroundPosition: 'center center',
                ...style
            }}
            {...rest}
        >
            {content}
        </span>
    );
};

export default Avatar;
