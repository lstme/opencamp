import cx from 'classnames';
import * as React from 'react';

interface IProps {
    variant?: 'orange' | 'green' | 'primary';
    noSize?: boolean;
}

const PlusButton = ({ className, variant, noSize, ...props }: IProps & React.HTMLAttributes<HTMLDivElement>) => {
    const cname = cx(
        'flex items-center justify-center cursor-pointer border border-dashed rounded',
        {
            'text-xs h-8': !noSize,
            'text-green-400 border-green-400 hover:text-green-600 hover:border-green-600': variant === 'green',
            'text-orange-400 border-orange-400 hover:border-orange-600 hover:text-orange-600': variant === 'orange',
            'text-primary border-primary hover:border-primary-dark hover:text-primary-dark': variant === 'primary'
        },
        className
    );

    return (
        <div className={cname} {...props}>
            <i className="fa fa-plus icon" />
        </div>
    );
};

export default PlusButton;
