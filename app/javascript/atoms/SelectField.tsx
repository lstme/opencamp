import cx from 'classnames';
import * as React from 'react';

interface Props<T> extends Omit<React.SelectHTMLAttributes<HTMLSelectElement>, 'onChange' | 'value'> {
    items: T[];
    onChange?: (newValue: T) => void;
    value?: T;
    valueGetter?: (item: T) => string;
    labelGetter?: (item: T) => string;
    label?: string;
}

const SelectField = <T,>({
    className,
    items,
    onChange,
    valueGetter,
    labelGetter,
    label,
    id,
    value,
    placeholder,
    ...props
}: Props<T>) => {
    const handleChange = (v: string) => {
        const newValue = items.find((item, i) => (valueGetter?.(item) ?? i.toString()) === v);
        if (newValue != null) {
            onChange?.(newValue);
        }
    };

    const v = value ? valueGetter?.(value) ?? items.indexOf(value) : undefined;

    return (
        <div className={cx('form-group', className)}>
            <label className="form-label" htmlFor={id}>
                {label}
            </label>
            <select className="form-select" onChange={e => handleChange(e.target.value)} value={v ?? ''} {...props}>
                {placeholder != null && (
                    <option value="" disabled>
                        {placeholder}
                    </option>
                )}
                {items.map((item, i) => (
                    <option key={i} value={valueGetter?.(item) ?? i}>
                        {labelGetter?.(item) ?? i}
                    </option>
                ))}
            </select>
        </div>
    );
};

export default SelectField;
