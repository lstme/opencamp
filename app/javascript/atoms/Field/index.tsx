import * as React from 'react';
import cx from 'classnames';
import LoadingIndicator from '../LoadingIndicator';

interface IProps {
    className?: string;
    children?: React.ReactNode;
    error?: Error;
    hint?: React.ReactNode;
    id?: string;
    label?: React.ReactNode;
    labelClassName?: string;
    loading?: boolean;
}

const Field = ({ className, children, error, hint, id, label, labelClassName, loading }: IProps) => (
    <div className={cx('relative', className)}>
        {label && (
            <label
                className={cx('label', labelClassName, {
                    'cursor-pointer hover:text-primary': id
                })}
                htmlFor={id}
            >
                {label}
            </label>
        )}
        <div className="flex items-center">
            {loading && <LoadingIndicator className="absolute right-0 mr-4" />}
            {children}
        </div>
        {hint && <div className="mt-1 text-xs text-gray-600">{hint}</div>}
        {error && <div className="text-red-600">{error.message}</div>}
    </div>
);

export default Field;
