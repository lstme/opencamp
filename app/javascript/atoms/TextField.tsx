import cx from 'classnames';
import * as React from 'react';

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    hint?: React.ReactNode;
}

const TextField: React.FC<Props> = ({ className, id, label, value, hint, ...rest }) => (
    <div className={cx('form-group', className)}>
        <label className="form-label" htmlFor={id}>
            {label}
        </label>
        <input name={id} className="form-control" value={value} {...rest} />
        {hint && <div className="form-hint">{hint}</div>}
    </div>
);

export default TextField;
