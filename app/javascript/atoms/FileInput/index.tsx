import cx from 'classnames';
import * as React from 'react';

interface IProps {
    allow?: string; // regex
    label?: string;
    onChange: (id: string) => void;
}

const FileInput = ({
    allow,
    className,
    label,
    onChange,
    ...rest
}: IProps & React.InputHTMLAttributes<HTMLInputElement>) => {
    const [error, setError] = React.useState<Error | undefined>(undefined);
    const [file, setFile] = React.useState<File | undefined>(undefined);
    const [progress, setProgress] = React.useState<number | undefined>();

    React.useEffect(() => {
        const int = setInterval(() => {
            switch (progress) {
                case 0:
                    return setProgress(0.1);
                case 0.1:
                    return setProgress(0.4);
                case 0.4:
                    return setProgress(0.9);
                case 0.9:
                    return setProgress(1);
                case 1:
                    return setProgress(undefined);
            }
        }, 1000);

        return () => clearInterval(int);
    }, [progress]);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setError(undefined);
        setFile(undefined);
        const file = e.target.files && e.target.files[0];

        if (!file) return;

        let allowed = true;
        if (allow) {
            allowed = RegExp(allow).test(file.type);
        }

        if (!allowed) {
            setError(new Error(`File type${file.type ? ` '${file.type}'` : ''} is not allowed`));
            return;
        }

        setFile(file);
        setProgress(0);
    };

    const labelCName = cx(
        'flex m-0 items-center justify-center cursor-pointer h-8 border border-dashed rounded text-xs',
        {
            'border-gray-400 hover:border-gray-600': !error,
            'text-red-400 border-red-400': error
        }
    );

    let labelText = label || 'Select file';

    if (error) labelText = error.message;
    if (file) labelText = file.name;

    const id = rest.id || rest.name;

    return (
        <div className={cx('relative', className)}>
            <label
                className={cx('label', labelCName, {
                    'cursor-pointer hover:text-primary': id
                })}
                htmlFor={rest.id || rest.name}
            >
                {labelText}
            </label>
            <input
                className="hidden"
                type="file"
                id={rest.id || rest.name}
                name={rest.name || rest.id}
                {...rest}
                onChange={handleChange}
            />
            {progress !== undefined && (
                // <div className="absolute bottom-0 left-0 w-full h-1 bg-gray-300 rounded-b overflow-hidden">
                <div className="opacity-25 absolute bottom-0 left-0 w-full h-full bg-gray-300 rounded overflow-hidden">
                    <div
                        // className="bg-primary h-full"
                        className="bg-primary-dark h-full"
                        style={{
                            width: `${progress * 100}%`,
                            transition: 'width 0.2s'
                        }}
                    ></div>
                </div>
            )}
        </div>
    );
};

export default FileInput;
