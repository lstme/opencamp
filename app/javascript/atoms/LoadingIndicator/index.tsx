import cx from 'classnames';

interface IProps {
    className?: string;
    large?: boolean;
    wrapperClassname?: string;
}

const LoadingIndicator = ({ className, large, wrapperClassname }: IProps) => {
    const loader = (
        <i
            className={cx(className, 'fa fa-spinner fa-spin', {
                'text-3xl': large
            })}
        />
    );

    if (large) {
        return <div className={cx(wrapperClassname, 'overflow-hidden flex justify-center items-center')}>{loader}</div>;
    }

    return loader;
};

export default LoadingIndicator;

export const PageLoadingIndicator = (props: IProps) => (
    <LoadingIndicator {...props} large wrapperClassname={cx(props.wrapperClassname, 'mt-8')} />
);
