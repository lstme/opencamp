import cx from 'classnames';
import * as React from 'react';

import Field from '../Field';

interface IProps extends React.InputHTMLAttributes<HTMLInputElement> {
    wrapperClassName?: string;
    labelClassName?: string;
    hint?: React.ReactNode;
    id?: string;
    label?: string;
    loading?: boolean;
    placeholder?: string;
    value?: string;
}

const Input = React.forwardRef<HTMLInputElement, IProps>(
    ({ className, hint, id, label, loading, wrapperClassName, labelClassName, ...rest }, ref) => (
        <Field
            className={wrapperClassName}
            hint={hint}
            id={id}
            labelClassName={labelClassName}
            label={label}
            loading={loading}
        >
            <input
                id={id}
                name={rest.name || id}
                className={cx('input', className)}
                readOnly={!rest.onChange}
                ref={ref}
                {...rest}
            />
        </Field>
    )
);

export default Input;
