import { ApolloError, isApolloError } from 'apollo-client';
import Message from '../Message';

interface IProps {
    className?: string;
    error?: ApolloError | Error;
}

const ErrorDisplay = ({ className, error }: IProps) => {
    if (!error) return null;

    let message = error.message;

    if (isApolloError(error)) {
        if (error.graphQLErrors && error.graphQLErrors.length > 0) {
            message = error.graphQLErrors.map(e => e.message).join('\n');
        }

        if (error.networkError) {
            message = error.networkError.message;
        }
    }

    return <Message className={className} message={message} variant="alert" />;
};

export default ErrorDisplay;
