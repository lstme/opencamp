import cx from 'classnames';

interface IProps {
    className?: string;
    checked?: boolean;
    onChange?: (checked: boolean) => void;
    label?: string;
}

const Radio = ({ checked, className, onChange, label }: IProps) => {
    return (
        <label className={cx('radio', className)}>
            <input
                className={cx({ 'mr-2': label })}
                type="radio"
                checked={checked}
                onChange={() => onChange && onChange(!checked)}
            />
            {label && <span className="text-sm">{label}</span>}
        </label>
    );
};

export default Radio;
