import cx from 'classnames';
import * as React from 'react';

interface Props extends React.TextareaHTMLAttributes<HTMLTextAreaElement> {
    label?: string;
}

const TextareaField: React.FC<Props> = ({ className, id, label, value, ...rest }) => (
    <div className={cx('form-group', className)}>
        <label className="form-label" htmlFor={id}>
            {label}
        </label>
        <textarea id={id} name={id} className="form-control" value={value} {...rest} />
    </div>
);

export default TextareaField;
