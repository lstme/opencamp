import cx from 'classnames';

interface IProps {
    className?: string;
    checked?: boolean;
    onChange?: (checked: boolean) => void;
    label?: string;
    description?: string;
}

const Checkbox = ({ checked, className, onChange, label, description }: IProps) => {
    return (
        <label className={cx('label flex m-0 hover:text-primary cursor-pointer items-center align-top', className)}>
            <input
                className={cx('leading-tight', { 'mr-2': label })}
                type="checkbox"
                checked={checked}
                onChange={() => onChange && onChange(!checked)}
            />
            <div className="flex flex-col">
                {label && <span className="text-sm">{label}</span>}
                {description && <span className="text-sm font-normal">{description}</span>}
            </div>
        </label>
    );
};

export default Checkbox;
