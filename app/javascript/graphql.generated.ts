import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** An ISO 8601-encoded date */
  ISO8601Date: any;
  /** An ISO 8601-encoded datetime */
  ISO8601DateTime: any;
  /** An ISO 8601-encoded date */
  Iso8601Date: any;
  /** Represents untyped JSON */
  JSON: any;
};

export type AttendanceInput = {
  note: Scalars['String'];
  paid_amount: Scalars['Float'];
};

export type AttendanceInterface = {
  attendance_onboarding_requirements: Array<AttendanceOnboardingRequirement>;
  coins: Scalars['Int'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  list_data: Scalars['JSON'];
  note: Scalars['String'];
  paid_amount: Scalars['Float'];
  room: Maybe<Room>;
  room_id: Maybe<Scalars['ID']>;
  should_pay_amount: Scalars['Float'];
  term: Term;
  term_id: Scalars['ID'];
  transactions: Array<Transaction>;
  transactions_count: Scalars['Int'];
  updated_at: Scalars['ISO8601DateTime'];
  user: User;
  user_id: Scalars['ID'];
  wallet_id: Maybe<Scalars['Int']>;
  workgroup: Maybe<Workgroup>;
  workgroup_id: Maybe<Scalars['ID']>;
};

export type AttendanceOnboardingRequirement = {
  __typename?: 'AttendanceOnboardingRequirement';
  attendance: AttendanceInterface;
  created_at: Scalars['ISO8601DateTime'];
  data: Scalars['JSON'];
  id: Scalars['ID'];
  onboarding_requirement_id: Scalars['ID'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type AttendanceOnboardingRequirementInput = {
  data: Scalars['JSON'];
  onboarding_requirement_id: Scalars['ID'];
};

export type Avatar = {
  __typename?: 'Avatar';
  /** {:convert=>:jpg} */
  full_url: Scalars['String'];
  /** {:resize_to_limit=>[48, 48], :convert=>:jpg} */
  icon_url: Scalars['String'];
  id: Scalars['ID'];
  /** {:resize_to_limit=>[1024, 1024], :convert=>:jpg} */
  medium_url: Scalars['String'];
  /** {:resize_to_limit=>[256, 256], :convert=>:jpg} */
  thumb_url: Scalars['String'];
};

export type ChangeAttendanceRoomResult = {
  __typename?: 'ChangeAttendanceRoomResult';
  attendance: AttendanceInterface;
  from_room: Maybe<Room>;
  to_room: Maybe<Room>;
};

export type ChangeAttendanceWorkgroupResult = {
  __typename?: 'ChangeAttendanceWorkgroupResult';
  attendance: AttendanceInterface;
  from_workgroup: Maybe<Workgroup>;
  to_workgroup: Maybe<Workgroup>;
};

export type CheckboxField = FieldInterface & {
  __typename?: 'CheckboxField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type ChildAttendance = AttendanceInterface & {
  __typename?: 'ChildAttendance';
  attendance_onboarding_requirements: Array<AttendanceOnboardingRequirement>;
  coins: Scalars['Int'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  list_data: Scalars['JSON'];
  note: Scalars['String'];
  paid_amount: Scalars['Float'];
  room: Maybe<Room>;
  room_id: Maybe<Scalars['ID']>;
  should_pay_amount: Scalars['Float'];
  term: Term;
  term_id: Scalars['ID'];
  transactions: Array<Transaction>;
  transactions_count: Scalars['Int'];
  updated_at: Scalars['ISO8601DateTime'];
  user: User;
  user_id: Scalars['ID'];
  wallet_id: Maybe<Scalars['Int']>;
  workgroup: Maybe<Workgroup>;
  workgroup_id: Maybe<Scalars['ID']>;
};

export type CoverPhoto = {
  __typename?: 'CoverPhoto';
  id: Scalars['ID'];
  /** {:resize_to_limit=>[256, 256]} */
  thumb_url: Scalars['String'];
};

export type DateField = FieldInterface & {
  __typename?: 'DateField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type DesignatedHead = {
  __typename?: 'DesignatedHead';
  avatar: Maybe<Avatar>;
  created_at: Scalars['ISO8601DateTime'];
  full_name: Scalars['String'];
  id: Scalars['ID'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type DesignatedRoom = {
  __typename?: 'DesignatedRoom';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type FieldInput = {
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  type: Scalars['String'];
};

export type FieldInterface = {
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type InstructorAttendance = AttendanceInterface & {
  __typename?: 'InstructorAttendance';
  attendance_onboarding_requirements: Array<AttendanceOnboardingRequirement>;
  coins: Scalars['Int'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  list_data: Scalars['JSON'];
  note: Scalars['String'];
  paid_amount: Scalars['Float'];
  room: Maybe<Room>;
  room_id: Maybe<Scalars['ID']>;
  should_pay_amount: Scalars['Float'];
  term: Term;
  term_id: Scalars['ID'];
  transactions: Array<Transaction>;
  transactions_count: Scalars['Int'];
  updated_at: Scalars['ISO8601DateTime'];
  user: User;
  user_id: Scalars['ID'];
  wallet_id: Maybe<Scalars['Int']>;
  workgroup: Maybe<Workgroup>;
  workgroup_id: Maybe<Scalars['ID']>;
};

export type Label = {
  __typename?: 'Label';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  label_group: LabelGroup;
  name: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type LabelGroup = {
  __typename?: 'LabelGroup';
  color: Scalars['String'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  labels: Array<Label>;
  name: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type List = {
  __typename?: 'List';
  created_at: Scalars['ISO8601DateTime'];
  data: Array<ListDataResult>;
  fields: Array<FieldInterface>;
  id: Scalars['ID'];
  include_children: Scalars['Boolean'];
  include_instructors: Scalars['Boolean'];
  name: Scalars['String'];
  show_age: Scalars['Boolean'];
  show_avatar: Scalars['Boolean'];
  show_name: Scalars['Boolean'];
  show_room: Scalars['Boolean'];
  show_workgroup: Scalars['Boolean'];
  slug: Scalars['String'];
  sort_by: ListSortByEnum;
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type ListDataResult = {
  __typename?: 'ListDataResult';
  attendance: AttendanceInterface;
  id: Scalars['ID'];
  list: List;
  list_data: Scalars['JSON'];
};

export type ListInput = {
  fields: Array<FieldInput>;
  include_children: Scalars['Boolean'];
  include_instructors: Scalars['Boolean'];
  name: Scalars['String'];
  show_age: Scalars['Boolean'];
  show_avatar: Scalars['Boolean'];
  show_name: Scalars['Boolean'];
  show_room: Scalars['Boolean'];
  show_workgroup: Scalars['Boolean'];
  slug: Scalars['String'];
  sort_by: ListSortByEnum;
  term_id: Scalars['ID'];
};

export enum ListSortByEnum {
  Age = 'age',
  Name = 'name',
  Room = 'room',
  Workgroup = 'workgroup'
}

export type NumberField = FieldInterface & {
  __typename?: 'NumberField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type OnboardingRequirement = {
  __typename?: 'OnboardingRequirement';
  created_at: Scalars['ISO8601DateTime'];
  description: Scalars['String'];
  id: Scalars['ID'];
  inputs: Array<OnboardingRequirementInput>;
  position: Scalars['Int'];
  term: Term;
  title: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type OnboardingRequirementInput = {
  __typename?: 'OnboardingRequirementInput';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  key: Scalars['String'];
  label: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type Project = {
  __typename?: 'Project';
  capacity: Scalars['Int'];
  category: Scalars['String'];
  created_at: Scalars['ISO8601DateTime'];
  description: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  prerequisites: Maybe<Scalars['String']>;
  project_alias: Scalars['String'];
  project_attendances: Array<ProjectAttendance>;
  slots: Scalars['Int'];
  teacher: User;
  term: Term;
  term_id: Scalars['ID'];
  title: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type ProjectAttendance = {
  __typename?: 'ProjectAttendance';
  attendance: AttendanceInterface;
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  project: Project;
  updated_at: Scalars['ISO8601DateTime'];
};

export type Room = {
  __typename?: 'Room';
  attendances: Array<AttendanceInterface>;
  attendances_count: Scalars['Int'];
  capacity: Scalars['Int'];
  child_attendances: Array<ChildAttendance>;
  child_attendances_count: Scalars['Int'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  instructor_attendances: Array<InstructorAttendance>;
  instructor_attendances_count: Scalars['Int'];
  instructor_only: Scalars['Boolean'];
  name: Scalars['String'];
  note: Scalars['String'];
  room_scores: Array<RoomScore>;
  score: Scalars['Int'];
  term: Term;
  term_id: Scalars['ID'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type RoomScore = {
  __typename?: 'RoomScore';
  created_at: Scalars['ISO8601DateTime'];
  date: Scalars['Iso8601Date'];
  id: Scalars['ID'];
  room: Room;
  score: Scalars['Int'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type RootMutation = {
  __typename?: 'RootMutation';
  change_attendance_room: ChangeAttendanceRoomResult;
  change_attendance_workgroup: ChangeAttendanceWorkgroupResult;
  change_room: Maybe<Room>;
  change_workgroup: Maybe<Workgroup>;
  create_list: Maybe<List>;
  create_project_attendance: Maybe<ProjectAttendance>;
  create_transactions: Array<Transaction>;
  delete_project_attendance: Maybe<ProjectAttendance>;
  delete_transactions: Array<AttendanceInterface>;
  update_attendance: Maybe<AttendanceInterface>;
  update_attendance_onboarding_requirements: AttendanceInterface;
  update_attendances_list_data: Array<AttendanceInterface>;
  update_timetable: Maybe<Timetable>;
  update_user: Maybe<User>;
};


export type RootMutationChange_Attendance_RoomArgs = {
  attendance_id: Scalars['ID'];
  room_id: InputMaybe<Scalars['ID']>;
};


export type RootMutationChange_Attendance_WorkgroupArgs = {
  attendance_id: Scalars['ID'];
  workgroup_id: InputMaybe<Scalars['ID']>;
};


export type RootMutationChange_RoomArgs = {
  note: Scalars['String'];
  room_id: Scalars['ID'];
};


export type RootMutationChange_WorkgroupArgs = {
  note: Scalars['String'];
  workgroup_id: Scalars['ID'];
};


export type RootMutationCreate_ListArgs = {
  list_input: ListInput;
};


export type RootMutationCreate_Project_AttendanceArgs = {
  project_id: Scalars['ID'];
};


export type RootMutationCreate_TransactionsArgs = {
  amount: Scalars['Int'];
  attendance_ids: Array<Scalars['ID']>;
  note: Scalars['String'];
};


export type RootMutationDelete_Project_AttendanceArgs = {
  project_id: Scalars['ID'];
};


export type RootMutationDelete_TransactionsArgs = {
  transaction_ids: Array<Scalars['ID']>;
};


export type RootMutationUpdate_AttendanceArgs = {
  attendance_id: Scalars['ID'];
  attributes: InputMaybe<AttendanceInput>;
};


export type RootMutationUpdate_Attendance_Onboarding_RequirementsArgs = {
  attendance_id: Scalars['ID'];
  onboarding_requirements: Array<AttendanceOnboardingRequirementInput>;
};


export type RootMutationUpdate_Attendances_List_DataArgs = {
  attendance_ids: Array<Scalars['ID']>;
  list_data: Scalars['JSON'];
  list_id: Scalars['ID'];
};


export type RootMutationUpdate_TimetableArgs = {
  term_id: Scalars['ID'];
  timetable_id: InputMaybe<Scalars['ID']>;
  timetable_input: TimetableInput;
};


export type RootMutationUpdate_UserArgs = {
  attributes: UserInput;
  user_id: Scalars['ID'];
};

export type RootQuery = {
  __typename?: 'RootQuery';
  attendance: Maybe<AttendanceInterface>;
  find_attendance: Maybe<AttendanceInterface>;
  list: Maybe<List>;
  me: Maybe<User>;
  room: Maybe<Room>;
  selectable_terms: Array<Term>;
  term: Maybe<Term>;
};


export type RootQueryAttendanceArgs = {
  id: Scalars['ID'];
};


export type RootQueryFind_AttendanceArgs = {
  user_id: Scalars['ID'];
};


export type RootQueryListArgs = {
  id: Scalars['ID'];
};


export type RootQueryRoomArgs = {
  id: Scalars['ID'];
};


export type RootQueryTermArgs = {
  id: Scalars['ID'];
};

export type Subscription = {
  __typename?: 'Subscription';
  list_data_result_was_changed: Array<ListDataResult>;
};


export type SubscriptionList_Data_Result_Was_ChangedArgs = {
  id: Scalars['ID'];
};

export type Term = {
  __typename?: 'Term';
  address: Maybe<Scalars['String']>;
  attendances: Array<AttendanceInterface>;
  cover_photo: Maybe<CoverPhoto>;
  created_at: Scalars['ISO8601DateTime'];
  current_attendance: Maybe<AttendanceInterface>;
  ends_at: Maybe<Scalars['Iso8601Date']>;
  id: Scalars['ID'];
  instructor_attendances: Array<InstructorAttendance>;
  lists: Maybe<Array<List>>;
  name: Scalars['String'];
  onboarding_requirements: Array<OnboardingRequirement>;
  phase: TermPhaseTypeEnum;
  project_signup_enabled: Scalars['Boolean'];
  projects: Array<Project>;
  rooms: Array<Room>;
  starts_at: Maybe<Scalars['Iso8601Date']>;
  timetable: Maybe<Timetable>;
  timetables: Array<Timetable>;
  updated_at: Scalars['ISO8601DateTime'];
  workgroups: Array<Workgroup>;
};


export type TermTimetableArgs = {
  key: InputMaybe<Scalars['String']>;
};

export enum TermPhaseTypeEnum {
  Created = 'created',
  Ended = 'ended',
  InProgress = 'in_progress',
  OffBoarding = 'off_boarding',
  Onboarding = 'onboarding',
  RegistrationsOpen = 'registrations_open'
}

export type TextField = FieldInterface & {
  __typename?: 'TextField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type TimeField = FieldInterface & {
  __typename?: 'TimeField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type TimerField = FieldInterface & {
  __typename?: 'TimerField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type Timeslot = {
  __typename?: 'Timeslot';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  is_highlighted: Scalars['Boolean'];
  items: Array<TimeslotItem>;
  label: Scalars['String'];
  time: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type TimeslotInput = {
  is_highlighted: Scalars['Boolean'];
  items: Array<TimeslotItemInput>;
  label: Scalars['String'];
  time: Scalars['String'];
};

export type TimeslotItem = {
  __typename?: 'TimeslotItem';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  label: Maybe<Scalars['String']>;
  title: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
};

export type TimeslotItemInput = {
  label: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type TimestampField = FieldInterface & {
  __typename?: 'TimestampField';
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  settings: Scalars['JSON'];
  slug: Scalars['String'];
  term: Term;
  updated_at: Scalars['ISO8601DateTime'];
};

export type Timetable = {
  __typename?: 'Timetable';
  created_at: Scalars['ISO8601DateTime'];
  date: Scalars['Iso8601Date'];
  designated_head: DesignatedHead;
  designated_room: DesignatedRoom;
  id: Scalars['ID'];
  notice: Maybe<Scalars['String']>;
  timeslots: Array<Timeslot>;
  updated_at: Scalars['ISO8601DateTime'];
};

export type TimetableInput = {
  date: Scalars['String'];
  /** Should be id of instructor attendance */
  designated_head_id: Scalars['ID'];
  designated_room_id: Scalars['ID'];
  notice: InputMaybe<Scalars['String']>;
  timeslots: Array<TimeslotInput>;
};

export type Transaction = {
  __typename?: 'Transaction';
  amount: Scalars['Int'];
  attendance: AttendanceInterface;
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  note: Scalars['String'];
  updated_at: Scalars['ISO8601DateTime'];
  user: User;
};

export type User = {
  __typename?: 'User';
  address: Maybe<Scalars['String']>;
  age: Scalars['Int'];
  avatar: Maybe<Avatar>;
  born_at: Scalars['String'];
  created_at: Scalars['ISO8601DateTime'];
  display_name: Scalars['String'];
  email: Scalars['String'];
  first_name: Scalars['String'];
  full_name: Scalars['String'];
  gender: Scalars['String'];
  gender_symbol: Scalars['String'];
  id: Scalars['ID'];
  is_admin: Scalars['Boolean'];
  labels: Array<Label>;
  last_name: Scalars['String'];
  note: Maybe<Scalars['String']>;
  parents_email: Maybe<Scalars['String']>;
  parents_phone: Maybe<Scalars['String']>;
  phone: Maybe<Scalars['String']>;
  school: Maybe<Scalars['String']>;
  search_name: Scalars['String'];
  shirt_size: Maybe<Scalars['String']>;
  updated_at: Scalars['ISO8601DateTime'];
  username: Scalars['String'];
};

export type UserInput = {
  address: InputMaybe<Scalars['String']>;
  born_at: Scalars['ISO8601Date'];
  email: Scalars['String'];
  first_name: Scalars['String'];
  gender: Scalars['String'];
  last_name: Scalars['String'];
  note: Scalars['String'];
  parents_email: InputMaybe<Scalars['String']>;
  parents_phone: InputMaybe<Scalars['String']>;
  phone: InputMaybe<Scalars['String']>;
  school: InputMaybe<Scalars['String']>;
  shirt_size: InputMaybe<Scalars['String']>;
  username: Scalars['String'];
};

export type Workgroup = {
  __typename?: 'Workgroup';
  attendances: Array<AttendanceInterface>;
  attendances_count: Scalars['Int'];
  created_at: Scalars['ISO8601DateTime'];
  id: Scalars['ID'];
  name: Scalars['String'];
  note: Scalars['String'];
  term: Term;
  term_id: Scalars['ID'];
  updated_at: Scalars['ISO8601DateTime'];
};


      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {
    "AttendanceInterface": [
      "ChildAttendance",
      "InstructorAttendance"
    ],
    "FieldInterface": [
      "CheckboxField",
      "DateField",
      "NumberField",
      "TextField",
      "TimeField",
      "TimerField",
      "TimestampField"
    ]
  }
};
      export default result;
    
type Attendance_ChildAttendance_Fragment = { __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null };

type Attendance_InstructorAttendance_Fragment = { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null };

export type AttendanceFragment = Attendance_ChildAttendance_Fragment | Attendance_InstructorAttendance_Fragment;

export type AttendanceTransactionFragment = { __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } };

export type AvatarFragment = { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string };

export type OnboardingRequirementFragment = { __typename?: 'OnboardingRequirement', id: string, position: number, title: string, description: string, inputs: Array<{ __typename?: 'OnboardingRequirementInput', key: string, label: string }> };

export type RoomFragment = { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> };

export type TimetableFragment = { __typename?: 'Timetable', date: any, id: string, notice: string | null, designated_head: { __typename?: 'DesignatedHead', id: string, full_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, designated_room: { __typename?: 'DesignatedRoom', id: string, name: string }, timeslots: Array<{ __typename?: 'Timeslot', id: string, is_highlighted: boolean, label: string, time: string, items: Array<{ __typename?: 'TimeslotItem', id: string, title: string, label: string | null }> }> };

export type UserFragment = { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null };

export type WorkgroupFragment = { __typename?: 'Workgroup', id: string, name: string, attendances_count: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> };

export type CreateTransactionsMutationVariables = Exact<{
  attendanceIds: Array<Scalars['ID']> | Scalars['ID'];
  amount: Scalars['Int'];
  note: Scalars['String'];
}>;


export type CreateTransactionsMutation = { __typename?: 'RootMutation', create_transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, attendance: { __typename?: 'ChildAttendance', id: string, coins: number } | { __typename?: 'InstructorAttendance', id: string, coins: number } }> };

export type DeleteTransactionsMutationVariables = Exact<{
  ids: Array<Scalars['ID']> | Scalars['ID'];
}>;


export type DeleteTransactionsMutation = { __typename?: 'RootMutation', delete_transactions: Array<{ __typename?: 'ChildAttendance', id: string, coins: number } | { __typename?: 'InstructorAttendance', id: string, coins: number }> };

export type AttendanceDetailQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type AttendanceDetailQuery = { __typename?: 'RootQuery', attendance: { __typename?: 'ChildAttendance', id: string, coins: number, wallet_id: number | null, user: { __typename?: 'User', id: string, age: number, born_at: string, first_name: string, gender_symbol: string, last_name: string, username: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } | { __typename?: 'InstructorAttendance', id: string, coins: number, wallet_id: number | null, user: { __typename?: 'User', id: string, age: number, born_at: string, first_name: string, gender_symbol: string, last_name: string, username: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } | null };

export type AttendanceSelectUsersQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type AttendanceSelectUsersQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null };

export type CreateDataListMutationVariables = Exact<{
  input: ListInput;
}>;


export type CreateDataListMutation = { __typename?: 'RootMutation', create_list: { __typename?: 'List', id: string } | null };

export type ChangeRoomMutationVariables = Exact<{
  room_id: Scalars['ID'];
  note: Scalars['String'];
}>;


export type ChangeRoomMutation = { __typename?: 'RootMutation', change_room: { __typename?: 'Room', id: string, note: string } | null };

export type RoomScoresQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type RoomScoresQuery = { __typename?: 'RootQuery', room: { __typename?: 'Room', id: string, name: string, score: number, term: { __typename?: 'Term', name: string }, room_scores: Array<{ __typename?: 'RoomScore', id: string, date: any, score: number }> } | null };

export type TimetableQueryVariables = Exact<{
  key: InputMaybe<Scalars['String']>;
  termId: Scalars['ID'];
}>;


export type TimetableQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, timetable: { __typename?: 'Timetable', date: any, id: string, notice: string | null, designated_head: { __typename?: 'DesignatedHead', id: string, full_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, designated_room: { __typename?: 'DesignatedRoom', id: string, name: string }, timeslots: Array<{ __typename?: 'Timeslot', id: string, is_highlighted: boolean, label: string, time: string, items: Array<{ __typename?: 'TimeslotItem', id: string, title: string, label: string | null }> }> } | null } | null };

export type UpdateTimetableMutationVariables = Exact<{
  termId: Scalars['ID'];
  timetableId: InputMaybe<Scalars['ID']>;
  timetableInput: TimetableInput;
}>;


export type UpdateTimetableMutation = { __typename?: 'RootMutation', update_timetable: { __typename?: 'Timetable', date: any, id: string, notice: string | null, designated_head: { __typename?: 'DesignatedHead', id: string, full_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, designated_room: { __typename?: 'DesignatedRoom', id: string, name: string }, timeslots: Array<{ __typename?: 'Timeslot', id: string, is_highlighted: boolean, label: string, time: string, items: Array<{ __typename?: 'TimeslotItem', id: string, title: string, label: string | null }> }> } | null };

export type FormOptionsQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type FormOptionsQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, timetables: Array<{ __typename?: 'Timetable', date: any, id: string, designated_head: { __typename?: 'DesignatedHead', id: string, full_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, designated_room: { __typename?: 'DesignatedRoom', id: string, name: string } }>, instructor_attendances: Array<{ __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null } }>, rooms: Array<{ __typename?: 'Room', id: string, name: string }> } | null };

export type ChangeWorkgroupMutationVariables = Exact<{
  workgroup_id: Scalars['ID'];
  note: Scalars['String'];
}>;


export type ChangeWorkgroupMutation = { __typename?: 'RootMutation', change_workgroup: { __typename?: 'Workgroup', id: string, note: string } | null };

export type CoinsAttendancesQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type CoinsAttendancesQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, attendances: Array<{ __typename?: 'ChildAttendance', coins: number, wallet_id: number | null, id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', coins: number, wallet_id: number | null, id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null };

export type UpdateAttendancesListDataMutationVariables = Exact<{
  ids: Array<Scalars['ID']> | Scalars['ID'];
  listId: Scalars['ID'];
  listData: Scalars['JSON'];
}>;


export type UpdateAttendancesListDataMutation = { __typename?: 'RootMutation', update_attendances_list_data: Array<{ __typename?: 'ChildAttendance', id: string, list_data: any } | { __typename?: 'InstructorAttendance', id: string, list_data: any }> };

export type DataListQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type DataListQuery = { __typename?: 'RootQuery', list: { __typename?: 'List', id: string, name: string, slug: string, include_children: boolean, include_instructors: boolean, show_age: boolean, show_avatar: boolean, show_name: boolean, show_room: boolean, show_workgroup: boolean, fields: Array<{ __typename?: 'CheckboxField', id: string, name: string, settings: any, slug: string } | { __typename?: 'DateField', id: string, name: string, settings: any, slug: string } | { __typename?: 'NumberField', id: string, name: string, settings: any, slug: string } | { __typename?: 'TextField', id: string, name: string, settings: any, slug: string } | { __typename?: 'TimeField', id: string, name: string, settings: any, slug: string } | { __typename?: 'TimerField', id: string, name: string, settings: any, slug: string } | { __typename?: 'TimestampField', id: string, name: string, settings: any, slug: string }>, data: Array<{ __typename?: 'ListDataResult', id: string, attendance: { __typename?: 'ChildAttendance', id: string, list_data: any, user: { __typename?: 'User', id: string, age: number, born_at: string, first_name: string, last_name: string, search_name: string, display_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, room: { __typename?: 'Room', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, list_data: any, user: { __typename?: 'User', id: string, age: number, born_at: string, first_name: string, last_name: string, search_name: string, display_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, room: { __typename?: 'Room', id: string, name: string } | null } }> } | null };

export type ListChangedSubscriptionVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ListChangedSubscription = { __typename?: 'Subscription', list_data_result_was_changed: Array<{ __typename?: 'ListDataResult', id: string, attendance: { __typename?: 'ChildAttendance', id: string, list_data: any } | { __typename?: 'InstructorAttendance', id: string, list_data: any } }> };

export type DataListsQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type DataListsQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, lists: Array<{ __typename?: 'List', id: string, name: string, slug: string, fields: Array<{ __typename?: 'CheckboxField', id: string, name: string, slug: string, settings: any } | { __typename?: 'DateField', id: string, name: string, slug: string, settings: any } | { __typename?: 'NumberField', id: string, name: string, slug: string, settings: any } | { __typename?: 'TextField', id: string, name: string, slug: string, settings: any } | { __typename?: 'TimeField', id: string, name: string, slug: string, settings: any } | { __typename?: 'TimerField', id: string, name: string, slug: string, settings: any } | { __typename?: 'TimestampField', id: string, name: string, slug: string, settings: any }> }> | null } | null };

export type HomeUserFragment = { __typename?: 'User', id: string, first_name: string, last_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null };

type HomeAttendance_ChildAttendance_Fragment = { __typename?: 'ChildAttendance', id: string, coins: number, wallet_id: number | null, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null };

type HomeAttendance_InstructorAttendance_Fragment = { __typename?: 'InstructorAttendance', id: string, coins: number, wallet_id: number | null, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null };

export type HomeAttendanceFragment = HomeAttendance_ChildAttendance_Fragment | HomeAttendance_InstructorAttendance_Fragment;

export type HomeQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type HomeQuery = { __typename?: 'RootQuery', me: { __typename?: 'User', id: string, first_name: string, last_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null } | null, term: { __typename?: 'Term', id: string, current_attendance: { __typename?: 'ChildAttendance', id: string, coins: number, wallet_id: number | null, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } | { __typename?: 'InstructorAttendance', id: string, coins: number, wallet_id: number | null, transactions: Array<{ __typename?: 'Transaction', id: string, amount: number, created_at: any, note: string, user: { __typename?: 'User', id: string, full_name: string } }>, room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } | null } | null };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'RootQuery', me: { __typename?: 'User', id: string, first_name: string, last_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null } | null };

export type OnboardingQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type OnboardingQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, name: string, onboarding_requirements: Array<{ __typename?: 'OnboardingRequirement', id: string, position: number, title: string, description: string, inputs: Array<{ __typename?: 'OnboardingRequirementInput', key: string, label: string }> }> } | null };

export type UserDataFragment = { __typename?: 'User', id: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null };

type AttendanceData_ChildAttendance_Fragment = { __typename?: 'ChildAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, user: { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> };

type AttendanceData_InstructorAttendance_Fragment = { __typename?: 'InstructorAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, user: { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> };

export type AttendanceDataFragment = AttendanceData_ChildAttendance_Fragment | AttendanceData_InstructorAttendance_Fragment;

export type FindAttendanceQueryVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type FindAttendanceQuery = { __typename?: 'RootQuery', find_attendance: { __typename?: 'ChildAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } | { __typename?: 'InstructorAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } | null };

export type UpdateAttendanceMutationVariables = Exact<{
  attendanceId: Scalars['ID'];
  attributes: AttendanceInput;
}>;


export type UpdateAttendanceMutation = { __typename?: 'RootMutation', update_attendance: { __typename?: 'ChildAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } | { __typename?: 'InstructorAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } | null };

export type UpdateUserMutationVariables = Exact<{
  userId: Scalars['ID'];
  attributes: UserInput;
}>;


export type UpdateUserMutation = { __typename?: 'RootMutation', update_user: { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null } | null };

export type UpdateAttendanceOnboardingRequirementsMutationVariables = Exact<{
  attendanceId: Scalars['ID'];
  onboarding_requirements: Array<AttendanceOnboardingRequirementInput> | AttendanceOnboardingRequirementInput;
}>;


export type UpdateAttendanceOnboardingRequirementsMutation = { __typename?: 'RootMutation', update_attendance_onboarding_requirements: { __typename?: 'ChildAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } | { __typename?: 'InstructorAttendance', id: string, note: string, paid_amount: number, should_pay_amount: number, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, born_at: string, email: string, first_name: string, last_name: string, gender: string, shirt_size: string | null, username: string, address: string | null, phone: string | null, parents_phone: string | null, school: string | null, note: string | null, parents_email: string | null, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null, attendance_onboarding_requirements: Array<{ __typename?: 'AttendanceOnboardingRequirement', onboarding_requirement_id: string, data: any }> } };

export type PeopleQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type PeopleQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null };

export type ProjectAttendanceFragment = { __typename?: 'ProjectAttendance', id: string, created_at: any, attendance: { __typename?: 'ChildAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } | { __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } };

export type ProjectFragment = { __typename?: 'Project', id: string, title: string, capacity: number, description: string | null, category: string, prerequisites: string | null, project_alias: string, slots: number, teacher: { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, project_attendances: Array<{ __typename?: 'ProjectAttendance', id: string, created_at: any, attendance: { __typename?: 'ChildAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } | { __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } }> };

export type ProjectsQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type ProjectsQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, projects: Array<{ __typename?: 'Project', id: string, title: string, capacity: number, description: string | null, category: string, prerequisites: string | null, project_alias: string, slots: number, teacher: { __typename?: 'User', id: string, display_name: string, full_name: string, gender_symbol: string, age: number, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, project_attendances: Array<{ __typename?: 'ProjectAttendance', id: string, created_at: any, attendance: { __typename?: 'ChildAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } | { __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } }> }> } | null };

export type CreateProjectAttendanceMutationVariables = Exact<{
  projectId: Scalars['ID'];
}>;


export type CreateProjectAttendanceMutation = { __typename?: 'RootMutation', create_project_attendance: { __typename?: 'ProjectAttendance', id: string, created_at: any, attendance: { __typename?: 'ChildAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } | { __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } } | null };

export type DeleteProjectAttendanceMutationVariables = Exact<{
  projectId: Scalars['ID'];
}>;


export type DeleteProjectAttendanceMutation = { __typename?: 'RootMutation', delete_project_attendance: { __typename?: 'ProjectAttendance', id: string, created_at: any, attendance: { __typename?: 'ChildAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } | { __typename?: 'InstructorAttendance', id: string, user: { __typename?: 'User', id: string, full_name: string } } } | null };

export type ChangeAttendanceRoomMutationVariables = Exact<{
  id: Scalars['ID'];
  roomId: InputMaybe<Scalars['ID']>;
}>;


export type ChangeAttendanceRoomMutation = { __typename?: 'RootMutation', change_attendance_room: { __typename?: 'ChangeAttendanceRoomResult', attendance: { __typename?: 'ChildAttendance', id: string, room_id: string | null, room: { __typename?: 'Room', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room_id: string | null, room: { __typename?: 'Room', id: string, name: string } | null }, from_room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null, to_room: { __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } };

export type TermRoomsQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type TermRoomsQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }>, rooms: Array<{ __typename?: 'Room', id: string, name: string, attendances_count: number, instructor_only: boolean, score: number, capacity: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> }> } | null };

export type SelectableTermFragment = { __typename?: 'Term', id: string, name: string, starts_at: any | null, ends_at: any | null, phase: TermPhaseTypeEnum, address: string | null, cover_photo: { __typename?: 'CoverPhoto', thumb_url: string } | null };

export type TermsQueryVariables = Exact<{ [key: string]: never; }>;


export type TermsQuery = { __typename?: 'RootQuery', selectable_terms: Array<{ __typename?: 'Term', id: string, name: string, starts_at: any | null, ends_at: any | null, phase: TermPhaseTypeEnum, address: string | null, cover_photo: { __typename?: 'CoverPhoto', thumb_url: string } | null }> };

export type ChangeAttendanceWorkgroupMutationVariables = Exact<{
  id: Scalars['ID'];
  workgroupId: InputMaybe<Scalars['ID']>;
}>;


export type ChangeAttendanceWorkgroupMutation = { __typename?: 'RootMutation', change_attendance_workgroup: { __typename?: 'ChangeAttendanceWorkgroupResult', attendance: { __typename?: 'ChildAttendance', id: string, workgroup_id: string | null, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, workgroup_id: string | null, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }, from_workgroup: { __typename?: 'Workgroup', id: string, name: string, attendances_count: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null, to_workgroup: { __typename?: 'Workgroup', id: string, name: string, attendances_count: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> } | null } };

export type TermWorkgroupsQueryVariables = Exact<{
  termId: Scalars['ID'];
}>;


export type TermWorkgroupsQuery = { __typename?: 'RootQuery', term: { __typename?: 'Term', id: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }>, workgroups: Array<{ __typename?: 'Workgroup', id: string, name: string, attendances_count: number, note: string, attendances: Array<{ __typename?: 'ChildAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null } | { __typename?: 'InstructorAttendance', id: string, room: { __typename?: 'Room', id: string, name: string } | null, user: { __typename?: 'User', id: string, full_name: string, gender_symbol: string, age: number, display_name: string, search_name: string, avatar: { __typename?: 'Avatar', id: string, icon_url: string, medium_url: string, thumb_url: string } | null }, workgroup: { __typename?: 'Workgroup', id: string, name: string } | null }> }> } | null };

export const OnboardingRequirementFragmentDoc = gql`
    fragment OnboardingRequirement on OnboardingRequirement {
  id
  position
  title
  description
  inputs {
    key
    label
  }
}
    `;
export const AvatarFragmentDoc = gql`
    fragment Avatar on Avatar {
  id
  icon_url
  medium_url
  thumb_url
}
    `;
export const TimetableFragmentDoc = gql`
    fragment Timetable on Timetable {
  date
  designated_head {
    id
    full_name
    avatar {
      ...Avatar
    }
  }
  designated_room {
    id
    name
  }
  id
  notice
  timeslots {
    id
    is_highlighted
    items {
      id
      title
      label
    }
    label
    time
  }
}
    ${AvatarFragmentDoc}`;
export const AttendanceFragmentDoc = gql`
    fragment Attendance on AttendanceInterface {
  id
  room {
    id
    name
  }
  user {
    id
    full_name
    gender_symbol
    age
    display_name
    search_name
    avatar {
      ...Avatar
    }
  }
  workgroup {
    id
    name
  }
}
    ${AvatarFragmentDoc}`;
export const WorkgroupFragmentDoc = gql`
    fragment Workgroup on Workgroup {
  id
  name
  attendances_count
  attendances {
    ...Attendance
  }
  note
}
    ${AttendanceFragmentDoc}`;
export const HomeUserFragmentDoc = gql`
    fragment HomeUser on User {
  id
  first_name
  last_name
  gender_symbol
  age
  avatar {
    ...Avatar
  }
}
    ${AvatarFragmentDoc}`;
export const AttendanceTransactionFragmentDoc = gql`
    fragment AttendanceTransaction on Transaction {
  id
  amount
  created_at
  note
  user {
    id
    full_name
  }
}
    `;
export const RoomFragmentDoc = gql`
    fragment Room on Room {
  id
  name
  attendances_count
  instructor_only
  attendances {
    ...Attendance
  }
  score
  capacity
  note
}
    ${AttendanceFragmentDoc}`;
export const HomeAttendanceFragmentDoc = gql`
    fragment HomeAttendance on AttendanceInterface {
  id
  coins
  wallet_id
  transactions {
    ...AttendanceTransaction
  }
  room {
    ...Room
  }
}
    ${AttendanceTransactionFragmentDoc}
${RoomFragmentDoc}`;
export const UserFragmentDoc = gql`
    fragment User on User {
  id
  avatar {
    ...Avatar
  }
  display_name
  full_name
  gender_symbol
  age
}
    ${AvatarFragmentDoc}`;
export const UserDataFragmentDoc = gql`
    fragment UserData on User {
  id
  born_at
  email
  first_name
  last_name
  gender
  shirt_size
  username
  address
  phone
  parents_phone
  school
  note
  parents_email
  avatar {
    ...Avatar
  }
}
    ${AvatarFragmentDoc}`;
export const AttendanceDataFragmentDoc = gql`
    fragment AttendanceData on AttendanceInterface {
  id
  user {
    ...User
    ...UserData
  }
  note
  paid_amount
  should_pay_amount
  attendance_onboarding_requirements {
    onboarding_requirement_id
    data
  }
}
    ${UserFragmentDoc}
${UserDataFragmentDoc}`;
export const ProjectAttendanceFragmentDoc = gql`
    fragment ProjectAttendance on ProjectAttendance {
  id
  created_at
  attendance {
    id
    user {
      id
      full_name
    }
  }
}
    `;
export const ProjectFragmentDoc = gql`
    fragment Project on Project {
  id
  title
  capacity
  description
  category
  teacher {
    ...User
  }
  prerequisites
  project_alias
  slots
  project_attendances {
    ...ProjectAttendance
  }
}
    ${UserFragmentDoc}
${ProjectAttendanceFragmentDoc}`;
export const SelectableTermFragmentDoc = gql`
    fragment SelectableTerm on Term {
  id
  name
  starts_at
  ends_at
  phase
  address
  cover_photo {
    thumb_url
  }
}
    `;
export const CreateTransactionsDocument = gql`
    mutation CreateTransactions($attendanceIds: [ID!]!, $amount: Int!, $note: String!) {
  create_transactions(
    attendance_ids: $attendanceIds
    amount: $amount
    note: $note
  ) {
    id
    amount
    attendance {
      id
      coins
    }
    created_at
    note
  }
}
    `;
export type CreateTransactionsMutationFn = Apollo.MutationFunction<CreateTransactionsMutation, CreateTransactionsMutationVariables>;

/**
 * __useCreateTransactionsMutation__
 *
 * To run a mutation, you first call `useCreateTransactionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTransactionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTransactionsMutation, { data, loading, error }] = useCreateTransactionsMutation({
 *   variables: {
 *      attendanceIds: // value for 'attendanceIds'
 *      amount: // value for 'amount'
 *      note: // value for 'note'
 *   },
 * });
 */
export function useCreateTransactionsMutation(baseOptions?: Apollo.MutationHookOptions<CreateTransactionsMutation, CreateTransactionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateTransactionsMutation, CreateTransactionsMutationVariables>(CreateTransactionsDocument, options);
      }
export type CreateTransactionsMutationHookResult = ReturnType<typeof useCreateTransactionsMutation>;
export type CreateTransactionsMutationResult = Apollo.MutationResult<CreateTransactionsMutation>;
export type CreateTransactionsMutationOptions = Apollo.BaseMutationOptions<CreateTransactionsMutation, CreateTransactionsMutationVariables>;
export const DeleteTransactionsDocument = gql`
    mutation DeleteTransactions($ids: [ID!]!) {
  delete_transactions(transaction_ids: $ids) {
    id
    coins
  }
}
    `;
export type DeleteTransactionsMutationFn = Apollo.MutationFunction<DeleteTransactionsMutation, DeleteTransactionsMutationVariables>;

/**
 * __useDeleteTransactionsMutation__
 *
 * To run a mutation, you first call `useDeleteTransactionsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTransactionsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTransactionsMutation, { data, loading, error }] = useDeleteTransactionsMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *   },
 * });
 */
export function useDeleteTransactionsMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTransactionsMutation, DeleteTransactionsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteTransactionsMutation, DeleteTransactionsMutationVariables>(DeleteTransactionsDocument, options);
      }
export type DeleteTransactionsMutationHookResult = ReturnType<typeof useDeleteTransactionsMutation>;
export type DeleteTransactionsMutationResult = Apollo.MutationResult<DeleteTransactionsMutation>;
export type DeleteTransactionsMutationOptions = Apollo.BaseMutationOptions<DeleteTransactionsMutation, DeleteTransactionsMutationVariables>;
export const AttendanceDetailDocument = gql`
    query AttendanceDetail($id: ID!) {
  attendance(id: $id) {
    ...HomeAttendance
    user {
      id
      age
      avatar {
        ...Avatar
      }
      born_at
      first_name
      gender_symbol
      last_name
      username
    }
  }
}
    ${HomeAttendanceFragmentDoc}
${AvatarFragmentDoc}`;

/**
 * __useAttendanceDetailQuery__
 *
 * To run a query within a React component, call `useAttendanceDetailQuery` and pass it any options that fit your needs.
 * When your component renders, `useAttendanceDetailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAttendanceDetailQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAttendanceDetailQuery(baseOptions: Apollo.QueryHookOptions<AttendanceDetailQuery, AttendanceDetailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AttendanceDetailQuery, AttendanceDetailQueryVariables>(AttendanceDetailDocument, options);
      }
export function useAttendanceDetailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AttendanceDetailQuery, AttendanceDetailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AttendanceDetailQuery, AttendanceDetailQueryVariables>(AttendanceDetailDocument, options);
        }
export type AttendanceDetailQueryHookResult = ReturnType<typeof useAttendanceDetailQuery>;
export type AttendanceDetailLazyQueryHookResult = ReturnType<typeof useAttendanceDetailLazyQuery>;
export type AttendanceDetailQueryResult = Apollo.QueryResult<AttendanceDetailQuery, AttendanceDetailQueryVariables>;
export const AttendanceSelectUsersDocument = gql`
    query AttendanceSelectUsers($termId: ID!) {
  term(id: $termId) {
    id
    attendances {
      ...Attendance
    }
  }
}
    ${AttendanceFragmentDoc}`;

/**
 * __useAttendanceSelectUsersQuery__
 *
 * To run a query within a React component, call `useAttendanceSelectUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useAttendanceSelectUsersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAttendanceSelectUsersQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useAttendanceSelectUsersQuery(baseOptions: Apollo.QueryHookOptions<AttendanceSelectUsersQuery, AttendanceSelectUsersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<AttendanceSelectUsersQuery, AttendanceSelectUsersQueryVariables>(AttendanceSelectUsersDocument, options);
      }
export function useAttendanceSelectUsersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<AttendanceSelectUsersQuery, AttendanceSelectUsersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<AttendanceSelectUsersQuery, AttendanceSelectUsersQueryVariables>(AttendanceSelectUsersDocument, options);
        }
export type AttendanceSelectUsersQueryHookResult = ReturnType<typeof useAttendanceSelectUsersQuery>;
export type AttendanceSelectUsersLazyQueryHookResult = ReturnType<typeof useAttendanceSelectUsersLazyQuery>;
export type AttendanceSelectUsersQueryResult = Apollo.QueryResult<AttendanceSelectUsersQuery, AttendanceSelectUsersQueryVariables>;
export const CreateDataListDocument = gql`
    mutation CreateDataList($input: ListInput!) {
  create_list(list_input: $input) {
    id
  }
}
    `;
export type CreateDataListMutationFn = Apollo.MutationFunction<CreateDataListMutation, CreateDataListMutationVariables>;

/**
 * __useCreateDataListMutation__
 *
 * To run a mutation, you first call `useCreateDataListMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDataListMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDataListMutation, { data, loading, error }] = useCreateDataListMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDataListMutation(baseOptions?: Apollo.MutationHookOptions<CreateDataListMutation, CreateDataListMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDataListMutation, CreateDataListMutationVariables>(CreateDataListDocument, options);
      }
export type CreateDataListMutationHookResult = ReturnType<typeof useCreateDataListMutation>;
export type CreateDataListMutationResult = Apollo.MutationResult<CreateDataListMutation>;
export type CreateDataListMutationOptions = Apollo.BaseMutationOptions<CreateDataListMutation, CreateDataListMutationVariables>;
export const ChangeRoomDocument = gql`
    mutation ChangeRoom($room_id: ID!, $note: String!) {
  change_room(room_id: $room_id, note: $note) {
    id
    note
  }
}
    `;
export type ChangeRoomMutationFn = Apollo.MutationFunction<ChangeRoomMutation, ChangeRoomMutationVariables>;

/**
 * __useChangeRoomMutation__
 *
 * To run a mutation, you first call `useChangeRoomMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeRoomMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeRoomMutation, { data, loading, error }] = useChangeRoomMutation({
 *   variables: {
 *      room_id: // value for 'room_id'
 *      note: // value for 'note'
 *   },
 * });
 */
export function useChangeRoomMutation(baseOptions?: Apollo.MutationHookOptions<ChangeRoomMutation, ChangeRoomMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeRoomMutation, ChangeRoomMutationVariables>(ChangeRoomDocument, options);
      }
export type ChangeRoomMutationHookResult = ReturnType<typeof useChangeRoomMutation>;
export type ChangeRoomMutationResult = Apollo.MutationResult<ChangeRoomMutation>;
export type ChangeRoomMutationOptions = Apollo.BaseMutationOptions<ChangeRoomMutation, ChangeRoomMutationVariables>;
export const RoomScoresDocument = gql`
    query RoomScores($id: ID!) {
  room(id: $id) {
    id
    name
    term {
      name
    }
    score
    room_scores {
      id
      date
      score
    }
  }
}
    `;

/**
 * __useRoomScoresQuery__
 *
 * To run a query within a React component, call `useRoomScoresQuery` and pass it any options that fit your needs.
 * When your component renders, `useRoomScoresQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRoomScoresQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRoomScoresQuery(baseOptions: Apollo.QueryHookOptions<RoomScoresQuery, RoomScoresQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<RoomScoresQuery, RoomScoresQueryVariables>(RoomScoresDocument, options);
      }
export function useRoomScoresLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<RoomScoresQuery, RoomScoresQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<RoomScoresQuery, RoomScoresQueryVariables>(RoomScoresDocument, options);
        }
export type RoomScoresQueryHookResult = ReturnType<typeof useRoomScoresQuery>;
export type RoomScoresLazyQueryHookResult = ReturnType<typeof useRoomScoresLazyQuery>;
export type RoomScoresQueryResult = Apollo.QueryResult<RoomScoresQuery, RoomScoresQueryVariables>;
export const TimetableDocument = gql`
    query Timetable($key: String, $termId: ID!) {
  term(id: $termId) {
    id
    timetable(key: $key) {
      ...Timetable
    }
  }
}
    ${TimetableFragmentDoc}`;

/**
 * __useTimetableQuery__
 *
 * To run a query within a React component, call `useTimetableQuery` and pass it any options that fit your needs.
 * When your component renders, `useTimetableQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTimetableQuery({
 *   variables: {
 *      key: // value for 'key'
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useTimetableQuery(baseOptions: Apollo.QueryHookOptions<TimetableQuery, TimetableQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TimetableQuery, TimetableQueryVariables>(TimetableDocument, options);
      }
export function useTimetableLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TimetableQuery, TimetableQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TimetableQuery, TimetableQueryVariables>(TimetableDocument, options);
        }
export type TimetableQueryHookResult = ReturnType<typeof useTimetableQuery>;
export type TimetableLazyQueryHookResult = ReturnType<typeof useTimetableLazyQuery>;
export type TimetableQueryResult = Apollo.QueryResult<TimetableQuery, TimetableQueryVariables>;
export const UpdateTimetableDocument = gql`
    mutation UpdateTimetable($termId: ID!, $timetableId: ID, $timetableInput: TimetableInput!) {
  update_timetable(
    term_id: $termId
    timetable_id: $timetableId
    timetable_input: $timetableInput
  ) {
    ...Timetable
  }
}
    ${TimetableFragmentDoc}`;
export type UpdateTimetableMutationFn = Apollo.MutationFunction<UpdateTimetableMutation, UpdateTimetableMutationVariables>;

/**
 * __useUpdateTimetableMutation__
 *
 * To run a mutation, you first call `useUpdateTimetableMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTimetableMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTimetableMutation, { data, loading, error }] = useUpdateTimetableMutation({
 *   variables: {
 *      termId: // value for 'termId'
 *      timetableId: // value for 'timetableId'
 *      timetableInput: // value for 'timetableInput'
 *   },
 * });
 */
export function useUpdateTimetableMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTimetableMutation, UpdateTimetableMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateTimetableMutation, UpdateTimetableMutationVariables>(UpdateTimetableDocument, options);
      }
export type UpdateTimetableMutationHookResult = ReturnType<typeof useUpdateTimetableMutation>;
export type UpdateTimetableMutationResult = Apollo.MutationResult<UpdateTimetableMutation>;
export type UpdateTimetableMutationOptions = Apollo.BaseMutationOptions<UpdateTimetableMutation, UpdateTimetableMutationVariables>;
export const FormOptionsDocument = gql`
    query FormOptions($termId: ID!) {
  term(id: $termId) {
    id
    timetables {
      date
      id
      designated_head {
        id
        full_name
        avatar {
          ...Avatar
        }
      }
      designated_room {
        id
        name
      }
    }
    instructor_attendances {
      id
      user {
        id
        full_name
        avatar {
          ...Avatar
        }
      }
    }
    rooms {
      id
      name
    }
  }
}
    ${AvatarFragmentDoc}`;

/**
 * __useFormOptionsQuery__
 *
 * To run a query within a React component, call `useFormOptionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useFormOptionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFormOptionsQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useFormOptionsQuery(baseOptions: Apollo.QueryHookOptions<FormOptionsQuery, FormOptionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FormOptionsQuery, FormOptionsQueryVariables>(FormOptionsDocument, options);
      }
export function useFormOptionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FormOptionsQuery, FormOptionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FormOptionsQuery, FormOptionsQueryVariables>(FormOptionsDocument, options);
        }
export type FormOptionsQueryHookResult = ReturnType<typeof useFormOptionsQuery>;
export type FormOptionsLazyQueryHookResult = ReturnType<typeof useFormOptionsLazyQuery>;
export type FormOptionsQueryResult = Apollo.QueryResult<FormOptionsQuery, FormOptionsQueryVariables>;
export const ChangeWorkgroupDocument = gql`
    mutation ChangeWorkgroup($workgroup_id: ID!, $note: String!) {
  change_workgroup(workgroup_id: $workgroup_id, note: $note) {
    id
    note
  }
}
    `;
export type ChangeWorkgroupMutationFn = Apollo.MutationFunction<ChangeWorkgroupMutation, ChangeWorkgroupMutationVariables>;

/**
 * __useChangeWorkgroupMutation__
 *
 * To run a mutation, you first call `useChangeWorkgroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeWorkgroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeWorkgroupMutation, { data, loading, error }] = useChangeWorkgroupMutation({
 *   variables: {
 *      workgroup_id: // value for 'workgroup_id'
 *      note: // value for 'note'
 *   },
 * });
 */
export function useChangeWorkgroupMutation(baseOptions?: Apollo.MutationHookOptions<ChangeWorkgroupMutation, ChangeWorkgroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeWorkgroupMutation, ChangeWorkgroupMutationVariables>(ChangeWorkgroupDocument, options);
      }
export type ChangeWorkgroupMutationHookResult = ReturnType<typeof useChangeWorkgroupMutation>;
export type ChangeWorkgroupMutationResult = Apollo.MutationResult<ChangeWorkgroupMutation>;
export type ChangeWorkgroupMutationOptions = Apollo.BaseMutationOptions<ChangeWorkgroupMutation, ChangeWorkgroupMutationVariables>;
export const CoinsAttendancesDocument = gql`
    query CoinsAttendances($termId: ID!) {
  term(id: $termId) {
    id
    attendances {
      ...Attendance
      coins
      wallet_id
    }
  }
}
    ${AttendanceFragmentDoc}`;

/**
 * __useCoinsAttendancesQuery__
 *
 * To run a query within a React component, call `useCoinsAttendancesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCoinsAttendancesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCoinsAttendancesQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useCoinsAttendancesQuery(baseOptions: Apollo.QueryHookOptions<CoinsAttendancesQuery, CoinsAttendancesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CoinsAttendancesQuery, CoinsAttendancesQueryVariables>(CoinsAttendancesDocument, options);
      }
export function useCoinsAttendancesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CoinsAttendancesQuery, CoinsAttendancesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CoinsAttendancesQuery, CoinsAttendancesQueryVariables>(CoinsAttendancesDocument, options);
        }
export type CoinsAttendancesQueryHookResult = ReturnType<typeof useCoinsAttendancesQuery>;
export type CoinsAttendancesLazyQueryHookResult = ReturnType<typeof useCoinsAttendancesLazyQuery>;
export type CoinsAttendancesQueryResult = Apollo.QueryResult<CoinsAttendancesQuery, CoinsAttendancesQueryVariables>;
export const UpdateAttendancesListDataDocument = gql`
    mutation UpdateAttendancesListData($ids: [ID!]!, $listId: ID!, $listData: JSON!) {
  update_attendances_list_data(
    attendance_ids: $ids
    list_id: $listId
    list_data: $listData
  ) {
    id
    list_data
  }
}
    `;
export type UpdateAttendancesListDataMutationFn = Apollo.MutationFunction<UpdateAttendancesListDataMutation, UpdateAttendancesListDataMutationVariables>;

/**
 * __useUpdateAttendancesListDataMutation__
 *
 * To run a mutation, you first call `useUpdateAttendancesListDataMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAttendancesListDataMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAttendancesListDataMutation, { data, loading, error }] = useUpdateAttendancesListDataMutation({
 *   variables: {
 *      ids: // value for 'ids'
 *      listId: // value for 'listId'
 *      listData: // value for 'listData'
 *   },
 * });
 */
export function useUpdateAttendancesListDataMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAttendancesListDataMutation, UpdateAttendancesListDataMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAttendancesListDataMutation, UpdateAttendancesListDataMutationVariables>(UpdateAttendancesListDataDocument, options);
      }
export type UpdateAttendancesListDataMutationHookResult = ReturnType<typeof useUpdateAttendancesListDataMutation>;
export type UpdateAttendancesListDataMutationResult = Apollo.MutationResult<UpdateAttendancesListDataMutation>;
export type UpdateAttendancesListDataMutationOptions = Apollo.BaseMutationOptions<UpdateAttendancesListDataMutation, UpdateAttendancesListDataMutationVariables>;
export const DataListDocument = gql`
    query DataList($id: ID!) {
  list(id: $id) {
    fields {
      id
      name
      settings
      slug
    }
    id
    name
    slug
    include_children
    include_instructors
    show_age
    show_avatar
    show_name
    show_room
    show_workgroup
    data {
      id
      attendance {
        id
        user {
          id
          age
          avatar {
            ...Avatar
          }
          born_at
          first_name
          last_name
          search_name
          display_name
        }
        workgroup {
          id
          name
        }
        room {
          id
          name
        }
        list_data
      }
    }
  }
}
    ${AvatarFragmentDoc}`;

/**
 * __useDataListQuery__
 *
 * To run a query within a React component, call `useDataListQuery` and pass it any options that fit your needs.
 * When your component renders, `useDataListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDataListQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDataListQuery(baseOptions: Apollo.QueryHookOptions<DataListQuery, DataListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DataListQuery, DataListQueryVariables>(DataListDocument, options);
      }
export function useDataListLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DataListQuery, DataListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DataListQuery, DataListQueryVariables>(DataListDocument, options);
        }
export type DataListQueryHookResult = ReturnType<typeof useDataListQuery>;
export type DataListLazyQueryHookResult = ReturnType<typeof useDataListLazyQuery>;
export type DataListQueryResult = Apollo.QueryResult<DataListQuery, DataListQueryVariables>;
export const ListChangedDocument = gql`
    subscription ListChanged($id: ID!) {
  list_data_result_was_changed(id: $id) {
    id
    attendance {
      id
      list_data
    }
  }
}
    `;

/**
 * __useListChangedSubscription__
 *
 * To run a query within a React component, call `useListChangedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useListChangedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useListChangedSubscription({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useListChangedSubscription(baseOptions: Apollo.SubscriptionHookOptions<ListChangedSubscription, ListChangedSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useSubscription<ListChangedSubscription, ListChangedSubscriptionVariables>(ListChangedDocument, options);
      }
export type ListChangedSubscriptionHookResult = ReturnType<typeof useListChangedSubscription>;
export type ListChangedSubscriptionResult = Apollo.SubscriptionResult<ListChangedSubscription>;
export const DataListsDocument = gql`
    query DataLists($termId: ID!) {
  term(id: $termId) {
    id
    lists {
      id
      name
      slug
      fields {
        id
        name
        slug
        settings
      }
    }
  }
}
    `;

/**
 * __useDataListsQuery__
 *
 * To run a query within a React component, call `useDataListsQuery` and pass it any options that fit your needs.
 * When your component renders, `useDataListsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDataListsQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useDataListsQuery(baseOptions: Apollo.QueryHookOptions<DataListsQuery, DataListsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DataListsQuery, DataListsQueryVariables>(DataListsDocument, options);
      }
export function useDataListsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DataListsQuery, DataListsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DataListsQuery, DataListsQueryVariables>(DataListsDocument, options);
        }
export type DataListsQueryHookResult = ReturnType<typeof useDataListsQuery>;
export type DataListsLazyQueryHookResult = ReturnType<typeof useDataListsLazyQuery>;
export type DataListsQueryResult = Apollo.QueryResult<DataListsQuery, DataListsQueryVariables>;
export const HomeDocument = gql`
    query Home($termId: ID!) {
  me {
    ...HomeUser
  }
  term(id: $termId) {
    id
    current_attendance {
      ...HomeAttendance
    }
  }
}
    ${HomeUserFragmentDoc}
${HomeAttendanceFragmentDoc}`;

/**
 * __useHomeQuery__
 *
 * To run a query within a React component, call `useHomeQuery` and pass it any options that fit your needs.
 * When your component renders, `useHomeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useHomeQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useHomeQuery(baseOptions: Apollo.QueryHookOptions<HomeQuery, HomeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<HomeQuery, HomeQueryVariables>(HomeDocument, options);
      }
export function useHomeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<HomeQuery, HomeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<HomeQuery, HomeQueryVariables>(HomeDocument, options);
        }
export type HomeQueryHookResult = ReturnType<typeof useHomeQuery>;
export type HomeLazyQueryHookResult = ReturnType<typeof useHomeLazyQuery>;
export type HomeQueryResult = Apollo.QueryResult<HomeQuery, HomeQueryVariables>;
export const MeDocument = gql`
    query Me {
  me {
    ...HomeUser
  }
}
    ${HomeUserFragmentDoc}`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const OnboardingDocument = gql`
    query Onboarding($termId: ID!) {
  term(id: $termId) {
    id
    name
    onboarding_requirements {
      ...OnboardingRequirement
    }
  }
}
    ${OnboardingRequirementFragmentDoc}`;

/**
 * __useOnboardingQuery__
 *
 * To run a query within a React component, call `useOnboardingQuery` and pass it any options that fit your needs.
 * When your component renders, `useOnboardingQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOnboardingQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useOnboardingQuery(baseOptions: Apollo.QueryHookOptions<OnboardingQuery, OnboardingQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<OnboardingQuery, OnboardingQueryVariables>(OnboardingDocument, options);
      }
export function useOnboardingLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<OnboardingQuery, OnboardingQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<OnboardingQuery, OnboardingQueryVariables>(OnboardingDocument, options);
        }
export type OnboardingQueryHookResult = ReturnType<typeof useOnboardingQuery>;
export type OnboardingLazyQueryHookResult = ReturnType<typeof useOnboardingLazyQuery>;
export type OnboardingQueryResult = Apollo.QueryResult<OnboardingQuery, OnboardingQueryVariables>;
export const FindAttendanceDocument = gql`
    query FindAttendance($userId: ID!) {
  find_attendance(user_id: $userId) {
    ...Attendance
    ...AttendanceData
  }
}
    ${AttendanceFragmentDoc}
${AttendanceDataFragmentDoc}`;

/**
 * __useFindAttendanceQuery__
 *
 * To run a query within a React component, call `useFindAttendanceQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindAttendanceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindAttendanceQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useFindAttendanceQuery(baseOptions: Apollo.QueryHookOptions<FindAttendanceQuery, FindAttendanceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FindAttendanceQuery, FindAttendanceQueryVariables>(FindAttendanceDocument, options);
      }
export function useFindAttendanceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FindAttendanceQuery, FindAttendanceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FindAttendanceQuery, FindAttendanceQueryVariables>(FindAttendanceDocument, options);
        }
export type FindAttendanceQueryHookResult = ReturnType<typeof useFindAttendanceQuery>;
export type FindAttendanceLazyQueryHookResult = ReturnType<typeof useFindAttendanceLazyQuery>;
export type FindAttendanceQueryResult = Apollo.QueryResult<FindAttendanceQuery, FindAttendanceQueryVariables>;
export const UpdateAttendanceDocument = gql`
    mutation UpdateAttendance($attendanceId: ID!, $attributes: AttendanceInput!) {
  update_attendance(attendance_id: $attendanceId, attributes: $attributes) {
    ...Attendance
    ...AttendanceData
  }
}
    ${AttendanceFragmentDoc}
${AttendanceDataFragmentDoc}`;
export type UpdateAttendanceMutationFn = Apollo.MutationFunction<UpdateAttendanceMutation, UpdateAttendanceMutationVariables>;

/**
 * __useUpdateAttendanceMutation__
 *
 * To run a mutation, you first call `useUpdateAttendanceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAttendanceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAttendanceMutation, { data, loading, error }] = useUpdateAttendanceMutation({
 *   variables: {
 *      attendanceId: // value for 'attendanceId'
 *      attributes: // value for 'attributes'
 *   },
 * });
 */
export function useUpdateAttendanceMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAttendanceMutation, UpdateAttendanceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAttendanceMutation, UpdateAttendanceMutationVariables>(UpdateAttendanceDocument, options);
      }
export type UpdateAttendanceMutationHookResult = ReturnType<typeof useUpdateAttendanceMutation>;
export type UpdateAttendanceMutationResult = Apollo.MutationResult<UpdateAttendanceMutation>;
export type UpdateAttendanceMutationOptions = Apollo.BaseMutationOptions<UpdateAttendanceMutation, UpdateAttendanceMutationVariables>;
export const UpdateUserDocument = gql`
    mutation UpdateUser($userId: ID!, $attributes: UserInput!) {
  update_user(user_id: $userId, attributes: $attributes) {
    ...User
    ...UserData
  }
}
    ${UserFragmentDoc}
${UserDataFragmentDoc}`;
export type UpdateUserMutationFn = Apollo.MutationFunction<UpdateUserMutation, UpdateUserMutationVariables>;

/**
 * __useUpdateUserMutation__
 *
 * To run a mutation, you first call `useUpdateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateUserMutation, { data, loading, error }] = useUpdateUserMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      attributes: // value for 'attributes'
 *   },
 * });
 */
export function useUpdateUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateUserMutation, UpdateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateUserMutation, UpdateUserMutationVariables>(UpdateUserDocument, options);
      }
export type UpdateUserMutationHookResult = ReturnType<typeof useUpdateUserMutation>;
export type UpdateUserMutationResult = Apollo.MutationResult<UpdateUserMutation>;
export type UpdateUserMutationOptions = Apollo.BaseMutationOptions<UpdateUserMutation, UpdateUserMutationVariables>;
export const UpdateAttendanceOnboardingRequirementsDocument = gql`
    mutation UpdateAttendanceOnboardingRequirements($attendanceId: ID!, $onboarding_requirements: [AttendanceOnboardingRequirementInput!]!) {
  update_attendance_onboarding_requirements(
    attendance_id: $attendanceId
    onboarding_requirements: $onboarding_requirements
  ) {
    ...Attendance
    ...AttendanceData
  }
}
    ${AttendanceFragmentDoc}
${AttendanceDataFragmentDoc}`;
export type UpdateAttendanceOnboardingRequirementsMutationFn = Apollo.MutationFunction<UpdateAttendanceOnboardingRequirementsMutation, UpdateAttendanceOnboardingRequirementsMutationVariables>;

/**
 * __useUpdateAttendanceOnboardingRequirementsMutation__
 *
 * To run a mutation, you first call `useUpdateAttendanceOnboardingRequirementsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAttendanceOnboardingRequirementsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateAttendanceOnboardingRequirementsMutation, { data, loading, error }] = useUpdateAttendanceOnboardingRequirementsMutation({
 *   variables: {
 *      attendanceId: // value for 'attendanceId'
 *      onboarding_requirements: // value for 'onboarding_requirements'
 *   },
 * });
 */
export function useUpdateAttendanceOnboardingRequirementsMutation(baseOptions?: Apollo.MutationHookOptions<UpdateAttendanceOnboardingRequirementsMutation, UpdateAttendanceOnboardingRequirementsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateAttendanceOnboardingRequirementsMutation, UpdateAttendanceOnboardingRequirementsMutationVariables>(UpdateAttendanceOnboardingRequirementsDocument, options);
      }
export type UpdateAttendanceOnboardingRequirementsMutationHookResult = ReturnType<typeof useUpdateAttendanceOnboardingRequirementsMutation>;
export type UpdateAttendanceOnboardingRequirementsMutationResult = Apollo.MutationResult<UpdateAttendanceOnboardingRequirementsMutation>;
export type UpdateAttendanceOnboardingRequirementsMutationOptions = Apollo.BaseMutationOptions<UpdateAttendanceOnboardingRequirementsMutation, UpdateAttendanceOnboardingRequirementsMutationVariables>;
export const PeopleDocument = gql`
    query People($termId: ID!) {
  term(id: $termId) {
    id
    attendances {
      ...Attendance
    }
  }
}
    ${AttendanceFragmentDoc}`;

/**
 * __usePeopleQuery__
 *
 * To run a query within a React component, call `usePeopleQuery` and pass it any options that fit your needs.
 * When your component renders, `usePeopleQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePeopleQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function usePeopleQuery(baseOptions: Apollo.QueryHookOptions<PeopleQuery, PeopleQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PeopleQuery, PeopleQueryVariables>(PeopleDocument, options);
      }
export function usePeopleLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PeopleQuery, PeopleQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PeopleQuery, PeopleQueryVariables>(PeopleDocument, options);
        }
export type PeopleQueryHookResult = ReturnType<typeof usePeopleQuery>;
export type PeopleLazyQueryHookResult = ReturnType<typeof usePeopleLazyQuery>;
export type PeopleQueryResult = Apollo.QueryResult<PeopleQuery, PeopleQueryVariables>;
export const ProjectsDocument = gql`
    query projects($termId: ID!) {
  term(id: $termId) {
    id
    projects {
      ...Project
    }
  }
}
    ${ProjectFragmentDoc}`;

/**
 * __useProjectsQuery__
 *
 * To run a query within a React component, call `useProjectsQuery` and pass it any options that fit your needs.
 * When your component renders, `useProjectsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProjectsQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useProjectsQuery(baseOptions: Apollo.QueryHookOptions<ProjectsQuery, ProjectsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ProjectsQuery, ProjectsQueryVariables>(ProjectsDocument, options);
      }
export function useProjectsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ProjectsQuery, ProjectsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ProjectsQuery, ProjectsQueryVariables>(ProjectsDocument, options);
        }
export type ProjectsQueryHookResult = ReturnType<typeof useProjectsQuery>;
export type ProjectsLazyQueryHookResult = ReturnType<typeof useProjectsLazyQuery>;
export type ProjectsQueryResult = Apollo.QueryResult<ProjectsQuery, ProjectsQueryVariables>;
export const CreateProjectAttendanceDocument = gql`
    mutation CreateProjectAttendance($projectId: ID!) {
  create_project_attendance(project_id: $projectId) {
    ...ProjectAttendance
  }
}
    ${ProjectAttendanceFragmentDoc}`;
export type CreateProjectAttendanceMutationFn = Apollo.MutationFunction<CreateProjectAttendanceMutation, CreateProjectAttendanceMutationVariables>;

/**
 * __useCreateProjectAttendanceMutation__
 *
 * To run a mutation, you first call `useCreateProjectAttendanceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateProjectAttendanceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createProjectAttendanceMutation, { data, loading, error }] = useCreateProjectAttendanceMutation({
 *   variables: {
 *      projectId: // value for 'projectId'
 *   },
 * });
 */
export function useCreateProjectAttendanceMutation(baseOptions?: Apollo.MutationHookOptions<CreateProjectAttendanceMutation, CreateProjectAttendanceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateProjectAttendanceMutation, CreateProjectAttendanceMutationVariables>(CreateProjectAttendanceDocument, options);
      }
export type CreateProjectAttendanceMutationHookResult = ReturnType<typeof useCreateProjectAttendanceMutation>;
export type CreateProjectAttendanceMutationResult = Apollo.MutationResult<CreateProjectAttendanceMutation>;
export type CreateProjectAttendanceMutationOptions = Apollo.BaseMutationOptions<CreateProjectAttendanceMutation, CreateProjectAttendanceMutationVariables>;
export const DeleteProjectAttendanceDocument = gql`
    mutation DeleteProjectAttendance($projectId: ID!) {
  delete_project_attendance(project_id: $projectId) {
    ...ProjectAttendance
  }
}
    ${ProjectAttendanceFragmentDoc}`;
export type DeleteProjectAttendanceMutationFn = Apollo.MutationFunction<DeleteProjectAttendanceMutation, DeleteProjectAttendanceMutationVariables>;

/**
 * __useDeleteProjectAttendanceMutation__
 *
 * To run a mutation, you first call `useDeleteProjectAttendanceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteProjectAttendanceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteProjectAttendanceMutation, { data, loading, error }] = useDeleteProjectAttendanceMutation({
 *   variables: {
 *      projectId: // value for 'projectId'
 *   },
 * });
 */
export function useDeleteProjectAttendanceMutation(baseOptions?: Apollo.MutationHookOptions<DeleteProjectAttendanceMutation, DeleteProjectAttendanceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteProjectAttendanceMutation, DeleteProjectAttendanceMutationVariables>(DeleteProjectAttendanceDocument, options);
      }
export type DeleteProjectAttendanceMutationHookResult = ReturnType<typeof useDeleteProjectAttendanceMutation>;
export type DeleteProjectAttendanceMutationResult = Apollo.MutationResult<DeleteProjectAttendanceMutation>;
export type DeleteProjectAttendanceMutationOptions = Apollo.BaseMutationOptions<DeleteProjectAttendanceMutation, DeleteProjectAttendanceMutationVariables>;
export const ChangeAttendanceRoomDocument = gql`
    mutation ChangeAttendanceRoom($id: ID!, $roomId: ID) {
  change_attendance_room(attendance_id: $id, room_id: $roomId) {
    attendance {
      id
      room_id
      room {
        id
        name
      }
    }
    from_room {
      ...Room
    }
    to_room {
      ...Room
    }
  }
}
    ${RoomFragmentDoc}`;
export type ChangeAttendanceRoomMutationFn = Apollo.MutationFunction<ChangeAttendanceRoomMutation, ChangeAttendanceRoomMutationVariables>;

/**
 * __useChangeAttendanceRoomMutation__
 *
 * To run a mutation, you first call `useChangeAttendanceRoomMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeAttendanceRoomMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeAttendanceRoomMutation, { data, loading, error }] = useChangeAttendanceRoomMutation({
 *   variables: {
 *      id: // value for 'id'
 *      roomId: // value for 'roomId'
 *   },
 * });
 */
export function useChangeAttendanceRoomMutation(baseOptions?: Apollo.MutationHookOptions<ChangeAttendanceRoomMutation, ChangeAttendanceRoomMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeAttendanceRoomMutation, ChangeAttendanceRoomMutationVariables>(ChangeAttendanceRoomDocument, options);
      }
export type ChangeAttendanceRoomMutationHookResult = ReturnType<typeof useChangeAttendanceRoomMutation>;
export type ChangeAttendanceRoomMutationResult = Apollo.MutationResult<ChangeAttendanceRoomMutation>;
export type ChangeAttendanceRoomMutationOptions = Apollo.BaseMutationOptions<ChangeAttendanceRoomMutation, ChangeAttendanceRoomMutationVariables>;
export const TermRoomsDocument = gql`
    query TermRooms($termId: ID!) {
  term(id: $termId) {
    id
    attendances {
      ...Attendance
    }
    rooms {
      ...Room
    }
  }
}
    ${AttendanceFragmentDoc}
${RoomFragmentDoc}`;

/**
 * __useTermRoomsQuery__
 *
 * To run a query within a React component, call `useTermRoomsQuery` and pass it any options that fit your needs.
 * When your component renders, `useTermRoomsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTermRoomsQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useTermRoomsQuery(baseOptions: Apollo.QueryHookOptions<TermRoomsQuery, TermRoomsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TermRoomsQuery, TermRoomsQueryVariables>(TermRoomsDocument, options);
      }
export function useTermRoomsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TermRoomsQuery, TermRoomsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TermRoomsQuery, TermRoomsQueryVariables>(TermRoomsDocument, options);
        }
export type TermRoomsQueryHookResult = ReturnType<typeof useTermRoomsQuery>;
export type TermRoomsLazyQueryHookResult = ReturnType<typeof useTermRoomsLazyQuery>;
export type TermRoomsQueryResult = Apollo.QueryResult<TermRoomsQuery, TermRoomsQueryVariables>;
export const TermsDocument = gql`
    query Terms {
  selectable_terms {
    ...SelectableTerm
  }
}
    ${SelectableTermFragmentDoc}`;

/**
 * __useTermsQuery__
 *
 * To run a query within a React component, call `useTermsQuery` and pass it any options that fit your needs.
 * When your component renders, `useTermsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTermsQuery({
 *   variables: {
 *   },
 * });
 */
export function useTermsQuery(baseOptions?: Apollo.QueryHookOptions<TermsQuery, TermsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TermsQuery, TermsQueryVariables>(TermsDocument, options);
      }
export function useTermsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TermsQuery, TermsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TermsQuery, TermsQueryVariables>(TermsDocument, options);
        }
export type TermsQueryHookResult = ReturnType<typeof useTermsQuery>;
export type TermsLazyQueryHookResult = ReturnType<typeof useTermsLazyQuery>;
export type TermsQueryResult = Apollo.QueryResult<TermsQuery, TermsQueryVariables>;
export const ChangeAttendanceWorkgroupDocument = gql`
    mutation ChangeAttendanceWorkgroup($id: ID!, $workgroupId: ID) {
  change_attendance_workgroup(attendance_id: $id, workgroup_id: $workgroupId) {
    attendance {
      id
      workgroup_id
      workgroup {
        id
        name
      }
    }
    from_workgroup {
      ...Workgroup
    }
    to_workgroup {
      ...Workgroup
    }
  }
}
    ${WorkgroupFragmentDoc}`;
export type ChangeAttendanceWorkgroupMutationFn = Apollo.MutationFunction<ChangeAttendanceWorkgroupMutation, ChangeAttendanceWorkgroupMutationVariables>;

/**
 * __useChangeAttendanceWorkgroupMutation__
 *
 * To run a mutation, you first call `useChangeAttendanceWorkgroupMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeAttendanceWorkgroupMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeAttendanceWorkgroupMutation, { data, loading, error }] = useChangeAttendanceWorkgroupMutation({
 *   variables: {
 *      id: // value for 'id'
 *      workgroupId: // value for 'workgroupId'
 *   },
 * });
 */
export function useChangeAttendanceWorkgroupMutation(baseOptions?: Apollo.MutationHookOptions<ChangeAttendanceWorkgroupMutation, ChangeAttendanceWorkgroupMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeAttendanceWorkgroupMutation, ChangeAttendanceWorkgroupMutationVariables>(ChangeAttendanceWorkgroupDocument, options);
      }
export type ChangeAttendanceWorkgroupMutationHookResult = ReturnType<typeof useChangeAttendanceWorkgroupMutation>;
export type ChangeAttendanceWorkgroupMutationResult = Apollo.MutationResult<ChangeAttendanceWorkgroupMutation>;
export type ChangeAttendanceWorkgroupMutationOptions = Apollo.BaseMutationOptions<ChangeAttendanceWorkgroupMutation, ChangeAttendanceWorkgroupMutationVariables>;
export const TermWorkgroupsDocument = gql`
    query TermWorkgroups($termId: ID!) {
  term(id: $termId) {
    id
    attendances {
      ...Attendance
    }
    workgroups {
      ...Workgroup
    }
  }
}
    ${AttendanceFragmentDoc}
${WorkgroupFragmentDoc}`;

/**
 * __useTermWorkgroupsQuery__
 *
 * To run a query within a React component, call `useTermWorkgroupsQuery` and pass it any options that fit your needs.
 * When your component renders, `useTermWorkgroupsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTermWorkgroupsQuery({
 *   variables: {
 *      termId: // value for 'termId'
 *   },
 * });
 */
export function useTermWorkgroupsQuery(baseOptions: Apollo.QueryHookOptions<TermWorkgroupsQuery, TermWorkgroupsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TermWorkgroupsQuery, TermWorkgroupsQueryVariables>(TermWorkgroupsDocument, options);
      }
export function useTermWorkgroupsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TermWorkgroupsQuery, TermWorkgroupsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TermWorkgroupsQuery, TermWorkgroupsQueryVariables>(TermWorkgroupsDocument, options);
        }
export type TermWorkgroupsQueryHookResult = ReturnType<typeof useTermWorkgroupsQuery>;
export type TermWorkgroupsLazyQueryHookResult = ReturnType<typeof useTermWorkgroupsLazyQuery>;
export type TermWorkgroupsQueryResult = Apollo.QueryResult<TermWorkgroupsQuery, TermWorkgroupsQueryVariables>;