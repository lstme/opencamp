import * as React from 'react';
import cx from 'classnames';

interface IProps {
    fluid?: boolean;
    title?: string;
}

const PageContainer = ({ className, title, fluid, ...rest }: IProps & React.HTMLAttributes<HTMLDivElement>) => {
    React.useEffect(() => {
        if (fluid) {
            document.body.classList.add('layout-fluid');
            return () => {
                document.body.classList.remove('layout-fluid');
            };
        }
    }, [fluid]);

    return (
        <div className={cx('page-wrapper', className)}>
            {title && (
                <div className="container-xl">
                    <div className="page-header d-print-none">
                        <div className="row g-2 align-items-center">
                            <div className="col">
                                <h2 className="page-title">{title}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            <div className="page-body">
                <div className="container-xl" {...rest} />
            </div>
        </div>
    );
};

export default PageContainer;
