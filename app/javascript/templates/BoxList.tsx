import * as React from 'react';
import cx from 'classnames';
import CardSkeleton from 'molecule/CardSkeleton';

const BoxListContext = React.createContext<{ hasLeading: boolean }>({ hasLeading: false });

interface IBoxListBoxProps extends React.HTMLAttributes<HTMLDivElement> {}

export const BoxListBox = ({ className, ...rest }: IBoxListBoxProps) => {
    // const hasLeading = React.useContext(BoxListContext).hasLeading;
    return <div className={cx(className)} {...rest} />;
};

interface IProps {
    children?: React.ReactNode;
    leading?: React.ReactNode;
}

const BoxList = ({ children, className, leading }: IProps & React.HTMLAttributes<HTMLDivElement>) => {
    const ctxValue = React.useMemo(() => {
        return { hasLeading: !!leading };
    }, [leading]);

    return (
        <BoxListContext.Provider value={ctxValue}>
            <div className={cx('row', className)}>
                {leading && <div className="mb-3 col-12 col-lg-3">{leading}</div>}
                <div
                    className={cx('col tw-gap-4 tw-grid sm:tw-grid-cols-2', {
                        'lg:tw-grid-cols-4': !leading,
                        'lg:tw-grid-cols-3': leading
                    })}
                >
                    {children}
                </div>
            </div>
        </BoxListContext.Provider>
    );
};

BoxList.Box = BoxListBox;

export const BoxListBoxSkeleton = () => (
    <BoxListBox>
        <CardSkeleton />
    </BoxListBox>
);

export const BoxListSkeleton = () => (
    <BoxList className="placeholder-glow">
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
        <BoxListBoxSkeleton />
    </BoxList>
);

export default BoxList;
