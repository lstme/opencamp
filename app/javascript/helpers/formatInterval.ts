import differenceInSeconds from 'date-fns/differenceInSeconds';
import addSeconds from 'date-fns/addSeconds';
import format from 'lib/format';
import { LONG_TIME_FORMAT } from '~constants';

export const interval = (from: Date, to: Date | undefined = new Date()): { seconds: number; text: string } => {
    const seconds = differenceInSeconds(to, from);
    const diffDate = addSeconds(new Date(0, 0, 0, 0, 0, 0, 0), seconds);
    // const text = seconds >= 3600 ? format(diffDate, SHORT_TIME_FORMAT) : format(diffDate, LONG_TIME_FORMAT);
    const text = format(diffDate, LONG_TIME_FORMAT);
    return { seconds, text };
};

export const formatInterval = (from: Date, to?: Date): string => interval(from, to).text;
