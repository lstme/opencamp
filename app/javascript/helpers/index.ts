export const genNumArray = (count: number) => Array.from({ length: count }, (_, index) => index);

export const parseSearch = (text: string) =>
    text
        .replace(/\\/g, '')
        .trim()
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '');

export const slugify = (text: string) =>
    text
        .replace(/\ /g, '_')
        .replace(/[^A-Za-z0-9_\-]/g, '')
        .trim()
        .toLowerCase();

export const pluralize = (text: string, count: number) => `${text}${count === 1 ? '' : 's'}`;

export const formatEuro = (amount: number) =>
    new Intl.NumberFormat('sk-SK', { style: 'currency', currency: 'EUR' }).format(amount);
