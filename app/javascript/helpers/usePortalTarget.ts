import { useEffect, useState } from 'react';

// This handle special case where component and whole DOM is rendered
// in one sweep. Since target renders in DOM, it is not available
// on first render. This rerenders component later - when target
// should be available.
// If elementId is not passed in, new div is created for portal in document body
const usePortalTarget = (elementId?: string) => {
    let initial: HTMLElement | null = null;
    if (elementId) {
        // If element is already available, just return it
        initial = document.getElementById(elementId);
    }

    if (!initial) {
        initial = document.createElement('div');
        document.body.appendChild(initial);
    }

    // Do not immediatly return, that breaks hooks

    // Prepare for retry
    const [backoff, setBackoff] = useState(10);
    // Initialize state value
    const [target, setTarget] = useState(initial);

    useEffect(() => {
        // When target is already found, do not search again
        if (target || !elementId) return;

        // Lookup target
        const t = document.getElementById(elementId);

        if (t) {
            // If target is available, save it
            setTarget(t);
        } else {
            // If target is not available, plan retry
            setTimeout(() => {
                setBackoff(backoff * 10);
            }, backoff);
        }
    }, [elementId]);

    return target;
};

export default usePortalTarget;
