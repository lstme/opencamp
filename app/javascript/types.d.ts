export interface IGlobal extends NodeJS.Global {
    client: any;
}

declare global {
    interface Window {
        // Used for syncing Timers ticking
        _ocTimerInterval?: number;
        _ocTimers: number;
        Alpine: Alpine;
    }

    declare module '*.png' {
        const filePath: string;
        export default filePath;
    }
}
