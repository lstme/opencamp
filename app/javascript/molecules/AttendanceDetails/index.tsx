import cx from 'classnames';
import * as React from 'react';
import { AttendanceFragment } from '~graphql.generated';
import { FaGraduationCap, FaMars, FaVenus } from 'react-icons/fa';

export type AttendanceDetailKey = 'gender' | 'room' | 'age' | 'workgroup';

interface IProps {
    attendance: AttendanceFragment;
    details: AttendanceDetailKey[];
}

const AttendanceDetails = ({ attendance: a, details }: IProps) => (
    <React.Fragment>
        {details.map((detail, i) => {
            switch (detail) {
                case 'age':
                    return <AgeDetail key={i} attendance={a} />;
                case 'gender':
                    return <GenderDetail key={i} attendance={a} />;
                case 'room':
                    return <RoomDetail key={i} attendance={a} />;
                case 'workgroup':
                    return <WorkgroupDetail key={i} attendance={a} />;
            }
        })}
    </React.Fragment>
);

interface DetailProps {
    attendance: AttendanceFragment;
}

const AgeDetail = ({ attendance }: DetailProps) => {
    return <div className="badge badge-pill bg-cyan-lt">{attendance.user.age}r.</div>;
};

const GenderDetail: React.FC<DetailProps> = ({ attendance }) => {
    const male = attendance.user.gender_symbol === '♂';
    return (
        <div className={cx('badge badge-pill', { 'bg-blue-lt': male, 'bg-pink-lt': !male })}>
            {male ? <FaMars /> : <FaVenus />}
        </div>
    );
};

const RoomDetail: React.FC<DetailProps> = ({ attendance }) => {
    return (
        <div className="badge badge-pill bg-yellow-lt">
            {attendance.room == null ? 'no room' : attendance.room.name}
        </div>
    );
};

const WorkgroupDetail: React.FC<DetailProps> = ({ attendance }) => {
    if (attendance.__typename === 'InstructorAttendance') {
        return (
            <div className="badge badge-pill bg-instructor-lt text-instructor">
                <FaGraduationCap />
            </div>
        );
    }
    return (
        <div className="badge badge-pill bg-purple-lt">
            {attendance.workgroup == null ? 'no workgroup' : attendance.workgroup.name}
        </div>
    );
};

export default AttendanceDetails;
