import Avatar from 'atom/Avatar';
import { FaCoins } from 'react-icons/fa';
import { HomeQuery } from '~graphql.generated';

interface IProps {
    attendance?: NonNullable<HomeQuery['term']>['current_attendance'] | null;
    user?: HomeQuery['me'];
}

const UserCard = ({ attendance, user }: IProps) => {
    if (!attendance || !user) return null;

    return (
        <div className="card">
            <div className="card-header border-bottom-0">
                <UserCardContent user={user} attendance={attendance} />
            </div>
        </div>
    );
};

export const UserCardContent = ({ attendance, user }: IProps) => {
    if (!attendance || !user) return null;

    return (
        <>
            <div>
                <div className="row align-items-center">
                    <div className="col-auto">
                        <Avatar avatar={user.avatar} variant="medium" />
                    </div>
                    <div className="col">
                        <div className="card-title">
                            {user.first_name} {user.last_name}
                        </div>
                        <div className="card-subtitle">
                            {user.gender_symbol} {user.age}r.
                        </div>
                    </div>
                </div>
            </div>
            <div className="ms-auto">
                <div className="card-title">
                    <FaCoins className="icon text-coin" />
                    <span className="ms-2">{attendance.coins}</span>
                </div>
                {attendance.wallet_id && <div className="card-subtitle text-end">[{attendance.wallet_id}]</div>}
            </div>
        </>
    );
};

export const UserCardSkeleton = () => (
    <div className="card placeholder-glow">
        <div className="card-body">
            <div className="row align-items-center">
                <div className="col-auto">
                    <div className="avatar placeholder" />
                </div>
                <div className="col">
                    <div className="placeholder placeholder-xs col-9"></div>
                    <div className="placeholder placeholder-xs col-7"></div>
                </div>
            </div>
        </div>
    </div>
);

export default UserCard;
