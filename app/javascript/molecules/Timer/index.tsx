import * as React from 'react';
import { interval } from '../../helpers/formatInterval';
import cx from 'classnames';

interface IProps extends React.HTMLAttributes<HTMLSpanElement> {
    marks?: number[];
    start?: Date;
    end?: Date;
}

const MARK_COLORS = ['text-green-700', 'text-orange-600', 'text-red-700'];

const Timer = ({ children, className, marks, start, end, ...rest }: IProps) => {
    if (!start) return null;

    const [value, setValue] = React.useState(interval(start, end));

    const classNames = React.useCallback(
        (seconds: number) => {
            if (!marks || marks.length === 0) return null;

            if (marks.length === 1) {
                if (seconds >= marks[0]) return MARK_COLORS[2];
            } else if (marks.length === 2) {
                if (seconds >= marks[1]) return MARK_COLORS[2];
                else if (seconds >= marks[0]) return MARK_COLORS[1];
            } else {
                if (seconds >= marks[2]) return MARK_COLORS[2];
                else if (seconds >= marks[1]) return MARK_COLORS[1];
                else if (seconds >= marks[0]) return MARK_COLORS[0];
            }
        },
        [marks]
    );

    // This is so complicated just to make all timers update at once
    // It only looks better, there probably is not benefit to this vs.
    // version when each timer would have its own interval
    React.useEffect(() => {
        if (end) return;

        if (!window._ocTimerInterval) {
            window._ocTimerInterval = window.setInterval(() => {
                window.dispatchEvent(new Event('ocTimerTick'));
            }, 1000) as number;
        }

        const handleTick = () => setValue(interval(start));
        window.addEventListener('ocTimerTick', handleTick);
        window._ocTimers += 1;

        return () => {
            window.removeEventListener('ocTimerTick', handleTick);
            window._ocTimers -= 1;

            if (window._ocTimers === 0) {
                clearInterval(window._ocTimerInterval);
                window._ocTimerInterval = undefined;
            }
        };
    }, [start]);

    return (
        <span className={cx(className, classNames(value.seconds))} {...rest}>
            {value.text}
        </span>
    );
};

export default Timer;
