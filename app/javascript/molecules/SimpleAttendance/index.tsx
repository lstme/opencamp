import { AvatarSize } from 'atom/Avatar';
import cx from 'classnames';
import { FaTimes } from 'react-icons/fa';
import { ProjectAttendanceFragment } from '~graphql.generated';

export interface IAttendanceProps {
    accessory?: React.ReactNode;
    attendance: ProjectAttendanceFragment['attendance'];
    avatarSize?: AvatarSize;
    className?: string;
    onClick?: (attendanceId: string) => void;
    onRemove?: (attendanceId: string) => void;
}

const SimpleAttendance: React.FC<IAttendanceProps> = ({ accessory, attendance, className, onClick, onRemove }) => {
    const user = attendance.user;
    const isInstructor = attendance.__typename === 'InstructorAttendance';
    return (
        <div
            className={cx(className, {
                'text-instructor': isInstructor
            })}
            onClick={() => onClick?.(attendance.id)}
        >
            <div className="row gx-0 align-items-center">
                <div
                    className={cx('col-auto', {
                        'text-instructor': isInstructor,
                        'text-child': !isInstructor
                    })}
                >
                    {accessory}
                    {user.full_name}
                </div>
                {onRemove && (
                    <div className="col-auto ms-auto">
                        <button
                            className="btn btn-sm btn-icon btn-ghost-danger"
                            onClick={() => onRemove?.(attendance.id)}
                        >
                            <FaTimes className="icon" />
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default SimpleAttendance;
