import cx from 'classnames';
import { HomeQuery } from '~graphql.generated';
import TransactionsTimeline from '../TransactionsTimeline';

interface IProps {
    attendance?: NonNullable<HomeQuery['term']>['current_attendance'];
    className?: string;
}

const CoinsTimelineCard = ({ className, attendance }: IProps) => (
    <div className={cx('card', className)}>
        <div className="card-header">
            <div className="card-title">Transactions</div>
        </div>
        <TransactionsTimeline className="list-group-flush" transactions={attendance?.transactions ?? []} />
    </div>
);

export default CoinsTimelineCard;
