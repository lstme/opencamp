import cx from 'classnames';
import { useScrollStatus } from 'lib/useScrollStatus';
import * as React from 'react';

interface IProps extends React.HTMLAttributes<HTMLDivElement> {
    wrapperClassName?: string;
}

const ScrollWithShadow: React.FC<IProps> = ({ className, wrapperClassName, style, ...rest }) => {
    const { ref, scrollable, atTop, atBottom } = useScrollStatus();

    return (
        <React.Fragment>
            <div className={cx(wrapperClassName, 'position-relative overflow-hidden')}>
                <div
                    className={cx('scroll-shadow scroll-shadow--top', {
                        'opacity-100': scrollable && !atTop,
                        'opacity-0': !scrollable || atTop
                    })}
                />
                <div
                    ref={ref}
                    className={cx(className, 'tw-overflow-y-auto tw-overflow-x-hidden')}
                    style={{ ...style }} // lets not have default max height...
                    {...rest}
                />
                <div
                    className={cx('scroll-shadow scroll-shadow--bottom', {
                        'opacity-100': scrollable && !atBottom,
                        'opacity-0': !scrollable || atBottom
                    })}
                />
            </div>
        </React.Fragment>
    );
};

export default ScrollWithShadow;
