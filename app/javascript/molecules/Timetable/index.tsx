import Avatar from 'atom/Avatar';
import cx from 'classnames';
import * as React from 'react';
import { TimetableFragment } from '~graphql.generated';

interface IProps {
    timetable: Partial<TimetableFragment>;
}

const Timetable = ({ timetable }: IProps) => (
    <div className="list-group list-group-flush">
        {timetable.designated_head && (
            <div className="list-group-item">
                <div className="row">
                    <div className="col-auto">
                        <Avatar avatar={timetable.designated_head.avatar} name={timetable.designated_head.full_name} />
                    </div>
                    <div className="col text-truncate">
                        <div className="text-body d-block">{timetable.designated_head.full_name}</div>
                        <div className="text-muted text-truncate mt-n1">Vedúci dňa</div>
                    </div>
                </div>
            </div>
        )}
        {timetable.designated_room && (
            <div className="list-group-item">
                <div className="row">
                    <div className="col-auto">
                        <div className="avatar">
                            <i className="fa fa-bed icon" />
                        </div>
                    </div>
                    <div className="col text-truncate">
                        <div className="text-body d-block">{timetable.designated_room.name}</div>
                        <div className="text-muted text-truncate mt-n1">Služba dňa</div>
                    </div>
                </div>
            </div>
        )}
        {timetable.notice && (
            <div className="list-group-item">
                <div className="row">
                    <div className="col-auto">
                        <div className="avatar">
                            <i className="fa fa-clipboard icon" />
                        </div>
                    </div>
                    <div className="col text-truncate">
                        <div className="text-body d-block">
                            {timetable.notice.startsWith('http') ? (
                                <a href={timetable.notice}>{timetable.notice}</a>
                            ) : (
                                timetable.notice
                            )}
                        </div>
                        <div className="text-muted text-truncate mt-n1">Poznámka</div>
                    </div>
                </div>
            </div>
        )}
        <div className="list-group-header">Rozvrh</div>
        {(timetable?.timeslots ?? []).map(slot => (
            <React.Fragment key={slot.id}>
                <div
                    key={slot.id}
                    className={cx('list-group-item', {
                        active: slot.is_highlighted
                    })}
                >
                    <div className="text-muted fw-bold small">
                        {slot.time} - {slot.label}
                    </div>
                    {slot.items.length > 0 && (
                        <div className="row mt-2">
                            {slot.items.map(s => (
                                <div
                                    key={s.id}
                                    className={cx('d-flex align-items-center py-1', {
                                        col: slot.items.length === 1,
                                        'col-6 col-sm-3': slot.items.length > 1
                                    })}
                                >
                                    {s.label && <div className="badge bg-primary fs-5 me-2">{s.label}</div>}
                                    <div>{s.title}</div>
                                </div>
                            ))}
                        </div>
                    )}
                </div>
            </React.Fragment>
        ))}
    </div>
);

export const TimetableSkeleton = () => (
    <div className="list-group list-group-flush placeholder-glow">
        <div className="list-group-item">
            <div className="row align-items-center">
                <div className="col-auto">
                    <div className="avatar placeholder"></div>
                </div>
                <div className="col-9">
                    <div className="placeholder col-9"></div>
                </div>
            </div>
        </div>
        <div className="list-group-item">
            <div className="row align-items-center">
                <div className="col-auto">
                    <div className="avatar placeholder"></div>
                </div>
                <div className="col-9">
                    <div className="placeholder col-9"></div>
                </div>
            </div>
        </div>
        <div className="list-group-header">
            <div className="placeholder placeholder-xs col-5" />
        </div>
        <div className="list-group-item">
            <div className="placeholder placeholder-xs col-7" />
        </div>
        <div className="list-group-item">
            <div className="placeholder placeholder-xs col-9" />
        </div>
    </div>
);

export const TimetableEmpty = () => (
    <div className="list-group list-group-flush">
        <div className="list-group-item">
            <div className="row align-items-center">
                <div className="col-auto">
                    <div className="avatar">
                        <i className="fa fa-clock icon" />
                    </div>
                </div>
                <div className="col-auto text-truncate">
                    <div className="text-muted d-block">Timetable for this date is not available</div>
                </div>
            </div>
        </div>
    </div>
);

export default Timetable;
