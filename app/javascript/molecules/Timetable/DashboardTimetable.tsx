import Avatar from 'atom/Avatar';
import cx from 'classnames';
import * as React from 'react';
import { TimetableFragment } from '~graphql.generated';

interface IProps {
    timetable: Partial<TimetableFragment>;
}

const DashboardTimetable = ({ timetable }: IProps) => (
    <div className="list-group list-group-flush">
        <div className="list-group-item">
            <div className="row">
                {timetable.designated_head && (
                    <div className="col-6">
                        <div className="row">
                            <div className="col-auto">
                                <Avatar
                                    avatar={timetable.designated_head.avatar}
                                    name={timetable.designated_head.full_name}
                                />
                            </div>
                            <div className="col text-truncate">
                                <div className="text-body d-block">{timetable.designated_head.full_name}</div>
                                <div className="text-muted text-truncate mt-n1">Vedúci dňa</div>
                            </div>
                        </div>
                    </div>
                )}
                {timetable.designated_room && (
                    <div className="col-6">
                        <div className="row">
                            <div className="col-auto">
                                <div className="avatar">
                                    <i className="fa fa-bed icon" />
                                </div>
                            </div>
                            <div className="col text-truncate">
                                <div className="text-body d-block">{timetable.designated_room.name}</div>
                                <div className="text-muted text-truncate mt-n1">Služba dňa</div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
        <div className="list-group-header">Rozvrh</div>
        {(timetable?.timeslots ?? []).map(slot => (
            <React.Fragment key={slot.id}>
                <div
                    key={slot.id}
                    className={cx('list-group-item', {
                        active: slot.is_highlighted,
                        'py-2': !slot.is_highlighted
                    })}
                >
                    <div className="text-muted fw-bold small">
                        {slot.time} - {slot.label}
                    </div>
                    {slot.items.length > 0 && (
                        <div className="row mt-2">
                            {slot.items.map(s => (
                                <div
                                    key={s.id}
                                    className={cx('d-flex align-items-center py-1', {
                                        col: slot.items.length === 1,
                                        'col-6 col-sm-3': slot.items.length > 1
                                    })}
                                >
                                    {s.label && <div className="badge bg-primary fs-5 me-2">{s.label}</div>}
                                    <div>{s.title}</div>
                                </div>
                            ))}
                        </div>
                    )}
                </div>
            </React.Fragment>
        ))}
    </div>
);

export default DashboardTimetable;
