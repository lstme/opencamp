import SelectField from 'atom/SelectField';
import cx from 'classnames';
import * as React from 'react';
import { v4 as uuid } from 'uuid';
import { AvatarFragment, TimetableFragment } from '~graphql.generated';

interface IProps {
    value: Partial<TimetableFragment>;
    onChange?: (newValue: Partial<TimetableFragment>) => void;
    designatedHeads?: {
        id: string;
        user: { id: string; full_name: string; avatar: AvatarFragment | null };
    }[];
    designatedRooms?: { id: string; name: string }[];
}

const sortSlots = (slots: TimetableFragment['timeslots']): TimetableFragment['timeslots'] => {
    return [...slots].sort((a, b) => a.time.localeCompare(b.time));
};

const TimetableInput = ({ value, designatedHeads = [], designatedRooms = [], onChange }: IProps) => {
    const designatedHead = designatedHeads?.find(h => h.id === value.designated_head?.id);
    const designatedRoom = designatedRooms?.find(r => r.id === value.designated_room?.id);
    const timeslots = value.timeslots ?? [];

    const addSlot = () => {
        onChange?.({
            ...value,
            timeslots: sortSlots([
                ...timeslots,
                { id: uuid(), is_highlighted: false, items: [], label: '??', time: '23:59' }
            ])
        });
    };

    const changeSlot = (idx: number, change: Partial<TimetableFragment['timeslots'][0]>) => {
        onChange?.({
            ...value,
            timeslots: sortSlots(timeslots.map((slot, i) => (i === idx ? { ...slot, ...change } : slot)))
        });
    };

    const removeSlot = (idx: number) => {
        onChange?.({
            ...value,
            timeslots: sortSlots(timeslots.filter((v, i) => i !== idx))
        });
    };

    const addSlotItem = (idx: number) => {
        changeSlot(idx, { items: [...timeslots[idx].items, { id: uuid(), label: '', title: '' }] });
    };

    const changeSlotItem = (
        slotIdx: number,
        itemIdx: number,
        change: Partial<TimetableFragment['timeslots'][0]['items'][0]>
    ) => {
        changeSlot(slotIdx, {
            items: timeslots[slotIdx].items.map((slotItem, i) =>
                i === itemIdx ? { ...slotItem, ...change } : slotItem
            )
        });
    };

    const removeSlotItem = (slotIdx: number, itemIdx: number) => {
        changeSlot(slotIdx, {
            items: timeslots[slotIdx].items.filter((s, i) => i !== itemIdx)
        });
    };

    return (
        <React.Fragment>
            <SelectField
                className="mt-4"
                label="Vedúci dňa"
                id="designatedHead"
                placeholder="Select designated head"
                items={designatedHeads}
                value={designatedHead}
                labelGetter={h => h.user.full_name}
                valueGetter={h => h.id}
                onChange={h =>
                    onChange?.({
                        ...value,
                        designated_head: { id: h.id, full_name: h.user.full_name, avatar: h.user.avatar }
                    })
                }
            />
            <SelectField
                className="mt-4"
                label="Služba dňa"
                id="designatedRoom"
                placeholder="Select designated room"
                items={designatedRooms}
                value={designatedRoom}
                labelGetter={r => r.name}
                valueGetter={r => r.id}
                onChange={r => onChange?.({ ...value, designated_room: { id: r.id, name: r.name } })}
            />

            <div className="form-group mt-4">
                <label className="form-label">Notice</label>
                <input
                    type="text"
                    className="form-control"
                    value={value.notice ?? ''}
                    onChange={e => onChange?.({ ...value, notice: e.target.value })}
                />
            </div>

            <div className="list-group mt-4">
                {timeslots.map((slot, i) => (
                    <div
                        key={slot.id}
                        className={cx('list-group-item', {
                            active: slot.is_highlighted
                        })}
                    >
                        <div className="row">
                            <div className="col">
                                <div className="form-group">
                                    <label className="form-check">
                                        <input
                                            type="checkbox"
                                            checked={slot.is_highlighted}
                                            className="form-check-input"
                                            onChange={() => changeSlot(i, { is_highlighted: !slot.is_highlighted })}
                                        />
                                        <span className="form-check-label">Highlighted</span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-sm btn-ghost-danger" onClick={() => removeSlot(i)}>
                                    <i className="fa fa-times icon" />
                                    Remove
                                </button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-sm-4">
                                <div className="form-group">
                                    <label className="form-label">Time</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={slot.time}
                                        onChange={e => changeSlot(i, { time: e.target.value })}
                                    />
                                </div>
                            </div>
                            <div className="col-12 col-sm mt-2 mt-sm-0">
                                <div className="form-group">
                                    <label className="form-label">Label</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        value={slot.label}
                                        onChange={e => changeSlot(i, { label: e.target.value })}
                                    />
                                </div>
                            </div>
                        </div>
                        {slot.items.length > 0 && (
                            <fieldset className="form-fieldset mt-4 mb-0">
                                {slot.items.length > 0 && (
                                    <div className="d-none d-sm-flex row align-items-center mb-2">
                                        <div className="col-3">
                                            <label className="form-label mb-0">Label</label>
                                        </div>
                                        <div className="col">
                                            <label className="form-label mb-0">Title</label>
                                        </div>
                                        <div className="col-1"></div>
                                    </div>
                                )}
                                {slot.items.map((s, j) => (
                                    <div key={s.id} className="row align-items-center mb-4 mb-sm-2">
                                        <div className="col-12 col-sm-3">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="Label"
                                                value={s.label ?? ''}
                                                onChange={e => changeSlotItem(i, j, { label: e.target.value })}
                                            />
                                        </div>
                                        <div className="col-12 mt-2 col-sm mt-sm-0">
                                            <input
                                                type="text"
                                                className="form-control"
                                                placeholder="Title"
                                                value={s.title ?? ''}
                                                onChange={e => changeSlotItem(i, j, { title: e.target.value })}
                                            />
                                        </div>
                                        <div className="col-12 col-sm-1 mt-2 mt-sm-0 text-end">
                                            <button
                                                className="btn btn-sm btn-ghost-danger"
                                                onClick={() => removeSlotItem(i, j)}
                                            >
                                                <i className="fa fa-times icon" />
                                                Remove
                                            </button>
                                        </div>
                                    </div>
                                ))}
                                <div className={cx('row')}>
                                    <div className="col">
                                        <button className="btn btn-sm btn-ghost-success" onClick={() => addSlotItem(i)}>
                                            <i className="fa fa-plus icon" />
                                            Add slot item
                                        </button>
                                    </div>
                                    <div className="col"></div>
                                    <div className="col-1"></div>
                                </div>
                            </fieldset>
                        )}
                        {slot.items.length === 0 && (
                            <div className="col mt-2">
                                <button className="btn btn-sm btn-ghost-success" onClick={() => addSlotItem(i)}>
                                    <i className="fa fa-plus icon" />
                                    Add slot item
                                </button>
                            </div>
                        )}
                    </div>
                ))}
                <div>
                    <button className="btn btn-sm btn-ghost-success mt-2" onClick={() => addSlot()}>
                        <i className="fa fa-plus icon" />
                        Add slot
                    </button>
                </div>
            </div>
        </React.Fragment>
    );
};

export default TimetableInput;
