import { useToaster } from 'organism/Toaster';
import { useState } from 'react';

export interface ITransactionFormValue {
    amount: number;
    note: string;
}

interface IProps {
    onChange: (value: ITransactionFormValue) => void;
    onSubmit: (value: ITransactionFormValue) => Promise<any>;
    value: ITransactionFormValue;
}

const TransactionForm = ({ onChange, onSubmit, value }: IProps) => {
    const [saving, setSaving] = useState(false);
    const { amount, note } = value;

    const toaster = useToaster();

    const handleSubmit = () => {
        setSaving(true);
        onSubmit(value)
            .catch(error => {
                toaster.current.showToast({ description: error.message, variant: 'alert' });
            })
            .finally(() => setSaving(false));
    };

    const canSubmit = amount !== 0;

    return (
        <form onSubmit={handleSubmit}>
            <div className="row align-items-center">
                <div className="col-3">
                    <input
                        className="form-control"
                        placeholder="$$"
                        value={amount === 0 ? '' : amount}
                        type="number"
                        pattern="-?\d*"
                        onChange={e =>
                            onChange({
                                ...value,
                                amount: Number(e.target.value)
                            })
                        }
                    />
                </div>
                <div className="col">
                    <input
                        className="form-control"
                        placeholder="Note"
                        value={note}
                        onChange={e =>
                            onChange({
                                ...value,
                                note: e.target.value
                            })
                        }
                    />
                </div>
                <div className="col-auto">
                    <button
                        className="btn btn-sm btn-ghost-success"
                        disabled={!canSubmit || saving}
                        onClick={handleSubmit}
                    >
                        <i className="fa fa-plus icon" />
                        Add
                    </button>
                </div>
            </div>
        </form>
    );
};

export default TransactionForm;
