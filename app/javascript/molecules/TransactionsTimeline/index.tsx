import cx from 'classnames';
import * as React from 'react';
import { AttendanceTransactionFragment } from '~graphql.generated';
import TransactionItem from './TransactionItem';

interface IProps {
    onDelete?: (id: string) => Promise<any>;
    transactions: AttendanceTransactionFragment[];
    className?: string;
}

const Timeline = ({ className, onDelete, transactions }: IProps) => {
    const sorted = React.useMemo(() => {
        // transactions are readonly
        return [...transactions].sort((a, b) => Date.parse(b.created_at) - Date.parse(a.created_at));
    }, [transactions]);

    return (
        <div className={cx(className, 'list-group')}>
            {sorted.map((t, i) => (
                <TransactionItem key={t.id} onDelete={onDelete} transaction={t} />
            ))}
        </div>
    );
};

export default Timeline;
