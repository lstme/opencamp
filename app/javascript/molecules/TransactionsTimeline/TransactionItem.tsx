import cx from 'classnames';
import format from 'lib/format';
import { TRANSACTION_DATE_FORMAT } from '~constants';
import { AttendanceTransactionFragment } from '~graphql.generated';

interface IProps {
    onDelete?: (id: string) => Promise<any>;
    transaction: AttendanceTransactionFragment;
}

const TransactionItem = ({ onDelete, transaction }: IProps) => {
    const { amount, created_at, note } = transaction;
    const positive = amount >= 0;

    const handleDelete = () => {
        if (!onDelete) return;
        onDelete(transaction.id);
    };

    const amountText = positive ? `+${amount}` : `${amount}`;

    return (
        <div className="list-group-item">
            <div className="row align-items-center">
                <div className="col-auto">
                    <span className={cx('avatar', { 'bg-green-lt': positive, 'bg-red-lt': !positive })}>
                        {amountText}
                    </span>
                </div>
                <div className="col-auto">
                    <div>{note || '-'}</div>
                    <div className="text-muted">
                        {format(new Date(created_at), TRANSACTION_DATE_FORMAT)} - {transaction.user.full_name}
                    </div>
                </div>
                <div className="col-auto ms-auto">
                    {onDelete && (
                        <button className="btn btn-sm btn-ghost-danger" onClick={handleDelete}>
                            <i className="fa fa-times icon" />
                            Remove
                        </button>
                    )}
                </div>
            </div>
        </div>
    );
};

export default TransactionItem;
