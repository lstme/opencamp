import cx from 'classnames';
import * as React from 'react';

interface IProps {
    children?: React.ReactNode;
    className?: string;
}

const CardSkeleton = ({ children, className }: IProps) => (
    <div className={cx('card', className)}>
        <div className="card-header">
            <div className="placeholder col-9" />
        </div>
        <div className="card-body">
            {children ?? (
                <>
                    <div className="placeholder col-9 mb-3"></div>
                    <div className="placeholder placeholder-xs col-10"></div>
                    <div className="placeholder placeholder-xs col-12"></div>
                    <div className="placeholder placeholder-xs col-11"></div>
                    <div className="placeholder placeholder-xs col-8"></div>
                    <div className="placeholder placeholder-xs col-10"></div>
                </>
            )}
        </div>
    </div>
);

export default CardSkeleton;
