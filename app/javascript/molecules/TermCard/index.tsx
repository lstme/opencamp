import cx from 'classnames';
import format from 'lib/format';
import { useSelectedTermId } from 'organism/AppContext';
import { SelectableTermFragment, TermPhaseTypeEnum } from '~graphql.generated';

const STATE_TITLE: { [key in TermPhaseTypeEnum]: string } = {
    created: 'Vytvorené',
    ended: 'Skončené',
    in_progress: 'Práve prebieha',
    off_boarding: 'Už končí',
    onboarding: 'Práve začína',
    registrations_open: 'Registrácie otvorené'
};

const isCurrent = (phase: TermPhaseTypeEnum) =>
    [TermPhaseTypeEnum.Onboarding, TermPhaseTypeEnum.InProgress, TermPhaseTypeEnum.OffBoarding].includes(phase);

interface IProps {
    term: SelectableTermFragment;
}

const TermCard: React.FC<IProps> = ({ term }) => {
    const selectedTermId = useSelectedTermId();
    const startDate = new Date(term.starts_at);
    const endDate = new Date(term.ends_at);

    const sameMonth = startDate.getMonth() === endDate.getMonth();

    return (
        <a
            className={cx('card card-sm card-link', {
                'bg-primary-lt text-primary-lt-fg': term.id === selectedTermId
            })}
            href={`/terms/${term.id}/select`}
        >
            <div
                className={cx('ribbon', {
                    'bg-green': isCurrent(term.phase),
                    'bg-muted': term.phase === TermPhaseTypeEnum.Ended
                })}
            >
                {STATE_TITLE[term.phase]}
            </div>
            <img src={term.cover_photo?.thumb_url} className="card-img-top" />
            <div className="card-body">
                <div className="d-flex align-items-center">
                    <div>
                        <h3 className="card-title">{term.name}</h3>
                        <p className="tw-whitespace-pre-wrap">
                            {format(new Date(term.starts_at), sameMonth ? 'dd.' : 'dd. MMM')} -{' '}
                            {format(new Date(term.ends_at), 'dd. MMM yyyy')}
                            <br />
                            {term.address}
                        </p>
                    </div>
                </div>
            </div>
        </a>
    );
};

export default TermCard;
