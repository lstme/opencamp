import * as React from 'react';
import WarningDisplay from '../../atoms/WarningDisplay';

interface IProps {
    className?: string;
    children?: React.ReactNode;
}

const NotFound = ({ className, children }: IProps) => (
    <WarningDisplay className={className}>{children || 'Not found'}</WarningDisplay>
);

export default NotFound;
