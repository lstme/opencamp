import Avatar from 'atom/Avatar';
import cx from 'classnames';
import { AvatarFragment, HomeQuery } from '~graphql.generated';
import { genNumArray } from 'helpers';

interface IProps {
    className?: string;
    room?: NonNullable<NonNullable<HomeQuery['term']>['current_attendance']>['room'];
    highlightUserId?: string | null;
}

const RoomCard = ({ className, room, highlightUserId }: IProps) => {
    if (!room) return null;

    return (
        <div className={cx('card', className)}>
            <div className="card-header">
                <div className="card-title">{room.name}</div>
            </div>
            <div className="list-group list-group-flush">
                {room.attendances.map(att => (
                    <RoomAttendance
                        key={att.id}
                        avatar={att.user.avatar}
                        title={att.user.display_name}
                        highlight={att.user.id === highlightUserId}
                    />
                ))}
                {genNumArray(room.capacity - room.attendances_count).map(n => (
                    <RoomAttendance key={n} empty />
                ))}
            </div>
        </div>
    );
};

interface RoomAttendanceProps {
    highlight?: boolean;
    avatar?: AvatarFragment | null;
    title?: string;
    empty?: boolean;
}

const RoomAttendance = ({ avatar = null, highlight, title, empty }: RoomAttendanceProps) => (
    <div className={cx('list-group-item', { 'text-primary': highlight })}>
        <div className="row align-items-center">
            <div className="col-auto">
                {avatar && <Avatar className="avatar" avatar={avatar} name={title} />}
                {empty && <i className="fa fa-bed icon" />}
            </div>
            <div className="col-auto">{title}</div>
        </div>
    </div>
);

export default RoomCard;
