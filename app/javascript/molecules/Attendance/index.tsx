import Avatar, { AvatarSize } from 'atom/Avatar';
import cx from 'classnames';
import AttendanceDetails, { AttendanceDetailKey } from 'molecule/AttendanceDetails';
import { FaTimes } from 'react-icons/fa';
import { AttendanceFragment } from '~graphql.generated';

export interface IAttendanceProps {
    attendance: AttendanceFragment;
    avatarSize?: AvatarSize;
    className?: string;
    details?: AttendanceDetailKey[];
    onClick?: (attendanceId: string) => void;
    onRemove?: (attendanceId: string) => void;
}

const Attendance: React.FC<IAttendanceProps> = ({
    attendance,
    avatarSize = 'sm',
    className,
    details = ['gender', 'age', 'workgroup', 'room'],
    onClick,
    onRemove
}) => {
    const user = attendance.user;
    const isInstructor = attendance.__typename === 'InstructorAttendance';
    return (
        <div
            className={cx(className, {
                'text-instructor': isInstructor
            })}
            onClick={() => onClick?.(attendance.id)}
        >
            <div className="row gx-0 align-items-center">
                <div className="col-auto">
                    <Avatar avatar={user.avatar} variant="icon" size={avatarSize} />
                </div>
                <div
                    className={cx('col-auto px-2', {
                        'text-instructor': isInstructor,
                        'text-child': !isInstructor
                    })}
                >
                    {user.full_name}
                    <div>
                        <AttendanceDetails attendance={attendance} details={details} />
                    </div>
                </div>
                {onRemove && (
                    <div className="col-auto ms-auto">
                        <button
                            className="btn btn-sm btn-icon btn-ghost-danger"
                            onClick={() => onRemove?.(attendance.id)}
                        >
                            <FaTimes className="icon" />
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Attendance;
