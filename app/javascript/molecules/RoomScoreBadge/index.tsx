import cx from 'classnames';
import * as React from 'react';
import Modal from 'react-bootstrap/Modal';
import { FaMedal } from 'react-icons/fa';
import RoomScores from '../../organisms/RoomScores';

interface IProps {
    className?: string;
    id: string;
    score: number;
}

const RoomScoreBadge = ({ className, id, score }: IProps) => {
    const [showScores, setShowScores] = React.useState(false);

    return (
        <>
            <button className={cx(className, 'btn btn-ghost-info')} onClick={() => setShowScores(true)}>
                <FaMedal className="icon" /> {score}
            </button>
            {showScores && (
                <Modal show onHide={() => setShowScores(false)}>
                    <RoomScores id={id} />
                </Modal>
            )}
        </>
    );
};

export default RoomScoreBadge;
