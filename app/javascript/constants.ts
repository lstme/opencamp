export const ENTER_KEY_CODE = 13;
export const ESCAPE_KEY_CODE = 27;

export const SERVER_DATE_FORMAT = 'yyyy-MM-dd';
export const SHORT_DATE_FORMAT = 'dd.MM.';
export const TIMETABLE_DATE_FORMAT = 'EEEE dd. MMM. yyyy';
export const TRANSACTION_DATE_FORMAT = 'dd.MM. (EEEE)';
export const ROOM_SCORE_DATE_FORMAT = TRANSACTION_DATE_FORMAT;
export const LONG_DATE_FORMAT = 'dd.MM.yyyy';

export const SHORT_TIME_FORMAT = 'HH:mm';
export const LONG_TIME_FORMAT = 'HH:mm:ss';
