class ChangeRoom < ApplicationService
  pattr_initialize [:room_id, :note]

  def call
    Room.transaction do
      find_room
      change_room!

      room
    end
  end

  private

  attr_reader :room

  def find_room
    @room = Room.where(id: room_id).first
  end

  def change_room!
    return unless room

    room.note = note
    room.save!
  end
end
