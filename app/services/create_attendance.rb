class CreateAttendance < ApplicationService
  pattr_initialize [:type, :user_id, :term_id, :room_id, :note]

  def call
    Attendance.create!(
        type: type,
        user_id: user_id,
        term_id: term_id,
        room_id: room_id,
        note: note
    )
  end
end
