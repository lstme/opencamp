class AttendanceImportService
  def self.parse_csv_file(file, term)
    rows = []
    i = 0
    CSV.foreach(file, col_sep: ',', headers: true, quote_char: '"') do |line|
      i += 1
      next if line.fetch('Tvoj e-mail').blank?

      row = {
        index: i,
      }

      already_attended = line.fetch('Bol/a si už na LSTME?').strip == 'áno'
      row[:already_attended] = already_attended

      # User
      email = line.fetch('Tvoj e-mail').strip
      first_name = line.fetch('Krstné meno').strip
      last_name = line.fetch('Priezvisko').strip
      born_at = Time.zone.strptime(line.fetch('Dátum narodenia'), '%m/%d/%Y').to_date
      phone = line.fetch('Tvoje telefónne číslo').strip
      parents_phone = line.fetch('Telefónne číslo zákonného zástupcu').strip
      address = line.fetch('Ulica a číslo domu').strip
      city = line.fetch('Mesto').strip
      shirt_size = line.fetch('Akú veľkosť trička nosíš?').strip
      parents_email = line.fetch('E-mail zákonného zástupcu').strip
      gender = line.fetch('Pohlavie').strip

      user_data = {
        id: '', username: '',
        email: email, first_name: first_name, last_name: last_name,
        born_at: born_at, phone: phone, parents_phone: parents_phone,
        address: "#{address}, #{city}", shirt_size: shirt_size,
        parents_email: parents_email, gender: gender
      }
      row[:user_data] = user_data

      # Attendance

      attendance_note = line.fetch('Máš nejaké špeciálne požiadavky?')
      should_pay = line.fetch('Ma zaplatit')
      paid = line.fetch('Zaplatil celkom')

      attendance_data = {
        attendance_note: attendance_note,
        should_pay: should_pay,
        paid: paid
      }
      row[:attendance_data] = attendance_data

      row[:other_users] = []

      # Purpose of this block is to check if the user already exists in the database
      # If the user already exists, just update id and username on the user_data and store
      # original user values in row[:user] so they can be compared
      #
      # If the user does not exist, generate a new username and get possible candidates for user based on username
      # - Set the new username on user_data, since it is not loaded from the file
      # - Set the new username also on new user, since that object is used in :other_users
      #   if there are candidates and user selects "Create new user" in the UI (in case they changed the username)

      # Check if user already exists
      user = User.find_or_initialize_by(email: email)
      row[:new] = user.new_record?
      row[:user] = user
      row[:attendance] = nil

      # If user is new, preset username on user_data as that is not loaded from the file
      if user.new_record?
        # Prepare username in case this is a new user
        new_username, other_users = self.build_username(first_name, last_name)

        user.username = new_username # also set on user,
        row[:user_data][:username] = new_username

        # generate new username. This also checks for duplicates and returns them as other_users
        row[:other_users] << user
        row[:other_users] += other_users
      else
        # Update id in user_data to the one on found user, or set it to empty string if new
        row[:user_data][:id] = user.id
        row[:user_data][:username] = user.username

        # If user is not new, check if attendance already does not exist
        attendance = ChildAttendance.select(:id, :should_pay_amount, :paid_amount, :note).find_by(user: user, term: term)
        if attendance.present?
          row[:attendance] = {
            id: attendance.id,
            should_pay: attendance.should_pay_amount,
            paid: attendance.paid_amount,
            note: attendance.note
          }
        end
      end

      rows << row
    end

    rows
  end

  def self.import_data(rows, term)
    Monetize.assume_from_symbol = true
    Money.locale_backend = :i18n

    ApplicationRecord.transaction do
      rows.each do |row|
        row.deep_symbolize_keys!
        user_data = row[:user_data]

        if user_data[:id].present?
          user = User.find(user_data[:id])
        else
          user = User.find_or_initialize_by(email: user_data[:email])
        end
        user.assign_attributes(user_data)
        user.save!

        attendance_data = row[:attendance_data]

        attendance = ChildAttendance.find_or_initialize_by(
          user: user,
          term: term
        )
        attendance.note = attendance_data[:attendance_note].to_s
        attendance.should_pay_amount = Monetize.parse(attendance_data[:should_pay])
        attendance.paid_amount = Monetize.parse(attendance_data[:paid])
        attendance.save!
      end
    end
  end

  private

  def self.build_username(first_name, last_name)
    i = 1
    other_users = []
    loop do
      suffix = (i == 1 ? '' : i).to_s
      i += 1
      new_username = ActiveSupport::Inflector
        .transliterate("#{first_name}.#{last_name}#{suffix}")
        .downcase
        .gsub(' ', '-')
      existing_user = User.find_by(username: new_username)
      if existing_user.nil?
        break [new_username, other_users]
      else
        other_users << existing_user
      end
    end
  end
end
