class SelectedAdminTerm < ApplicationService
  def call
    RequestStore.store[:admin_term]
  end
end
