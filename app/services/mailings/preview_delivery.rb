module Mailings
  class PreviewDelivery < ApplicationService
    class Result
      vattr_initialize [:payload, :subject, :body]
    end

    pattr_initialize :delivery

    def call
      Result.new(
        payload: payload,
        subject: render_template(delivery.campaign.subject, payload),
        body: render_template(delivery.campaign.body, payload),
      )
    end

    private

    def payload
      @payload ||= delivery.to_liquid_hash
    end

    def render_template(template, payload)
      Liquid::Template
        .parse(template, error_mode: :strict)
        .render(payload, strict_variables: true, strict_filters: true)
    end
  end
end
