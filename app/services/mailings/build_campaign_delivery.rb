module Mailings
  class BuildCampaignDelivery < ApplicationService
    pattr_initialize :campaign, :contact

    def call
      raise 'Campaign must exist' unless campaign
      raise 'Contact must exist' unless contact

      build_delivery
    end

    private

    def build_delivery
      delivery = campaign.deliveries.new(
        {
          contact: contact,
          to_email: contact.email,
        }.merge(copy_campaign)
      )
      preview = PreviewDelivery.call(delivery).result
      delivery.assign_attributes(
        subject: preview.subject,
        body: preview.body,
      )
      delivery
    end

    def copy_campaign
      @copy_campaign ||= campaign.attributes.symbolize_keys
        .slice(:from_email, :from_name, :cc_email, :bcc_email, :reply_to_email)
    end
  end
end
