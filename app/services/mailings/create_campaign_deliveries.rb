module Mailings
  class CreateCampaignDeliveries < ApplicationService
    pattr_initialize :campaign

    def call
      raise 'Campaign must exist' unless campaign

      ActiveRecord::Base.transaction { create_deliveries }
    end

    private

    def create_deliveries
      campaign.start! unless campaign.started?

      deliveries = []
      campaign.list.contacts.find_each do |contact|
        deliveries << Mailings::BuildCampaignDelivery.call(campaign, contact).result
      end
      deliveries = append_delivery_times(deliveries)
      Delivery.import!(deliveries)
    end

    def append_delivery_times(deliveries)
      time = Time.zone.now
      deliveries.map do |delivery|
        delivery.deliver_at = time
        time += campaign.send_rate
        delivery
      end
    end
  end
end
