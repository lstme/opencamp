class CreateWalletTransaction < ApplicationService
  pattr_initialize :term, :wallet_id, [:amount, :note]

  def call
    a = term.attendances.find_by(wallet_id: wallet_id)
    return nil unless a

    Transaction.create!(attendance_id: a.id, amount: amount, note: note)
        .tap do |transaction|
      transaction.attendance.reload
    end
  end
end
