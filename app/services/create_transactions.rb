class CreateTransactions < ApplicationService
  pattr_initialize :user_id, :attendance_ids, [:amount, :note]

  def call
    Transaction.transaction do
      attendance_ids.map do |attendance_id|
        Transaction.create!(attendance_id: attendance_id, amount: amount, note: note, user_id: user_id)
            .tap do |transaction|
          transaction.attendance.reload
        end
      end
    end
  end
end
