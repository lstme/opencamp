class DeleteTransactions < ApplicationService
  pattr_initialize :transaction_ids

  def call
    Transaction
        .where(id: transaction_ids)
        .destroy_all
        .map do |transaction|
      transaction.attendance.reload
    end
  end
end
