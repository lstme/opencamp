class ChangeAttendanceRoom < ApplicationService
  class Result
    vattr_initialize [:attendance, :from_room, :to_room]
  end

  pattr_initialize [:attendance_id, :room_id]

  def call
    Attendance.transaction do
      find_attendance!
      change_room!

      build_result
    end
  end

  private

  attr_reader :attendance
  attr_reader :from_room
  attr_reader :to_room

  def find_attendance!
    @attendance = Attendance.find(attendance_id)
  end

  def change_room!
    @from_room = attendance.room

    attendance.room_id = room_id
    attendance.save!

    @from_room&.reload
    @to_room = attendance.room&.reload
  end

  def build_result
    Result.new(
        attendance: attendance,
        from_room: from_room,
        to_room: to_room,
    )
  end
end
