class ChangeAttendanceWorkgroup < ApplicationService
  class Result
    vattr_initialize [:attendance, :from_workgroup, :to_workgroup]
  end

  pattr_initialize [:attendance_id, :workgroup_id]

  def call
    Attendance.transaction do
      find_attendance!
      change_workgroup!

      build_result
    end
  end

  private

  attr_reader :attendance
  attr_reader :from_workgroup
  attr_reader :to_workgroup

  def find_attendance!
    @attendance = Attendance.find(attendance_id)
  end

  def change_workgroup!
    @from_workgroup = attendance.workgroup

    attendance.workgroup_id = workgroup_id
    attendance.save!

    @from_workgroup&.reload
    @to_workgroup = attendance.workgroup&.reload
  end

  def build_result
    Result.new(
        attendance: attendance,
        from_workgroup: from_workgroup,
        to_workgroup: to_workgroup,
    )
  end
end
