class ListDataForList < ApplicationService
  class ListDataResult
    include ActiveModel::Model

    attr_accessor :id, :list, :attendance, :list_data

    def self.build(list, attendance, list_data)
      new(
          id: [attendance.id, list.id].join('_').freeze,
          list: list,
          attendance: attendance,
          list_data: list_data
      )
    end
  end

  pattr_initialize [:list_id]

  def call
    find_list!

    query_attendances!

    prepare_all_list_data
  end

  private

  attr_reader :list
  attr_reader :attendances

  def find_list!
    @list = List
                .includes(:fields)
                .find(list_id)
  end

  def query_attendances!
    @attendances = list.term.attendances.includes(:user)

    case @list.sort_by
    when 'age'
      @attendances = @attendances
        .joins(:user)
        .order('users.born_at DESC')

    when 'room'
      @attendances = @attendances
        .left_joins(:room)
        .order('rooms.name ASC')

    when 'workgroup'
      @attendances = @attendances
        .left_joins(:workgroup)
        .order('workgroups.name ASC')
    end

    # At the end always sort by name so users are always sorted by something
    @attendances = @attendances
      .joins(:user)
      .order('users.last_name ASC, users.first_name ASC')

    @attendances = @attendances.where.not(type: 'ChildAttendance') unless @list.include_children
    @attendances = @attendances.where.not(type: 'InstructorAttendance') unless @list.include_instructors

    @attendances = @attendances.to_a
  end

  def prepare_all_list_data
    attendances.map do |attendance|
      self.class.prepare_list_data_result(list, attendance)
    end
  end

  def self.prepare_list_data_result(list, attendance)
    ListDataResult.build(
        list,
        attendance,
        prepare_list_data(list, attendance)
    )
  end

  def self.prepare_list_data(list, attendance)
    list_data = attendance.list_data
    list.fields.reduce({}) do |acc, field|
      acc[field.slug] = list_data[field.slug] if list_data.has_key?(field.slug)
      acc
    end
  end
end
