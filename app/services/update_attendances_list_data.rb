class UpdateAttendancesListData < ApplicationService
  pattr_initialize [:attendance_ids, :list_id, :list_data]

  def call
    attendances = []
    Attendance.transaction do
      find_list!

      attendances = Attendance
          .where(id: attendance_ids)
          .map do |attendance|
        update_list_data!(attendance)
        attendance
      end
    end
    attendance_ids.map{ |id| trigger_subscription_event(id) }

    attendances
  end

  private

  attr_reader :list

  def find_list!
    @list = List
                .includes(:fields)
                .find(list_id)
  end

  def update_list_data!(attendance)
    attendance.update!(
        list_data: prepare_list_data(attendance)
    )
    attendance.reload
  end

  def prepare_list_data(attendance)
    list
        .fields
        .reduce(attendance.list_data.clone) do |acc, field|
      acc[field.slug] = list_data[field.slug] if list_data.has_key?(field.slug)
      acc
    end
  end

  def trigger_subscription_event(attendance_id)
    OpencampSchema.subscriptions.trigger('list_data_result_was_changed', { id: list.id }, { attendance_id: attendance_id })
  end
end
