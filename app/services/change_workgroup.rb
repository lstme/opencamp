class ChangeWorkgroup < ApplicationService
  pattr_initialize [:workgroup_id, :note]

  def call
    Workgroup.transaction do
      find_workgroup
      change_workgroup!

      workgroup
    end
  end

  private

  attr_reader :workgroup

  def find_workgroup
    @workgroup = Workgroup.where(id: workgroup_id).first
  end

  def change_workgroup!
    return unless workgroup

    workgroup.note = note
    workgroup.save!
  end
end
