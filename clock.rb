require 'clockwork'
require './config/boot'
require './config/environment'

Thread.report_on_exception = true

module Clockwork

  every(1.minute, 'send_scheduled_mailings_deliveries') do
    Mailings::SendScheduledDeliveriesJob.perform_async
  end

  error_handler do |error|
    Rails.logger.error(error)
  end
end
