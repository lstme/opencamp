source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 7.0.3'
# Use postgresql as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'sassc-rails'
gem 'haml-rails'

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

gem 'sprockets-rails'

gem 'vite_rails'

gem 'rails-i18n'

gem 'devise'
gem 'devise-i18n'

gem 'activeadmin'
gem 'arctic_admin'
gem 'active_admin-duplicatable'
gem 'amoeba'
gem 'active_admin_import'

gem 'activerecord-import'
gem 'counter_culture'
gem 'activeadmin_addons'
gem 'activeadmin_json_editor'

gem 'graphql'
gem 'graphql-batch'
gem 'graphql-pundit', git: 'https://github.com/durcak/graphql-pundit.git', branch: 'master'
gem 'pundit'

gem 'rails-patterns'
gem 'attr_extras'
gem 'request_store'

gem 'active_link_to'

gem 'pry-rails'
gem 'image_processing'
gem 'friendly_id'
gem 'jsonb_accessor'
gem 'redis'
gem 'rack-cors'
gem 'postmark-rails'
gem 'chroma'
gem 'activerecord-typedstore'
gem 'active_record-events'
gem 'liquid'
gem 'sidekiq'
gem 'clockwork'
gem 'doorkeeper'
gem 'colorize'
gem 'monetize'
gem 'dotenv-rails'

group :development, :test do
  gem 'faker'
  gem 'ruby-prof'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console'
  gem 'listen'
end

gem "phonelib", "~> 0.8.2"

gem "annotate", "~> 3.2"

gem "nilify_blanks", "~> 1.4"

gem "paper_trail", "~> 15.1"
