import react from '@vitejs/plugin-react';
import { resolve } from 'path';
import { defineConfig, UserConfigExport } from 'vite';
import checker from 'vite-plugin-checker';
import RubyPlugin from 'vite-plugin-ruby';
import legacy from '@vitejs/plugin-legacy';

export default defineConfig(({ mode }) => {
    const cfg: UserConfigExport = {
        resolve: {
            alias: {
                atom: resolve(__dirname, 'app/javascript/atoms'),
                lib: resolve(__dirname, 'app/javascript/lib'),
                molecule: resolve(__dirname, 'app/javascript/molecules'),
                organism: resolve(__dirname, 'app/javascript/organisms'),
                template: resolve(__dirname, 'app/javascript/templates'),
                page: resolve(__dirname, 'app/javascript/pages'),
                helpers: resolve(__dirname, 'app/javascript/helpers'),
                '~graphql.generated': resolve(__dirname, 'app/javascript/graphql.generated.ts'),
                '~constants': resolve(__dirname, 'app/javascript/constants.ts'),
                '~bootstrap': resolve(__dirname, 'node_modules/bootstrap')
            }
        },
        plugins: [
            RubyPlugin(),
            react(),
            checker({
                typescript: true,
                overlay: true
            }),
            legacy({
                targets: ['defaults', 'iOS >= 12.5']
            })
        ]
    };

    if (mode === 'development') {
        cfg.build = {
            rollupOptions: {
                external: ['/@vite-plugin-checker-runtime']
            }
        };
    }

    return cfg;
});
