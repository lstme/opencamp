namespace :graphql do
  desc 'Dumps the GraphQL IDL schema'
  task dump: [:environment] do
    definition = OpencampSchema.to_definition
    File.write(Rails.root.join('app', 'graphql', 'schema.graphql'), definition)
  end
end
