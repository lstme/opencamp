class ErrorsSerializer < SimpleDelegator
  def initialize(target)
    if target.is_a? ActiveModel::Errors
      super
    elsif target.is_a? Hash
      super create_error_object(target)
    else
      super ActiveModel::Errors.new(target)
    end
  end

  def full_messages
    Hash[messages.keys.map do |field_key|
      [field_key, full_messages_for(field_key)]
    end]
  end

  private

  def create_error_object(values)
    object = ActiveModel::Errors.new(SimpleErrorClass.new)
    values.map { |k, v| object.add k.to_sym, v }
    object
  end

  class SimpleErrorClass
    include ActiveModel::Model
  end
end
