src_term = Term.find_by!(name: 'LSTME 2022')
dst_term = Term.find_by!(name: 'LSTME 2023')

src_term.onboarding_requirements.map do |x|
  dst_term.onboarding_requirements.build(
    title: x.title,
    description: x.description,
    position: x.position
  ).save!
end
