Phonelib.default_country = "SK"

phone_numbers = []
until STDIN.eof?
  line = STDIN.readline

  phone_number = Phonelib.parse(line)

  phone_numbers << phone_number
end

puts

phone_numbers.each do |phone_number|
  puts "'#{phone_number.e164}"
end
