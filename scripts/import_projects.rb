input_file = ARGV.fetch(0)

ApplicationRecord.transaction do
  records = []
  term = LatestTermQuery.call.first

  CSV.foreach(input_file, col_sep: ',', headers: true, quote_char: '"') do |line|
    next if line['Alias'].blank?

    project_alias = line['Alias'].to_s.strip.upcase
    capacity = line['Maximálny počet účasníkov'].to_i
    category = line['Druh zamestnania'].strip
    description = line['Popis činnosti'].strip
    prerequisites = line['Prerekvizity'].to_s.strip
    published = false
    slots = line['Rozsah projektu'].to_i
    title = line['Názov projektu'].strip
    teacher_name = line['Vedúci projektu'].strip
    teacher = User.find_by!(%{word_similarity(concat(users.first_name, ' ', users.last_name), ?::text) > 0.5}, teacher_name)

    records << {
      alias: project_alias,
      capacity:,
      category:,
      description:,
      prerequisites:,
      published:,
      slots:,
      title:,
      teacher_id: teacher.id,
    }
  end

  puts "ALL #{records.size}"
  binding.pry

  projects = records.map do |record|
    term.projects.create!(record)
  end

  binding.pry

  # raise ActiveRecord::Rollback
end
