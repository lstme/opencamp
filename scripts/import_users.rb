Monetize.assume_from_symbol = true
Money.locale_backend = :i18n

input_file = ARGV.fetch(0)

class ImportUser < Struct.new(:user, :note, :should_pay, :paid, keyword_init: true)

end

ApplicationRecord.transaction do
  users = []
  CSV.foreach(input_file, col_sep: ',', headers: true, quote_char: '"') do |line|
    # User
    already_attended = line.fetch('Bol/a si už na LSTME?').strip == 'áno'
    email = line.fetch('Email Address').strip
    first_name = line.fetch('Krstné meno').strip
    last_name = line.fetch('Priezvisko').strip
    born_at = Time.zone.strptime(line.fetch('Dátum narodenia'), '%m/%d/%Y').to_date
    phone = line.fetch('Tvoje telefónne číslo (opravene)').strip
    parents_phone = line.fetch('Telefónne číslo zákonného zástupcu (opravene)').strip
    address = line.fetch('Adresa').strip
    city = line.fetch('Mesto').strip
    shirt_size = line.fetch('Akú veľkosť trička nosíš?').strip
    parents_email = line.fetch('E-mail zákonného zástupcu').strip
    gender = line.fetch('Pohlavie').strip

    # Attendance

    attendance_note = line.fetch('Máš nejaké špeciálne požiadavky?')
    should_pay = line.fetch('Ma zaplatit')
    paid = line.fetch('Zaplatil celkom')

    user = User.find_or_initialize_by(email: email)
    user.first_name = first_name
    user.last_name = last_name
    user.born_at = born_at
    user.phone = phone
    user.parents_phone = parents_phone
    user.address = "#{address}, #{city}"
    user.shirt_size = shirt_size
    user.parents_email = parents_email
    user.gender = gender

    puts email.colorize(user.new_record? ? :red : :green)

    if user.new_record?
      if already_attended
        puts 'ALREADY ATTENDED'.colorize(:red)
        binding.pry
      end
      i = 1
      user.username = loop do
        suffix = (i == 1 ? '' : i).to_s
        i += 1
        new_username = ActiveSupport::Inflector
          .transliterate("#{first_name}.#{last_name}#{suffix}")
          .downcase
          .gsub(' ', '-')
        existing_user = User.find_by(username: new_username)
        if existing_user.nil?
          break new_username
        end
      end
    end

    if user.changes.any?
      user.changes.map do |attribute, (from, to)|
        puts ('%16s  %s => %s' % [attribute, from.inspect, to.inspect]).colorize(:yellow)
      end
    end

    users << ImportUser.new(
      user:, note: attendance_note, should_pay:, paid:,
    )
  end

  puts "ALL #{users.pluck(:user).size}, NEW #{users.pluck(:user).select(&:new_record?).size}, EXISTING #{users.pluck(:user).size - users.pluck(:user).select(&:new_record?).size}"
  binding.pry

  users.map do |import_user|
    user = import_user.user
    user.save!

    attendance = ChildAttendance.create_with(
      note: import_user.note.to_s,
      paid_amount: Monetize.parse(import_user.paid),
    ).find_or_create_by!(
      user: user,
      term: LatestTermQuery.call.first,
    )
    puts ('  %s' % [attendance.attributes]).colorize(:light_blue)

    puts
  end

  # raise ActiveRecord::Rollback
end
