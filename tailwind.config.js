const { colors } = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        './app/helpers/**/*.rb',
        './app/javascript/**/*.js',
        './app/javascript/**/*.jsx',
        './app/javascript/**/*.ts',
        './app/javascript/**/*.tsx',
        './app/views/**/*',
        './app/controllers/**/*',
        './app/assets/stylesheets/**/*.css',
        './config/initializers/*.rb'
    ],
    darkMode: 'class',
    plugins: [require('@tailwindcss/aspect-ratio')],
    theme: {
        // Sizes from tabler
        screens: {
            sm: '576px',
            md: '768px',
            lg: '992px',
            xl: '1200px',
            xxl: '1400px'
        },
        extend: {
            colors: {
                ...colors,
                coin: 'gold',
                primary: 'orange',
                'primary-dark': '#e69500',
                instructor: '#c05621',
                child: '#4a5568'
            },
            spacing: {
                72: '18rem',
                84: '21rem',
                96: '24rem',
                128: '32rem'
            }
        }
    },
    variants: {},
    prefix: 'tw-'
};
