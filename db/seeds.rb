require 'fileutils'

Rails.logger = Logger.new(STDOUT)

def seed_admin
  Rails.logger.info 'Seeding admin'
  User.create!(
    is_admin: true,
    username: 'admin',
    email: 'admin@example.com',
    first_name: 'Admin',
    last_name: 'Adminovič',
    born_at: Faker::Date.birthday(min_age: 12, max_age: 40),
    gender: 'MALE',
    password: 'password',
    password_confirmation: 'password'
  )
end

def seed_terms
  Rails.logger.info 'Seeding terms'
  (Time.now.year - 3).upto(Time.now.year).map do |year|
    Term.new(
      name: 'LSTME %s' % year,
      starts_at: Date.parse("#{year}-08-17"),
      ends_at: Date.parse("#{year}-08-31"),
      address: Faker::Address.full_address
    )
  end.tap do |terms|
    Term.import!(terms)
  end
end

def seed_onboarding_requirements
  Rails.logger.info 'Seeding onboarding requirements'
  Term.all.map do |term|
    Faker::Number
        .between(from: 5, to: 10)
        .times.map do |i|
      OnboardingRequirement.new(
        term: term,
        title: Faker::Lorem.question,
        description: [Faker::Lorem.sentence, ''].sample,
        position: i
      )
    end
  end.tap do |onboarding_requirements|
    OnboardingRequirement.import!(onboarding_requirements.flatten)
  end
end

def seed_workgroups
  Rails.logger.info 'Seeding workgroups'
  Term.all.map do |term|
    4.times.map do |i|
      Workgroup.new(
        term: term,
        name: "Group #{i + 1}",
        note: [Faker::Lorem.sentence, ''].sample
      )
    end
  end.tap do |workgroups|
    Workgroup.import!(workgroups.flatten)
    Workgroup.counter_culture_fix_counts
  end
end

def seed_rooms
  Rails.logger.info 'Seeding rooms'
  Term.all.map do |term|
    room_counter = 1
    rooms = []

    # Children rooms
    rooms += begin
      num_rooms = Faker::Number.between(from: 10, to: 15)

      num_rooms.times.map do
        capacity = Faker::Number.between(from: 2, to: 5)

        name = sprintf("Room %02d", room_counter)
        room = Room.new(
          name: name,
          term: term,
          capacity: capacity,
          instructor_only: false,
          note: ['', Faker::Lorem.sentence].sample
        )
        room_counter += 1
        room
      end
    end

    # Instructor rooms
    rooms += begin
      num_rooms = Faker::Number.between(from: 5, to: 10)

      num_rooms.times.map do |i|
        capacity = Faker::Number.between(from: 2, to: 5)

        name = sprintf("Room %02d", room_counter)
        room = Room.new(
          name: name,
          term: term,
          capacity: capacity,
          instructor_only: true
        )
        room_counter += 1
        room
      end
    end

    rooms
  end.tap do |rooms|
    Room.import!(rooms.flatten)
    Room.counter_culture_fix_counts
  end
end

def seed_users
  Rails.logger.info 'Seeding users'

  120.times.map do
    gender = User::GENDERS.sample

    first_name = gender == 'MALE' ? Faker::Name.male_first_name : Faker::Name.female_first_name
    last_name = gender == 'MALE' ? Faker::Name.man_last_name : Faker::Name.woman_last_name

    username = [first_name, last_name]
                   .map(&:parameterize)
                   .map(&:downcase)
                   .join('.')
                   .then { |s| "#{s}#{Faker::Number.between(from: 100, to: 999)}" }

    born_at = Faker::Date.birthday(min_age: 12, max_age: 40)
    User.new(
      is_admin: false,
      username: username,
      email: Faker::Internet.unique.email(name: username),
      first_name: first_name,
      last_name: last_name,
      born_at: born_at,
      gender: gender,
      address: Faker::Address.full_address,
      phone: Faker::PhoneNumber.cell_phone_with_country_code,
      parents_phone: Faker::PhoneNumber.cell_phone_with_country_code,
      school: Faker::Company.name,
      note: ['', Faker::Lorem.sentence].sample,
      password: 'password',
      password_confirmation: 'password'
    )
  end.tap do |users|
    User.import!(users)

    male_avatars = Dir.glob(Rails.root.join('vendor', 'fixtures', 'avatars', 'male', '*.jpg'))
    female_avatars = Dir.glob(Rails.root.join('vendor', 'fixtures', 'avatars', 'female', '*.jpg'))

    log_level = Rails.logger.level
    Rails.logger.level = Logger::WARN
    users.map do |user|
      avatar_path = (user.gender == 'MALE' ? male_avatars : female_avatars).pop
      avatar_file_name = File.basename(avatar_path)
      user.avatar.attach(io: File.open(avatar_path), filename: avatar_file_name, content_type: 'image/jpeg')
    end
    Rails.logger.level = log_level
  end
end

def seed_labels
  Rails.logger.info 'Seeding labels'
  groups = [
      LabelGroup.new(name: 'Programovanie', color: '#ff7c6b'),
      LabelGroup.new(name: 'Robotika', color: '#001EC3'),
  ]
  LabelGroup.import!(groups)

  labels = [
      Label.new(label_group: groups[0], name: 'Programovanie v C++'),
      Label.new(label_group: groups[0], name: 'Proceduralne programovanie'),
      Label.new(label_group: groups[1], name: 'Zaklady robotiky'),
      Label.new(label_group: groups[1], name: '3 pravidla robotiky'),
  ]
  Label.import!(labels)

  user_labels = User.all.map do |user|
    labels.sample(Random.rand(0..3)).map do |label|
      UserLabel.new(user: user, label: label)
    end
  end
  UserLabel.import!(user_labels.flatten)
end

def seed_attendances
  Rails.logger.info 'Seeding attendances'
  young_users = User.where("born_at > (now() - INTERVAL '18 years')").to_a
  older_users = User.where("born_at <= (now() - INTERVAL '18 years')").to_a
  Term.all.map do |term|
    # instructors
    selected_instructors = older_users.sample(Faker::Number.between(from: 10, to: 20))
    instructor_attendances = selected_instructors.map do |instructor|
      InstructorAttendance.new(user: instructor, term: term, note: [Faker::Lorem.sentence, ''].sample)
    end

    available_capacity = term.capacity - selected_instructors.size

    # children
    selected_children = young_users
                            .select do |user|
      user.attendances.where(term: term).count == 0
    end
                            .sample(Faker::Number.between(from: (available_capacity * 0.8).to_i, to: available_capacity))
    children_attendances = selected_children.map do |child|
      ChildAttendance.new(user: child, term: term, note: [Faker::Lorem.sentence, ''].sample, paid_amount: Faker::Number.between(from: 0.0, to: 250.0))
    end

    children_attendances + instructor_attendances
  end.tap do |attendances|
    Attendance.import!(attendances.flatten)
    Attendance.counter_culture_fix_counts
    ChildAttendance.counter_culture_fix_counts
    InstructorAttendance.counter_culture_fix_counts
  end
end

def seed_transactions
  Rails.logger.info 'Seeding transactions'
  Term.all.map do |term|
    attendances = term.child_attendances.includes(:term)

    transactions = []
    transaction_map = {}

    attendances.map do |attendance|
      num_transactions = Faker::Number.between(from: 5, to: 20)

      attendance_transactions = 0.upto(num_transactions).map do
        transaction_map[attendance.id] ||= 0
        current_amount = transaction_map[attendance.id]

        possible_amounts = [
            current_amount > 0 ? Faker::Number.between(from: -current_amount, to: -1) : nil,
            Faker::Number.between(from: 1, to: 10)
        ].compact
        amount = possible_amounts.sample

        transaction_map[attendance.id] += amount

        Transaction.new(attendance: attendance, amount: amount, note: Faker::Lorem.sentence)
      end

      transactions += attendance_transactions

      transactions
    end

    transactions
  end.tap do |transactions|
    Transaction.import!(transactions.flatten)
    Transaction.counter_culture_fix_counts
  end
end

def seed_lists
  Rails.logger.info 'Seeding lists'
  Term.all.map do |term|
    Faker::Number
        .between(from: 3, to: 10)
        .times.map do |i|
      List.new(
        term: term,
        name: '%s %d' % [Faker::Commerce.product_name, Faker::Number.between(from: 100, to: 999)]
      )
    end
  end.map do |lists|
    List.import!(lists.flatten)
  end
end

def seed_fields
  Rails.logger.info 'Seeding fields'
  Term.all.map do |term|
    Faker::Number
        .between(from: 3, to: 15)
        .times.map do |i|
      type = Field::TYPES.sample
      name = '%s %d' % [type.gsub(/Field$/, ''), Faker::Number.between(from: 100, to: 999)]
      Field.new(
        term: term,
        name: name,
        slug: name,
        type: type
      )
    end
  end.map do |fields|
    Field.import!(fields.flatten)
  end
end

def seed_list_fields
  Rails.logger.info 'Seeding list fields'
  Term.all.map do |term|
    fields = term.fields.to_a
    term.lists.map do |list|
      selected_fields = fields.sample(Faker::Number.between(from: 3, to: 10))
      selected_fields.map do |field|
        ListField.new(list: list, field: field)
      end
    end.map do |list_fields|
      ListField.import!(list_fields.flatten)
    end
  end
end

def seed_admin_attendance
  Rails.logger.info 'Seeding admin attendance'
  InstructorAttendance.create!(term: Term.reorder(name: :desc).first, user: User.find_by(username: 'admin'))
end

def seed_mailings_contacts
  Rails.logger.info 'Seeding mailings contacts'
  User.find_each do |user|
    Mailings::Contact.create!(
      user.attributes.symbolize_keys.slice(
        :email,
          :username,
          :first_name,
          :last_name,
          :born_at,
          :gender,
          :address,
          :phone,
          :parents_phone,
          :school,
          :note
      )
    )
  end
end

def seed_mailings_lists
  Rails.logger.info 'Seeding mailings lists'
  adults = Mailings::List.create!(name: 'dospely')
  children = Mailings::List.create!(name: 'deti')

  Mailings::Contact.where(email: User.adult.pluck(:email)).find_each do |contact|
    Mailings::ListsContact.create!(list: adults, contact: contact)
  end
  Mailings::Contact.where(email: User.child.pluck(:email)).find_each do |contact|
    Mailings::ListsContact.create!(list: children, contact: contact)
  end
end

def seed_mailings_campaigns
  Rails.logger.info 'Seeding mailings campaigns'
  Mailings::List.find_each do |list|
    campaign = Mailings::Campaign.create!(
      name: 'Oznam o hygienickych pokynoch',
      list: list,
      body: Mailings::Campaign.new.default_body.join("\n"),
      from_email: 'from@example.com',
      from_name: 'Awesome Camp',
      subject: 'Oznam o hygienickych pokynoch',
      cc_email: 'cc@example.com',
      bcc_email: 'bcc@example.com',
      reply_to_email: 'reply@example.com',
      send_rate: 2.minutes,
    )
  end
end

def seed_cleanup!
  Rails.logger.info 'Cleaning ...'

  FileUtils.rm_rf(Dir.glob(Rails.root.join('storage', '*')))

  ActiveStorage::Attachment.delete_all
  ActiveStorage::Blob.delete_all
  ListField.delete_all
  Field.delete_all
  List.delete_all
  Transaction.delete_all
  Attendance.delete_all
  UserLabel.delete_all
  Label.delete_all
  User.delete_all
  LabelGroup.delete_all
  RoomScore.delete_all
  Room.delete_all
  Workgroup.delete_all
  OnboardingRequirement.delete_all
  Term.delete_all

  Mailings::Delivery.delete_all
  Mailings::Campaign.delete_all
  Mailings::ListsContact.delete_all
  Mailings::List.delete_all
  Mailings::Contact.delete_all
end

if Rails.env.development?
  Faker::Config.locale = :sk

  seed_cleanup!

  seed_terms
  seed_onboarding_requirements
  seed_workgroups
  seed_rooms
  seed_users
  seed_labels
  seed_attendances
  # seed_transactions
  seed_lists
  seed_fields
  seed_list_fields
  seed_admin
  seed_admin_attendance
  seed_mailings_contacts
  seed_mailings_lists
  seed_mailings_campaigns

  Rails.logger.info 'Done.'
end
