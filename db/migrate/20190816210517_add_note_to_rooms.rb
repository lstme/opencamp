class AddNoteToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :note, :text, null: false, default: ''
  end
end
