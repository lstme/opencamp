class CreateRoomScores < ActiveRecord::Migration[5.2]
  def change
    create_table :room_scores, id: :uuid do |t|
      t.references :room, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.integer :score, null: false, default: 0
      t.date :date, null: false

      t.timestamps
    end
  end
end
