class AddDetailsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :address, :text, null: true
    add_column :users, :phone, :string, null: true
    add_column :users, :parents_phone, :string, null: true
    add_column :users, :school, :text, null: true
    add_column :users, :note, :text, null: false, default: ''
  end
end
