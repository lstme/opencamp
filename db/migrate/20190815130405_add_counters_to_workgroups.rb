class AddCountersToWorkgroups < ActiveRecord::Migration[5.2]
  def change
    add_column :workgroups, :attendances_count, :integer, null: false, default: 0
  end
end
