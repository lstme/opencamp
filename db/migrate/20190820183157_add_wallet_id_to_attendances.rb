class AddWalletIdToAttendances < ActiveRecord::Migration[5.2]
  def change
    add_column :attendances, :wallet_id, :integer, null: true, index: true
  end
end
