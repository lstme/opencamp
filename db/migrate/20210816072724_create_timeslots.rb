class CreateTimeslots < ActiveRecord::Migration[6.0]
  def change
    create_table :timeslots, id: :uuid do |t|
      t.references :timetable, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.string :time, null: false
      t.boolean :is_highlighted, null: false, default: false
      t.string :label, null: false

      t.timestamps
    end

    add_index :timeslots, :is_highlighted
    add_index :timeslots, [:timetable_id, :time]
  end
end
