class CreateAttendances < ActiveRecord::Migration[5.2]
  def change
    create_table :attendances, id: :uuid do |t|
      t.references :user, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :type

      t.timestamps
    end
  end
end
