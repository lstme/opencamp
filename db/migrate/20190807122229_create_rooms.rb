class CreateRooms < ActiveRecord::Migration[5.2]
  def change
    create_table :rooms, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :name, null: false
      t.integer :capacity, null: false, default: 0

      t.timestamps
    end
  end
end
