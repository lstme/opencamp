class CreateTerms < ActiveRecord::Migration[5.2]
  def change
    create_table :terms, id: :uuid do |t|
      t.string :name, null: false
      t.date :starts_at
      t.date :ends_at
      t.string :address
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
    add_index :terms, :name, unique: true
  end
end
