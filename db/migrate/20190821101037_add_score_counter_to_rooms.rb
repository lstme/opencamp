class AddScoreCounterToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :score, :integer, null: false, default: 0
  end
end
