class AddWalletIdConstraintsToAttendances < ActiveRecord::Migration[5.2]
  def change
    add_index :attendances, %i{wallet_id term_id}, unique: true
  end
end
