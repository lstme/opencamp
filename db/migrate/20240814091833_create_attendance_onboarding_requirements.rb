class CreateAttendanceOnboardingRequirements < ActiveRecord::Migration[7.0]
  def change
    create_table :attendance_onboarding_requirements, id: :uuid do |t|
      t.references :attendance, type: :uuid, null: false, foreign_key: true
      t.references :onboarding_requirement, type: :uuid, null: false, foreign_key: {on_delete: :restrict},
        index: { name: 'index_att_onboarding_requirements_on_onboarding_requirement_id' }
      t.jsonb :data, null: false, default: {}

      t.timestamps
    end

    add_index :attendance_onboarding_requirements, [:attendance_id, :onboarding_requirement_id], unique: true,
      name: 'index_att_onb_requirements_on_att_id_and_onb_requirement_id'
  end
end
