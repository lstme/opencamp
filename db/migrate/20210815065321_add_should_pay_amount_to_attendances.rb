class AddShouldPayAmountToAttendances < ActiveRecord::Migration[6.0]
  def change
    add_column :attendances, :should_pay_amount, :decimal, null: false, default: '0.0', precision: 8, scale: 2
  end
end
