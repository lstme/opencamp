class CreateMailingsContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :mailings_contacts, id: :uuid do |t|
      t.string :email, index: { unique: true }
      t.jsonb :data
      t.timestamps
    end

    create_table :mailings_lists, id: :uuid do |t|
      t.string :name, null: false
      t.timestamps
    end

    create_table :mailings_lists_contacts, id: :uuid do |t|
      t.references :contact, type: :uuid, index: true, null: false, foreign_key: { to_table: 'mailings_contacts', on_delete: :restrict  }
      t.references :list, type: :uuid, index: true, null: false, foreign_key: { to_table: 'mailings_lists', on_delete: :restrict }
      t.timestamps
    end

    create_table :mailings_campaigns, id: :uuid do |t|
      t.string :name, null: false
      t.references :list, type: :uuid, index: true, null: false, foreign_key: { to_table: 'mailings_lists', on_delete: :restrict }

      t.string :from_email
      t.string :subject
      t.text :body
      t.string :from_name
      t.string :cc_email
      t.string :bcc_email
      t.string :reply_to_email

      t.integer :send_rate, null: false, default: 0
      t.datetime :started_at
      t.timestamps
    end

    create_table :mailings_deliveries, id: :uuid do |t|
      t.references :contact, type: :uuid, index: true, null: false, foreign_key: { to_table: 'mailings_contacts', on_delete: :restrict  }
      t.references :campaign, type: :uuid, index: true, null: false, foreign_key: { to_table: 'mailings_campaigns', on_delete: :restrict }
      t.string :to_email

      t.string :from_email
      t.string :subject
      t.text :body
      t.string :from_name
      t.string :cc_email
      t.string :bcc_email
      t.string :reply_to_email

      t.timestamp :deliver_at, null: false
      t.timestamp :delivered_at
      t.timestamp :failed_at
      t.jsonb :error
      t.timestamps
    end
  end
end
