class CreateLabels < ActiveRecord::Migration[6.0]
  def change
    create_table :label_groups, id: :uuid do |t|
      t.string :name, null: false
      t.string :color

      t.timestamps
    end

    create_table :labels, id: :uuid do |t|
      t.string :name, null: false
      t.references :label_group, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.timestamps
    end

    create_table :user_labels, id: :uuid do |t|
      t.references :user, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.references :label, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
    end

    add_index :user_labels, %i[user_id label_id], unique: true
  end
end
