class CreateTransactionsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions, id: :uuid do |t|
      t.references :attendance, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.integer :amount, null: false, default: 0

      t.timestamps
    end
  end
end
