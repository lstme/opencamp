class AddAuthorToTransactions < ActiveRecord::Migration[7.0]
  def change
    add_reference :transactions, :user, type: :uuid, index: true, null: true, foreign_key: { on_delete: :restrict }

    reversible do |dir|
      dir.up do
        Transaction.update_all(user_id: User.first.id) if User.any?
      end
    end

    change_column_null :transactions, :user_id, false
  end
end
