class AddCountersToAttendances < ActiveRecord::Migration[5.2]
  def change
    add_column :attendances, :coins, :integer, null: false, default: 0
    add_column :attendances, :transactions_count, :integer, null: false, default: 0
  end
end
