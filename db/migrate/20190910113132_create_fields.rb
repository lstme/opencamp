class CreateFields < ActiveRecord::Migration[6.0]
  def change
    create_table :fields, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :name, null: false
      t.string :slug, null: false
      t.string :type, null: false
      t.jsonb :settings, null: false, default: {}

      t.timestamps
    end

    add_index :fields, [:term_id, :slug], unique: true
  end
end
