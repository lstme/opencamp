class AddSelectedTermToUsers < ActiveRecord::Migration[7.0]
  def change
    add_reference :users, :selected_term, null: true, type: :uuid, index: true, foreign_key: { to_table: :terms, on_delete: :restrict }
  end
end
