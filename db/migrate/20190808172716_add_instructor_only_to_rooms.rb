class AddInstructorOnlyToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :instructor_only, :boolean, null: false, default: false
  end
end
