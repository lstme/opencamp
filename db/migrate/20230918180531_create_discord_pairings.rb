class CreateDiscordPairings < ActiveRecord::Migration[7.0]
  def change
    create_table :discord_pairings, id: :uuid do |t|
      t.references :user, type: :uuid, index: true, null: true, foreign_key: { on_delete: :restrict }

      t.string :pairing_token, index: true, null: false
      t.string :discord_id, index:true, null: false

      t.timestamps
    end
  end
end
