class AddUniqIndexToAttendances < ActiveRecord::Migration[5.2]
  def change
    add_index :attendances, %i{user_id term_id}, unique: true
    add_index :attendances, %i{user_id room_id}, unique: true
  end
end
