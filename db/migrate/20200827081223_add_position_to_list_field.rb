class AddPositionToListField < ActiveRecord::Migration[6.0]
  def change
    add_column :list_fields, :position, :integer, null: false, default: 0
  end
end
