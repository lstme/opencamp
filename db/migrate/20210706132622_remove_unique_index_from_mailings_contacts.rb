class RemoveUniqueIndexFromMailingsContacts < ActiveRecord::Migration[6.0]
  def change
    remove_index :mailings_contacts, :email
    add_index :mailings_contacts, :email
  end
end
