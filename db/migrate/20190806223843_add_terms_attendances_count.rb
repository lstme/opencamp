class AddTermsAttendancesCount < ActiveRecord::Migration[5.2]
  def change
    add_column :terms, :attendances_count, :integer, null: false, default: 0
    add_column :terms, :child_attendances_count, :integer, null: false, default: 0
    add_column :terms, :instructor_attendances_count, :integer, null: false, default: 0
  end
end
