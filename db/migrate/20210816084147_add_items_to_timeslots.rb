class AddItemsToTimeslots < ActiveRecord::Migration[6.0]
  def change
    add_column :timeslots, :items, :jsonb, null: false, default: []
  end
end
