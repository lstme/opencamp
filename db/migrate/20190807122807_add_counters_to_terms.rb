class AddCountersToTerms < ActiveRecord::Migration[5.2]
  def change
    add_column :terms, :rooms_count, :integer, null: false, default: 0
    add_column :terms, :capacity, :integer, null: false, default: 0
  end
end
