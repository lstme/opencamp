class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.boolean :published, null: false, default: false
      t.string :category, null: false
      t.string :alias, null: false
      t.string :title, null: false
      t.text :description, null: true
      t.text :prerequisites, null: true
      t.references :teacher, null: false, foreign_key: { to_table: :users }, type: :uuid
      t.integer :slots, null: false, default: 1
      t.integer :capacity, null: false, default: 10

      t.timestamps
    end
  end
end
