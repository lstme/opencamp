class CreateTimetables < ActiveRecord::Migration[6.0]
  def change
    create_table :timetables, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.date :date, null: false
      t.references :designated_head, type: :uuid, index: true, null: false, foreign_key: { to_table: :attendances, on_delete: :restrict }
      t.references :designated_room, type: :uuid, index: true, null: false, foreign_key: { to_table: :rooms, on_delete: :restrict }

      t.text :notice, null: false, default: ''

      t.timestamps
    end
    add_index :timetables, :date
  end
end
