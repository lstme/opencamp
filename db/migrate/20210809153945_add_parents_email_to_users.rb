class AddParentsEmailToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :parents_email, :string
  end
end
