class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.string :slug, null: false
      t.string :title, null: false
      t.string :mode, null: false, default: 'inline_code'
      t.text :code, null: false, default: ''
      t.string :local_file_path, null: false, default: ''

      t.timestamps
    end

    add_index :documents, [:term_id, :slug], unique: true
  end
end
