class AddDataColumnsToMailingsContacts < ActiveRecord::Migration[6.0]
  def change
    add_column :mailings_contacts, :username, :string
    add_column :mailings_contacts, :first_name, :string
    add_column :mailings_contacts, :last_name, :string
    add_column :mailings_contacts, :born_at, :date
    add_column :mailings_contacts, :gender, :string
    add_column :mailings_contacts, :address, :string
    add_column :mailings_contacts, :phone, :string
    add_column :mailings_contacts, :parents_phone, :string
    add_column :mailings_contacts, :school, :string
    add_column :mailings_contacts, :note, :string

    Mailings::Contact.find_each do |contact|
      data = YAML.load(contact.data)
      contact.update!(
        username: data['username'],
        first_name: data['first_name'],
        last_name: data['last_name'],
        born_at: data['born_at']&.to_date,
        gender: data['gender'],
        address: data['address'],
        phone: data['phone'],
        parents_phone: data['parents_phone'],
        school: data['school'],
        note: data['note']
      )
    end
  end
end
