class AddListDataToAttendances < ActiveRecord::Migration[6.0]
  def change
    add_column :attendances, :list_data, :jsonb, null: false, default: {}
  end
end
