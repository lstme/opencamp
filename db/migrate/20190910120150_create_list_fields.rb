class CreateListFields < ActiveRecord::Migration[6.0]
  def change
    create_table :list_fields, id: :uuid do |t|
      t.references :list, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.references :field, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.timestamps
    end
  end
end
