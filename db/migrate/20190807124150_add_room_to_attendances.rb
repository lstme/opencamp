class AddRoomToAttendances < ActiveRecord::Migration[5.2]
  def change
    add_reference :attendances, :room, type: :uuid, index: true, null: true, foreign_key: { on_delete: :restrict }
  end
end
