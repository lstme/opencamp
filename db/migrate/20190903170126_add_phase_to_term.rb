class AddPhaseToTerm < ActiveRecord::Migration[5.2]
  def change
    add_column :terms, :phase, :integer, null: false, index: true, default: 0
  end
end
