class CreateUploads < ActiveRecord::Migration[7.0]
  def change
    create_table :uploads, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.timestamps
    end
  end
end
