class CreateTimeslotItems < ActiveRecord::Migration[7.0]
  def change
    create_table :timeslot_items, id: :uuid do |t|
      t.references :timeslot, null: false, type: :uuid, foreign_key: { on_delete: :restrict }

      t.string :label
      t.string :title, null: false

      t.timestamps
    end

    rename_column :timeslots, :items, :old_items

    Timeslot.reset_column_information

    Timeslot
      .includes(timetable: { term: [:workgroups] })
      .find_each do |timeslot|
      workgroups = timeslot.timetable.term.workgroups.sort_by(&:name)

      timeslot.old_items.map.with_index do |item, i|
        timeslot.items.create!(
          label: timeslot.old_items.size == workgroups.size ? workgroups[i].name : nil,
          title: item
        )
      end
    end

    remove_column :timeslots, :old_items

    Timeslot.reset_column_information
  end
end
