class AddPaidAmountToAttendance < ActiveRecord::Migration[5.2]
  def change
    add_column :attendances, :paid_amount, :decimal, null: false, precision: 8, scale: 2, default: BigDecimal('0.00')
  end
end
