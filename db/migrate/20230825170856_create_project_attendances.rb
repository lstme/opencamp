class CreateProjectAttendances < ActiveRecord::Migration[7.0]
  def change
    create_table :project_attendances, id: :uuid do |t|
      t.references :project, null: false, foreign_key: true, type: :uuid
      t.references :attendance, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }

      t.timestamps
    end

    add_index :project_attendances, [:project_id, :attendance_id], unique: true
  end
end
