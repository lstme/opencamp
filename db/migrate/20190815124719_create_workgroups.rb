class CreateWorkgroups < ActiveRecord::Migration[5.2]
  def change
    create_table :workgroups, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :name, null: false, default: ''
      t.text :note, null: false, default: ''

      t.timestamps
    end
  end
end
