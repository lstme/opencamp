class CreateLists < ActiveRecord::Migration[6.0]
  def change
    create_table :lists, id: :uuid do |t|
      t.references :term , type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :name, null: false
      t.string :slug, null: false
      t.jsonb :settings, null: false, default: {}

      t.timestamps
    end

    add_index :lists, [:term_id, :slug], unique: true
  end
end
