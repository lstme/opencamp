class CreateOnboardingRequirementInputs < ActiveRecord::Migration[7.0]
  def change
    create_table :onboarding_requirement_inputs, id: :uuid do |t|
      t.references :onboarding_requirement, null: false, foreign_key: true, type: :uuid,
        index: { name: 'index_onb_requirement_inputs_on_onboarding_requirement_id' }

      t.string :key, null: false
      t.string :label, null: false

      t.timestamps
    end
  end
end
