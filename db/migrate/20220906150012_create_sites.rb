class CreateSites < ActiveRecord::Migration[7.0]
  def change
    create_table :sites, id: :uuid do |t|
      t.string :title, null: false
      t.integer :singleton_guard, null: false, default: 0

      t.timestamps
    end

    add_index :sites, :singleton_guard, unique: true
  end
end
