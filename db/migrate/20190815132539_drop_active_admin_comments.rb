class DropActiveAdminComments < ActiveRecord::Migration[5.2]
  def up
    drop_table :active_admin_comments
  end

  def down

  end
end
