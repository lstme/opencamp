class CreateOnboardingRequirements < ActiveRecord::Migration[5.2]
  def change
    create_table :onboarding_requirements, id: :uuid do |t|
      t.references :term, type: :uuid, index: true, null: false, foreign_key: { on_delete: :restrict }
      t.string :title, null: false, default: ''
      t.text :description, null: false, default: ''
      t.integer :position, null: false, default: 0

      t.timestamps
    end
  end
end
