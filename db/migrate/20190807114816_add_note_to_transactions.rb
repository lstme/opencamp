class AddNoteToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :note, :text, null: false, default: ''
  end
end
