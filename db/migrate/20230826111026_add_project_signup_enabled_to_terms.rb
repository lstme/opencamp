class AddProjectSignupEnabledToTerms < ActiveRecord::Migration[7.0]
  def change
    add_column :terms, :project_signup_enabled, :boolean, null: false, default: false
  end
end
