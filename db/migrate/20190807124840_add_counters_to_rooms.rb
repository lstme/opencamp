class AddCountersToRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :attendances_count, :integer, null: false, default: 0
    add_column :rooms, :child_attendances_count, :integer, null: false, default: 0
    add_column :rooms, :instructor_attendances_count, :integer, null: false, default: 0
  end
end
