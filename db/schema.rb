# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_08_14_103800) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_admin_comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.uuid "resource_id"
    t.string "author_type"
    t.uuid "author_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "attendance_onboarding_requirements", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "attendance_id", null: false
    t.uuid "onboarding_requirement_id", null: false
    t.jsonb "data", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attendance_id", "onboarding_requirement_id"], name: "index_att_onb_requirements_on_att_id_and_onb_requirement_id", unique: true
    t.index ["attendance_id"], name: "index_attendance_onboarding_requirements_on_attendance_id"
    t.index ["onboarding_requirement_id"], name: "index_att_onboarding_requirements_on_onboarding_requirement_id"
  end

  create_table "attendances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "term_id", null: false
    t.string "type"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "coins", default: 0, null: false
    t.integer "transactions_count", default: 0, null: false
    t.uuid "room_id"
    t.string "note", default: "", null: false
    t.uuid "workgroup_id"
    t.decimal "paid_amount", precision: 8, scale: 2, default: "0.0", null: false
    t.integer "wallet_id"
    t.jsonb "list_data", default: {}, null: false
    t.decimal "should_pay_amount", precision: 8, scale: 2, default: "0.0", null: false
    t.index ["room_id"], name: "index_attendances_on_room_id"
    t.index ["term_id"], name: "index_attendances_on_term_id"
    t.index ["user_id", "room_id"], name: "index_attendances_on_user_id_and_room_id", unique: true
    t.index ["user_id", "term_id"], name: "index_attendances_on_user_id_and_term_id", unique: true
    t.index ["user_id"], name: "index_attendances_on_user_id"
    t.index ["wallet_id", "term_id"], name: "index_attendances_on_wallet_id_and_term_id", unique: true
    t.index ["workgroup_id"], name: "index_attendances_on_workgroup_id"
  end

  create_table "discord_pairings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.string "pairing_token", null: false
    t.string "discord_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["discord_id"], name: "index_discord_pairings_on_discord_id"
    t.index ["pairing_token"], name: "index_discord_pairings_on_pairing_token"
    t.index ["user_id"], name: "index_discord_pairings_on_user_id"
  end

  create_table "documents", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "slug", null: false
    t.string "title", null: false
    t.string "mode", default: "inline_code", null: false
    t.text "code", default: "", null: false
    t.string "local_file_path", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["term_id", "slug"], name: "index_documents_on_term_id_and_slug", unique: true
    t.index ["term_id"], name: "index_documents_on_term_id"
  end

  create_table "fields", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.string "type", null: false
    t.jsonb "settings", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["term_id", "slug"], name: "index_fields_on_term_id_and_slug", unique: true
    t.index ["term_id"], name: "index_fields_on_term_id"
  end

  create_table "label_groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "label_group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["label_group_id"], name: "index_labels_on_label_group_id"
  end

  create_table "list_fields", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "list_id", null: false
    t.uuid "field_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position", default: 0, null: false
    t.index ["field_id"], name: "index_list_fields_on_field_id"
    t.index ["list_id"], name: "index_list_fields_on_list_id"
  end

  create_table "lists", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "name", null: false
    t.string "slug", null: false
    t.jsonb "settings", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["term_id", "slug"], name: "index_lists_on_term_id_and_slug", unique: true
    t.index ["term_id"], name: "index_lists_on_term_id"
  end

  create_table "mailings_campaigns", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "list_id", null: false
    t.string "from_email"
    t.string "subject"
    t.text "body"
    t.string "from_name"
    t.string "cc_email"
    t.string "bcc_email"
    t.string "reply_to_email"
    t.integer "send_rate", default: 0, null: false
    t.datetime "started_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["list_id"], name: "index_mailings_campaigns_on_list_id"
  end

  create_table "mailings_contacts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email"
    t.jsonb "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.date "born_at"
    t.string "gender"
    t.string "address"
    t.string "phone"
    t.string "parents_phone"
    t.string "school"
    t.string "note"
    t.index ["email"], name: "index_mailings_contacts_on_email"
  end

  create_table "mailings_deliveries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "contact_id", null: false
    t.uuid "campaign_id", null: false
    t.string "to_email"
    t.string "from_email"
    t.string "subject"
    t.text "body"
    t.string "from_name"
    t.string "cc_email"
    t.string "bcc_email"
    t.string "reply_to_email"
    t.datetime "deliver_at", precision: nil, null: false
    t.datetime "delivered_at", precision: nil
    t.datetime "failed_at", precision: nil
    t.jsonb "error"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["campaign_id"], name: "index_mailings_deliveries_on_campaign_id"
    t.index ["contact_id"], name: "index_mailings_deliveries_on_contact_id"
  end

  create_table "mailings_lists", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailings_lists_contacts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "contact_id", null: false
    t.uuid "list_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_mailings_lists_contacts_on_contact_id"
    t.index ["list_id"], name: "index_mailings_lists_contacts_on_list_id"
  end

  create_table "oauth_access_grants", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_owner_id", null: false
    t.uuid "application_id", null: false
    t.string "token", null: false
    t.integer "expires_in", null: false
    t.text "redirect_uri", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "revoked_at", precision: nil
    t.string "scopes", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_grants_on_application_id"
    t.index ["resource_owner_id"], name: "index_oauth_access_grants_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true
  end

  create_table "oauth_access_tokens", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_owner_id"
    t.uuid "application_id"
    t.string "token", null: false
    t.string "refresh_token"
    t.integer "expires_in"
    t.datetime "revoked_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.string "scopes"
    t.string "previous_refresh_token", default: "", null: false
    t.index ["application_id"], name: "index_oauth_access_tokens_on_application_id"
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id"
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true
  end

  create_table "oauth_applications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "uid", null: false
    t.string "secret", null: false
    t.text "redirect_uri", null: false
    t.string "scopes", default: "", null: false
    t.boolean "confidential", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true
  end

  create_table "onboarding_requirement_inputs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "onboarding_requirement_id", null: false
    t.string "key", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["onboarding_requirement_id"], name: "index_onb_requirement_inputs_on_onboarding_requirement_id"
  end

  create_table "onboarding_requirements", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "title", default: "", null: false
    t.text "description", default: "", null: false
    t.integer "position", default: 0, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["term_id"], name: "index_onboarding_requirements_on_term_id"
  end

  create_table "project_attendances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "project_id", null: false
    t.uuid "attendance_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attendance_id"], name: "index_project_attendances_on_attendance_id"
    t.index ["project_id", "attendance_id"], name: "index_project_attendances_on_project_id_and_attendance_id", unique: true
    t.index ["project_id"], name: "index_project_attendances_on_project_id"
  end

  create_table "projects", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.boolean "published", default: false, null: false
    t.string "category", null: false
    t.string "alias", null: false
    t.string "title", null: false
    t.text "description"
    t.text "prerequisites"
    t.uuid "teacher_id", null: false
    t.integer "slots", default: 1, null: false
    t.integer "capacity", default: 10, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["teacher_id"], name: "index_projects_on_teacher_id"
    t.index ["term_id"], name: "index_projects_on_term_id"
  end

  create_table "room_scores", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "room_id", null: false
    t.integer "score", default: 0, null: false
    t.date "date", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["room_id"], name: "index_room_scores_on_room_id"
  end

  create_table "rooms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "name", null: false
    t.integer "capacity", default: 0, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "attendances_count", default: 0, null: false
    t.integer "child_attendances_count", default: 0, null: false
    t.integer "instructor_attendances_count", default: 0, null: false
    t.boolean "instructor_only", default: false, null: false
    t.text "note", default: "", null: false
    t.integer "score", default: 0, null: false
    t.index ["term_id"], name: "index_rooms_on_term_id"
  end

  create_table "sites", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "title", null: false
    t.integer "singleton_guard", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["singleton_guard"], name: "index_sites_on_singleton_guard", unique: true
  end

  create_table "terms", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.date "starts_at"
    t.date "ends_at"
    t.string "address"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "attendances_count", default: 0, null: false
    t.integer "child_attendances_count", default: 0, null: false
    t.integer "instructor_attendances_count", default: 0, null: false
    t.integer "rooms_count", default: 0, null: false
    t.integer "capacity", default: 0, null: false
    t.integer "workgroups_count", default: 0, null: false
    t.integer "phase", default: 0, null: false
    t.boolean "project_signup_enabled", default: false, null: false
    t.index ["name"], name: "index_terms_on_name", unique: true
  end

  create_table "timeslot_items", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "timeslot_id", null: false
    t.string "label"
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["timeslot_id"], name: "index_timeslot_items_on_timeslot_id"
  end

  create_table "timeslots", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "timetable_id", null: false
    t.string "time", null: false
    t.boolean "is_highlighted", default: false, null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["is_highlighted"], name: "index_timeslots_on_is_highlighted"
    t.index ["timetable_id", "time"], name: "index_timeslots_on_timetable_id_and_time"
    t.index ["timetable_id"], name: "index_timeslots_on_timetable_id"
  end

  create_table "timetables", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.date "date", null: false
    t.uuid "designated_head_id", null: false
    t.uuid "designated_room_id", null: false
    t.text "notice", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_timetables_on_date"
    t.index ["designated_head_id"], name: "index_timetables_on_designated_head_id"
    t.index ["designated_room_id"], name: "index_timetables_on_designated_room_id"
    t.index ["term_id"], name: "index_timetables_on_term_id"
  end

  create_table "transactions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "attendance_id", null: false
    t.integer "amount", default: 0, null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "note", default: "", null: false
    t.uuid "user_id", null: false
    t.index ["attendance_id"], name: "index_transactions_on_attendance_id"
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "uploads", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["term_id"], name: "index_uploads_on_term_id"
  end

  create_table "user_labels", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id", null: false
    t.uuid "label_id", null: false
    t.index ["label_id"], name: "index_user_labels_on_label_id"
    t.index ["user_id", "label_id"], name: "index_user_labels_on_user_id_and_label_id", unique: true
    t.index ["user_id"], name: "index_user_labels_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.date "born_at", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "gender", null: false
    t.boolean "is_admin", default: false, null: false
    t.string "email", null: false
    t.text "address"
    t.string "phone"
    t.string "parents_phone"
    t.text "school"
    t.text "note", default: "", null: false
    t.string "shirt_size"
    t.string "parents_email"
    t.uuid "selected_term_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["selected_term_id"], name: "index_users_on_selected_term_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "versions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "item_type", null: false
    t.string "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "workgroups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "term_id", null: false
    t.string "name", default: "", null: false
    t.text "note", default: "", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "attendances_count", default: 0, null: false
    t.index ["term_id"], name: "index_workgroups_on_term_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "attendance_onboarding_requirements", "attendances"
  add_foreign_key "attendance_onboarding_requirements", "onboarding_requirements", on_delete: :restrict
  add_foreign_key "attendances", "rooms", on_delete: :restrict
  add_foreign_key "attendances", "terms", on_delete: :restrict
  add_foreign_key "attendances", "users", on_delete: :restrict
  add_foreign_key "attendances", "workgroups", on_delete: :restrict
  add_foreign_key "discord_pairings", "users", on_delete: :restrict
  add_foreign_key "documents", "terms", on_delete: :restrict
  add_foreign_key "fields", "terms", on_delete: :restrict
  add_foreign_key "labels", "label_groups", on_delete: :restrict
  add_foreign_key "list_fields", "fields", on_delete: :restrict
  add_foreign_key "list_fields", "lists", on_delete: :restrict
  add_foreign_key "lists", "terms", on_delete: :restrict
  add_foreign_key "mailings_campaigns", "mailings_lists", column: "list_id", on_delete: :restrict
  add_foreign_key "mailings_deliveries", "mailings_campaigns", column: "campaign_id", on_delete: :restrict
  add_foreign_key "mailings_deliveries", "mailings_contacts", column: "contact_id", on_delete: :restrict
  add_foreign_key "mailings_lists_contacts", "mailings_contacts", column: "contact_id", on_delete: :restrict
  add_foreign_key "mailings_lists_contacts", "mailings_lists", column: "list_id", on_delete: :restrict
  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_grants", "users", column: "resource_owner_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "users", column: "resource_owner_id"
  add_foreign_key "onboarding_requirement_inputs", "onboarding_requirements"
  add_foreign_key "onboarding_requirements", "terms", on_delete: :restrict
  add_foreign_key "project_attendances", "attendances", on_delete: :restrict
  add_foreign_key "project_attendances", "projects"
  add_foreign_key "projects", "terms", on_delete: :restrict
  add_foreign_key "projects", "users", column: "teacher_id"
  add_foreign_key "room_scores", "rooms", on_delete: :restrict
  add_foreign_key "rooms", "terms", on_delete: :restrict
  add_foreign_key "timeslot_items", "timeslots", on_delete: :restrict
  add_foreign_key "timeslots", "timetables", on_delete: :restrict
  add_foreign_key "timetables", "attendances", column: "designated_head_id", on_delete: :restrict
  add_foreign_key "timetables", "rooms", column: "designated_room_id", on_delete: :restrict
  add_foreign_key "timetables", "terms", on_delete: :restrict
  add_foreign_key "transactions", "attendances", on_delete: :restrict
  add_foreign_key "transactions", "users", on_delete: :restrict
  add_foreign_key "uploads", "terms", on_delete: :restrict
  add_foreign_key "user_labels", "labels", on_delete: :restrict
  add_foreign_key "user_labels", "users", on_delete: :restrict
  add_foreign_key "users", "terms", column: "selected_term_id", on_delete: :restrict
  add_foreign_key "workgroups", "terms", on_delete: :restrict
end
