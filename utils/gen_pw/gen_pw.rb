require 'digest'

salt = '< generate salt for each generation to get same result >'
ActiveRecord::Base.transaction do
  Attendance
      .where(type: 'ChildAttendance')
      .includes(:user)
      .map do |attendance|
    user = attendance.user

    new_password = Digest::SHA1.hexdigest([salt, user.username].join(':'))[0..11]
    user.password = user.password_confirmation = new_password
    user.save!

    printf "%s;%s;%s;%s\n",
        user.first_name,
        user.last_name,
        user.username,
        new_password
  end

  # raise ActiveRecord::Rollback
end
